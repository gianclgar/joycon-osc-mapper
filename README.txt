Feel free to contact me @gianclgar (twitter, telegram, mastodon) or gianclgar@gmail.com

This "code" is distributed as Free (Libre) Software under the GNU/GPLv3 license.

You are encouraged to distribute, modify and redistribute this software under the terms of the license of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

More info:
https://www.gnu.org/licenses/gpl-3.0

Every part of the software that belongs to Cycling74/Max its propierty of their owners. This license applies only to the "written code/patch" inside Max Framework.

I'm aiming to develop this as a 100% free software.

