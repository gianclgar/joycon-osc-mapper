{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 0,
			"revision" : 0,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 268.0, 149.0, 612.0, 546.0 ],
		"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 0,
		"enablevscroll" : 0,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"title" : "JoyConOSCMapper v0.1",
		"boxes" : [ 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-10",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 642.0, 546.0, 105.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 200.375, 513.0, 204.625, 15.0 ],
					"style" : "",
					"text" : "JoyCons must be connected before running this app"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 424.0, 565.5, 150.0, 33.0 ],
					"style" : "",
					"text" : "Note: Images are embeded in the patcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1787.0, 53.0, 150.0, 20.0 ],
					"style" : "",
					"text" : "Making our life easier"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-5",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 59.0, 41.0, 94.0, 24.0 ],
					"style" : "",
					"text" : "Click here to enable scrolling"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 179.0, 565.5, 163.0, 62.0 ],
					"style" : "",
					"text" : ";\rmax launchbrowser https://www.gnu.org/licenses/gpl-3.0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 5.0, 4.0, 72.0, 22.0 ],
					"style" : "",
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1787.0, 111.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "r #0_handlepatch"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.0, 0.0, 0.0, 0.05 ],
					"fontsize" : 10.0,
					"id" : "obj-32",
					"linecount" : 43,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 5.0, 177.0, 168.0, 487.0 ],
					"style" : "",
					"text" : "This \"code\" is distributed as Free (Libre) Software under the GNU/GPLv3 license.\n\nYou are encouraged to distribute, modify and redistribute this software under the terms of the license of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.\n\nMore info:\nhttps://www.gnu.org/licenses/gpl-3.0\n\nEvery part of the software that belongs to Cycling74/Max its propierty of their owners. This license applies only to the \"written code/patch\" inside Max Framework.\n\nI'm aiming to develop this as a 100% free software.\n\nFeel free to contact me @gianclgar (twitter, telegram, mastodon) and gianclgar@gmail.com"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 12562, "png", "IBkSG0fBZn....PCIgDQRA....I....RHX....PFqvm9....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wY6cmGmcUVd3.+64bu2YexFDB6xtrDj.AUTPICIgfTWnpDs0Z8WUacqZs0sZssAZcokpkZqZKV0tXsJg1B0EVxLCSTQpfD1WDjEksPHjkYRl068dN+9i22ybuYxLYgEa82u974y7Yl4dOm2y644848Y+44Mw+K7+2CWEGPabR04TRXQYbzIbf4zApgGOg0kxkdV7cfUSoUR8j+6cp++B+2EL.GbFuJ7py4zmCcWAChpr9bdBLVNkvAzEGRqXSb4swa6kwVVMkJ+z4gmSRB4CPa03hS3nwv4TJg7hu+Wz+8z7pmlyDoLbFyo3Z9u64Yy+NNOyRIKmJITpN4kndFYITMiYUmW1Bn0gvH7.aguSB+PLvDbumKiW7RuZZoLG+v7QOPV45o0bdkIOc4.EITp2GGeJ20bDdZoOcFreAAxQaXi7Xo7H6Km1nZrh8+Df73OsfJXLLBpRVER6BcF+rMy8fUWhqZLt0lIX1UvZ3Ku+7V2HugkxpeZwA5KFnUpmvIUJLYph7jH9L2SeDa1zbqoSO2fmSF2hqq4uK9G40BqMeMzwXbZamQxokor6+4bHk7ldexDv8okoRILLaImGJgQyo6DNnJrOakI1JWA9Gqx01LQypHcIjtQxuKxufld+u.RVBszCikvmXHdq4bdd5R.M2vjVNuv1P0.AUp3K0dJlLwNuB9bAWrj8hwcFtt7RjTK728hWSkveWNkxEOiedAIgexyodIZoMgEjQ4mUkaNIrg9vvw0MyqJFgqnDe5yhePw3L.k6ljEGXFjcgw0U3B2wGY9EvDPYdxQ4ovyK9+6cPTda83KxImE9r7TRxCzCY4OM4XjDF6RSyyr9S2wrIHMcJzGSy3tSWS75xpPow4QJyOLiWS1TuneNBYABjxyhxSfQ31R36FlpdI3D5hNqfsvMmxezx4JKt+An7FIumfEV6QvkE2iTiVQG4AIg68DPqMr.Wa.N35brwYPRF0RHoBk1IJf8PnNpFVTShKj443YxXV.0Bicsjf9aI4j2BkRmx0TqIQwEPBYsPow3V5gs2GUym7q94GjGPQYcFTN113Gky2pT.m8JJwYTQXEdvf3lO9b4hN0.AmAn7ZIaugvYZfiYNzwfb67zf.p6F54bBor+wYRdmTNAaOPYtsotHrqfl3BL6tnswET7KkzjveO3DgwMk.mp8xwNIg8oaJOBxhDISvVRXrrv3lgNKwrxlBQTdCQy+n3GMXjyaxOOnfxCys5sPkNozl4dS3uCaGu1TdEsJffpfgXcI7tWF2HbSTYwgMOOSHbJfWWovbpWdZP.ssFltdJcDdCp0VXg4tR4hKyMjw1Zg7w1KF2zvhTWivubNezT5HE0X7R7lR3GTi4gZ6sxxpQZEl2v71x4czRPbzVS47R4twrSYSU4y1N+5CGDCmVvoJkxiEdmus3P9T0iS67ogi0ylPNUSnxrIcP11V4hRCDFqLi2xbo7VHqF5fzg3qzE+1uTFc0T57CyupqlRCzz7bsSQmmcEL.k6gZWC6GdWah6sJCvdIATbGWMnNKFZkxiyc1NK8z4I2aFuY.tq0v76l2WUTiGNg96IXU5VeFLt+Tby8xgLOdUOA2yR46E+tmB5mCh.WqlrnLqffKm6GRX8UQIRqG7sxy5DPEbc5jJ4Xq7ulxeMN057OOGVvHgOejJzQTj1GdE7WPfqy1HesQaUVYTu0o7LlI+csCvFiWSY9nymN1HelykwGfx6sbfRQ8AXN043yDTHJgK8z4IWMsuRF8p4XKy6KKn6RsrYPwzxzVcdzLtnUvvqlVVISjD10Ixp7t6gwtIpbpT8aybagODVffb9ocrSBtg+91BelymZWFIQjX833davURqQjwgUmSrPmtBJhjnBzSDTfd8PVf.Z7JzZ1yBtXXZl+USnxbHcK7Sy48mvijymraVwDXqAQR0agNlHLG90WA+Kv.z1CR0lIZtI5XKbvXeRXvkwcuabZJBNQbkLQ+7x6j2+F3FWNeIXIg0v8bXsMzD+nS3vxDssKpP0AGutxblKf20nw+epaOyE1dMK73bCqfUAqjIVcft4EU3TrxbyvSFITZkipC9nsKnY3zY1cMAYcOLW0J4SsJRuPp+CX+FgEGc.3sAsGmdYbLoreEDPMOciJv+y5I3oekY80Xyk3.lHvs5YDT32rn9Z0ZmJoXq74pvmrNu5LtzYSqasIhhVn0IB5Jtxyh+8USKyOnj7XPuL6D5ImUrUdwIAS66.k6iO0x3OZWQDE2zNQurOY7uLAJw6F4CDz4s1dEATAqrDNo1n8LjEX+emv3Qs8y3ErcAEpSB50MUDVtfb61R3ZnAmf4FDibL0EHNqGIfJfDdA4XSAtOyjwY0xncb0vKNLGFe6bzsxgrkfdM2wTlSmbGX3cVjTAA1CK99uMdxN3wKwArmHBX2AQhm5vroxVYCI7lwsOA+cyiyaXrUFOIDFg5kobV3d++bV7ueITYkw8yqkitF+530WlisCgAuFFir4GbX3Yi+ncbJz.J33mSRe70O.NjMv6dortUSoBK41iIfZ1+O4r3xBTKI7.KMnegdBJ3VpeNonWEqLcDPh5XD24c6vQ034bbUXAEDmYbWP6M14s3Vwngc.6z7OpzakIB+cw8l.obRyFahGpF+DXiMH5O4zv8jk1z3lGmmo7XMgGFoWdnTV7yFVhkS0RToKLHeyt4MOFmTUts4xB1RzeUoAhmbjzFFl+vkwWEdGTcMbbo7dqxuR2A0LLdXLq0Dml5iFrz+pJdeLCDOv0xW8PX4OJ+MKi+V3tZ552ab7aRwfKDx+ha91SHK94VKGRNO+IZL4lNDVdYLFaOKDSF2Zb3JwI2pIcq88OXXmeAwYJVTMyb3MxC5EnFqOkeLbeQhuTNkVBWy8rhfB+Iqj5WAcmxID2Qm2zXkmDBFIrgHxqXCw8FuvzmAN4LOmIZKFBhg3CuLdMai2dY9dcDHdlPvAmED00lEoamKeY7Ifd4P6kOcB23r3c0ByY6TeDpUOtgHIHVLucZc6g2kuBA+50767pokBhm93e9f3M8vboKi2W76K0r0a6wbfJ7D4134kvwTn+QJqK94kP05bbsv9NA1EhXxZgzI3GurfYzVIiRPTBgaLiaakAk1K2C0tZNzJwmczL6cBhJ8ZzvX+XD1cdSTYvPNu.2Jbkzx4x3cyQjygO0wMOn+SZ0v+tIXn36TJ2UTmfmVVhUnuSWzxvrEbdKkuWu7UmG+ZCg5TMMD+s.RiZcFdOV+n7qA8xuG98lCGzHg42jKMoMMmhQhubR3K+iVJObAdMNeRVGkOUlX.5pNW5gx49HbYKmeEZXNeyuG609ApNKrElWTGkZkhKFyORUlxI2t.ayzYX7SBrig6e.ZaT5Vvpl4fEU2NRbNZz62svwWh8ItfNSbOSRBOiakFri2BGdBGyvnD2BLm3XjwBamNGeJ9zIIpfb0vh8fS44b2SvvUny8VKwxhA.c1AhgaOik2MaaMbiyiW3lioIQyh+yHqDkiqd+tswA0Ge0N4EWOL4pJP7Os5blR8YQkg3KuT96gkzPkjBknq1KGSM9ZylS8g4uco7amDUZdpDOrWHB67afjVbKMtwGYhntDWVbvyh9GZ2nbY4gCWyJpwc1J2PkPLattbNh5XjfHqaaJ22ozlFJbNUH56jxQte2B7fMB14IzAyZ6r87nR+sz.gbxQQa0lpaAh+yDXXXqM1n7.47vksCdRe2B4AhijYSoA4asbNobZcXtm4vKbKAS32gXBVL3sfw41yC5XdayhW71IarnY+SGG+3bq5rBDq8uofUTV8zj6V8xuEtsCjScq78Kw6KgrqjVmNhmlvO61W5jjHhKgSMSfLOg65bXy4jbggGzrvBihBlQV5IgXmkWl41AGYqb3syQ1BGBStE5m0QzwcauAw4ozDRY5lm4UHYLFIKRjzUbdmwI2Y3xtuwiJ8WHqOgEkO4PrCiWgFlUEMMd+hiWOr8DtiRMt1cKQTLdgk5hjswkrbd08xIVlasaddCFHTKOM91JOkzwCymirM9nsP6C0fXa5LTYRhmYSKCxMLFm+JChnJW3inKHtN0Gq.umT135YKcyKqN80GGz4x3qdFTGYOkCTB78X93DlHdi4QSrur3KPKAiodd6FQLECXRMxGk5i03mLQcXR3tdorYXkT+GxrR3DhrdRlNcNJz+ImeZUd.3UDMsMgEEkmd6EdQEVSH2eO1B8elg4Z8XDvssH673m+iJTjdWi9BDOkobagw3SrTdmWKuXb8cv71VHSGaYFduRXRcx5bBxmHvIqxLo6UVHbL0iDO+W47K8JYKWBUZlaxE1XC12uSNswYgk3kNBe9EvYlwmmvZvzwTXORGn0FC1XUNt7PtzJp.4MCyuAB7EzNsN5NFJf7YRjSC7ibwToHKXUi7nHnB+CMRv4kGRj3Lel1wWRPA2ykghroqO.6aMN9naGVWSu60pvwjyAEE6sS67iXrrRSSbix4FFNpn8tRQ5BhmxXb9HKmK5ZC5tzWGz0vQhmcANp.QkTugu3lwDTnfS2ro7Pb0swJOC1VylmOUXEQQzBFyLD9s6kCpCNu94jVJ215B3rc392ayeqE2NIkBixVpEsfZiMDus3nNAYQph7TR5jxcMy+Tp6vNyz7PXFJOlFJA2jhtmXKzd0FIyV9T9IKOffk2Pw9Jw40QmvgOHJT5ez3BQcVTmgeuS5+zDjWoIB10FmCiwclyCFW4m1.SVP7TBSvu2YwE0KmXctxNnqQ2CIdJfjYf66jSTlnR.ulLDe4Mwqb2Q7PHiDWMkVMktof2pkv0zV3qOlY591i3.sjFZqepE2z3b+0h9novCnYbRQrXdz4aI0o1HAG5MVBkZhyQAgPRR3EaA4bDwb4cyhDmszP+mdlGFbFR5qB8xh9x4VgViH55rv4R5VCwS5mPC8pxYwQKdlNNZIQtgoi0zN9B19uR1Ru7iZgibro49yhaHJiQ4Cd1bwWMGRBeyNYdaeuj3YWAQt7YQ2BXX9vKMFX0ABlmOiDOM+NEu9XDpzZ78eFSCjcKATzuHYey.U4KntvabRTWh7nXhuKGxDAS.If3yZiRiD7WxKdpIs8.Md1k6gw5iORa7mUNL1+j8Opn6hiS9Dt10yFxC4g7NQ.EsjnRZfU75nQnUvh5.al6b4QGBtRpu5P3NdA0LIg7z75SJkxlBtpo5hZsY7FEE81jOjpmRonGi+iOa9Lql1qvp6lCavmcIdpVhJcPosEBQyGXYzaNIWEsrjPB.VlP3nltHy2LTDxpbVzXnDOzLcs6VBn0E8oWGbT4w.nVWC8eVaXie85AezrfZZRolvPb2mKiemzxXj+fjL+cLq3JHPNwn3OYbWKLtK3BhC2x4eZ2MWmJzC05mWZFutnXwaiF9FZdb34bDwsaSGQYRb9Tobv8+6Djy0ucFsLsWKhZJDm1E1N+kKm+TXt7E6lSKZ8zzZ4zdBjaxPAkKF70IvP7Yve5xi9rJxUcOpZKJfhMFCPW0BQ9+9SiVzt3ogvauIVXKpE5BFMPwe6S46mLXjhxoiQieo8xss9vyp97Xt0CocvGsHUA5kYmGCRZLLE2BMVn6kYWginZXBmWa230230ru4b1471ak4t8vWcazvq44bhsQWSXlSJrnboVxh5ET.E9EqL2Sct413z29j6sTe1Am18utL9.Pe796H3g477PpztW445lgHwSyAe8Qvu0xiAOt.FHj1wGcRHUUlWYFsN8sbtuB++L0w9HhorScNu4yA7T7w6IxAKYZDksaIf9VME.0JB+SBOZ8oDLx7oIXj0CJeteUX+JDv1EdpPpXNo0aIgL7+PywDgLP7VoQJbf2VJelDFtdSU+wzAoQBrJzV6Bo4Y8vOCWOtSZzFrnWbELdfne5xqnjntUoUC93ZG953hPs9XfTN8nn0pcGbZ20Wl2AzWH8T9DQQk0mIOzumBYAqGK2E1BWQUdimKieMzYK7BqyYhWdMNgRrfViHrVvfb++.N8jP9aUpYwYqlRmJUGfx04C+TAig9RzPr1Tgc4KRg9O4gP5unbSpI48trftDIqj5WGcONGej3ZxGTBIUC9rHKI7SkQCVk7CngRt4gviL6nNDO9nbuvMDINS4z5BalNJuGryMFGMaK36hZcEBf3CHJKuYeCEURbZyomjv3j2RvmUyAVRCynK7GTsb5e67GlPoVBNI7IpyaY4r8qLH56hlEcLzyB58jS81hgzXP9PKmO8Z435k2BVQcNoYEE8Ng.Bbz.w934zVFU1xLnP87MY9d8tNHNw0ymbo7yhzASqdS6RBn0F8+y0xglDyQmxBA4jFAib7YHXjLoYmkxDvjiwSVk6CtiFVBsnVMI+w68bYiqJjDXYWGcOFGeTb3D0ho34tCJdtZjAh2wJBkecZBY8Er56X2UNlTjFpbXb123mM4iuXWYVHY6uwYwKZaAZ2eqyI5E8J7a0Mm4PA2D7zVum3yIqbH8Z2TNexbt+94aWiyYVQb7XXaMpZjXHEUqTnvZkve3qjsLvThsUSpKbLo7YWOORFWDMBj9zA6ogx33SY+KRxqznBzyogHnE1FcNckDSyCSjZ8dOadbBQIOd+EgRXRe3DSBLQhyiLtkobRzL9c2OMojYon3yaAVajCPJGKNnnyEmVHRDlEQRKnXJlGukymrUQ5JX3DVW2gu+SrL9Vv2kC.ejpMMdyLVdWC4wvYDqFjeRBu1N3+bV7KUlRCQsgoZsnGMRZDarZogp4vv7mtrXZutjl3njGEcEEI+UlaPz86Z4L3.ME1ioC1UbfRVaCeCbJsKPcWkgpDyluBezTOFLxwlgcYEVMD82xsB2IsrPl3p3.RibBhaGVGMRBrDVXGz1HAQf6sN9LqDkhAt81fMZxLLbQcR5HMB40zBM42pCLNexWULRNqMjaL09tb3Sva4w4VVZL8bEdedmyhCJx84YTosULAiQk+zZSX8XrXT3SiomdyuHYLQqzRLWitvylKH9NklzvObSJdpOt3ClS+Q4yrb9NwMJ6RS9mwEjUIDfz3+dJIlLA5enQ3AY2GLxof.JEUhbcvFajiyGSdL7H0XvjHwY2wIdVL5+IwnXuqdYlJjGThWFOV4XxkM+Fy4SNJZq9tivL9RUjx2tfvmkTHBXbt3YErR6sWnCXurO3W+YCtOMFBRi9aZjfkRYMwoYxwOFGrpcSKSv3ixae4QhmX3cxH384BwS8w6Y+424QYfkEJZAWVf3ZWpwvLh3Ng3.+8Yt3DikwhbtilCF4.r+IbLiaRe3jM0ehKRFMTBJ2NMxeHbRcPRbh7fSMR4Ybx6xs.6BHogXy6qGdTB9F5JCdXsPo+54SybNNuyDCORFG7.QEour.24RPu7J2OdMCwe9x3luxF9K5U1JG136F77SGHxQuzT8HedP7Z0RjNaprctiLVxx3KmGDomVHNZUjdBQif5myeN74dBdnN3MVXfvtygirGXN43bzEJH2lcNXj043JygjgRgnIuSPcRhEg3Oa7H2qlyenl3tc6EoNvJo9Z3.SiQJ2d4hPDYVnP+sSCwlk4HvwFofaoztvGP0IOlhe6+3r+XqGAoE5LzOWzFCEVvmhFgHIgUDyLxYLw5d5BSG2rX.TS5ngSE+7Svev4xPwM60aVrUbbpuFd0syp2JClyuzoySFiX+tLzGEvt8EKkEEq7z7QCS7aiFbPRXwwjAeWJjOllE21JX3BKrh6VOwhLPLKRb1U7EtLGG1+pSh21ygnRmkhdY9logXyRAGH1cjvXmBgQgRQQy3SqEl+yJmCC+3BGQ1Ouk8giaygpUXvAnsdXreHyZawzmc2IB3YJDcnX81nkVvPbGo76uzXyTXpVaEyqmrDx6kWW67uMJilxRWF2S752iHdXFHfxmD2QcdgogKLoVnTVtEZrXjy0NH+pX7rYXQNg7sRqUhNxaIzxExXsFBMxyOyjMnp6BlHl7c4MJe585bNtP70XAcMuGn63btN2wn7qjDJhwjoFF87fRoilGTb9uHgY0JpFrb6p6Iz52R5O3rsG9.3KSCGTNDKHkEDyLfmSpe9XNNWOVMGk1Jacb9qx3Su7f6JRtLRal3oYho942ra9hCFbp6YsbV2TI11Sfok.5BhJPeSA2jexQKjxEJvrWDVSOwLzaYgc227zMNyDzCi88YtiyE0RrF3y3AJGK03yisEuzSJJ2JallqyDTn.cUtmkEaJBmZrkjb1ABp6YOYb5meyV3TirQdAM84uxYywMH+tKLlkeZz1aZCs7rMqmnEgY4jWJ3I5zAIaP9mpyEcNQCEhUHSsoTYpSlNG8xELeV0Sxilx4rTtqmNDOLCKJEJPuENbghTifr7Vy3q1Ke8btsRAuv1xdh40YAEkqkGrZ3XGmyqcNrQBACrLFuNuw9ibaxCJ5d5EJgN0cwQY9ENGOOdMMeYoQQHI8xapTnP7RRiwhxtNgrRJGTjtdNsFGG4bREKDI71FjQK0vuJYqMhGRYn7fug5d2gW1Sfnx70QZkFQ32P7eTgO8R3+hIEOo4T2HmjuXLcNtRZsE96OHdyOVPcgWyR4w1az4Ypvzxc8Rnx6fp8wquUtrwC5SjzgvVqVEdiFSH80hH2cX.ad2WxT96XBoYzfS5RyhDQyIdMiJHRqpfRrSU7UN4cPRqBrpJj2N0qMtSMY5hcvLMeSlx0LQXrqWgRUCqaO+1XnwBsQtu9x4MWnSWy9WoOVaWbla6oY3KhFATOIrgpk1i3sgBU662NkuvYEZpTX5K4ll5NGYCvgUiK8f4E8X7eTOTK8CWrVu2N+JfokCzwzHXiuvJXhfdBaa6b6aKHyLMI7PedB5ETOO3k3nk7DulRMgHJFyhpAPRf3IqifR5acLVSbrVdaru0azAw1g6sRHOitigC8AmWkPd8LNlSyr4SCwLpV8.GsIqr1o7tWSCK1JkrisOlxQBxzpAh1NGgieBxlSPuiUyjh7EWnJVH+6x4LKEhj+3hDQQBzcZCQS+NqfyZaTt03K1Hb+Yb4k3qszlpVkAn7RB32cf3oYQVCv4TmKceYVOJexkyGiFLJlNZf8TX5JM3IagKobJ0D5rmCyUuTV4.ruiGpK9mrU9rG.mvvTZ6QrRWQ1niDdwqkPZ2TthvftcSZgTRF06HPLb6k4MUMTgGC1ByebtlVCwkQ6BTkaswB28LAmaKbbYg460lvmeeIYiQtYwFsTVWwZHehvbKMQfJJldG5NjqOFO7Nl2IUh5NY6Zj604AkxSx4zqQKakpsDZKttrlveKIRjtL9F8whmEevZzZzi2YY6HytbM7PeoxAtkowMsFgMLAqE+m4rlyNVbiHY.JsjogvYUjtjnaFHnuSWrpAIaK7FWNWJSxw5YDwSwKwN.E6fFHD6qeTEN31w132JITDbe27fN.if6Hg6HO3Gj2cNckDR6hIx3c1FOunXf0fKOmSOkesnHm5owFUcMNi5jLa99CxCWmStL+U6Ou40y0mD1oej47tSYyIb9I7CpyWIguw3zeK7OG4r8ZERG16tU9Ulf9xBy2UjxWJiaIiWRZnxNGFWtP9wrv14MNZnYTdU04bpvubzRpbnMRFMPzzAFY4g9Q3NDZfhEwKrgnr2dNe3V4nidTeZEeVEiGx1xG.2PXov0s7XZCSPjTzGT0ZZX1o0t3eeT04KcPblOVnGJ9lVJ2Ug0Y6INIbOA1INPEsvtpgVX2AP.Kmw8jv1x3RSXi47ZKwe+Ywk1Ou7LVbJet7PKE4wQ+YbiiyWnBWbMNqx7dyXvt38LXijuRFyKiqcH98SBW25pvg8D7uh2WNujxbI0Coz5qZo7c6Kj.ZOda78y4jvMWi+gRg1Xx6FaXLNqTd63CkwGNiuaBG+YyaoONnDl8R4sbcz8n7wFm+D7ukEJdf2xXTsKV41B6zKEUneQcRaCGZwbEvNrXdgwfrdgjsL9RWIqNgybBdQYACSlcT2uQKwSkvCmyOoD2y13Ae0QqEKfnEdhDG6zB+poz7aJzJ8xasFWx9P4GmO2D7AaJ5A0e1h3gog.pnE1UhSo0fNLpFVnd3ZjUmKtTHy15NmasW93GHerGO3LsqsO9XYAKWVyD78qvplftKEp2nKOKjKPuGglVPVKAKW9hUXUY7OtL9y6iOVUtvI3izJu0ilK5dCUI4Ug+f94HweP67FlfOcN+wIgPh70GiUuLt99nmR7GmyQVl2SF+dY7RRXtCD5v9smyO3ay9OAqOgGbobj8wojyhpx2HkanDqrPw7HmnJQj1FHvswzvMHpTcxkQ54FJSluU7m8Hnvhp6xL2MUyMY8rWEh3kOy9yq4w4oFjeykF5KzSqR1Oa.6.ATy5+jwoTj5eiyOdrPfN+tcxIUFCyMkEDc8qtkvsesqIjIbslx5qDRvo6qFUqvuL9w0BolwqL9Vj2dXW8ePFy8n4RFkKoedqYgVVxKok.h4CESWvaKmWH9YororPwucqwWfSOmIFMnv7eUeglN481NW2fb8035wZKwKJg0TiNSBDzWbJ8bD3mvkGaHSqHIvp+jpwxmhRBIHOK7GSz7GNcPTY354jt1ftIMqjbANOcsQy+atIemrK3RjGiEWbspZNo8G5Hb+E6Wfqy+RY9P8vSTPD+bAwCSw+MWV7+utP2Megw3aIgdqEj6uv5lzL9qokfkEGRLGgRRB934kTimLiSIiid4gNZ5Bqy9UmIx4LKbweL7Fa7r4Ceu7odD9r4AWv+tS3XVZXb9BOHq59C953ClPusRZWg1Hy02SfH91wbN6.mxNwG3kwCNHuh8KTJRqJi6qNutrPGj8fxIaXttLVbLQ3ZeKr85gd9SGIgJ73kzbzzKzeIhz5FtfnEk6JHIVDAIQqQyiVEJLlY8PsdhN96BmBAVyPdP44lEmoOVdertCHblk7Daje4kyatGdhhVtyylhrlJrCbfJRowQ4nR3vqKXcRNcVl+OwlMYsgobNsTkeiVhEM2n7EvGKmemRbdY75Sos942uDeppAcN9yZgCpHGjiYY3ed+gm6WVvMSuuD9Hsf9C9tofh...D8lDQAQEcjzuTcl+gy2bdbTagwFk21bQJmz.gZw+G2Fu7947qwqMgiaMrhrfXPBhL2F9rYgxu9jSYKsF3Xt7MgD90mK+3x76mGRfsKpMl83SIOjRaDCviL96IyOn8Tj9LQfLSPgnpHGmhPQbB04BmCutQv5Chx+3EUjw.6A0B1yFvNv8sHJ38xaoC9Gi4RadIRKoAOvByvSEHsyChiRFGYwjJezveqMLRrgT1hfoxwGbRtvg.RkvmWqLk6zjl5q83m2RzeHaK53wX80lWwjkcib6nicJJxvZg5SKczXAezUjvsd7YmGulJwwcTFqMZKJldmbhYgSOGkGIgW3xXCSM4ze1BVEoupX1BV7YqgiNMzdWdWsiMy2Ji+3UDSTuADZl3WXSVE9bILIGnlczVJmRSohPElj3ovuEIz.ikDLusdofY4U11jEugjgIukX3.1dSo1PdXwIcBxl.UBb0JZGakHr.VITg.1lI6x7SRHWMRHW7NTqw3lTsomwnABjx4lrGHllFC0QbyPZUxpE7CSaSHjckoSS3NRnzXTuKNjgCG3HWhXtW+LbsXx0gKizBqpJHDVaHmrd2k3ctuXCb8UC0.1USCktetRWmYBlD4W3KiUSo4xZ6lyX6wBfKuItFMc8yzmkmLEcqhQNdFqo67Hmtjv3M0jjJSS26TetM++41wl5vTlaSNNEe2zbMEyicY8m2DWnGtENsyj0ekz5MP081c9MSvLcUMZerb7NagW6rvF4lRB4c8UTbM6t5d+4RXRNPEo1X2gZ294WXhQwBzT+6cwmkL0OapK1SGrKtlo8ymo+eWLeSltq6o4bszXTsSNzQ4esWNuBcOtDpbLjuQxO+omXJYsAuymDcYxN4WlA3vpxqKkeiYyID8f+.aLTkqe6lttxqkr+6h3gowOPsvwWg4Gaqak2U6D++mg7PWVqd2rjgYs8x6c4bc6AwVpPssc.VCGcIVRNudb1K.aLbzF7kx4Ke1wvlvNDjzetJtZ5fBBnjhR0sNmw9hsJzFW9egYDRxidmdeYQix2uWt7T953F2DO5LoX8UGJy3CNIzYXew3LagEMWAeRTid2HWZI9N8DN6RQCNNOWZV9dKrSbW5K3rsiJiwKKjP4+uP.JzTt32YAkwqlvbx3ctO7hRwSE7208KXo13wqKCskGJBgCtUlWmBrPFJDD4uWBe6JbsmYScCibRWGkVbi7e5+QA6.ATzRr+G2j7WDfbJs1PeE7WBmtfehlSz3j77faOplwlSCU84OVHSNuo14NOiFYgI1o3e8+XgchCzphk6w+cLY94ET3vzYpgA7zAlpXkqft6Hz.ySfRAt4UKwf8DSG3oBEAEcIAqVeF4GmnmteNOo9a1D1Iq0ooB6IMkneQAJbWwyTD7TwWaL3jxDBMEhcm474wXfEsVqv8G+hI2+7+ebNNMA6z64Sm288j6YUjN.kK56f4M9c5+uD9t7ph6HiVFb9wDmebwPMHD6zm7A4+3czn.7+EtcJqJleNwz03OIgGdhP9GMTw2smNNIj0aHmdVYdHHyioI7UJO0B3eegAko+4F9pHjJ8yyKi+JgBy7SrT9lOWEtEqNlqt8wq5GR9MR95H+VI+lH+AHu2FmrexaTOXIqdJ0jcw2uZJspcQkZrmbMSEVkFcQz3bXmL.n36m5317N9d4Je.xuNx6K13xGXZRqkYZdTDg694rudx+QSO95FJt9hPL7bM9JmjhxpteV4MGmaWanAhO46XAdLeOC2mLMW6N7YSVKShHygBbe1PVHVP1ZHGY9OZB4Uqn+K2z8lrZRuqfb7I2IOcc.qye2bMSEFX2DbvBcQRlgr0CEoQZRR3YeaivqXLtxIhExXwkECqPRRroZOco9YQB2I1vIhcW9mPzZosvbJvWCDS38B8tlJ9Z2gK1E3qh3GaUjdAMhi43znbw2J+jxwp889hK7MO9w6Uw3mSxED9IO9tOoOmZ54rCeVy6Lu78iyaibKk30VmQqPKiGBb3VdowSSmlgAXemOCcBAegjGeAae1z80yScgy.xYfP.Km0froYh3YpJ4FOCPm8Dj1BC0Sr5PaV7vUGNnB8yXaS0ivMsHp+Pptd68DOqT2UPyOil+693RmOqbibWk37lfgRC94o1Xr0olVpE3qQYauhPEwluqvEMiutRZsDytE1byl0270jSxMP2CRakX0GPHWn+OOaNulEQupPhsMuAY7hB3bWIBefPi0ZrdZTGB5k8oLi2CauLgivfw4DhAy756I1gLZF5kSTH0Q+w472mxmtNGySvJVHa3p4nJER3qUfNeY7v8wGeYbECDSmxd4PS4CUmysLcOOdzd4Oe4boMu3rplVr6MTZ0ussxKOm4UJzqCcsgj2+KDIHdy47txCkYTGGYnKY7FOW1Xy6h6kSLMzzK2VVHkTep0Dx.weW7c5ju1ng7o9LDHHtnDtoBD7ZiMroABGlaE3qeXOwtQVyP7ve6BxCNE7KjGN5JWT6bNI7nWCGdY9.0Cm26cOOdz94SsTtrB7UegDe6CgWYJypNOde7oWF+KEWykPkij2Q+7qlyQjFrvb1wyH8alP509CYVCGxu7WaMNvtXa8yUlxp5IdXF+8Ccr1+r7PmY8eOiOPsPNSMde76TgacB9qSYw0XhqkORYXbd93viaYd.3ZX+RodmjGOyJdYGBu9GgeFNqiiW5cEN.V2vZ4EViq5.YedBgbBZtL+My+T+7h5g6c.N0ZbEGHGTje+Dyk4uE9GGf6Jg6LhTpWranOd+47YV.oaBUYzVYAciMGaYa8w6uat3XgJNdWz5PjctrQB4TbrYdlUhy9P3s7Xg2yuh.Avu1QwJt+vof765fBkqrNwFBGy0K4Bil8u1HwQ8POM5HiNy49aFeAKmMUhWxAxJezvYV1oeBbF2cfq2i1engreMG.KXCgwq.W7O0K+3d3NtFVTBW9Awgsg.9Z74Dtl+gd4d6gezUx7ag+w8gyMCwS5mJolj82sD2DdnCGpKrSaDgTZoKVP6791X37O4WMRGzyAv4u9PNu+V6HzF.0J1J+swRi5fpJzwQeJ9iJTN5j6hxam7L9H8w8UND7taeTNMAp4i6oB+94gi8t4hqv675Bc4hKc1rOONWPRHUOeqaITQCyJI1XlpyWadbPOF+YUBe9aZKLVmgF+3wSvWJqNpbVLo1t3YS5F3xhUyvEVNLwGMITpPx42tk.B6JpwANDGeBus32kbgZzSpqyoNRfP76uDtknXxE9nnBKBOv54CLL+zn75tJTL8BhoaQDWrnNossE1r796i6KMfutiDd4Be9wuov0dPXg2MeV71+lzQNei4wBdL9DiFzw7MuYFqaZuDGJTlu5b4vdL9KqF7n8aXqL7rBA394Csve29w4tI11l48TOfq+7yN7N9DhbfR3ucAbZajMMBugbN5sw0D2.bdWS3cWR33lRqzdBeyw3XGi0MB5NvMr+swwLAe2nr1zhj65TKG9c0RrfDVPKgczilFaHl4bbw1p1v3MrL5KxA3irub3aJTzackENhpW5bBcCzglfeTe71mGGylXnDZcLdu3rlCsMXnTcKZYLo8vDwcyqpi.Q0UsLdCIAqaNfXy69g1HOvURqITIpryoTITq2+CDTfMIpverwYOm5MPT2IbsAhsCu8vNru4x30Demd8cxgsMdvhNr+EDtu5QhiSsRfnchB7U7HZnZRDekvwEOaWGOMTSVWYbre+ykicyr0D5nEdu4bVyk11Rvc.+v93WaNrvMyvYgTF9cmyR5lNiJhLvZ3LamW6PAhz+vypg374DOeKt6kwisFVQqbtQh8O0YGql1d4JSXE4A8q5NtFuntB37e3xCmcX5mmZtXCby+.9Mtvfp.aL14++okulXEJzZX.tpjfdLUFI1vC5g6uuvtniNRj80Wdj3Imzd4bhsC+8Y+4CVIr.6o3IR4cdtLTu7ZiId+rNP9ciHeaHnGxu2x3dilnVTQruh14v2bb2SRvr6tGKnahjvQg4nwEjKZL9byMry8qzGm2b3W4TCEoWx5Biacbr4bTiDt+aRjnqcl8Pg2k+FXsbb0ZzwYuM1wVA2.ANlmXDe0e8fX1R0nbJSrTt2qg8KmmerPBWcAwSDmcNwS5n4b.QbwvgEsMlx6Yorod40UKbucdv79hbX8jgMoevHgweRWgO6AJGIJhqSmdLyLuoHtbEyJbcanB+mEyiTN7nazGoF+zuGyeBN13y8aDeWOrZwFwUJ+aWHYWU3jpdgQM4uixkBU74wTCko2oSgvjPQFdnCGFnqp3y+OoyNCMRbiPeOF+aB639IkXc8vX2DcLXPVssy28w3ajDZl32eUtwlOCMJD0jvIEaR3OAtQXLVRJKNR.bKE2yx3y2G+WiyecKb5cxqdnPYD809hTto57ewygxaMbP+dyBTUmZWXTt+xQtR03Xak8evv8rNZzsRhvgkyyOlNr8e1QNNMCoANaGQbtVfuRVcPusEDIHV6iEJy3wvCzA2zKkQihL2uX2X35VO+qYA70CDwoaefPM+eTwI0i0SLkOx3zaiCanvy8GEeuOr30sgtC5u5p3.x4biNS61NGdj93U0NydqA2NrtHt3npvyaKgw9GAsvgmwQEKk8apbR3nfrq34z4cPnUvsQx5ljXqbaQcisvSUtoi3fIBJsNdovCX7J78R4opSmQEMuCLZeAV8xCk770IbhI2d6bz2I22BapFqhHh8ItKp6TN694wx3yUVvOL4wEs0voVhYkxMWOTQomdRfvnLb5jrvF8ZwEGaer229zv+Ouf32cWKqQd27B5BaJzSEu6oRbjww2JycKQj+zguRhM66gBYoXQiPH+7Y7qkwJvE045ZmMTkNFgid0gy4rw6MhSCqgt9LVeKAQ+GUd3b6JYqzdTGlidMbtww7hKEHN2pXyZWns4HkCYPdyWC2XBe7YywLT36+zQBsWbbS682Z7dy4jhq6+zxwdOTNmX7bHaS4bWohIPVNOZkXSW5to9ZaJUIS3kGa8J2+Rh4pRT2hIRhNMae3WpMt6prg4wCkwkulP8UkmFTv09vxmM2QE1vb4mlwZ1XzS3mPvQdoPIt0JnC5bV7OWh9EN0exaMDU668JCgL3ZlUPLxlagKdNX6bCcGp28IW8u7fRnmQ6g2k65TCmw5Gco34aVolDUgWV7LM39ZttzKRO0bNi8KPjt973iXp3K7xmWfX6AmHZUaTmrrZ7ejg4wYOatip7j6SvsIem4FOKRJwULNlKKoy.tXCcwCWh9+uXtmZHsPtk1C3nCXtgVx62BCE6ikasy3IVM9WFL77laa7k6j63.BGs3lHn9vUDWieIwlYz87xiVvlxKe1QbQOwlTJN88IRKLWdnxYLueVH8AVyKuISeu.bgBco0wniGJraXMIl7nOrvhj+rmLH150KjrTS7jr8R7MSihm1D+k4g3B8FEp29ZaJ3Dq0L630b9jcAQQECykjENdweUiD1w9mTgGtJmxXASjWeqb.0392ZPlbxDrwmhqA+UmVL9VmeTo2YErfn9iDZjj8Gm2GVNa6wY6owidgyJbZC05Cw3IzaDgkb9MEo7L1mGNPLcMEML8lwWWYvB2tdnfm7ulBQzqMR.d87YdYAtTugrvbu5lBhUupCLpWWO72L.0pEZDEGXN02Zn7nu1VhoBREV0SD1fdFiDbyxpJwgtY9qS35KNtPWNem940rQduIgbTZhw4GlwWZ4AoAh5rU4mFNS25ORvOm5z4OML+K9r1pxrdn.dcMmJU++RwQMgczVV.oA....PRE4DQtJDXBB" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-73",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 5.0, 107.0, 125.0, 62.5 ],
					"pic" : "/Users/gian/Desktop/1440px-GPLv3_Logo.png"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-364",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 5.0, 83.0, 108.0, 22.0 ],
					"style" : "",
					"text" : "s #0_handlepatch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-365",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 5.0, 30.0, 46.0, 46.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 824.0, 369.0, 67.0, 22.0 ],
					"saved_object_attributes" : 					{
						"allwindowsactive" : 0,
						"audiosupport" : 1,
						"bundleidentifier" : "com.gianninoclemente.aglayaplay",
						"cantclosetoplevelpatchers" : 1,
						"database" : 0,
						"extensions" : 1,
						"midisupport" : 1,
						"noloadbangdefeating" : 0,
						"overdrive" : 0,
						"preffilename" : "Aglaya Play Preferences",
						"searchformissingfiles" : 1,
						"statusvisible" : 0,
						"usesearchpath" : 0
					}
,
					"style" : "",
					"text" : "standalone"
				}

			}
, 			{
				"box" : 				{
					"attr" : "statusvisible",
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-29",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 824.0, 339.0, 150.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 818.0, 36.5, 170.0, 33.0 ],
					"style" : "",
					"text" : "Set window title and add current path to search path"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 867.0, 140.0, 174.0, 22.0 ],
					"style" : "",
					"text" : "title \"JoyConOSCMapper v0.1\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 824.0, 86.0, 58.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 824.0, 224.0, 96.0, 22.0 ],
					"style" : "",
					"text" : "prepend append"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 824.0, 140.0, 32.0, 22.0 ],
					"style" : "",
					"text" : "path"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 824.0, 177.0, 67.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 824.0, 260.0, 103.0, 22.0 ],
					"style" : "",
					"text" : "filepath search 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1816.0, 242.0, 97.0, 22.0 ],
					"style" : "",
					"text" : "enablevscroll $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1787.0, 202.0, 98.0, 22.0 ],
					"style" : "",
					"text" : "enablehscroll $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1941.0, 159.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1941.0, 95.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-21",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1955.0, 242.0, 98.0, 21.0 ],
					"style" : "",
					"text" : "window flags title"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-52",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1926.0, 211.0, 123.0, 21.0 ],
					"style" : "",
					"text" : "window flags nomenu"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-22",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1947.0, 186.0, 119.0, 21.0 ],
					"style" : "",
					"text" : "window flags nogrow"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-24",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1955.0, 160.0, 107.0, 21.0 ],
					"style" : "",
					"text" : "window flags close"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-26",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1938.0, 128.0, 123.0, 21.0 ],
					"style" : "",
					"text" : "window flags nozoom"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1961.0, 301.0, 77.0, 21.0 ],
					"style" : "",
					"text" : "window exec"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1942.0, 353.0, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 0,
							"revision" : 0,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 698.0, 286.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 70.0, 456.0, 73.0, 22.0 ],
									"style" : "",
									"text" : "R-1202"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 93.0, 87.0, 59.0, 22.0 ],
									"style" : "",
									"text" : "tosymbol"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 21.0, 213.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "$1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 314.5, 167.0, 67.0, 22.0 ],
									"style" : "",
									"text" : "remove $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 333.0, 75.0, 73.0, 22.0 ],
									"style" : "",
									"text" : "sel reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 314.5, 119.0, 63.0, 22.0 ],
									"style" : "",
									"text" : "v #0_in"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 21.0, 303.0, 63.0, 22.0 ],
									"style" : "",
									"text" : "v #0_in"
								}

							}
, 							{
								"box" : 								{
									"comment" : "write/load mapping",
									"id" : "obj-6",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 333.0, 25.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"annotation" : "",
									"comment" : "mapTO",
									"id" : "obj-79",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 169.0, 25.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "", "" ],
									"patching_rect" : [ 70.0, 148.0, 178.0, 22.0 ],
									"style" : "",
									"text" : "t b s s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-75",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 93.0, 340.0, 79.0, 22.0 ],
									"style" : "",
									"text" : "route symbol"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 93.0, 198.0, 95.0, 22.0 ],
									"style" : "",
									"text" : "join @triggers 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 93.0, 241.0, 71.0, 22.0 ],
									"style" : "",
									"text" : "store $1 $2"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "", "" ],
									"patching_rect" : [ 93.0, 303.0, 116.0, 22.0 ],
									"saved_object_attributes" : 									{
										"embed" : 0
									}
,
									"style" : "",
									"text" : "coll #0_mappings"
								}

							}
, 							{
								"box" : 								{
									"comment" : "mapped OUT",
									"id" : "obj-2",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 70.0, 497.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "Data Input",
									"id" : "obj-1",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 93.0, 25.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 396.5, 288.0, 102.5, 288.0 ],
									"source" : [ "obj-14", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 324.0, 288.0, 102.5, 288.0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 238.5, 411.0, 133.5, 411.0 ],
									"source" : [ "obj-76", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-76", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-79", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1320.0, 1060.0, 61.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p mapper"
				}

			}
, 			{
				"box" : 				{
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 0.08 ],
					"bgfillcolor_color1" : [ 0.376471, 0.384314, 0.4, 0.05 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-15",
					"items" : [ "Joy-Con (R)", ",", "Apple Mikey HID Driver", ",", "Apple Internal Keyboard / Trackpad", ",", "Apple Internal Keyboard / Trackpad 2", ",", "Apple Internal Keyboard / Trackpad 3", ",", "Apple IR" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1536.0, 259.0, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 441.0, 447.0, 100.0, 22.0 ],
					"style" : "",
					"textcolor" : [ 0.0, 0.0, 0.0, 0.51 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1194.5, 346.5, 95.0, 33.0 ],
					"style" : "",
					"text" : "enable/disable \nprefix (L-)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1455.5, 228.0, 95.0, 22.0 ],
					"style" : "",
					"text" : "loadmess menu"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1194.5, 222.0, 95.0, 22.0 ],
					"style" : "",
					"text" : "loadmess menu"
				}

			}
, 			{
				"box" : 				{
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 0.08 ],
					"bgfillcolor_color1" : [ 0.376471, 0.384314, 0.4, 0.05 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-2",
					"items" : [ "Joy-Con (R)", ",", "Apple Mikey HID Driver", ",", "Apple Internal Keyboard / Trackpad", ",", "Apple Internal Keyboard / Trackpad 2", ",", "Apple Internal Keyboard / Trackpad 3", ",", "Apple IR" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1276.0, 249.0, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 69.0, 447.0, 100.0, 22.0 ],
					"style" : "",
					"textcolor" : [ 0.0, 0.0, 0.0, 0.51 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.12 ],
					"bgoncolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontsize" : 18.0,
					"id" : "obj-326",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1889.0, 847.0, 66.0, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 301.25, 468.0, 75.0, 31.0 ],
					"rounded" : 17.19,
					"style" : "",
					"text" : "Reset",
					"texton" : "OUTPUT ON",
					"textoncolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"usebgoncolor" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-324",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 1804.0, 1087.0, 43.0, 22.0 ],
					"style" : "",
					"text" : "dialog"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.12 ],
					"bgoncolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontsize" : 14.0,
					"id" : "obj-323",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1813.0, 847.0, 66.0, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.75, 468.0, 75.0, 31.0 ],
					"rounded" : 17.19,
					"style" : "",
					"text" : "View/Edit",
					"texton" : "OUTPUT ON",
					"textoncolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usebgoncolor" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.12 ],
					"bgoncolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontsize" : 18.0,
					"id" : "obj-322",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1735.0, 847.0, 66.0, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 301.25, 426.0, 75.0, 31.0 ],
					"rounded" : 17.19,
					"style" : "",
					"text" : "Load",
					"texton" : "OUTPUT ON",
					"textoncolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usebgoncolor" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.12 ],
					"bgoncolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontsize" : 18.0,
					"id" : "obj-321",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1656.0, 847.0, 66.0, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.75, 426.0, 75.0, 31.0 ],
					"rounded" : 17.19,
					"style" : "",
					"text" : "Save",
					"texton" : "OUTPUT ON",
					"textoncolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usebgoncolor" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-320",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 97.0, 1360.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-318",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 329.0, 1394.0, 61.0, 22.0 ],
					"style" : "",
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-317",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 172.0, 1394.0, 61.0, 22.0 ],
					"style" : "",
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"id" : "obj-316",
					"maxclass" : "led",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offcolor" : [ 0.376471, 0.384314, 0.4, 0.13 ],
					"oncolor" : [ 0.131345, 0.999677, 0.023624, 1.0 ],
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 102.0, 1394.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 337.75, 371.0, 24.0, 24.0 ],
					"style" : "",
					"varname" : "JoyLedL[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-315",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 329.0, 1337.0, 129.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 233.75, 371.0, 37.0, 20.0 ],
					"style" : "",
					"text" : "Port"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-314",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 172.0, 1337.0, 129.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 233.75, 343.0, 25.0, 20.0 ],
					"style" : "",
					"text" : "IP"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"bordercolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-311",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 329.0, 1362.0, 129.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 274.0, 371.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "8000",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1,
					"wordwrap" : 0
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"bordercolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-310",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 172.0, 1362.0, 129.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 262.0, 343.0, 100.0, 22.0 ],
					"style" : "",
					"text" : "192.168.0.21",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1,
					"wordwrap" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-308",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 75.0, 1325.0, 87.0, 22.0 ],
					"style" : "",
					"text" : "r mappedOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-307",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 172.0, 1425.0, 81.0, 22.0 ],
					"style" : "",
					"text" : "prepend host"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-306",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 329.0, 1425.0, 79.0, 22.0 ],
					"style" : "",
					"text" : "prepend port"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-305",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 75.0, 1478.0, 137.0, 22.0 ],
					"style" : "",
					"text" : "udpsend localhost 8000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-304",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 75.0, 1287.0, 150.0, 20.0 ],
					"style" : "",
					"text" : "OSC"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.08 ],
					"bgoncolor" : [ 0.131345, 0.999677, 0.023624, 1.0 ],
					"fontsize" : 18.0,
					"id" : "obj-299",
					"maxclass" : "textbutton",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 625.75, 827.0, 100.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 224.75, 287.0, 151.5, 34.0 ],
					"rounded" : 17.19,
					"style" : "",
					"text" : "OUTPUT OFF",
					"texton" : "OUTPUT ON",
					"textoncolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usebgoncolor" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.08 ],
					"bgoncolor" : [ 0.131345, 0.999677, 0.023624, 1.0 ],
					"fontsize" : 18.0,
					"id" : "obj-298",
					"maxclass" : "textbutton",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1307.5, 57.0, 109.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 224.75, 10.0, 151.5, 34.0 ],
					"rounded" : 17.19,
					"style" : "",
					"text" : "INPUT OFF",
					"texton" : "INPUT ON",
					"textoncolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usebgoncolor" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-295",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1351.5, 331.0, 72.0, 22.0 ],
					"style" : "",
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-294",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1144.0, 315.0, 72.0, 22.0 ],
					"style" : "",
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-293",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1168.5, 923.0, 43.0, 22.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-291",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 397.0, 218.0, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 281.5, 131.0, 38.0, 20.0 ],
					"style" : "",
					"text" : "TO",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-288",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1399.5, 361.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-287",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1155.5, 351.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-285",
					"int" : 1,
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1408.5, 388.0, 39.0, 32.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-284",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1479.5, 470.0, 71.0, 22.0 ],
					"style" : "",
					"text" : "s toMapper"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-283",
					"int" : 1,
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1160.5, 385.0, 39.0, 32.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"bordercolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-278",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1168.5, 965.0, 129.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 236.0, 98.0, 129.0, 22.0 ],
					"prototypename" : "nice3",
					"readonly" : 1,
					"style" : "",
					"text" : "R-1202",
					"textcolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"textjustification" : 1,
					"wordwrap" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-277",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 397.0, 51.0, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 281.5, 69.0, 38.0, 20.0 ],
					"style" : "",
					"text" : "MAP ",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-274",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1408.5, 470.0, 69.0, 22.0 ],
					"style" : "",
					"text" : "s JoyConR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-275",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1160.0, 475.0, 67.0, 22.0 ],
					"style" : "",
					"text" : "s JoyConL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-273",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1495.0, 1115.0, 50.0, 22.0 ],
					"style" : "",
					"text" : "8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-271",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1720.0, 942.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-268",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1428.5, 437.0, 89.0, 22.0 ],
					"style" : "",
					"text" : "sprintf R-%i %i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-267",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1180.5, 434.0, 87.0, 22.0 ],
					"style" : "",
					"text" : "sprintf L-%i %i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1320.0, 1141.0, 80.0, 22.0 ],
					"style" : "",
					"text" : "sprintf %s %i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 753.0, 807.0, 150.0, 20.0 ],
					"style" : "",
					"text" : "OUTPUT ON/OFF"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-254",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1324.0, 792.0, 150.0, 20.0 ],
					"style" : "",
					"text" : "MAPPER"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1320.0, 941.0, 137.0, 22.0 ],
					"style" : "",
					"text" : "zl.slice 1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"id" : "obj-228",
					"maxclass" : "led",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offcolor" : [ 0.376471, 0.384314, 0.4, 0.13 ],
					"oncolor" : [ 0.131345, 0.999677, 0.023624, 1.0 ],
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 611.0, 448.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 432.0, 472.5, 24.0, 24.0 ],
					"style" : "",
					"varname" : "JoyLedR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 220.5, 903.0, 88.0, 22.0 ],
					"style" : "",
					"text" : "JoyLedR bang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-226",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 83.0, 903.0, 85.0, 22.0 ],
					"style" : "",
					"text" : "JoyLedL bang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-223",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 79.5, 1037.0, 116.0, 22.0 ],
					"style" : "",
					"text" : "prepend script send"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"id" : "obj-216",
					"maxclass" : "led",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offcolor" : [ 0.376471, 0.384314, 0.4, 0.13 ],
					"oncolor" : [ 0.131345, 0.999677, 0.023624, 1.0 ],
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 246.0, 447.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 69.0, 471.5, 24.0, 24.0 ],
					"style" : "",
					"varname" : "JoyLedL"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.13 ],
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 18.0,
					"id" : "obj-214",
					"ignoreclick" : 1,
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 387.0, 478.0, 183.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 233.75, 227.0, 135.0, 40.0 ],
					"prototypename" : "niceparam2",
					"readonly" : 1,
					"style" : "",
					"text" : "R-1202 8",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1,
					"varname" : "labelout",
					"wordwrap" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-210",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1320.0, 1188.0, 99.0, 22.0 ],
					"style" : "",
					"text" : "s tomappedOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-207",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1651.0, 941.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-204",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1596.0, 941.0, 35.0, 22.0 ],
					"style" : "",
					"text" : "read"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1450.0, 841.0, 43.0, 22.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-198",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1528.0, 941.0, 37.0, 22.0 ],
					"style" : "",
					"text" : "reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-196",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1476.0, 941.0, 36.0, 22.0 ],
					"style" : "",
					"text" : "write"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-187",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1445.5, 902.0, 61.0, 22.0 ],
					"style" : "",
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"bordercolor" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-188",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1445.5, 874.0, 129.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 236.0, 161.0, 129.0, 22.0 ],
					"style" : "",
					"text" : "R-1202",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1,
					"wordwrap" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-184",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 835.0, 831.0, 97.0, 22.0 ],
					"style" : "",
					"text" : "r tomappedOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2170.0, 1576.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 785.0, 922.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "o o"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 747.0, 922.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "- -"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 747.0, 892.0, 36.0, 22.0 ],
					"style" : "",
					"text" : "sel 0"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 14.0,
					"id" : "obj-127",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 450.5, 448.0, 43.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 279.0, 195.0, 43.0, 22.0 ],
					"style" : "",
					"text" : "OUT",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 364.0, 833.0, 105.0, 22.0 ],
					"style" : "",
					"text" : "labelout set $1 $2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 364.0, 792.0, 87.0, 22.0 ],
					"style" : "",
					"text" : "r mappedOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 83.0, 734.0, 123.5, 20.0 ],
					"style" : "",
					"text" : "GUI LABELS"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 747.0, 855.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 835.0, 975.0, 89.0, 22.0 ],
					"style" : "",
					"text" : "s mappedOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"int" : 1,
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 815.0, 875.0, 39.0, 32.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1320.0, 840.0, 69.0, 22.0 ],
					"style" : "",
					"text" : "r toMapper"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 220.5, 796.0, 67.0, 22.0 ],
					"style" : "",
					"text" : "r JoyConR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 220.5, 837.0, 108.0, 22.0 ],
					"style" : "",
					"text" : "labeljoyr set $1 $2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 83.0, 786.0, 65.0, 22.0 ],
					"style" : "",
					"text" : "r JoyConL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 83.0, 837.0, 107.0, 22.0 ],
					"style" : "",
					"text" : "labeljoyl set $1 $2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 79.5, 1073.0, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.0, 0.0, 0.0, 0.05 ],
					"bgfillcolor_color1" : [ 1.0, 1.0, 1.0, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"id" : "obj-83",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 642.0, 448.0, 79.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 462.0, 473.5, 79.0, 22.0 ],
					"style" : "",
					"text" : "R-1202 8",
					"textcolor" : [ 0.0, 0.0, 0.0, 0.56 ],
					"textjustification" : 1,
					"varname" : "labeljoyr"
				}

			}
, 			{
				"box" : 				{
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.0, 0.0, 0.0, 0.05 ],
					"bgfillcolor_color1" : [ 1.0, 1.0, 1.0, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"id" : "obj-82",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 274.0, 448.0, 71.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 97.0, 472.5, 71.0, 22.0 ],
					"style" : "",
					"text" : "L-1202 8",
					"textcolor" : [ 0.0, 0.0, 0.0, 0.56 ],
					"textjustification" : 1,
					"varname" : "labeljoyl"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1231.0, 475.0, 71.0, 22.0 ],
					"style" : "",
					"text" : "s toMapper"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1180.0, 146.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1259.0, 91.0, 36.0, 22.0 ],
					"style" : "",
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1259.0, 57.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"data" : [ 191390, "png", "IBkSG0fBZn....PCIgDQRA..BfE..H.EHX....fw7K0x....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI68tG7sbcUemeV6c2844u2+t2qtW8VWIYoqkjM9skrL1XisA+bdDvClPJBTSfYBEUBwCIgPvEjLlLELwyLvLEUwPlIEXLkKbfZlwfswgfsbvXKikrdKekz8otu+897p6duWyer284b987dkjSvR5rTcU+6zmS2m9zcu2820Z8c8cIduWYh87xD7fWA0CF.ifB3.bn3QwhAEGFuRpIAbBgOjG.5JJjUiD.AvpgcU3yng0Zhul3GxGVpB3rvZ8yYokVhUWcU5r95zoSGVeiUoWudr1xKQYYI862m986SQdeJKKw68nphXR16eihrqumW.WofXMjXrXLFLD1u1vOOrnXLFbC5iy43VuoajOzG5Cwr6+.rwJWh1yMCNwiHx19tTUwHlc86WwO10hD.K3MgyUJfnfqfRwgS7PhEkLbXnDO44CXtrFX0MODX7iCUmL7XhMw1IyIk3ImRTTpgCCVrX.R8g4xDA79vRwRXtKmFl7HATiCu32w8uhhvtO+SXmE92nQog4KbwsqDkyegyyu6u2mjidzixhKteDQXPgCUELNCBw4tLgs068C+2NYUyOHJTCCBilmXqyW3bNLFCIIITqVMZ1rIsZ0hFMZPZpkCrvbzrUclZlYX5omllsCuWsFYXyRINiZbJeY3bdg04nFVv6oTCygZMYCOOjWVRVRX9cgv0AeQ3cyRfvTqdPbfyAFS37mpgkR7gLieMQjvpGthIyOtW1d+z0I1Ufoidf9XSFHHXHLPvPBFSbBCeYDfTBXMjYEJQoTc3KJIAgZoYgAwFELB3fA85wxqrBm6bmiSclmkyctywJquFm4BWj9EEzuee50qGC51M.lZPWJKKQDEQDLHXsVrVKIogA7FIg94CvK.wi1maKghxvDQk4gigt81fAc6QddN9RG862G0UPd+ALXPONzAOHO4IOAez+g+bL2hKfyUFFDKw+skSsX1iIXEa0I9HdJkgXtptdjlPhXHAHWcTn8QwPMScZj0.CSlfXhMwd9XF.KfAAGDgVEmEL9PXAvXAmWwUVhngOuAAsPwm3Qi.rp.tLNHkK6nyw.mE1tQtcoBr1xqwuv+3eQ9Lel+HDqgYldN51sKYY0Y5omFCFRSSoQVCp0rFMq0DalkLaFXAKV7hGiZPMJF0L70fG+.W3XPCNRO9RDOMazFuVh2ANeAtREuVh5EP7TTzmrrLp0rAsZ0hVsZwTSMEsZ0f50qygtlql4laNtpq5p3.G3.L8zSSZ8TRrYjfg7xBRSRHgDJJKHu2FnpPiZMoYZ3w6tRnz6wZMjTKbtwQ.vEk4H3HMIgp4zGhfRtRt.Lw1KSlDAqm+lnPE.K0WRt2g2JnVSH5RZHZNIlDL.t7BLdk5oYXLonZICzBL1zpasQvvFquFW3bmmkVZI9VequEWZ4U3Tm5T7rm8rrVuN3QIMMkTaFVugZIoTqVMxxxHIII9u.HJQDBikUTbaxSKOf2Ziig1YfTpJ635G5onyMLBYc5zgM1XC51sKCFL.mFhTVkGam8rOKOwS7DL6ryxu0u0uE+fu62CokJIU.S2F.KM5U0tcAviW7wiFChZCWSFywyBWN1DAiI.104bfWvXLXsYn6vLHShf0Dahc4MgRPKiuHEzD7Rb1g3vlt8FPiV0XTLWJwDb4L3BpWfcKJU5t+VwCfXDThyGoZ.fiLZy+s+s+2vO0+c+zruEO.G9VuExxpCRvYyM1XCloccRRrTOqN0ZTiF0ZPV8LRsoHVAqXQEEQCYRPTYyu1l.hh5I7.AU1zqKxKQLfQrgHFM16qhhTOkBmiACFDl2rWOFLX.9xP1QFzsG0qWm1MawTsZwzSOMG5ptJt1q85Xw8uH21q9UhMIgoa1hloog4+TfAtvUn7AjTOMFxJnaYNCTGXMTyXoFAPxAKbNz68gn4IB3ksFDKDQlDAqqPaRDrdAXp.NmGq0BlLRvG8bYz6GB+sAOdLoBIRBwojXiNavLsawkV9hbxSdRdlm4Y3TG+jbxSdRN0oNEKszR3PHMKiVSOCSs3bru1WCXD51uO9dELsTGqSwoJ44Cny5cYvfPZ.cQ.EdUC.p79.HK.TEESv8x8523kY.TYdNZb+W3B.3TArIVRRRoY6l38dRxpy9N30vRazmUVcY5qBCvfWDJqRgvl9dCKk83qWwRoXhSuF1ABUg9lP1USxH26fxRxLBIFKh5g7B7ZIlFM11DHSrI1D6Jv7lv7cJT4vkw.HQbS.1LA0O.iIPdBqO3zUgWvhEqVKrOfQNX8b8g1UfrTCHtPjrI.zp6JqQcSBut65USZ8Zr5pqS6olBu2yRc6vpt9nFkTiEIwNboEA0HXhYyznrsW6EvsG9+ENzhYPvXFtb36APRJ1zQNH2ZpoY94RIwjhUL3JJPTnnnfdazgKdtiyS7nGkzjDRqkvzKNKMZzfa35tdtq63UxMcC2HKN6LL8TSgsVJI0Z.3nnnOdbjjlPljD8AMjTWcHvpQNfipXjsCtZh8byl.v5xX6E.Cm.cUO0jjX3xMHdvTnnNnzkCpRRZJ9DCCDgNZAcJFvEVdIV6Rmmu5W3OiKd5SywO9w4RW5RfXY1Ymk4WXQtpq9PjlUmd4CXid8oW9.V4B8nvUR+7AncK4BqmSZtO.RwnfXGFzGOJNUPUcXzz7BfIlxPDLN2d6j3dvAKQgZpAmyQdoCedN4NONTzDChwPCsF8xGP2UGP+xbVeotzestzeiBDDFXAuMBLZqm62g0s02uLvDtQQ.bK6HCfXrHFKdbj4cXcNRTvX2bz3lXSrI1yQSMQd5LxwR+XT2odZBE4afaPGpUKARhC1MtPtqLUa6ND85qjuajQN0piE5ZQwnJotbjxdb1S9LXRrTKqA8zBJJJnYyDVurCNQIg.XwT0hnVRDSvSMueXD4TMBvJ9ZOfK2smGhIIIndEsb677zhkh9dLlDLFKo1jPj0CmfPUkoZM8PtaM0ryw7yu+3oGE7JKegyyZCVlSbziwW5KcejzHkENvhbGup6fibjivMcS2.S0pAyznN0kDnzAE4gy61T5WL.uMgTqEiwhM5cpy6HOufrrMCQPhTun5pktmyPOwl.v5EnkllExmMvfACHyaoVZBhERswDdKPutc4Xm8z7fO4iw8+vOHO7i+Xb9ycFlFkopUi4maQtqa3Fod8FzsaWVZ4U4hm7Lrd2N37gTw4QvaL3TEmSAmmFZBpwhSc30.w6UUBAjWUDIPnamFHBoWUTmh5CCRBznbSQ2dSKMH635qhrj5Jw4bTTTPdYA8Kc3QQcFzDCYarJsmdJld9EX1omha+v2Dq2YCVo8T7.q1g9pGmX1FI2uhSM2XbvPzwADpANNzuGsyxXeS0lEkTvXwXUDJwlt2QuahMwlX6gIDAFYF5gjWFUGOBvfdaP8DEpUCFL.d1yQ+KdA5mO.qj.tXzmwfHwHOotgKkgLie6TTPTHoJ5WQxfCQ.dwWefNqy661tMla9YoT8TqdS1XiM3rm+bzWcTOExsdjnioFaZjdEYAZE3YSeuie7nR.bjW763woHJkk9g+dp1dQTLlDDrzLqAtxRbtAndgRifKVQ.p.mai0CDtGAaZJ0a1HPB9ZMHwXYtomhYRliz50nzprTmU3oO8I4gepGG9iDNv9Vja6lOL2yq60w2yscDt54lGij.CJA0S8omFWbJyRWIdeYfqtFKoY1HGh2Yx9Owt71DNXcYrKWDrxkvvZsTw58zLsFUkRXuUWgye9yyC8HOLek+puFO7S93b90VFxRXgCreVb+KP6oawFquJqr7ZzsSGFzcPjX3PhjPhIAegRQgCszEqHGAiwRQZBmt+ZTVOizzTrYoXSSvXsXRyvjXoYiVPpkjzZXyRC7xxXPDaXhH0sq.ntbKAnWuNQ9LYIIKkr5MnViFTqUaRaTi42+9vlkhZrrv92G6+.GfN86QoWYoUVAxRAy1qfvqDy5gjRIT4kwpno5.Sij.QbkT2X3ZlYVt0CrOt1jlLMPlKmZdkjjrssemvAqI1D6JzhfhppmuRFQAxLb3W6RTqVFr953+lO.Ow23ulKclyPo2QppLkZHysc.J6M2OGAvxpLLbYiCrp50MZ1l061gjzZzseOFTVDbV06Yk9C3XqsBCDAuWQUOhXvXByuFp.xgkjbLHaxvW6sDbPzr42e7OWRRJp5w6UbtRJKcTVVPYoiBDV06ouBkkkTFclsv.kJjKfIMiB0SgH3sBjDluzoJ3fVo0w3DrYoznUf390a0fLa.15kN+EX4KdA5sxJruYliW+q5Uw2687V30bmuJZevCQuhBRaUGfHMSxQDgrrrXwqWEiJevQb7iE+JS75zDa2rI.rtL1d8.VUfhhACycuAgxxRN6ydN95e8uNOvC7.7HOzCwkVdYvX4pulqgCbnCh26Y4UWg060kKr5xz2UfuLTSyduRoKLokMqF85O.UEbwajSRxndiFzpUa7SUmZ2vgvLaKlYl4n4TsoV85j0nNMZ1lZMpSoGRxRIodcRpkg0F7LxUFIyXxdO.4xAvHIFEnJRyaLVLIVr0piMMgUWeclclEnqa.qsw5wgoPuA8AfbWfQZCK84cnRh1My5ELNKVeXfuMxvSu3GV8g0sor54OKI8x4V22h75tlqgaHqAsAxJFPizrswyqI.rlXSrKu4jfbHTEsJOgpSqxpSAIk4vpqBesuNO3e5WfUNwI4.yMGslcZb85PiRGoQ.QacreE+k1MSTvL1iu7phOlhKk..KuBMZ0jBui1saS+AEzunOSM0LrVub7smgbUv4b3hbHsJZ5FSf9Ca66s53TfAFEm.VIPUgstrLOGLFRLAPnhpT583KKY.vpkNFnJCJKXPYIaTLfU60iU60g0GLf06OftpiNECnu2GjDCaBlDKpMgRukzr5Xs1gxuSVVFsa0hZ0pwLsmh1MaQBBm+rmiybpSipJ27MbibCG9l3s88+t3fW20v0dsW8vrzlWVRQjOVspExBiDgMavGi3HwqQSxBvdYurGf0VG.WoOTUuW0frhhhg5YREotMhhw4AqkhA83AejGl+h66Ky23A+VblKddJbkr+EO.Kt3hzJqIqrzxbwm8rzeiNX8gb7W3DJMFbdkA3Yf.4VvkjPdpkzYlhlKt.KdsWCyenCwL6aezZtYnwTSgsQMpO2r3MgphwjXAwFH2tOnEWpDl3orhGVdIrdUwnPh2G0qlMedXqSxsS5TUHBX9fGiQtdUw2KuO3QjwjDcvLnKKNWHhYVaJoVCYwiipssR6YpHE5dYdwv.IqxexPwhKkfHCIeZlMA5O.Y0MnU2NL6fA7AeCudNDf1uGSUqAVc20gqI1DahsylSf7H.qpD0UAGQQooe.1hb3AeX9lexOEkG6TbaG7.zJMktk4jq4XqaP2iJYYSR1ftYfXBPhNhEPiELqg7.yGI6d0dwKgHKohfmDbtTDRG9c7b0QupC8mWhbiJjZyFVXPkHTHJEhDhhkQ37qrBc8d1HuOq0uOqzYcVZs0Y00WikxyYIikdVAAK1zDRqWGiMEGJEdELIXrozt8Tr3hKxTyLGEEEbtycAt34OKk4C3Vu0ak2xa4sva7M8F3FuoahZY1.3P0G3hFUIf0CNeTxepNqmLLhfoogyiUfRs1KO3qWpOG6K6AXEzJJYSBM2315quNMa1b3MKkkkAoWHIAQTV8bmiu9e0eE+G9JeYdxm9o3BqtBkhxhG3p3fW80RmNc3hWbItz4uDk4EzLIiTSBtAEzK2QgoNkFCkFApkR8Ymg4ulCx9toafoNv94FuyWIZq5P6lPsZTlZvYs3MBhXX0kVFH.HoJD4ppTpAfUXh.phSA5YDemL3wTF.XA69M66DPmgQrZXDvLCAIE.JE9L0xpOTz9zwptGCBhBY3Qh.bFGfU0w3dmhVKCL0varXixGJDDtPuwhJBE4NZmjPsdEXV5hjdtyw64U+p46YgoHSgLXB.qI1D64g4hfBfPo9KZHEgA.MNZ5F.arNq9m+WvS9u6+OlqSOt48sePKovnjmpTXIHBvWFaqiEEQhByrhQ8CqZwg.rFdbX196EWZTAwmgw+7KJLBAZJrWU57d4jnAH0MVEIJAmFcFvIFJMfsVcJTkbfAtR56Jn+fB5kOf07NdhkVgKlWvxquFq2uOcckjKBElDJDHs0zTZLTnwZFzlRy1Sy7yOOyzdJ5rzEY4KbdVuyFL67yyq808Z3Me22M29cd6r3BKhAneQ+.eeqWanj5TjmS9fATuQKRR1LUtKKCYkX2dl531K0mi8k8.rpF.rUk60683bNZznAP.Udud8nc61.vS7DOA+G9R+47k9J+E7TG6nzc8db3CeXtka4VQUgScxmkycgKR294f0RZ8FTZf0VuCKu9FXpkR84VjNMZx7W20wMeq2J2vsbXV3ptJZM+Lj0pMR85bg0WiBAJTO4.ENGEwiOTCMsoiHbdzSnveWET2J4hXrnPM1e6Iv8fga+X2vuafNF+eI0pMrBfzn2jUum3UxxpG.kFUtdqMM78V5nz6PDGhYm8X7xAvRwB9Z.FrThgRDbQEt2fSLTjqzNqN06Wf6bmE2wOFumW0qj6952Os.LwzKNAf0Dah8byB.XzfFzUxPls6EnTbj4G.KeQd5OyeDm8K9k4VZzh8M6bn86Pu5V7YVJbvURU7tS.r.enKLLtXiFWNdkLN951pkp1gQP+J46cbyKaQSn1AaOSwIgm4DEwmv9pp.Ap3HqeTj3Da0mwDk5BCtzlrVdAK0oCmsy5b1MVmmsyZb5NavRCFPWDJRsHo0QSqQhMEIIEijPFFN3byvAVXAjDKO8y7L7ne6mfZ0qy891uWt268d4dtm6gYlYFxDCJJ485g0ZodVH0gCFLHLedzA5jjjMEIqKWTrdo9brSphvnMdJoF+g8CFL..pUqFsa2lm7IeR97e9OO228ceb7m8Dzbeyvguy6fElaA5t557sdnGgKd9KgwjQilsPRxX0d4rVmknLKkrYmg8ejaiq6luIl+FudNvQtCRldFlZlowVOihROWnaG1nypLX0KRZsFTJBNerUHXSIIwFZuChfNnZlMPhspmpeKA.JaF3jpJUwzOHXoV7ZTGTp.HAaaooZhDUGtzqZPuXDInrwQM+BIHRdpQQbgRfVMAYgPp11pP3Gj1YLhM5JmAOND0.FEKInhGQMaaIZfyZV0gU8XDOFbgva6DDCTqdSLdvW3Q7FpkVm5o0IAnLWI6xvAsI1DahsylnQpNp9QsnJInEVFIR68jDJJJvJBMa1DLBEIBZyL54bjZxvpQ8zR765RUkM8ZAabYE85GYQM.ET1Tzq19i58giU0CX2T0KVkLOiY2qhQKd7RX9nc+3W10eWpH3LNJE.bQQB0L5XSAuSAMnf7hHXplW2CIH3KTZljwAlcVN7hKv5ZIWXv.NSmNrTdOd7y7rrxfbtTu9rwfd3LIHoYn1DJTgmc8k47m5zL0TSwhW0948diuGVqWGdrG9Q3O8O8Okibjiv6889d4G7c+dX+KtH0azLHZ1NkM1XMlYlY1w6MpDf5KWDrdot8x9HXkmmSZZ5lhVhHAROlmmSsHI+N4IOIetO2mi+3+3+Xdlm4Y3Ztlqga8NNBqaJ3RqsBKctKQ+M5PJITOqId0R+RGqze.1omgFW0hL2Mdcbs2wqjq91eEL0UsHkYYLvlP27b51uG8yyQkPqaHMKCSZVPUei7axffwjD0IkQVUjipN1GlxSpJ64QQTZb0bGfzJoJvGi9UbYU0BpN+PcyZbYa.eDkVbBAaPliGVhgiCnyZC85KQGwwsf2k9Xu5hPIaGiU93.oLX2U.VpXvkG9smQIhNJBVkFKNSMzjVHNEYidjrxJjbtSy64tNBeO6uMIQ.VShf0Dah87w7AoUPIPbzpPFk.klBR7CfNavQ+jeJV6qb+7JldNZkYoG4Ttu1rxFcXFsMokQ8rZGcsShQXezxwqVuc5w2dXSQ0Z2LU7nFWjCXa+6ABxbytcbIngzLp676e4V5LJ4ThyDoEQ0bp9.uTQUxRRAWHRWBfwahNIqTnJCHjNwfWpIXRRPsI3LoLHQXcWImueedlkt.e6ycVN9xWjUKxwUOgzZMorK3cAva15Yzb5oXpYmglS0lr503IO5SypqtJW8UcPdeuu2Gu6206hCcfEo5xdudcwXLTqVsgye1ueerVKoooureNzW1CvpBXUQQAkkkCa2LppzqWOVc0U4y9Y+r7G7G7GvIO4I4vG9vbjibDJKK4YN4IX0AcCsuFmgzZ0Iuzwxc5fOqFsleQV7FtIt4W8cws7Zesz9pOHE0RYCQY87ArZ+tH1Dbwl0YfD8YARZGqpkzzvMtVInWUUbWZnmTlfmWduOB5IXVQvPTEgqVspT068FFkNb6I3hsxIpw+bF0fwIwRrdylKR2UmygMIYSbXqTKGke9RWLhaAOEGu7rEQ2lNzL9REvoVL3IQyIYXJBEJkDJr0oeokTacRy8XVYIbm3o4661OLuwqdezDHYRJBmXSrmWlJdDz.M2yqb1JDpnb6.x7EvJqvS8o+CY066qyM2ZZZ2HkAYB46qMKuw5LquAYNykUVX1pieCczaWRunnWg.rD+Nl5vg6mKSg1X75dxAq896GJI.vSUfnXPWU.R3GQ4BIN2tohCsdkB7jzrFEdGt9E3JJQK0nldE.ZkNyTzSf0DOqTlyY6tNG6RWficgyvYWsKqWlBYsQro3DMT8foVp2tEMZ0j4WLHrom53mficriwMe3CyG4i7Q3c81emLyLswXCAoHOOeXQfU0yagIyg9x9TDNN2qp.WUVVxINQ3FpO9G+iyktzkXt4li26688RiFM3wdrGiScpSQ8jTlswrv5BqUVPuFBCZWmousaga9M954U7Fe8bMG4NniWY07BNa+9TzsanThUCPFpBBJoXQTCVOXLBolDTwRwfAXrQU9kflOIJjF0dJOF7hhZBdzI9.mDLHXzHHKBvQDifnw9vmDpjl94knFvNFgDGmiUDGnLN.jpAMFDRDaLcdQtrUArh.I6cV.oRJVAIERTF95jDarreioyLt+EIjVR2dvvAu.RhGDOI3IwqAgVU.0lhWTDqfZTTT7QQEru3X.PMBoxv9x64.lXSrmWVP2qByuTKggYpya7QBaGm2QrndACVLoMvXJBRYPiLJcdTueOq1NmpaZoJRbYUzk1tMzoxp4c1oOjZP0PSNd2.Rc4lZveY9L6E.CiB1ROheL8+BkJwouRK+B6COpIPGip4HQczsaWrFHyXvTOMF4KSrmI5oykNKNum5FKW2TM4lWXAd8yOOWbe6mmYid7UNyxb7N84BKuB4dG0a1.SpgNqsNm+hWfkWccZ0pEW6MbCbS2zg4QdnGleg+o+y3y9F+r7g+v+PbOuk2DSOcaxxxFVMgUfqFLX.YYaWmAe4j8R..VQBZuEuYp7pX6dWDBQa0Mp4dOVaBogdgIqsxpb+2+8ym5O3Sxm8O4Og63ttSdsu4WOsZ0hm53GmSbriiQMLyzSSVZCN4EWiApfLy7bM24syQ9deybcul6B+ByyJTxit9xrddNNwR8ZsIoVJ9AkHkPpUvlTEN5HvFmCSjrfIoo3MFRhZrEdOk4EfyiX7XzDjw3VfXTLhEiDa2BQ9QD3iPHDyhBRjOWpnHtvfaCJlnmgd.0G5ugIF6HOGiaeHnRJhVRPvz0gAOSjfZwWMYQsZYT3C57kj.YFKHoCA1lXLXpj3AD7wvyajPP3qT24fs4kNCH1PDzrXQrdLpE0HjhE0XoUs5TVpT5K.JgDAa8jnH54XWl5chMwlXWQVXdTuIvARuZwIwnaqFvFpxWDO1TCzJCeQA97RpklBdGX7CYQ0NsLD.qQ7CsZ96v6WMAejyTQNN4GxITY3bWa0Lp.daz8tJNcYGxQJwn3crGbnZDGr1YJL3QzcmhCnfTFTq9Pj4rHQM7q52c4v9CnLj2ppp37dTmiZ0qgUCYuv6KwGy.fMl0iVMxnopT3TJ61E+Faf0HbnzDlawE45ugivyrVGdpSdRdpSeBN6pqPmdcHsYclqYC5rwpr5pqxEtzk3.6+fbqG4N4lukamSbrSxO2+n+G3c8tda7g9Pe.daus2FVqvf98w6JB5wXVxHdsUMMqZ170fmug+6EI1KxSQXHDug+BpRaj.gG3Fei9azk50pCIFvA8GziZMaf2BC.JJgFV3XOwSwu2uyuC+Y+Y+oTucctta9ln0hyxwN6o4zm6bnNncsoHwYn2Jqy45li+ltYN7c+l3MbOuENvMccT1HkMvyZ9B1vURNAICvZsXkDRTAwKA8Dw6HU7XMrsPplZB7VxW5FCVQUzmFEoojjjgfUDIJzmnCqBxjjj8LD11zjfp.68T58HUxuP7bZVRBENG9xRbpRp0BFC1XpUcEiRw3vBEXrncU3cwlHZHxbgFi8HMyxWt2gxuxCnMQReF2yv.Y3USQnkU38Au2TKkZBXafJB486it5xnm9D78c6Gl27B6mlTRhWFJbdOeTS9I1D6kqlFcBJTCbNTb3wFRikZPbkvFqwi+o+z39peMttV0n1rMnvZPM0voUyXuGeGWwoXZmiAlHxll+ay6uwbHeWTR98pU8LjpB6I.qw.TYzMuDPKCQ7O3G4n4eppPwPaxYL4uYrrKH3wHtXDv.TGFMvQKQqhx0l4CqptgbfU8FbkojzXJ70qwYGziG87mku0YNIO4RWjymmSs4WD6TSSmbOWZ40oUqo4v2vgYtoliA8VmG3a7UnVF79e+ue9Q+H+2v0dnqNbl0Zo2f9znYSFTThSExxpEDiVMPR9ZIwLxrk6AdojPO+hd.Vhn3IzqlBwLwF4pjEKPdmAj0nFz2S2tco4bsAAxK7nVCpETE9c+296xuwm3SPdutbWupivbyOOdb7HO0SR2hRL0qiIqFWZo0YsM5xgO7sxs7ltat422GD1+hjTuFaLnGqzqCCDONikRQQrlgfhRM1XJ97HNv3bjk.IxXcY83MTIxXZHheDuoLr4T0M9+FGjy1AhrcSijEs5y0eDvbY...H.jDQAQUkpzMSB9zg7AC1r1lrykN8l+6p86Nc7489mS5jx1..oFDmGu3wk.pTN77k0mh5sXxZFhTYw.7quD5oNAuia+V3tWX+zTKBdZOAf0Dah87xjXjxUJvQAJIwlHeZ3Io8VkG6S+ov+U+pbsMxvtPCJQHw2D0GqvtmyRz4yukiWcfaGH1y8V0yUJ.rw4T53a2vT8Aa93zrkOWrJJGuJGqN9T0EdxmNNQ4Y3yQvUk2VcXZEqddQpyPKMg98xYC0QdyFzuYcNu53nqrBO8JqvW+weB5kVijomkjFSw5CJoaubld5Y4fGZ+r+CLCO8S8D73O7CwMe82H+8+o96w67c7Nnd85fwvxquJSO6BnXXo0WiolZpv8GvnLp7RX.Vu3OEgZHxNgA4gU3GqRMxZUitazglMaQy5sIOufAkETucSRA9K+heI9DehOAeouweIu527afa+Ucmb1yed9lO42lhN8nllwhsmmNC7b1KsFr+E4d9u3d4U+N+dYwa4V47aTRtpzcskoSutThRZy5jUKE0HAEfmvIZqpDZwmPjjQjYsaieSg+NJ+AlPnpGci1nbvGBDlODt7HnE0sYQdqbGZ0Cia9pJIdK7qpZYQTXUGeZ.7iHiYUkhnphtEUvuZYkWWnZrhaFANL4x.poRHX259LbrnnkQhppJdwgXTDufuJ0mtv4IerAWOdkOt0vUOwlXSruCZJToQd3UTeITVFD+SWYnhgoXOk4.ijrqonCwOBj1U36uae9wSM33etst9muK259I9Tfv7iCo2RDvlerHiIJRLBXdohOY5vyq9XkLRUgFMjaLUWDphXUX92X62.TkRQYsxB70C7jqVlgZhg5IoL+BGfaatCvcN2UwCehSwi9rmh0XUZ1tMY0SovsFm3Lav46zl8eU6m290+93IdfGhel+A+i3G7c9t3m4m4mga6NtClo87jmO.OFlapVH3oXv.JcPpMgjrQpn+KEsWTCvJDlUkfF0F+Kg.glAB+Ej1nFXfACJXPQNMmpEm9Bmm+m+U+U4u7y84ncyV7AduuGZrv77fO3CxIOyYocyonwzKhq.N9pcPZ0l67c8Cva7C7d4p9dNBmWy4wVZI5OviXqgHJl5YzHKkzzT7pRQdA0LVLBjHVRibbJvOJArlgoFrJpNiptOFNHwMVzkfQfSbpO1JZjg4kuRo4qZ.yiuc6j4QCbchPWdGI5gTf4C3KcC87RDMFMsQdTo9jPt+8LzypvjBg823ddsSd1UTTtiqeSS3nQO.qVuVwaAE0qHlvbRdCAgCUC.rEGAupEKZ3.L30zjHUMwlX+m.aGbXIlhNwqHkdbEkA9T4sARTRIdiicqDBUb635uRWFnzY30J9M89AWwMfnHa46oZ6155e9tb66mQNBOh+v9HPoQ.rTwuI.ZUQLa3xsDfGYaYDXyY4PkfivArdB80Bp0rFMsYTlWR2UWBJElqdS1WyY3lt5qmCOy7bK6aQdjy9r7LqtDKkuFZsTbMpyxqziKt5xbf4VfW8a3MvMeS2Be1O+eF2+C7P72+m9mhOxO1eaD0PhEv4v6KoQVR34eAx7NrKi7RQ6E0oHLHLaQj4lvkpBYDA1Av6c3KJQTCMp2fAk47+6exmkeuO0uOO5i7P7JO7MvMei2.W7bKwC+POF0ZLEys3947q2iSr5xLnUK9ddGuct6Ov6mCbauBVqrfmckknmygTqFYMaML24A0JOD+Lbk3JJoUiFXPwJIXknVVgFqxC.ioZKnp8yT82ppXEIzbP2R567dON0OTVGfnDHDAXUUprUssfcyBUenLDPjH1g.kBx+.aBvz3gnVUW.f2XutBHlwDZkPEEtga+llXHtz4B.11M.VIIYr0P1W88XTvTRnJAMJpMTf.hJX8IHZJkpEwlf5yIo65XN6o3cd6uBdyKLGMb9gWSfI.ulXSrmqlDkKAOEwJH1LJEgkJLXc91epOIke46iqNSPlNHvkVWCTGHVOHtm2QF5xEAKAa.j13jSerWKXQL5Nt8JtWvQvpZ62oiSuvXTjH5TnT0aFCu1XrgpgFSniWDWFzph3yMhSas0H7CamFIAGyCYRvIJjoTVliz2gsTngOgDLHkVbXoqWoesDJmtIWxp7fm937W8seLN4ZKS+lMY0zFL09uFVa4UnLuj67HuRlY5o4IejGii+TOM2ya7MyO+G8eH21q71QK6gjl.9AzoSGZM8rflrM.VSRQ32sYJgHqXHF2kQozJ0jhlYHQRY4UVlO4m52m+29e+2j94C36+6+cfPAeyG4gnyJcY+W0gnzY3QepSha1Y4Vu22Ful2+O.y9JtYj8MOO7ZqxF8GPqVsYpj5zoeOVqeWLoIjYSBo6xGT2bqMgF1TxjfzHXoRLPqzMkpTkEpbt.YFi+bpHzn2SADZRypFh1jLFAwcPtuHbCoZPwipUSHXQUnvc4CgtX7fRbhFcSSH3cfwFGKG89I3IX3jcYgarOeLBQlPPmDCT5zX0MR7hiBR30hQBADSjp7TtskEk9Mc7nQwRkHQNsgtYMdqF+R8X8gTiZ7gdiVfHk9Wh6qzDah8etMYW96JyGp1XuKPcgRPKib4nHGbfj.dyt6D3k6ArWt2e25ipi9.gTUN9mcSzjP1aGTur1Xa+13rJftUMDbbvEBfMvoXQDTSbtQejRItPKXViE8TUwEolQ+dF8jvX5HieuHQAV1Hf5QckjHFpmlhkDv4ornO0UfA4zc0ALW657Fu9qka3.yxCermlG5zmgSWnr7oOMyL0zT1LgG9wdTNvAuJt1a8VXeG3fb+ei6me5e5+64m5m7mfe3ejenv9srfVS2l06rJsaMGuTlpFu3Gf0vzDF44yvXlFfqzsaOlpYad3G5awu9u9uNes+p6m63ttCNz0dMbhSbLt3oOIFig4OvUyk5zmG4XGm88JtcdWe3OLW+q+0R6Cei7LKuDW37WBodMjFMYst8oHwyryMC4quL1jDxRyHyXw5bPgCiFKU1HP.FqgjVkK8fNr3nzCtJxHR.f3nJCbDGl7R03unlQIZLBPg9InD8HRDKNUwUF7VgJ.c61xpz9UVQ3zQJXr2GSymIPjxPAG5hQoRFV8hgeORDDkDZXMtv1aPBsDGHtO7XLt31Wc8ZmWFhVkF+MOb5AFBTUC6auRHcgn3bAPV383ho6zHf0KAfZ5d2+vlXSrI1KPa7AXdEiWQcJRQnhhEmCbJJkCqD7mO1y0XNuM.YhLBTS0xpOipaBvyUz9aqGeimxtcnviLxnGAODfW0gCfVVtI.SL1+BzHIEUbCeO0XP7xPvTipLwpiCc3OIq.LHmljhsQJlRO4CJQbwV3clEQ8Lc6VjpkbgUuDo8x3VmeFNzMeK7JV3.7EdvmjmM2wYNyYnw9VfYu58yot3E4zm6rb62xsxa5sbubpi9T7w9k9k4+388k4e9+heQV3p1OWbiKxzsmmQ4O3kl1K9AXUYUiMfMAzZpls4y+4977+zu5+JN1wNAu0u2uWZN0z7neqGibWAYMlCepgG4LWfhFM4s8i+iwq8c+CPiq8ZXP8F7WehSfsYKZL0LQ.NJMqUGJKYkUWMjZxhxXjVrjogHXkXDxLI3cEgiqHQwKIT0iCGD4Ebtfhk6GKz0tRkReIIIY3hLkhHGoBsmBCnJVMz3O0Pt7fnOKUxzvnPPuy.XLlQ8qvg.tfPznDA04PUSTpEFUQfFCnZbvgVMl2Dvs40Q6mPHpBQ0RAuaDoKUi44PHfGwOMYnGagDtFzzKWvStHvywiBX3beL0piMZN7caXaDYXhMwlXuvMEPcwA9JFuGzPjkEmh383oLR8fme1kKs9iO+hp51dX93EXjrC.rtrzF3xAvZLPUC2Sapxn29ug.krBqaH0PLaFXUUSlVrfXrCAXI9v7pHRn2z5q.rElAzNdkdqFLZBpOPqDuJHooDBfUHh+IIorQ+0wIFluUSbdO8N+EngX31aMC2v63cx+wm7n7vm4T7sWYIN44dVl5pOD012b7HOxCy0evqgq8VtIlYtY4y+kuOdpex+d7O8e1+Ddcuo2HazeCls9T6842WjaeWOGr1yaviOGu25cnwzsvKJq1YMZ0ZJ.Oc1XC9M+D+F7+8+l+uXt4Vf29a+cvwOwI4nG6YX14WfRigys7pbw98Y1a9F3s9e8+k7JemucFzpMmb4kX4dCnVVy.+oPBBOmnPBn1XzhbPhwhU8Hk9PXVSRwnABclUKz1cJzPzTJvGAYEaOB4R7o9i31jmQkgaRRFkZIdG3nD0GTlbQBcAdoLBR3+LXiSB+cKO4asJD2oqe6ppIuCSVc4yGuAm.kRApwEj.C0PlOAQSPwhpJ0xrHquB1y+r7deU2Au94mgYzfZJK32wuu85XcuOllXSrWZXWV.FwGf6jBTBQK1pB1xDnzCCVmi968uk7+7+Bt5DPZYw48j4aANvYywK6ckNekdLtSo366Tie2s4EtbEQzkyraoETu011iX2E9IMrRySCZZkLVqLKB3RALooCcl2CAtuZhUmt5IAI.zUBYNwwHs1xiFjmmXGxPhQhLHxzF7hvpNCxbyv4cE7UO52lu7S8jb1xRRupCvT6+fbpyeQJJ8bmux6jV0av+9u3WD7d949G7yx+s+D+DjOnO0ypMzY3p1Tmy4FV.Xa0dwDGsdwcDrBArgFS2hM5zkFsaR6VSifxIN4I4W4i8qve1m+Kva9Md2L+7Kxi8jOI8KTNv0dibwUVlm4Bmg7olg2xOzGl25G78Riq9.br0Wiybwkfr5zXpYBBBJDamJBh3GFsGiBFwPpFZKMDqPPyXYprauAAcMwXwIJkVghHY1UOC4QzvzugO5fS..1fhhg2r6iQFxqfHAPXIX+NR.XhmJ20kUdcIwvlWsb35A7pNrZNqTOYyNr+1suOut80aq7TCFxe.cruOkHmxn5btYavMqZ2NdALaJMxD11pf18bDb0Dah8xdaq.BFeERftAidO+vOQj.BH5KbdQJaY4lxv2kYauRe971h70vcvU11ektes5nTDB.kinVx3bpJ72BhMJhyQY+gprQHBpXPKKAjgBWJlPEb5MAvmt3AgGSvw935BokLP8DyPde4hZLIf5wpFVLyv5qtD6KMk24MeSb86aQ9JO8Q4gN+44zW3Bz9ZuVJRavCdzGiCdvCwa8c7N4ac++07+w+K+Vb5idR9E+X+BaJaCkki5SsiqmiuX09td.VW9bbKjmWRilMYkUWg1sayC7.O.+x+x+x7jO9iya7M9lY14Vfm9jmlybgKxTKtOV9hWfkVacl9U7J3d9692g4u0ag9ys.ma00oWeGGr0hnYozsee7YVv.NQ.wG5eTpRhSi8MvDp5n7RbxiP3VgRQnWQIXAwJ3MJkBTnJkdGRIXcQIDHtcCuYSpd8XdHEugyTI5WUxNvKDRBFAmXzJ.HadoDaxpFM.ToJ6jhBdIjNzpOmL91sK6OSbBjcb86wwgIDW7gGGUuNxY9.VOh+sRTsv7vXdHFtWJvyLODaVsa5QBS.UMwlXOGMQHLOPEDppG1GQFTI0.iLenYLqipY3WPe+aY4N8duPscCf0Kz8+3ybWcdXb++7dOLLMhi6XXrJzGOJcwJBejVXEn1QPxGXXJFEiGLBpXIOwLd76C+mT86JTs2R7fRLA.dNhQF.OVxoo3ndgioDgYmddV3VtcVrVC9pO6I3bKeAr6e+TXJ3nm3Yv3MbG2wcw4d7iwu+uyuGm5DGmeke0eYt1q8ZY4kWlEVXADQnWudznQicLSHWIQl76VruqGf0tYUm3KcBoYIT3UlclY4K9u+KxG6i8w3bm4LbO288hpBOwy7zzyoz5pN.O6Zqyk50iWya8sva8C+Cg61tEVFgS2sOBVp2rFIIITTVBNGVuIvida3BpI1PlSzvMiFBZlRkPZ5AF3B4ztT83sBJlH+ifhROEpGuOHHlhF5T5atcFnaOTwhD7jXSnALuvlcJBXrxp96wWVA5QGySSwqihR0Xim2osCYm2uWwKkQe+aJpXZ0wfOzCE0JQbHpYVU+tjXUDJBFCAvtR.TkbEL83doV8SrI1K0s81A2KyHHgMMeUvglW5VwXuPscCrYXdOc6fLT.bio76UDeO5vtXCskspxqGKXMghAxZvaTbNKNSbqkfiqCeVCVpZ9zhIvSX03CQFK97NJyIUDbCxo+5cIKqN2xLyRya8V4ZtlCw+OO5CxSb7iwBG5Pjt3bbxi9swM6Fbja5vru4lkO+m+ySuxt7y9y9yxce228PgktQiFzoSGZznwneuuHJ0fU1Kp.XsSb8In8SPRhgOym4eG+7+i+nXLFd+uuOHG+TmjSetKfyjh1pImc80naRBu8ejeH999fePJmeVd7M5vfjLZTKkTqk7AErQ2UwZLzrYcJbkHdC3FSyRLIPjz4NuGuuHxYJFFhV0GZcOdSnSx6b9n1UED1SKogn73pZWoUgCM5kwXsGm.ffQwZYygNUeAUENAfb6w1WEIsgU0218nXG2rJMZYGhg9t24529m0qa9XaSCrTSH9TdvZFQhTi5wfMbsvHQPUJXBBv5vtT+lRZ4n8+NMPd6Js+DahMw.Fly9pwSdX71pWjiPwQ2U+s3i5m0K3rrsiGNiu7J4y9B8654iM9rH61LvUbvZXlQFaiBNd5Bz0PqnNRHRWUKCBYpFipeLsrhDZJ1FaHCIlv7ig+E5yrUsjMM9bopHS5CgTHpMWJljL5sQGRPY1oah2YX0UWl4qWi2v9O.I1WEekm5o3QO2EgVEbcyLE86tFG83eaN3AOHen+q9P7Y9i9C4Ye1mkekekeEt268dGxqslMatiOy+ESy+Z+k9k9k9X+M8AwUhsUxSWsLKKg98y427272fO5G8ix0dcWCu025akG3a9fLnzwTyu.me8043qrDyd8WOe+e3+V7l9.uOxmtEO9YOCMlYQTfA85Qu98.Qo1T0IslkB0E7bvHjZRvHRnIlRnYlhXv4KC+yMhnfEtPW4xogTX47dbNOtRPTACIjZRHURBszEY623TcC9ltYxGAQnJimZKsxSwmW+yumuuXjg+cXBRggwPNNo5n0O91t0We4eupIf275zpZYXzDzUempfESL8fNDbC2z.zKCXBxbQRh.4cQ1XMt0q5.bnZMnNUR8vnSwacv6VATssqISrI1KSMQkgicTwsI8Gz5ihgmKmK7vOHticLlQ.IKD3iDMkpJ98EJIR2oBuYqu2KTa23m4K7HoXh.e1kuWir4eeiQl8Pz6iDPmXDnhYTPphvuPLKIQkqWkQuuF6tEph04wpwn8GyVw3f9Fd7EOVLwiEuOH2No1TvFHNuUhxxQ9.1e6Y3lu5qF+FC3rG+TjXSnQ6VbotavotvYXgElma9v2DG6XGiuvW3KPqVs3HG4HjkkQUytduN++c61KJhf0tAtRUkh7R9W8q9+H+1+1+17Vu26ka5ltAd7m7InVsZrRutbgNcXC0yc7Fd87t+H+HbcutWKOy5qvybwyRxTso+JqSqZMocyV3LdblRbFG4piA9BpUKkTID70DMAoDTm.dIzjgsB4dGNQBk.qZnTD7dvX.WoOPPPeTvQUazChJrJxPwla7R4cXZ0YrAxAWTFFQK.7haaoS74hYtLyOrWB023QR66DQ1YGOT1ovBObcgI3EMpE9wUKw0ARnWMhFjYBuGip38kTFE+ByXOjX29crSQ05EKCvmXSr+SpEcPQEMpueAoRwqZnfRFy7RjG1RLx4SFBAryQuZbsvB1I4kgQfpBqMreF6btQCTZIvi2nipwmgD.eIjUwWNIl9PqA0HTZLnpYnFbUQQDqN5BmpFxKDp2nEk9BVsy5XECyLUalpzCq1kV8KYllyvO7s8p4Vl8.7m7DONOxweJJOvBz9pVj+5G5axq4NtKdSuo2D228ce7w+3ebJJJ3G8G8Gk77bZ1r4vu+8JyBe2p823.rzMUAXwKcQRaOLqaUKIdhUFs7G+m7uK2+8e+bm24qjElad91O5QoW+BzzTz5Sw4VcYdMu62IuyO7OLoG7.7W9zGkAMpQ68ue52Im50yvUjiuufoQBdUnedNXglsaiqnDHAgjP+oJFxT0q3hUcAXh5MhfHganEUidKXh42N1RF7f583h7txjFUybfpNS+3rTvZBxLf5FocVvnHo3zWXk3b0.2cib4VohD6iQ5ciL78UczqcLhz6V1cxp+bYY09amdeAAFOEhahr+lH22BbgKQEHJdqQdiFtVnaNwjAPiiOPtZcfYhdYMwdYlEzrN1V60CpF+AHUUpqIV.JlvXnJTTpAcXbZ9NAs1qrfTATkNr.cr2991KilWH3PoeHvkuaeDcEPBCiJtGsZdXzfFXE+MGZ9Xi9sJHAmxiWCqDTZhABPEOViEHHjz3DPLnRPI2wLhCWUmupbHObefPVVM5LX.dbzXpoQvS2d8HM2yr0piTZnyZqwz0axcs3BTH2BMO6w4n82f0O2o4PKte9leyGja7FuQ99dGe+709ZeM9X+K9WxydwKx+je9edbdEqFhtVnasni.nCHryR4v2sX+MJ.KUFKnxQB1QkHUFuoXsNcYpomAEXi7tXsVpkTim9oOJ+q+e8eM+ge1+P9.u+2O0KS3wevGhYZu.yzZAdpKsDm1Uv89i92l69u0Gh9KNC2+oOAoyNC0p2j0WtGMxpgOy.IofDjsEQLzJqMVqEi2PtKGQgxfPTAdMV9uwJ9KWQTKVuGM2gWKQqFHvXkfbrh0BBEZbPBBp2w1.gO1n97xhskhJkJ0TWBg38EDH9QnWEoxCmniPBTV5FQjcAPCUYR0m2ZRGJf6CCOc3xY32dr5Bq97L5x6l9dp.xDJbfQa+3GORrTAq1OJgpxLbujPUECpdEuOTkMEBXSSndyZzYsKQBPcaMZhkDErVnW9.xxpADZr11JQ9SAmCRsB85kSq5Yf.E4kXsRbxoI1D6Ey1V33nrYRn68AAqrSmtf0P6F0Gwwon+M1DvPJN7XPPMfULfl.kVZ2dZNSubpOWa5UzGPHQLQNXEi50X1VmuaWOxEAUSwIfU8TpEQGuFMApCIFTmQw4w5kgfRBSob4Id+VyDX0qsRx1xrxkSq.2zq2ked6jub1gEmS09NTkfUfp1tfoZFoSWikcjvAe.fVAUo5MlUjxQhVsJlvYxntZE3fUknlF5khEpCapgD0fTnXbdRzDLhmbWn5QMYFLZOlxY40efY3PMud9qexGmuxybb5enTtpCd0bpS9rrzZqyq6ddK7MdnGjOwuy+mboAC3i+O+eNZthsvAIVPfAquJoyNM9vCA1VUzO94g+llL7+MeDrh++M27RBpWmAXlomgKsxxTepVXLFpkTiKt7E4W6W6WiO8m4Sy66889XvfAboyeQldw8iykv27odFZc82.+c9w+w3f2yqG2hKvo6sF40ZPsFso+fBJGTPyZMXfub3MOAs2v.dYXDpBdioQ4RJJPnDTh7P9qCUxmnlPWJmP6wYqV.SfhLjP5iFLtswXaIRU654tpAzeG5dnppzaXzDuRVJ686+Bc+sWaekoiszD6QhnV7BjllPm7tHcKodi5L6ryyEtvk3TaTvTtRJ84TpkjW5Xs0ViAENpUqFYY0nYylXsVRSSoc85zrQFkENxKJncZCbkkXRl.xZh8RWSrB85OflsCop4YNwICcHBaJqt5ZX8FVdskoa4F7+O68dGrkcdVlu+9BqvNbRcNoVoVoFIYIrrjrkSRN.lAalwXOyXJfKEbuS3dGBEyvEFiuSUSQMvPA1fYXt3wdLAyEC1XCEXDXLX7XCFG.KGjPHoVxRpyce5SeB6zJ7Et+w2ZsO6yoOmNnta0sZsepZ069riq8ZsWeeueOuOuOuSMSaZOwLH8RlP0joTZh6uHVqmFMZfPHPqi..agkhrRTom9ofNcSPV+Hxg5Jp1uBCXTFuqexCqDZOAZ485Wvjpx0b7uUDbgXU2d5PcPgUYgwUmUD0vU8JjAaZXXSutdku0AtHBonTPnZ3UgY+BR1PDLDaqygW4qjXiiIsNZzLkI28Uw1lZC7w12gIKpEacCyvI5zgG5Q9FrgssE1o.9Pe3++H1A+D+e9CylmYFvClkNIoyLIVbXbVhEwm2GWuXhK4AXINk+2xQ16DPQVelY5oH2TPZTLG7v6mepexeR9K9j+47Ju26knBAyc7N3QSOAbvkNAMuocyq4s+14N91e.NrIiCO6wXgr9LQq1LcZKLhBxQPRRLc62CoThVqCFbFgfqLUsFlHY3Gbi11UbNWESVg.uXU1r.rbfQ08SvQw4RT0moW6k64f9REBCV.5nHn.JKKosVyBKr.+FerONwO8AY6MRIqnO8y6yANzgYt4lCUTR0wTAsZ0hImbR52uOW6UcU7C7C7Cva5M95HJpA4Cx.f3wAXMFWgCq0xrmXN9nezOJerO1GqxalTjmWPVmAzcPWJHi1S1fsr0cxFmdizjDhKJoQdWdoS0h6LOGar.Cgler1GQ61sYfI6z9Ye5FqT3AovDrNGuohkFGxJYC3pXfwVqGfJbknQQ7bUinCmCCF1uXoZ7Omi.a9dQ08UY.hhprR3VtZQWNaJhPgEEtWPPvQ1EfwVhorDsVyV1vFQugsxAxh4KcvCgYl1ztUBy6FfW34518UwTQw7q+e+8wNmZC7i9C+ui4m+jLyVlFqqjhxBRiSwegJayWjvkzpHTf+TJnr5LQAgT7nihHuHmj3DN9rGi20+w2I+E+4eJdMupWMab5Mw9dz8Qq1SyBFKO9wOBSbCWOem+q9A4Vt+WE6uWGN9f9XQRZiVDqhvlWFFfv6oa2tDEGiRoBoDTtbtlqClZz+dz.rb1pGuRBTqWfOqW6kY0O15dL5Lvf0k5.rtP+4eN+9MTCUgUVUyGnzGRAQVYAdIDGIY5FIXlaN9Tezee9G+q+awzsCFaIG3PGfG8e7wv68rictKhiiIOu.myQ2tc4Dm3D7k9a9a3fG5P7ZeMuFld5onamNzdhqr6iViwKFvpBfYUW+UZbzHMgO9G+OfeleleFd1m8YQHDLGP1qYE...B.IQTPT0bmDuGhjZ1vl1.5DEO8y7Tb7YmiVMZgYPAGc+6mG4u+KwMukMwcussxFi0XD1fWGYjDoiv3smBgKm89cTUUyQUCm26FVQcPP380ueilVSDtg1DwpSI54JjmlJW7rYrry2wOOepp4ZYcrBA0WI8hf+Y3GpA3ZogHoJsgCkLRHchRePoV9Q2mDfw6Qo0HURrdGNishfBG3kbs6dOXJKXtEOIKUzGerhNcWhrt845upqAkC9a+a9a4HmXVdMugWONomBiglwoTVVhrRCYq9Xxn2doDWlvfUsXbBHTXWgK.xyyHIIgNKtH+r+m+Y3S8m8I4U8JtOlYpMvi9vOJSNwFI2HY+KrHSbi6gW++pue1wq3d3wWbIN9fNL4DafHYDIpDJxxnSudjzpAosZBRAwUTVCUojphsJUf+ygAUgyiutwAWeakP1gUNPvYSfTOeka3ynS3eYvOBuXfZFrvCQQQ.glt8jSNIRoj33XZ2tM862md85QbbL20ccWb+ut2.VqkNc5hRovXLL0TSwm5O8OkG+web9xe4uL6d26lImbxKoe+Fiw34CTVVRjVwW9K+kIOOmumumuGtwa7FYwk5FzEoA1vl2.G9DGjE9nywAN3QYvfADokTTTvTSMEMZzfzzTx6NOh3fIRhSPVVFDc9L9iqJXoJsbK8HqT0UcpybUyk3pBjvupONo6LWI0WICoX4tOR.hk8lLO3stfUQ3qDHiLz5c.UnPFD9gAlUWg6AIzDz9l2A1pNNsVHQVmhX7HLdRFzi2z2xsP6oh4S70+JX7EzZyagtc6wy7j6ia81uMdz+gGietegeQ1vN2F+a+W++ANoGGZJJGfVc4MejWhCvptS0sZgVt75pRSSwXL7t+E+E4O3i+w4Nti6fVMmfG9geX1vF2FCJD7O7rGhc8xuCdS+a9Aowd2COQmSx7VCztAERAYY8w5KIIJloZ0FeTPWMwsZPYdwxrRMRfS0WzYs1kCvxuxMgasCfY0li1pYyZsLOsmK3J0fiNWPPK.qLH2QOtnTJDJIlhLJK8DIkjkkQ+EVfm4YdFDJOmbwSR+AY3bN52uOCFLXnn5kRIkkkr6cuaVbwEqV0DHDJ5rzRzdxwrXMFW4h33XJMAVmt1q8ZYlYlg986iTJYvfLnzyhKtHEEgwQIKiCdvCRToDY+Ajl0gYmcVLacyTVVhQ3Bi8pSvK.CmeUAMBakljBqlZXUzcZXlppXvC507E3X87iqyIYnT893pqXwpim0s7LeU+HST0.cENABQnClHUxgbi3k0LDVWg4fLIBu0g0X.WnMuIkRjNGJumNyebRsSy8t6qBqMmu392OyuTWjQMfXM+i66w4Z16dnquje520+IlYCafu625+L.nUyIF1l4tbEWRCvZYw5UGnE3E1glJY3bkh+q+r+W3C799.by6curkMuMdl8e.lYSaldNGGpaFadueK7.+K+dXO268ve+wNHGcPelbKak77RPDg1IQZECMurtEYzco4QGmPRT7PcTwHrSUaiBCM5rpGeXz5CSW8J8Bp0CqUvUmoWyZdLabPUmB7mlJoLOOmHU7HEwP3I1b5oYCaXCzrcJpXEG5HGkYmcVdnG5gnWudzrYKLFCaYKagEVXAl6XGC.lZpovZ8nkAMZMFiwUxPUMA5N1wN3ge3GlG4QdD.PGkfVGQ246fJVgOxRddNoSOMae6aG5aIozP2CFV3xDSLAs8ErXYGJJJP4zfKz9pV8Tjidorb04ObEOQO5gUImDmWFrjAuL3uc.08jTekNrpYxpNPLo+TKZlyIbYxvwmsyCcJuNmeDKOf.qUU+mfWMFXDT3qSyZEyTUelRgJb9SFr2FABrxfgai.TZMFmCuutQQKPXcnbNDNGaZpFbv4NDaNZG7Fu58fnWN+UOwiyIkRl3ZuNls2BTbPE25cdaXLF9Q9+5eGS2rEu9W2qi3XMJw444uKx3RdJBAFFM0HwsL7t+.u+2Oev+m+Fb0W60xMcK6kG8I1G8sVbdOO6IW.812M+K9w+QXi68F3K+XOAClLkY1vlY9N8nQRSxyJY5jFnMdxJxCdUkJTMhwoITjYF1q6VASVUs1l5IjqYz5r8GumM5ip9y7Lg0684xgxP8xYHDgfpUZM3rXLkzqWObNGW60bM7JtsaEqujVOaKVpaO50qGG4HGASUGn+jm7jbvCdPlYlYnWudjkkwLyLCRofNKsDSL4jKayHiwXbEHFLHilMRINNlm8YeVxyyYlYlg4N4gINNAUkU1XUkrvBKvTSuItka4VnkHk3hRdby.LFCKszRP2tXTFhiiIlXFzKCk74NKRAKYHbqs1OB8QD5HoxPeI06PAHpL+t5qVc0y2Htv4JWuPD0ywMzHSqhdJX.1g6R5qte4pjxCdPVY6PUdKnSNrHDwQHEydmCkRgVHC8vWqqRBVdTBCyjnPszhDk530cc2.S0dJ9S22iyydrix1tpcv7484QehGia7VtQnnfe7ejeLdeuu2Gug2vqEuqRs8WlhK5bjNzV+WufM7RJGjOrzO6NneE8tR9Tep+RdW+z+mX5MLC2wc9x3o1+A4Dc5hrca1+hKfd6ag+0+r+mo+V2.Ow7yhMIAmShafioiaQKhoQTLVqECdbwZ7Qpfa0JBranX4R7EWHUfUssNzBYvv0rAuAwWIN9ZlrFM20qmH2qYMY8ZiCRo7LtsdGCWcJKWcPh0W7b51NcmutPvV1Y5y+L84cl99OZAJLJCU0Ot0ZwZsTTTPTTDBgfImbR50qGyN6rrvBKDF7mvfMMZzfFMZDJ.hnHlZpov4bjmmS61sYwEWD.Z2t848wlwXLtbGMZjRQoAiwPylMGtntFMZPbbLZsNrHFkholZJ7dOG8nGkNc5vRKsDYYYL8zSGr1AojlMaN7ZRkRslWaO5XYvxiSVecNDtV0Z83MglPrRmR+bGnSYfQRtShSDiPDE5UoVOwdIIVOIdEI4NRrqj8iUqiV4HA+sdiSMprRVMVKomb1tn505XwYZa81WOcikuLABxv1nGKb9fXzcNvaAmYEaNuAu0M744LgtkgnZNTkPLzlazRU0bsgGKRpHVoHaPGZEqnk0PRmtr4bK24F2Lu7ce0rAukNG8vL8DMIKuKG7XGj63k9sh0Z4+vOxOFOwirOjhPPbBgfhhhPuWr5XwPO.aM9NegZ9syDtrfAKq0Rj2So0PTTDdfu5W8qxO9O9+A11N1N23MrWdzmbej6EL0t1EO19eF1x0b07l+A+gXw1MoiVgwYw3Ao2inzEbHRYnBF.vJCR0YTQN58AQrK7bJWDrRg+MFqGtTdL5r4Bj5Awqe9BQPbsKc7iySKEHTdNw7mHH3VVYfwxyiUWOFiwKFPHMQRDdAkkkzqeON7gOLc0KBc6QdmNXsVzZMhhQJZHQ80Zm8YDXzaCrtHPzdBVnSWhhRnXl1HZOM1bGBgjBWIQRKtLM17Aj2uCJufj1oHQRosDtLWjzWrwxigNhtUYs4DZTW7GpX1plMJA3jhfsI3C1EcHiT9phGCbt5d+ZUNF8dZjjDRcXoCoyfurfMqZvssgMhIRxmd+OEGZ+OMSu4swR8WhE6LO20ccW7097+c7S9i8um+G+lueldyyf26IIIg986SylMYokVhImbxK4ygeIO.Ku0P5DsoW+NzrUKVrWGNzgOJuy246h4laNtu68UiAXPoCSRBG6jKP7V2Fu129+btt66kyCszhzWF5CfZqjDu.oupag6E3rgfqbhZe9vsrCi6k3s9Q3KtJWyN+v1hSsKwtdSkONDrKuwnh.UJkfThwXXvIOI6uHGGFFTLfBicX.X9JNtGc0PiwXLFmJjRIJsJnwFoDyfAL6ryxR14wrvhzzza4.rDglCr26CMQ9yhQOWOsEIDBLZMKjDwW3IOLkysHKV3ouSQVtkDsBU4.lRZY2S2la6Z1ERcB1dYXc4nzZbpnWzmhe2YcRrpBrcTF+DBr1pdgqLjSGuwOLnUQU2zX3oMWc0I5CMrEgGr9pNxgGqvinLiXIbcQIzb66frhA7G8vODSssshIUwS7zON24MdGbS6454u6y+E3+m206het28OOabiajxxxfmaALwDSLLqEWJwk7.rDZMlxRZ1rYv180Zdmuy2IOzC8P75df2.lBGmXtEo4Tyv9WZQVvY3M+19t4N91di7XycB5Don.OoHIVFrreMU8JODXHzgarUWJI7dbdOBaHZZq0Ozs1WAcqimX8JBD7bkQRWaUJEgv.1klxSoisW+af5zZLFiwXr1v4bnPsFouK32Q1U8bcN2P1gMd6YsFwWqTF5zR9De4uD+we4GizM1BSxjzsLnGqoZ0hDWNc1+94V2wzL41lhs1HEsvSeCHcNhU5vBreQLNazI759Z8db3P3jK6p9gW0POtbn.5qmRUHpDDOfPhI2ChJqfHRAFK9rdzPEwVEM3d2wNnSVW9pydbhSaPhDN7g2OW8V1Auz68t4i7Q+nbc27d3G8G8GkjjfIQaLFzZ83.rp6gcc50kolYRDH4C7A9.7fO3Cx8+pdsXJrL6IlGYiVbj4lm7zDd8u0+obGu9WGGzVvA62m3MuIREZRbAlqvFx6sw4vX8HSiGVAI3ppBPmODcsyAVOVeMMlCUx9v8wU+yuQ8LE2Y6nCiwyYb95b8gT.u7.+TW7AooLwDSfwkP2AcoeV9v2eoThy8h6AdGiw3rA444DIhvosCsLknnHZFmBNOpNkUWOcpZB04bnTin6m038ulmK2HK5UJBsQMiPybREyOAbi68Vo8NtJ55hvHDzTKI0lyIFLO6q6B7zcVDUjfszrAhLC1RWn8tHVt85TW450ethUseLJFgTlWPiU6KXmxiurmruFOXskXDLazPy+VLrxLW11mqdZ0hjWtbEIJhRBiKKqbMdoEUdAhRKMx7rqFI7Vt86jY+reZdzCcD1ysbabhkx3IO7yxd1yd31M2Iu6286lq65tNdqu02ZX2x6wXLztc6K4Yf3RNCV4UUlkEG+Yex+T92++8OA28K8tY5o1.G5.GFPPVYIGa9E4U81+t4a+s81Y+9BNvhKhblowa7nDNDgB.MbwqJPM4JboV+HAWYcHrA2oyV2npNGqRvZb1qhfqLwE6e.egxoiqEbpsrrpWpoHIIgDYDktRFT4GZCSkH9wrWMFiwY.0WiLbbfpqqZpahDAthd.Ac1V+7WAaTbtO9Y8q2ffRULEpDxSZh0oY1LKENGRSASJJg1SRVVGzMRIJJBkPhKq.oAhiiBh29Ew37Z76p4cC++ZawnZyEzY2vQPqBtxErd+pw0kHRhQXrXskfyhxaQW48BJJwV3wz0w+juk6.wi7HbfCdDZtwMwQKx4om6nrscsSVXgSxO+O+OOaZSahG3Ad..FpAqK03R+LHRAN77nO9+H+T+zuSld5MvN1wN3XG6Xgx.VEQmdC3ddE2Gul23ajbkji0qK4MRQ1nA9BKpLCJiEg2gWIvokPRXyJCdvgvCBqGoyizVWgDKucJUhAqxOV7u31weuTgSKE0mEAeUWYg0AXMrRKsAySb0m2Wg2mM1ywFiw3zhnnngobG.wHU6W80d0W2UWwu0UQ34x0WqY064bX5YnrqgNmnGG9YNNyd34YwSzmNmrOkCrLXtdH5CaJtES4hnYOGM5VxTC7zp.zuPmBpK.vIN0s.2bKevwupsZHDB7Rwv4GEN+vJuWTs4qr8HKA44XqzbUovStPhQEiPnvYbU+tvgP4w6Koovge1Svcukqhuy8dmznSeJ51kV6bibLSedlCePtu6693odpmheseseMdlm4Y.fFMZTY2NWZwyKAXUeBolMo5VVPHSsRxKs7S7S8ejm5odJ9N9N91wZsbxEWh9NGGrWO7abSb+eeeOn2414qefCfrYavIP6DzTGSpNZX0hY8dJbVJMNJLF70FNWcInZqDttyeZkY0obA8H4SdzTC9h8XtB89qvVfp3ps5G2KWwV3Nqed1U75W6MVgsXTuU2hG3TbiYWHPXwxAIEBhBj9v.+ZQvrVLkETTTLrRRA.qCiITV50q5FN0yy9UGA9XLFuHC0xpHHICKFmEu0h2XozZHur.W0e6viPGFi1YVY.VB+HVkC.Us6l5QQjtPfPQVGQdO3KwRIVugTcLsSlfzFMQkFSiIZS6olDQZL5nDRRZfvB9tFFbxdXKJoQiFjjDQQ4.bBGNgCqvQozQgZ4sxZSP0Kp1GEC2GqecuPGK2URV9VwZP3vZ+hqX52OhF4pIrvtrUAML8v0l08nDaXrnkK6WgBkDuVhS4w4snrV14Laf7ibDtkMuE91tiuUzKLO14lklJChXMG3nyxc8xtG9y+y+K3W+272fA4YH0BTJABevPSqOuYEAMY+7071W.BvxshsgAOMLHJnz4qh7UPu9YXbg++h8FfONheg26uBe5+p+W7xtm6lNKrHG6XGio13FY+yu.yOwD75+g+2hYu2He0h9TrwYnTHnQeCs6YP4.CdrgBRXXFiEBIJTHsfv.T5vUFdRVWnWIgb88JDWkX3Wcj60Eb3vBO7z3AJmMzud15sImKuOvoFf3584M5E.mK6Kgxy1SQVOZlHQRIIwBRRkXL4jm0mHsNH.Vzgfa709xrGjFDxRzROIHPCn7djNGBqEbV7VCS1pIQRARuCaQNMiinbPeRTRjdHRFgPnv6D37V7dGdL3q8skphXPIzHkg8mPoB6PgGAtgqntdU1wJ8vUdOpWk47dL9pFKqTrt9rxXLFWofSQ6QqpR9p0znrl0ppthgU.Nk.o0SjPgTovfmA85SyjzpBHQSQVIwhHhIHvYbVLdCkXfHAdqCkwQZoiTufF3PigBYOx78nHuOfidjSGUFK5WhkDcwz.lunOFjDISv0WP6FSReELqX.GOZ.CZ4vG4ozlQg1PVbIKoyXAcNKRN1TIFo.gPgvoQ4jC68rBE30fP5C98zHaAWb7raK3KTq+1Y50elvYZtjZOebzaCaUKHdsn2xIv6Dqvqrp2eAOduCGNbNKBQH6gBIA5TpFiWacDYsDaKPTzGmICmBJ0Bx7NJ8BjpXrEVRizDK7TL+rbm6da7Fuk8vFO1gXlpVzz7VGISLC23McK7q+a9g3C+Q+cAAXJyCroUZBd4EPuxR5QHHqfLau3tJ4K5LXY7fwXnvF9BFklfRJX9E6Rq1M3u3u5yx6+C7AXO6YOL8jyv7yOOwMZxw61AyDs408u3eNoWy0vw7N5p0HRaPjNlFBMIBp9kgXEMxSgnRQVCWYTkv6Fo2AtZpNOageU29hU38db3QkjhSowKUzqWO52sG38DqzTlUh25wY73sf05vargU2XKGFTmE+JBJOj1XAdgjCerixfhbPoov5nS29jWXnnzBHI2TVesSX.eQHkvdFIEDdQ0psVVPshQn+d38MxI0Qqnv5aGZfc0GCFyf0XLFCQcuqS5YX5fBJsggsnlQkYgWDlOHylSu7AAm8NRhSIHyWv.a4vp.GCPlGegAqwf0GR4zf94fMzy6RlnApHA8KGfQXowTsXotcvYgh94LXoLLFGMa0h33HFzqO1bCBibnzQhPRqjDZDoIuaeTdPXEgaqFCwAAombE.CVvxi6cwnkyX4TqN+gYgvGZ0QxfmHgW.VgDqPhuZT4XcB4K1EkwPKAzrHiaYlo4d111XhNcYGSLEKL+7jWVv0dC6gRb79+f+O4Qdr+AhhifpdfHDzkURTLZfd4FJcW7mG+htH2khk0AC.ZUnjNa1rIG83mfeo2yuHyexSxc+RdsbricDzwQTp0bndmj63M7swcc+uFVLsIy2c.wNEZiGGkfLPgKrRm9VNZ+P.FRK4ov.S0suXJPomKBZb8zIQHvHE9jTNwfRjFAZQJS1dBRUQ3JczueFQ5TbTIrUoCHFDFDxjQ38b437Wt8LDtuFwojjDzhWjPQy1sIoR+TY8GfzRUCDc48qy23dpWY9XLFiwoGqGK4mMWCZENbQvfhBjZCIZEVIAVhUIHhSoHuRWWdGRiAuShPFQryPpuAaZ5sRxI5Sd2brwIDKinzki26vXJHpQLSDmRiFMnUTB19cPjYXZsFh2LXE3kJbVKY1LJL4jlZIIJkbSNQ.ASQrJsUBGVoCSkU.coWDyWdigYTwQXRW0x+xHzlhBEXlCvNL6KJDhfTOzRE8y5SilMQhjdc5wtlZJDW6MvrKly9N5rLcTJ44cnapf8r2aju3eymm+au2eU9UeOuGR0MvlmiRESRRBZfR73bVTJMiVfaWLvyKUQnTJQIEXrtgodINRwu568Wgu7W7Kxc+RuKjdHOuDYyVbfElmMtm8v88VdKzORyI62GPyDIIXKJovjiNUgORAV+vpGrFqVzxm9ztc9WkZWti0KElmMX0L4r5+e8Z313TSirLmIPhbv.TNXBEnhCdQlWHvITUzhKPfAgPPt2uBlGqC1p9y0gDm0RYVFo44zNMlnnHJbBR27l4Xys.k3v4L3wgVHPVY.KNu+r9z6Zc7XY8a4WASViwXLFqMVKFeWO3DPeWFDKHlXbFClxb7Iw3RRXIqCiNhznDRhjHMN7thfdozV5pi3PK0gdVOphRb4FhRiooHLwYYQF8JFvbkVNZ+tbiW8NPaSn67yiI2vzSNA8DBTQInDVRSZfpWGb8xfjBzFORkGONrh.IZV4oxD2Xr93T9Mvn+sOjpQgRhutdREJbBCdQf3DqsjjHMoQIfAjc6Sq3FbMslh64pudN5W+wPNshAdMG9XGja5V2KW+r2H+ger+PdM26qhuuu+u+f9.KJIIMkxBCdWISznwyKe+edI.KiwfHJJrxFgfHsh+r+7+R9M9M+fb8W8tYxVIL2wmkMu4sxid3CR4TSva3s8VY58b87jysDYNAsSaQhtACJcHzQPpDizgyIF1OAAF5LuKK5N2J96K+CG5BKV8OvuPFffDGYY8Yisaxj9B9a+K+jL+S8T360iFQZzZMcxyoTHvJBUKpz6QSfZXUHQdA+PqNPUQnhjp+akRg2KF13PUQ5fNohS3luq6gcb6uLl2YIOOOnIJkrpTvCNx+Y5D95E7zpMcz05XWfoqy2ihiwXbkAVw0ImEAXIwAVCwZEwRAE4FbdO5jDNbdednu4yvSezShJtEQpFHLURLP5oTTRGD74elCPuVSx1mdC3RRPXAWtkjVwjnbLwFmgit+Cxu+e2mk+wYeFhanwYKQzsOQxTxcQjpRnouj635tVtossYj3n.OJMThEuPhS5wV6p7tP6ARNLsgiw5AQU54Vt9l7qnD8cDz0lejECGxFEfPfwTRZbBRqGcogMF0fxrbzVK21l1JKcsF9qd1mBSKXhIS4fG9.rma7Fvr3.9E9Ed2bauzWF25sdKbx4WhMmjfFOwIIXwPVQFMhacQ86+E8.rpYsxZsDoCSdN2ImmeoeoeIxGjwt1xVvVjiCOytzhrfywq667MyMduubNvRcnLJFbJbNnHKGgGRihIWXYPdFIpUFI5ovT05rhpWHv7zEZ7bI3pSGyNBbjJcLYjj412Svm625WGN9wYGaeyzPByuzhzblYnTJorRCFZKn8dRbdT9koEtNUuixlE.YCBANIzgR71KjzqWOV7HGgm8g+F7u4W5tV2FB84avjqECViwXLFmdTuP1ylqYhkBTVanWzYbjpiQnavgNvQ3S9E9ZLXxFHaaPEGZtyhp4SLdC8sV13stWlHsA5oZQu9YDWBZimHmih7b18McCTtkMPuidb9RKcB5sPFSL0TjnT3KxQEGSwByx7G5Pbhrtros7pYCowXJyQK0XsFbhP0m4kg4ujdPU2W8FGe04DF0rYotUz4DASHMTV33QhWHvKCzkHkRJyFfDIaZxIoWQAy2oOS0VycsisygVXV9hmbVlblql8cjiQic2ja81uM9zexOM+2++8Wi2y+s2KSL8jjU5HUH.bXKyIIRinJ8uWrvE8.rDBAwQZJJCdRw7KrH+V+V+V7U9JeEdI25sgMOCUbLh3DNvwOF238dObWuwuc5p0bhk5Q5DsQXkTl6vXKQGA9pFKJNeP6M9k6nTqtrSquuZw0wHOuWLEj05wj04RqRXzmqnx4dSzRZhi4WXAX1iwtldJds2zdHU3Y9EmGZDSoRPdkwxocPrERsUAXYKwIBZtZs1WzwoTVVh06HJJh3FMoS2t7kVXNRx5iqeWboMQq0A6yvGpJPbRjR8YzokeNEzIicngwXLVKL5XtmoqQTNHVHvWTPjSQjMBiHhrRnWtkiL.t86+twN8FwmzlbCHrPjLzNhyMkD0pAGao4Y998YP1.ZpZhvEJrlAEErTrhsumqml23MPTiDNXukHoYSDkd7ENlZxMP9Ilim9S7GyyZJoWbDMcPd+RPKAoGqvgUnwKrn7JTdP6jgVs1HEW0XrLFJX9gVwACI6PTEHEHPPkP1bdjUEonPHpJBMIDoBETfrpcG5LD48zv6wjmyF0IbuWyUyy7jmjCc7iwzMSoSmEoYqswMdy6k+3+jOI26q3iy2626aCgVRYoAUYIRuEsPeQWpsOuDfETIJZmmCbfCv6889doc61bMWy0PuYOAG7nGihImfItpqha69esj2rIGaodjzdZrNIZkFhBcSPmFrVCHbzrQC7kL78u91yVJpuP986xYbwHMg0AXUT3vFaIQEAxHjYkbnG+oPkOflshoqujbkrJ.KAQNIIFH0FrdAuRfS3Cs4n0Ze2IPnUTXJI2TRbRChSSPaLLUZJ48GfMJgnnP.VtxhPIfS0phNKvZm5um65VaLFiw3rDU1niRFEZD6kfcfm3nVLyFZx27HGm7bO1jLxJCO2DcDZgDiMC2bGm948wnTjpiHNJlE60CsHBU6IXAy.FL27j2oGMmdRN5fdHzojZ0zHIE+BGhMojrToitElf.5qp8wHkjRuAPfW3vgjZKUUUsxMmeb.VmqX3h68PHLJONUk8PfDuHX2SNQPeVk3ItYJk4Er3hyiTEQyzXJ8AFPupFobG6XWbzu4iyj6XmrTYIyu3IYK6X6r+CbD9fu+OH2+8e+rqctQ7pPE2G.YyC..f.PRDEDUJpURbEYHiZeQUkGm2bisZejpt6CU6wRBAXcdhiinWud7K+K+KywN1w31tsaiSbhSxhc6wzaa67zyeRtkW08wM+Jekb7xRHsMC5WFZ2BNGNgGWh.izAZIIRMxBO0NqjzyPGjE6Hd5Tki+N5IUgmJab3L+84RMNa8lp0a+d8Rw2nsPly12+QeOsVKFqfnzoXPYLGat9fKk9CDHksIKGxGHwTDgsPguTi0DgsTisLFSYDElH5XjrjQwhVIKZkzwnFtsnQwhVEKZjrPoDW5TjqaxhFE9lyvrcyoeQISN4jTVVR2tcWQEqV6vzFigzzTxxxF9cPV43zZsFiwT0WzTC+tALrYOW+9MzcpqdOF6CViwU5HP7voVnGmt6CnRmjwDGGOrvlbNGoooCaBukNO4.Dkf2IACDG0DuWQ+dE3LJjhD7VMtRIdiBeoDx.F.pBILHiFZAMiinUqFjYJP0HkxHMc8POglLYLNUC52wyTQalIJlj18Zg+nFhGHI+jcIx3YRhP2qfFkvL5TjYkD6DnqZDapULePXAimsiedwBqkGF97IVqOyULWjm0cC.Mhf4lZbfwNLySN7T5cXjRJEB54JoTApFwPTHEwdeIR6.lDK29l1B21LaB4BySaofRaAVgm65NtS9B+0+07Q9c9v3AxwhPoAiEYkYXcw7X2EcFrJKCthcyFo7fO3Cxm4y7YXu6cuzsaWbVKQ5HdlicLtpa+145eYuLVRJYIiEIFhiiqpVCePP6BGBIn8ftxzJc3C1bwyy+vtFuPmUi0JHxyEcLszhcnc6IBU+mWVY2TQDkzjzFsnnLinJ8K3AzRIBU3bmW3PnCAgOzKVfgJhTQfx33zFPQAFui7hfP28QwHhhorrDQY4ozHYw+B+yMiwXbkL7.p3DPHwXMTjkiiBx0Zp6lYC60cUlBsTJp5.HACdIIRCZO88VxxxH1JvIDT583UJzwZj5HjZvWJvlAxRAM7oDmlRoHiFZMSpSHQHvUTxfbChx9zHViA2EE+gZLpL1TH3gCtpTG5sAQuiGDhUjBVQ8+3pRunyQV+knU6IXmpXt8MscNxAdZ1+Ili3o2JKs3IogMgqa2WKejO7uC288cWbe22KmBWeZHipreiKxeGuP+F54TCvoYiTVpSW9PenODG5PGhYlYF52uOYNKGpSONIRtyG30yN+VtMNQuLLhHbHQKkASHSXvqr3Ud7hPY8KbBjVQ3DiaMV4P8VEVYqX3rqLhewBNEVHOKYvSfCoufFwBRz.wRhZlfLQhWIIyjgVoPqTDKTDKCBUWoEPj.gVfVpPqDDoDDKO0MMdh.zBHRIHMVSRrthYzPfUkUAXMJaS0qZdLFiw3xS3DDbsaofBsjbsDSjDehDar.ixhJUhLQfLBTMDHa3glNbMcPJnhjU9jmpRqlfPowXbnjRJxxnreFwJMsZjPijHhRzHzdDQfR6v5xH2lQla.ERCFoiLgCZjTUAzg82kc67U2ZetzgK4YZQJNEi9dEO7oYKXLCg1klDGBuEkiJScsZNZQUmZQDrHiPwFHBwAHAu2f1WxLVGuzstSto1y.GeNXPebECnHuG6b6alu9W+qwG4C+6Rgq.a8dlN9h+gmy22f0KHkUe++d+d+d74+7edttq653Dm3D3bNxcddp4mmq4tuatgW1KitFOK1qfFMZQbbZf5ypdVWviLriP6n.gUhuJcfiRS5pwk7eDdIDmtfmV8wky0iSRuiXgEQ4.51adnnKCrCHiRHUCw5JGcd4.ZcXwfkRQXyKbH8UUjivWkG9k6EgROTjO.aYNQJIIQw38VJJxnnn.kRM7b+nAUUm9ywXLFiKeQo0D7wNk.YjDYhBTf0jQV29j3MDYFPbQWRMcI11GosC3VBuuGc6r.EY4DEEMrRi0xPa2IxIXKslfTqgF1RZ3LD4ynoNGA8vYWj1ZKhxdgLkHMnRDHaqIGGY1BbRvKVcXArtATb4.tTSdvYymes9qB8cRaEyULzQ8ktvVsKruhW6vWuiFSjfob.5d8Y6QIbmacWbU5FTb7iQBNrtB5Uzmsr4Mwexm3Svm6y74P4UXKKAk7h94vKHoHbEGLWU5khiz7vOx+.uu226ixxRlZpoXwEWDDJd1SbBXqam69s7VHdCag8crYINJkHQRH2xJEdQINgAkPTYu9NjHw4qoJVh2ubS4c0GuVsytO798qroMOFm6PfmTkCmYIJrcA5whG5.7Weh8SKYDMUJxGTT8LkC6eiAa8MDLkFAJuqxOYbnbqLlegRQo0RtyPRyFjacjaxwze.slbRhajRsxpFUqGQQJ71wmfGiw3xYDozn7glCLdGXxgBKQE8YBG7rOzWiNQfQpBsPEuMvngJvfQqFahzo2HyrqqhDcBpBIJOHKczLB7KtDy9DOAKze.klb5IrD2HEYtgXoDkzfHKOjtRggLaeJiZfW6oT3GossHWQDUtJ5qtbMHK3RuDIFsRRWO19rhkWPbnWFJP57HpluPT4+ndenPnD9pG2GzUhSHHRF7Mq7SNO6cxMxq5puAN3i7Uom7DPqInuwwN10N3q8U+p7a899.b221syF23LTXsn0WbUI0E7280J50O9G+iy23a7M3ptpqhidzixl1zl33yNGcWXddIui2A65kdmbhEyneeCadmyP2AYTfklS0fBSNA6HySb0AXbf0urHCWu5Ad8LBxv9X3gjmmdE4EBuV5RIF0tJNastgguVAT3rXTJ10Md8rqW2CvAe3uAr+CPuhR50u.jQ0QUUszCWcWNM7lTuBEenwPI7tUnCKvARINzzy5ASIzpMSb0WM24q80.5ngLXIqBvp96zo62FiwXLFWZgzCIJE3LHskfsDk.ZEK45lYFd06Y67E22QnWAX8VbXwBTRHcQ4QPmdyyrKkQ6Y1BosiwZsgNxh2QjyvQdpmlNG9nrcfoAZCjVVhKChHzmCmPB6dFEeq6bWzpzfjbzBEZuJL4NRbiLlnMTjgW1jhvZboHfpymOeufvYTQvOrp4GTh.m0Gzl6HyOKHT8lBW32NdAzuLiIZ1hXYDcO1hrwFSy8dc2.ewm8axm+HGF8tuJRlbZVbtEYhVs4y7m7I4q7892wa365eBkhPm64h44wy6.rVO+jp9f823geDdvG7AGlm7d85QmNcX94mmsda2J28a5MxhRAy1oKMSmfXeD9h9HSkT3sXjNjBOQVKZuBgGbdAFgDm2irJBog6AmhWMsx8G4Jkl0Xvy8KLsBACbBrYVl9ptdt+2w2Gy8xeVNx91GokdxlqCoxXTtvkMNgCizG5kWpfunU2xIDUmCq6Ag0+n2X8D0LkLSINo.QjhsrisyjyrAtk65t4nVAEEAqYHRFW4Tv0UHoab.ViwXbYJD.XJQ6LjR07HE4zTI3VmbZ1vM9sv8tyqiA4V7ENDtPSbuvWhU4nSqI3i7M9l7vK0CUIDohnuq.gVgNJ3N7MckbsR363ltYtgMNCFSOZIDXO4hjF2.ZMEJkhcuwoYKsaRpo.cgAk2iKa.MRaD5OhBI1JeZBoCK05w5xSuF8bcwxWngndNkU84uBcPKpBVUXQ58UU1uB7Aqa.YEgJUP5q2BrXYkgJ61UI78IhSQ0ufcL0T7xtgahGsyI4ol+3roIawBKMOaZ5o3jG5H769A+s45tiamcb06Bm2Oz5MtXfy+.rBDIUcfKPipKH0c.G+Q+Q+g7M9ZODaX5YXtiOKabG6fu4y7Mg3T91eKuY14sbK7XyNOFigMMwzzueWTJAIsRYgAKRThhPx.Eq5jiC2x0XxY8OjpSSU8D6WoGq0Y53xnBAes56fmV3kLwTafYO9wHuvQyo1Da4FZRxjahITwL+gONZqbXZ+7hPWn2JcXEgl+bsStO77nSthArb.IMRoWVFVbzZxIY6W0tna+dbzAEjKUXp3Qdn0SvHhb+xwQ+Fiw3JPHCY3ox3fqEwb3Z35fQFUVFRO3sdThP.Q3KHqeWDFCMRavtSaxViZPwfBbCLHcV7NCVeIVsm4aMEO111FGL+fjDIHJIFOCvqhQnD3Udx6zkM4fanUadISOMY8gl.wSIYCSsIxSRYo9CXSxHRFXXP2NzrcKjMaiuHGe0B5ErxXEpRjBpKPSfHpXjY02d4NFc+8rYxzgUDX0yU5qZWN0O9nD13BYkXM+bEBDBESzdJ51qGEYELyLSyfN8IZI3d10t4qs+uI+CO99XgjifRBNumoaMIe7OwuOu4en2A67p2NWrmf37J.KuvE7VBaNMTMorzhWnPpCgD8PeiuF+de3ODshkHKxnQbJOy9eVXCa.1y0yM+c7l3fGaNzpDhmpE4TDtvPJnLOiVpX7NPhBmyRITk5HOBeHsgA4WIpN2tFLoU+eF4gpOmI37mVUo776DzEaZcWq2+Quuyz9+JbC+QP8EA48yYplSfyZo+fBLkRn0LrXQIhstcJxKP5Eij5X+xCxJ8HWUWS0yJYXzgmLgf31ShJRhVEyBCJQG0j9EVTsjUdZUDFiAsH3AO0Bf2JDnUJFLnOaXxlqQ.kqLEoCSwXkOYsZJvWOuAasN1rVG2Fiw3JITESEpJOhCcvFVzHPZ8XDdDQJbVKIJMYlLjZENSXZUoJAg2QeqInCyVMwYc3Mlv.A4kD4bnTlfXnwhRDLmXiqDcwhjn5SgX.Cx6QTyVLHyRYTHMgYFGw.ynhHMu.erBaQFoowXKF.BXhTEVWYvrRmZJJDdDkEAFykBb3BjF3gvx88CmX47c54ZFZFZSMq91ZuJr54aGIJFu2idjcfULZzY4hkOiUJ9YAwEtZV7Dm5jrCm+XcdajnBrRU82dA3G86HtUTY3Uk6FV.GBJybfHFapkkJ6RCsjlltrstJ99uq6ku991GG3nGGeqoX9xb1xjSgsmi+G+JuGdM22cS6MsUxKJwXLzpUKxxxHMMEHz6aOe0n0E.MXsbnqQQJJbPdIj6x4y9Y+r7j66IIUBBULkNak02Bu4en+2IYqakxYWJbgjrxCkBBvAOVvAppl+qG0Po7.f2aqsXzS+d25Ukim2euO8u+uX.BBC3.pvETQRjhHr5DboV7dOlhxg1nQc.sppybNQXfqS2Yi3Qp1QkRMzbPUJENoDjZpaNnhpe6LtILOFiwEeL5jliZaANwx+sej+dXVCF804BSZJD0i+qHHzYKBGzHJb+BoBes07PUilWqokPPipxJy3qoHShPpPo8LyLyPixbZqzLYbLfAQbLyj1fHTzw4wIkHURPICUynTwvxlZDUZGVZ+EGM6TeL4Ttcjfqb0TBVuKcVD7yyGoH74RPl0e+V8qc0FqyPlsDK+G0mabhfVs.INcvqLwaHxpXiBEVcDu9adu767nOJtDGER33cVf3Fo709JODet+x+Jda+KeGjjjrhOSqM3VAQQQm2yueAQj6IpDJcVTREBAHUvyrumjOxu8uMNCPph9lR54AzJ1z8bO7pefWOGXwtqHWw09XzYxMbubJnly2UH7BcHkR7hJsv4DfThvEVUBdOlnngBYutypu7QDGBkbXE4rdX0Nod8FBIFgrxDBqLrTHnIOwxcUfwXLFiKtXzwAOWrIfP.D9pTvIv6Wdt.viPpQn7CsjGzxJFODDEkf1pHtPiOWgKBnvgnzgPKQpgx7BxJsf2SyzTbNCpRHwqBNGebDkQ5gc1gZCMc3D6WnOPs5u+mgw9BVayx6LBg3TRy1oCmK8Z1KWQnXkN0uGBmGoUfTovK8X8A+KSU02BmPo40d6uD9zO5ixA51A4jsoqMmVos3jyu.ejOxuOuhW4qkcrqcN73TM6UEEEDGe96SVmWLbFVkRUQrJkjaLf.TBOOzW3KvW+u6qRiDIBkhABvZJgcrK9Ne6uC5jUxIlaQPtbZXNaRAyKD9AwKVfGF58LC0+Tc5yqDsfRqQGGgJRirZSo0Cueod4+d81jJERkBgTBBwPNu7hQZeMq0EfWgGb6XLFWpwpCr5bsBhpsXmUuPauLzG5PDjLhWovqkfRiSIvnHnLWQDJglXzjJ0U8iPMQJAJoGQHNJLRW30WYLlHkf2MjMboTNL3pmOgifPuWqacvPMpViUv92yC6qmoVAzymsJnU+aMgGhkUKl2E7XwbokbeIklA350kqeho3McquDhs.CxIIMkEy5iG3y849a3q+0+5.UwujmO78+B0w1yyTHKQgBSoMzWrq3.9Ye5uI+w+g+An.L4gxruDAni4FdkuRtt63N4HmXAhaLAppT7Tq+kZMxLjkhJr5SXWpMSswnBBQnvAjgaCLzKpZVmrh.pTQ5UDnkHRWYjem9skGTbkad4xrdNNXpwXLtzfyVyld8PvooVVmMdoHrXJkDaUUhYEfSJvIEXkJrREkRIV.iyi0Vh2aQo8nh.zVLhRhZGS7DJJapnirjAZGkw.oRncBdEg.4jKuHe6Eo.DVMbTUrVB2Zd6ZsMpILi3J+NUwp0.7pOuHq7MqP+JFbwRrIBrtBXPWZ2a.ug8dabaSMI9rRjBOXJo8zyvrmbd9S9SePVbwEQq0XsVJJJBZa6Bj+XcdyfEdEk4gRtWK0Xoju1C82ym4S9mQDg1ahQnCoIZmamW820+TJhRvE2DD5vESqZxwgVpfTtl+P+b4G9OeFg8kie9Wrgy4vUoS.oThVGBdJJJBcbDHWN.rZgR4qBNBgJ3ltZ45dqJNBQbU.YibqPqpnF9Tat0WIc7cLFiWHfmqK9czQ9WQSnuZ7hPfVZjZEBsFzZHphMKsjFMhQKbP+tPm4gdKPT1RDMnKpdcY1CdH7kVbJA4BGploHZlPtvhUFR43nKPazPVd9pJ9jmlCUqnSaLxyW5WVxEWoiSW.70aJOnExvB5iTnhfXgkndc4pajx21K4ako.r85BIZ5MHvh0G8i9w3IexmD.Z1rYnvnr107y64BtvnAKcRU43ZwXKHIVQqFMYot8IsYalqz.yLC68M9swNu4ag4LNbpDFjWRZhdXkBrbt2CXs5kbWtMo4UB4397AtJSHSHkqTS.UeusFSnAs58AVmFIU.NAnj5gk08ZcqRpBhj0Ep9v.SoAQr6kpghjXkAVE978qvfzFiwXLtPiy2TDJpDN4PMOM7kW04GjAG7N3LR1pquEU8dTHevRzKqGcdzGkdBHxqwf.SDzTYoQNr4ozLYRJdu.UbDXDzcv.RDRzRUnifrJoo3EfrVOvWjFCIz6JF4uqFtRNxvViN+hzCXWVGqdBqY8zs+ck57O05xJbLPPjSfyAVSvNZUJAwIBhyrHyx4kesWOuhm5I3Sc3CiMQRdYIxzXhpzYUcurcz.rrV64sKAb9GfUtGUhlbuEi2PCUB20ccW7.2+8yezm3AYorLPHYx8dq7.eWe2LeYIcLR5OvwDSMEFWdU45ubYwO5jkWtm5mWrGf0nkPasenM54u5Jwv68Ul75xmSCsBgkM5s05VekfSCtygOTlxtJgeZAsJZXY8t5.rFGb0XLFuvBi1CQq0YosdbEgpRJIlJFtr7Ft2uUVHqCmPX4DcyPjKw6E3iTDK8r0VZdI6b6r6ImAa+N3GTyfdDMZMEEElUXlk.AsXUsu3tHKycUUDVitKLpWgIDhkKNnpcE4nKp7L3RlmuEg0467WWHm+dshGnTBZofDqBiwPosLPziPPjVSrzgxZXWQs39ugalG5PGFaQI43PnU71d6uUtka4Vv4bTVVtB8DqTpy6u+m+AXoBJxSHgDoFqqjst4sv+ke1eNdI208xC8jOErisS+MuUFnhPohY9NcXx1afnzDrEkHp9Eiy4VIknmFOHp9wubuJ9tX+4+b46+4x9zY7BvQBtI72rLiVC+6kofez2svZCbm9UHN70sF0DnWVsRlkEG6P5y8xgqL04BhYsnnflUMGZsVGV0hdYAxuhTK5W92jQQQC++0qnwZsgpNZMN7ekdP0iwKtPsdXq0H6vz348m1BLo9wJJJHIIgxN8PkFXHv68njpfc7vpXgYj2OmcjlResliDAeQTYsrEE7+1q9kyIKJHK2AYdD1fzCz++yduYAaYYm042u0ZsGNy24btxJq4rJIUpDH.wPyfCb2xpAraHfvFBPgvlFgwtM1QzOADgi11O4vF2c3G3AhN3AvzsLDDDzHFjDsLZDPhpJoZtxJmmtim4g8duF7Cq89bO2SdGxrtYVUVYc9WwsN27dl1iq025+2+u+e33n0qQEikpaM.UfB8PI1PHNnBooZbVguR9l76u3det0IzK9W6k5mtc0iVgHsCTBbZCHE9wjbNLZMVq0aKMAAdsnZriWjpf7L8nD9dkn3V+ttcIn3c64GucQw9Rwi9LfHvHcfwQn0QrSfSEPZtohlZ0n.jYCIXKKmswB7e2O4OIaLeMNW+d787C+ivu3uvmxONNrCcWc2hbmCe.V4eBFiAkvgv4HHLjG8Y9.7eywOM8hJwKt957ktxU3lBHav.JWsFBojM2bSpVOde+32ufnbN265jTLaxzae3XpwwXp+v6x38JC1LCyv62fOHBIVgYrlNUVCA5LjlThLNzFKgNIABARmDkTQ0QYnbBBEfSHIwJvZkXyW.3dM9y6T5uRgBiyfy.ZzfvWQiggwHCkzq6.TBGJQPtDVUXcFLYFr3PEGsmbr8f93YNgusqIxC7TYjDHL4s0HCYJnZb.tgZpZTrBN9QdpmhO3OyOI1G9TjUsF3j6nPoJjkzjKj3vfCoStu8hMjFKQh.xLFZ2sKUqu.UVZNFAbyKMhsFkQKqCQPBK0XQjnHKM2DQOfnEmtEtLoNdlg2cwcpo6cKub2stBro9F1mm4v2lCl9ZoYWSMCyv8OvIEi0ZT9e.HO0YRKJaJgXIVXQpfn.Hz4vqQd41qpSJwVXlnBANgDjt8bBvCZTf6VMXk.oBTduBzX8dFu1p8ULXl.UXX99tza+1V75QSjyPkc6L.7f7XWSmMqsiIXmbIpLAnvPVfCDVFY0nbFJqBQsYa57VuEYW5xD9TONwdU8sC1HucxJ1cBNzAXMJy2RRrZCDDPXXDkqDPeCLRAe8ycc9aek2fA0pwbKsBog91Xhv3nV8RjpS7BWdJQFtaX5cd+A5CydvL7tMtepeaMYPVdYbc+uF.mgY3AcbKooaBYCHCCPIbDJ885CEfzB9dnlCBi8VIiRRlRhU5q7XizyPw317x6RXvvQi06SPXDHDHEQXxEegw3YpBqdb0xIDBBURjBEVr6nPetSWn386YfYbfTS828o.02tkbXvHCPX8lSqz4P5BvglLxPFHPVJfkpDS6ls34+q9K469zGGN6Y854MvSzSASVSl16200fkKPhTHv3LPZFDDSTX.CEPKG7lqtAcsRpVeYhpMOoC5R1ngHkNjxc2oTukJ5Xpxte1jd2+fa2yD6IM1Gv0u2qOSOs19lcs0LLCuSBe8muM1Y3NJD2xfGt7+y6IVRbggnr9tSmSYPZE3rBrBIDJQKkXTRLBAVgM2tXx+TDR1O2NPbOTiui84u7sAi1fFGVInsVxvRoRk1w3Sx7zFYbfT3vkZ8svm8XLr620n7gABGHsYHH2HYUBbVutXUVIH.qTSlTS1ftDEJ3T0ZvEWcM59puL0ehGGERzS9YdW1SEOTAX4MJMGFrTNtDtAI3RbjEUhdARtvF83EN+EXTbUBQRZug3zVpNWLAQRFLrCRUrufu1icp8xSUF+5eWN.7Gju.98aXu7isYXFlg6M3fXvd2XRXaFrTXsRuM+3rHTFHX6T9XEdCtNSJPK8uu.KHcND4oVRHdWj+Jmv6sWJoWb6NKnjHCTDpjHDNxLFPkKzeGjYcX0ZrFCBskFQQHx0MzChjPrMCV6VQB3HHeE5ZbnURTNIfj.aFNgDs0QXTHoYILJIyGLZutb0W3E4o+XeevIN8XQtOIrV63h33vfCWJBAFoSoZPLHjHJUFAADE.Hfq2pECPQ0kVAUPLBU.QQgDHcHDZFNrOUq4E499EA9tU8faei2gYOXFtafcyn7rSc+8z2te+5os6VTCOCyvLb6gIGqXuLcysY5dhzCJTnyxkXkUiTXwpLfzGjkSIHy48OOm.jVKQFAQFKRz3PRZfAy9DjUwW68hvvrBvXzHUwdCUFm29IBjjYMjXMjjkMd9t.khvvPhJWlHU.kDBRa0drMS71oJ6euB1g8cTDDIVBMVL3HQIwhBkChLRBb4V5QXHRETpQMBJYHMENR0JjrUSXysfid5cDEzNjIxcgicGp.rD.kCJCjmdPY.fiDgfq1Kiu8EdKDkJgrTIFljQsvXBjNFzqMkmuD0qWGic+04x9sSVnUl2KicaOuPWluWYea2LHz2qfGTF.ZFlgGDwjkl+srHbGHb49jmS3SulCbVuITZMNu+AIxSmjCv4aqaJqz6uV4edEl94t839t8kyDlSH8A0MwH5dKUvg.OaHt7WmQ.Zg.sL.akXHJhTqgl86yZasIWay03ZqtFq1ZK5OLE.BjP0xkXw4Wfit7JbrkWgEqTkyr7RDlZPpsHEdqqHv48JP+981hf2JvW0bSr8+dkLvHb4yKNwiR7VwSgA9Trm3DV7YBVP0x0X00uAUTJpWoNxrDpIbz7RWjQuvySom7rHBjHbdlCcn7m2y0E2j4O1I1NPec9AQI6uLWNTAX4IiyhSmAgAfLfU61BYzBrloOmu0ZT5nmAWnhFkqgzIASJUJ2fLqFiv6EJhbqkTTbvZxM3ciYqbaJ585SMJvau+Ed0z3HzE9drTQyLF.yTtZuP4+6N892Opljx3cK272t+sCB17Sa1oN8smErvs0m5AiBCNM2LrFe6Vw+unCzmllQX4Jn0ZDBAY4qLTJE6f0poOFL4plld.dw6oBCdFlg29Xuxhf25.xuuapwMrVKj6mSFiY7quvC4bF+RIk6SXLEkM+1V5R98i4s7cYXdGjP.fBIJb49kkexOAEEZlDeq2IS.FUwjxx74wDERyxW3TVekHhcaeWZ5wGDBGRWpeLOWHVgBiL.mUfx4SaoYXela44Q2uOM60gRyuHDGy.sAWsFrkTwqciqyq8lmi23pWlqtQW1x4mCHtd.hR4Y3wZwVcmbrl...H.jDQAQ0aDtl2ff25FDqfxB3G44dNdzUNBe3S+HLm.ba1jEBCnjRRqlaPoJwDUoDoVCYYoDnh.Q.BiEkJ.iYREHsKiWe.CuMsQrdXCH6VtRX548x+o304KsAHzpHv4Pfu3FzBuH3G0Kk4pNOljQjLbDgAADLZ.mJPxMdg+ddjO9OJp5mFFjBxRLxjgSEhdzHZTpTt7.sfL+RLiDTfFO4NgrsQYua3PKx8.D3D9OFm.HNl9.Wcq0ISBBgivhl3rKmYFkMuSgK7kS6gci38vvZs9apc9fBrj634ECgLk.+KfaJSY81A6WUwcqUm4c3m8TONCyvL7fOFO1zzRB31TrvGpknHb.lcQGWxI9ssWItPrsvxs4ulhwdKX4p3uMIaI681tuoK6Ys2hUnxsPAv5jnvRiFMnypqQT4Hle4kns1QhvwfvHdqatJegW9U4MVaUt5FswEBK8XmlydxSfpZMzJnR059uLqCWllr9CYX2NLrUWL86wev23E3gqVh+tEeE9gelOH+fOyGfrVM45quJm7HGgN8Ziz5aTzAAAdBMLFLF69FX68iPN0i4mcA.kqvxFxsvBge9SkP5I.Bul1.HVIIHYD81XC3lWGN9I8Ucp0fKLz2WcEAjGCedTc1s+1yutv.Dd.WAeWoWD5aJyPpwRXbYVUOhKcoKOtbGKVMinnxMxC3xlGY3Cx3fX.ZLSb4CRTP2oI+jl0ZGGnErsnPKZIMGzPX6VvRueKsXGV2reFlgYX2w3E.RQfVhIX25A+6wJXs2Nd7aK3DiSczVsZyhKsDiLYr0nLDyu.sSy3u4EdQ9xu1qwa1KgpKsDO8y8rL2wNJUVXQBaTGY4xXBTiCvRJDHcVriRoe61zdysXPqsXtEWfzM2jm+pWk23JWkW4Mec9O+G7GfS+3mgKe0qRoPwXGfG7DhXrNuyvKsOvytgPHvjSFgTJvXLDDDfKSRmsZxZu444HevuCHPAN+qyfinBWcWhOQHEmeyoPS.6KyUE3voAKGdZTyChJwpQnBX8ls352bMhJ2.gTtcYjVP0Z9+VI2k1ex6ifCP6rnXmCHYxCQt3XFLQ.AuMOfsa5XX29aS+buWGSqSuION6et2eEr4LLCyvcG3YqpfEEYtFur3bBOiJBHtZE5osz2XQL27rdlgOyW3uluzEtNkqWim74dFpbjivhqbDBqWgfxUIpdcjwwjhkVc5M1alhjJBhqR8UJSsEVAmNgg8ZyEesWl34mCZ0huxku.q+Wzh+oeuee7bO5YHoUSTVChzDjVGt.K3b9ttRtfYuexKBuW.my2SbENCYZMQAReOxcPO130eKNRytvJKh1ZPhOvoXIfwWnDE5qSBHj9eSQgF81+4ON79fkygTHIihxhEt9ZaP2goTZwpfJ.qP.BKNo.gDPJFqwn2uCmyMVrk6Vk14iec6iUhodu2teG6EKVSGvE7fYkz89kUTOCyvCR3vdO685wvbEZk04+WEAX4aN0VDggLDGpFKwarwF7G8+2eCO+M2h4N5w3nO0Sgs9bT6nqP4kVFgLfAYYr15MoU2NzpWODpvwAXEHkDpBnToHpUoBkhhPVuAG6C9An65afqaaZuPM9Jeq2j09b+47y8I9D7jKuDQtHDFK1rTLBCJmEWt7cbBG2KSiz61hnuvpETJEnM99fox22GaDFQuKbM3p2.VoAYAdEcEfaLkUFA4cKSOj4YDTsM2G6KtqjhPCNxvhMHj1NGWY00QEDQXXDtfPzth1cf.oDb4S1IEB7mouarU7dSrWBJ2wTA5H1cwXePXmsUf8mwpc687f.lkhvYXFlg6EvwDra3r9lLuy40gk.1pWWV9QeTd0UWk+fO2mmWbqdbxm5o33O4yPWgjS9TOAI.q0qGqt1FrYyljp0DEUhv3HF0OYbAMUzjrkRIwgQDFGvIdniQi4lm3f.VylP5RyyQ+vOFab0qw+mel+C7u7m9GiGqwBrPTYTFGViFeGDxW.BtGv0ni0ZG2DmKN1YsVj3nQoJza0sv9lmG4G9QwFFP.1sM2VAnwGfkhsSGrW6cRT2FLDcnCvRlqaesyGM+F85wkWachpTCDpwkSqCCHU3xcSWg0Ap26OI2siFn12meWdcSx1RQUzHrS7k4biqtxCJPgwLiUDzzsgtsdPFyXxZFlg6dXR8VUL9xjElyCx2o4Scz1hLvWcaV70xl.sTRkkVgW7BWj+nuzWl2rUONyy9rnN5I3poFZbrU3psaylasEc5zgffPle4kPJCneugzueele9E8Z7BuNl0FCVqkTbjLJgu8q75nhk7nO5Y3nO4SyMCCYnRQDBZ06b74969l39.OKO2INIpfXu9mw.Bvh4tR+b89cHkRevoFiO0flLxxxnhrLk5Mflu44Xozuebwkv5RHTTcbTUF7AYUXaGEZuRTvXIruYh6PdzMueNgCcdfVsGkxFs6QPop3bhwYqz47hZ2I11NBDyxQ3XraUKnPHFaeC61OV69aQC2NemS+3tk5v2Ki8q2bMKPqYXFNbXuVvxcBK6uWFSy.TQkHZDfVJg503q9JuBuzpagXkUXTs4XT4pDcjifsVUN+0tNM62CafBqRPqtcXiM1.sIkEWXNxRGQ1ngjNZHZcJREdqoP3vhi33xDnh47W5J7FW3pnlaAjysDihKyod5mlu3ktAu90uAcMVzAAjJ7VLfQ32Fe+.DBu31sVqWKV41zSnyRkTMitwMgzQnbFDFM3rPltvkexcxrbaQh8OfpowgO7UmCmSPnJfV.u5EtHAUpRl1Qfpj2ug.P5P6zncVuFrDBvXwkG.wjowZROgZmeU2ZPFG7l2tGbx8xA.ldUc62OVqEiwLlYIkTlm5TGViAkTNlAJqwf0XvY8BUTNg2rXyqLy8x2qtkUZd.PJkG51Dfey9vc7+fd+E626UfnS5gMEGqm7091Yen3uMCyv62wsbOwtLNi2G5RQJkjkkQPPvgdwg2tnXbrhJZeu9Y58ooeb54jJ9aAAAim+KaTBQQQLLIAiPhqbI9ae0WmO+qbNBOxhL+i93nqVivkVlMFLh27RWAM9L4nBjXsFJUJlFMpgxYwjkPfzhIc.Jmk.bjNZHQARZLWMjHHzIPoAaJztcGVaiNnJWgZG4DLPDvYN4w3y8BeK9xuvyiNJhTg.QbHsGzi5y0vy.2TmC2s+8jiyN43mGz7JS9YcPG2e6foIaX5wkCCC8oDTJIHHXr+GVpTIDFGyIkjtw5z80echc9pGTOZHDGfSO821jyGZYm8PycGGZmbGmED9Fl3.qkM5MfTY.QpHzNe4gVHRLmv50e031cv688MoaG8Lseu9v7AaJtPwX7GsJtPtnOIIuUqlAmytmqBo3hWuEZrSMUcPay60q48pXuB5bFlgYXF1OruiWH.iwhNMCsxOYtVqwJAU4Rr4vg7W+O7MPG.UO9IwM2bLRDP5fALRmgS34FIIIgpyOO5zDV8FWixwkXtp0X3ftn0ZVbwEYTRFiRFQoJkYqMWiNc6wJKcDhBJ4YQSBirNrZKiDJTHg3xTdtEvs1F7hW3R7jOxCySc7ifMsOwUpRyNcIZeLaf6VyAreAe8NgH2YW1O7Z.GJI.W+tjt4FT2X.qFkveLYaq2vKEJYtO8usw1dvwub3E4tyOyeJPqQobilMwphQDFg0.Rku7Uc4dfkM+fsxH86.uKmB3C6EQNmaO6eVPdPjSl1ua46zhv48FrhJFTHDnjJeI.WrJA1EAwKDLcyBZ5Th4JbhXqabUOTrM3D6OU9OHDjkeeXVPUyvLLC243fBvB61LnTJJjgIYnURrgA7Jm+B7Mt4VT+gNAAKsLIgQnERxFN.i1aRowgJrNKcZ0DkzKT91M2DgIkEledzYJ50qCBgfPkf1atAs6zFgJDWVJNi.oJj.AD3DXFoI03HzERX4Zn0VBVbId0UWkW7BmmG8zmjQCsTsTMFzpEQkpMd24dQ.OGDyVuaFfkP3HHDz85PuqbUVJUCgY3DA9BAvrslqxsCKe0Cx1oJb+ZSNvci.rxO.lArY2NrVmNHmy2bmMH7FaFVuWYIUiMoKfcvt06UwzAWMcPISZ1pEO+joCsnxPr4lg13ex+Xb4N8NBwNpKTIf04vlmx0Chcl87B5aC1r1Ob+NiP6lt1teeadFlgY39CreiE5GVVRonHBx68co5Lj0pRmzL96dkWACv7O7YnkTQujThq2.yvgnLd1qRyLTtTIFNbHe3m8CyO7O3ODW3sdS9BetOOqt9M4DG+TLnYSpVsJCFMDkRvO+O2OKO4YeZ9ZekuNeiuzWi3nxHhBP3bXsNzVGRkjf3xzocKprv7Lb0U4kt7k4it4Vr.VpEGRovR94DXmKb+t43iuaqOOWtzaldNfhGChUXZ2ktW5JPugv7UPnvGmhZ6pGTRdvUEZryan6nNffrN77GkuwZ.VaqlLLyhHrDDD5sad7tPqmtMe+A5AAlQJvto0oIwdke6hzBJQrc6ZH2gcsZC1LMlzrsSMXQj3E+X2Nx7I+bMFuWejkkQZZ5s77Smi88RORuSdSv8Rra2XMCyvLb+ANrZz7cysOrNbZCQAgd8kYb3PfpbUtdq17suwFz3TGG0BKQ6LMCrZjRvllfJaDkEPnPRnHjAcFfN0v24242I+K9e7Wi+Y+T+jzrUG1Xq04Tm9jjjkxku1U4G3G3Gfeses+m3i8c+8hIMiTSFFaJBrDJ.o28EHSaISHXfCFFFPkirHmeyN75W5xHCCwncDGFMtifLsrVte3X+8ZXEVHRhTYI4la.qtNXAmz6sYNeaGj.lH3p7dSnyWKlGnJrtKnhY+IjLfU2bKDAAfx2SlDpsajy.i6CTfyaaCGP50duD1qH0KzRUQfO61ONmarH7JEFQ4nXhiioT9OwwwdeOQEL9mHU.QQQ65OgggDDDL1+OJ1VlVrl6kvsueZ.tCKlwf0LLCyv8JXzZ+hcs9E0JBivJk7lW6ZzAHbtEniVSpPhLJFsViIYHJcJkbVJoBwpsX0V9pe0uN+a927+Ma0rM+L+r+b788C9OhKe8aP2gi3xW+Z7vm4Q4e9m9WAPx+5+0+q4y8W+EHpTHhHIRo.kx6yjXcnsFFZz3pThtFMTuNMAt30uNpvxHL.5cFdv8Bc3taeFuSNF7tUE4iIW.GVki3RAna0At70gbarv3ajeHbPfMOSUS7iA7Yl6.vcEiFMyZPqTrQysHrbEbREFiujHKjAlPf2byPfSnNvbW9NEtanAq86wcyXO2QPM3O4IkBOaUBF2qFcFKoiR7ud81U+VgIsZkBzRPn7NUqRo1Qk+sCiJcp80oetI+a2MNtb+H1OlFmgYXFlgowAMNgRHvp0dS6z4PEGxlCGxq8VWjHkjv4lmMGlPpPPnTPRRBQRAkADooj4rXUgbzidb52uK+4+E+UbpG4g4S+o+myO6O+u.u94OOe8u42jkWbI9u8+9+EbpScJ9L+6+i3u7u5ygPJQEGfUXH0k4obImyDiy2S8jwwzyXnrTRnBt4FaxvAIzvHPZD3jj6l6db2NEgSNO3cxw06VPjKslcaNXKNRcYDUNBwfQz5xWk4cFLt7JRDHrf4ph2d9u6vGj08VQt67JAS6jncPm9CHNtNFmCsyPjXxCtSuob+gAmMYySdLDSEYO6TlzBmbLyaNawu3OgASznlcNBkQfL2XUy+dD41SmzBQRuP0cYYd1tR04deRBYYYjklh0p8oLLmsKgzy3kSHvnTHCCHLLj33XBhBIPEgLPgSJnbkZXbT7M5SUqPhwkWMmEhk2IFKZ9suVxbqW.4j6Pbe2uGnRQN3KvgZ6U7NSokOCyv6UfMWZnaKkAxKpoI9g7mO+0Hb92mxcvSP8tMNnwKBhTXLFu4SJDHCBoU2tbw01fRKu.UluAWsSOLx.BxzXMFpEWBoUyvACIZtUX8VcHNNjkOxJr9lqye1e1eFeueueu78+O5GfeheheB9M+M+M4+jejeT9I9m9iwW+u6avu++t+.pUqFQQgHJpjQ6HrBIBY.H7tOo.vpAsQPpTR44mis51k06zjiTuNJkfLqYGZ681AtbcTW73A95eWLHKe.VJO4Nt73Pb94+HWCyUTQTt+HxVqIXrjY0TRAF6DwEHY6KVEEUV3Au+enBvxJD3jAjYgKe8UQmYIwMjnpMHtjhAi5S4xkwhCiyhyl6EI3uqThBCam5pIoyqfkm6FdwzdAm.rN+muT.XM4cCcGt7ISEJe.EXy6J4VPXsHLNDN+JXLNGNgDgThQHw5L9+lTf1XHPBJbDJjDnbnbBbYoPVJ1d8wljPxvALraeR5O.8ngHxxPXcjlLDbVF0um2bzhT3j9iMpf.DwwHChvEE4C1JHjfRkHpRUhJWggkJSbi4ItRczAfQDhUEhw4HMUSnH.iNCEBjgJD9CDHvhRJGGt01lqlEq.j4AHW3dy64w38Pbgi+zN.lx1+aBKtVx+660mjRovla2ESVLA9sMINq.u0hI7elB43U8LlwvI7RG+1UQB4u+N.yYXFNrnPlCS+ukRkuOpJ.LNjFGn8Lu6G+CDJAZmknfPBRRI.Gp.EYZKpvPDZ69eGztnQnI8zpCxOsNP+1ZOj1wt862xqSXwEKIajgXqDkJfLfdiFRp.xTPmjgjkkPkxgDXrDXAmPPaSJxxgDHLDUJ.oRR2tc3TOzI3hu044e2u++ObxS9uj+K+Y9uhW4kdU9k9k9kvZs7+6+9+.dku82lyd1yRutcIPFgRDPVfCiTChLDNAwlPBrRTxHRDkoe+NTtbCZuUaN+MuBO2weVFt0lDGVAmc6rYLlvg7iKEjF3mxVTbJYGcfj8KaTJwDyeuGutIGmcxwlucXSaGRPxlK8HJFU1gwvXxM.oWC3VGNT9sM2HDZGKaTnWaKneOBmqFxjgDDT0+oHActkZDjGWP.aWcg6GNzoHz.j4fNc6wnQiP1nDJgCsNEWgysKbaOC83vdk2mzrmKNDYQJDHy29rBQtqyaxKISIBWwjxdgoKAuumfmlUqSfUBBoXbKpwZ0XMVBxEztx4vNZDCZ0jQcZSRys7TEOXDoCFvvt8neylzcyMYTutzsSKBDBxRGQpNAvh1Yw5LnBBILpDQkqPsZ0nZsZTodCp1nAUZr.gUpRX8FXKUFQ4ZHqUmRyMOQy0.UPIhvG3WfLuWMYc4ziZwJrXst79fkEgPkepS36USEU53ai.jdmL8i2hvTmPGZ29aGS95lzf4rv93iLyvL7fLr4LTYwssPfm54E9YAP3b4SzMQg9LE6x2N3Nk0i6krjXE4EIjb6h3RHTXb94DkkBwhAmy.Ve.nBmCqzQV9XnIC6QPPjmMorTTJEyO+77U9JeEV72YQ9o9o9o3+s+U+uPPP.+t+t+t7RuzKwxqrHa0bCVY9kIqWJVo+XoWSPZTNIRKHMQHsBv4agwoV+bUZEjYSPga77Y2tGurS8n59cJHm.dBrjEBUBvfzAJKDkXYXmAP+tnbGwSff0BRIFga79aPNIXEUW38daZ.+A6VcZSRVJgggnT4zlpB11XtdODDBePDVTncdaRvNQ5zbNI17C5Z0DwNJy8VCgfHD3LVJGGiTavlLhjVsX8MWi9a0jzd8fzQzZ8Uo6VqyZW6FzdsUgt8fACgjLHKELY.9SznjPX9oVcJFqOB8QRAcjRHHDhhf50Idt4IpbMdrOvyfpZcpt3RTYwkwMnC1tyQ04lmxy0fQFHHtDRojDcFVmYr330ZMBw1MHZkPhPTzIwKDJ38EQIuuX5.rdPQ.+yvLb+HFyBA6RU3wN+22tidrWts9c56aZbXFGP5fLs1y9uCeWJA+3lF11Ew04tvtU.3b9fNs.BKUKWkNc5QbbLRojQiFQiFM3Mey2jO+m+yyG8i9Q4e7O5OJO+K9h7Y9LeFN24NGenOzGhUWcUbRuNqLNeVWv3ivU3DncjK2CGVIXUBRRziSUqSucm.Y2Vj7casXc+JbNGRg+ZwAc6QmM1hFOwS.J0ARdvsCNjoHzGEmRA8FLBveQkTJQac4hbemn3TVQ93e2G6LlbeapA7IAy5SMnf7KU8ZUxgCUdpKyX6fMv5HDAg3vkYPjkgLIgdarFcVacL86go+.5r4Zr5UtBMW+lr0kuHLnOzqKn0PP.xZMXgUVjxkJQ8RkvXyHJJhRkhHHH.YfhLqlQiFgSaQmYY3vgzqWO5zuGYsZRxFaPBvKb92.pOOy8PmlG5wdLl6nGmx0ZvfFMHpdchN5Qo77KhpbITNMRgf.k.oSQlyfzI2148cBjBe22RRgFKbic7VX+yy9tI196z7+emhcqhU1AUz2S+1mgY3AaTv.xdUsXtI98B4graREX++7uyet6jWyaeHAMHyo0vZcXrFLlLr.AXwZ0dsr5Ld4vHv6x54uemSPbbLCGNjJUpPiFMnYylDFFxS9jOIO8S+zrUqVTqVM9ve3OLm6bmi986yBKr.as0VTJnrWWrVgeNImmkLvqEXsPSpvfCCo5DJCDJU3xznrSvv3tLdnyceyjz2yf0ZITF3IXXz.5tYSZfzGfhw.JuXX1c+57fkuzglAKG9p8bvfAHTAXERLFu1VTxvcjSz6GmLabNmsEBwbh7uOggeh.rNguJ+bBb3Sg3HqAkTgxYQ5bHrZjVfQiv1qKabyUwNnOllaQyUuI23hmmqc9yyn0VCF0Cv.0KyxO7IYkEVj4pVgRAQDpTDlq8GrFhhhHIKklMahS6Xg4ZPT8ZjMJi.oJumX4XznQzseOZ2sOCRS4FqsNI23Jz9JWj1uzKP4ScFN9CeZN0C8vT8DGmjz9XxFxbKt.pfPbxHTl.vIQY8UmHNFa9oRqM+DowGXkxWIJSVUh2OsxmcquWMK.qYXFt6g8K3pcXdxSDf0cRo5uerWcXYn5vLVkvAAHPY8SBasYi60cA.YCF4GuzX8VBf.jh7BMBIRmjs1ZKN1wNlewxNGUpTgm+4eddtm643S8o9TTsZU9re1OKe7O9GmekekeEdi23M3EewWjm4YdFhhiw4T3bFjjy3R9BfwIwIELLSSBZjBCZSFgRnbTLtLCJq.M4sut6iFy9cR3bFeVmBk3zFF1ryD81ucdsi+3ZwyIusB97PGfUFP+gFZ2s23TC5LFuvhsVlTiaEAqHcjWQV2+oekBQ6IbfP5Ewtdrtw75rxgu5Cb3EboRIHzpPnSQlZvkLBcy1jr0VL3lWitqtJqdwyy0N24n6UtHLrOgKrHG+LODyuXCpTNlZUpRjJ.WVJ5rTHwPFfU6SYWf0Rq0WmW9U91XEvSd1mhSd7SPV2d3jRbAdezphTR0F04HyUGqSvS+HmgMa2gqs5pby02fguwqw4u1kn8ot.ychSxBOySS89cPL7TTowbDVtNBm.UXDkTg3bRLEUAB1bgf6qIQeAYryUvd6Pk+NX05dv4vo+t1OVrlgYXFd6io0vyzXutO6twBbteXwbABIBStFUcNzZMkiKQ0PXiA8oTRJlrLLFCBmEI9p+l7hqpRkJr95qS0pUob4x7pu5qR4xk4S+o+z787c8cw+1e2eW9s9s9sHIIg+q+TeJ9E+E+E423232fKbgKviblGigCz3Phz4GadRFBMRHUmRpNgHmFqNiRgP8xkPZxk7h0Mtnol7b0XYT79jwHCBBPXMj1pSt1qrfb63SjSIFFu1C8EG29IApCU.VE0QUqdcoUmNfR4Y4QJG23KiC8ajN2tPyl3c+pvRHlxnMsBe+6ygWj2AJj48DPS9EwEos14rDEJHDPZLX5OhztcHcqsn60tNct90o6MtFu029aQ+23UAgikO4w4zO6YYo4lm3RJLVKVmFynALLSiSaHVEPoRknTTLQ4obsZsZDJfMVaEbRAOzwNAm3XGG8BKQVRJiFNjQiFgNMY6JmQJPfjGZ444LG6HrUmtb4qeCt9MuIa9puJa9ReKJu45z3gNI8dzmfUN0oowRGkRysHQ0Wj3p0XjUmOn.fXaF+rE2HJt0A5teXfuIwLMXMCyv6bX2pF7cyDiuStGb27zuam2+8xwgDNHPFRRxHBC8oYxZ0TuZYVpdIVcvHbII9pE23sgGs.TEl8rURkZkoUqVTudc52uOat4l7I+jeR9w9w9w3MN243K7E9Bb4KeY9C+C+C4i9Q+n7I9DeBd8W+0429292lUWcUpTaA.PZr9tBBJbBKVo.i0RlIkTSJQ1LD1TJGpndTYuNZsh7EH+9WHj.BeEwhwRV6dvfTnbDDFsKL.bm4pA2URQ3vQoze3HndMbRAQggDEVhjjDfvbp1baSw18Oy8NFSdSuz5qJCegdTTu.L1vwb4soFk0fx4H.AtgCIq4VLZ00n6MuIqet2hMtzE35esuBD.KcxiyYN0wYw4qCFMYCZSROs22pBBnTTDkqVkRgkHPp7ZACnToRjkZHJHl4laAN1wNANgfJkpxngo3LFjx.pUsA0qWGq0RVVBII49nUVFC5zljTMh.Em8Lmlm5QeDtwMWiKbiqylO+2fgW8hjtUaLs6R5I6S8UFRii4PhjfvPzROcyEh32p1twQWrJm8JnpCxlFtWGny3JVpXPsaY.96CuXbFlg2ig8iA6cbO3aSQtWfcy3lOnW+sy18aWHQ.FKhHAJgDIFpUpLKTpBAMGQPZFxjTr5LLFMBYfup+r9x9eiM1fG5gdH50qGW7hWjyd1yxm7S9IoUqV7G+G+GyK9huHOwS7DbkqbE98+8+84W+W+WmO0m5SwW8q9U44ewuMOT04y2OxYjJu5DcBuA6XsZPmPfNiHsipQRpFF3qVdquT3e+5hMsBPImHCMFCh94EXVr2Dr7kxlbruWcmF+xgN.KAPmg8IyYICnV4pzseeBkk76DV61dnwDUVx3cRqEmXmdLzjSVe2HG560mgOEejmtuBWRW48OUmm0JcVFD3yusvXwYsDHkDFDPLAT1pYXyMYsqbExZ0EQ+dbsW9k409peE3ZWkv4mim5QOMm5nKiNsOC61hnn.pV0e7oQi4.766ABIgpnws4FozuMTet5ToREZ2qOQgknT0JDGWlkVYEFNHIefphUEoIvDAbBa0M...B.IQTPTQboJiaQOc62CwfA3bNF1e.ZqgEpWiUN5ywZO5iv27keIZ9k9Rz7BWhm4696G4SYQZgQ8GPz7KP8irLFojTmAYTDsGzAsNiUVYERRz2xw5oGDb2Fzc+B15Nw9Djhhz2lacDTXQFE++o7TEgXGsnncK8l9qY85OKK2.XOnqklgY3AQTb2Yg2WM48JdugaBAR61t0fM98K11C4bNGwkhoeZBwQMvZ75V8fDJvtwV0a2T7O8myAc+79s.w7hBjvvn78aCRgkRREevG8w3Ke8sHYysnwbywl85Qbk5nTJRRRXgJ0na6VDkaPzuwa7FDDDvu5u5uJO8S8T7G+m7mve5e5eJRojEWbQ1XiM3K8k9R7Y+reV9o+o+o4W9W9Wl+m+W8+JW6ZWgG8QebhbN5zbKhKEQXPLYlQnBkjzqMxjATwAAovwVoAGa9EYiW+03QN5wv.Hjpcc9VgPrCeJz411tBJfjacb7amiqSiaI0jGhOmcLd9A79UJE5zLTpPDINRa1E5N.leQLNKR7ccF4NxOnOyc2NaYGtpHL+mzLs2CnvabjVjiSmFVmukSOEDuCRdvcxpdrNGEmWJRG11SFaHRJIDAtrDriFQulsvMnKAs6vZW3s3M9GdA17keIvZ4g+.OEOzJKSk.InSAcFwAJBi7VYgPIQJCHLnneCF5o3chKxLFKc6Mft8Fv5a0j9iRXPZFZCDTpBIIInBC7F4WPfWriVEBkAgSiIQSk50HtbUxxxX3vgze3HRSSI0nogRwOzG9ivUWaCdqKeUdk+p+RV6lqxy9w993nO5SfV3nqyv7m33HiBnY+NTuQMBKGyMuwpTuZCenLSkV.HuDXyGXcuFL7fzn0AAeoeCriUFimop8X4FSK388CSqKg7uoYreMCuuBSqkw6WRy9gcA3GjQVaLlweNSu.vhLZXLZrBuTXjNGAlLlOHfyHgU61EUTLDDxftcnwRkHLLjzzTpVtBhHIu4a9lTtbY94+4+44G+G+Gmyc9yye9e9eNW+5WmSdxShVqoVsZb0qdU9898983i7Q9H7e1+j+IztUW9e++i+un4lqSYUHQQQnMVRMCQKLXRRHqWWZHjTIIkZ.emO9SfMY.KrvBjYLdK.3N332zGME6wXf2OIQj8F9tNH3mqW4rnRRggi.mAqHZG8J42N1M0glAKMvvzDzNOST9.qx84icnso7fVvB4BBbuN4bmf2t2j6SWjWCeiajiEOG3yiMNeKmAGNsEANJKED3fjQivztECt5kf9cY0KdQ9Ve0uF8e0WCJWhm7wNCGcg4Qkkvf9iP3LTtbLUpWiRUJSXoXe.Q1.BBhPJkjpsLZzHFVnmJslNs6xfACXznQjpynb4xjZzb4qdCtwpqCAADGGmKRxXJWJhRkhoTbHgQwDphPY0XRMHCBILtDkpYoe+9LreWBzNhhT7HG8nDIU7pW3xrwy+2yKjlvYS5yIexyRudsQEBMN1QoTfjrrLbJAQQ9UtI2ifql9386joFbZ718F96j.CmgY3AYL4hn1KWTe2VPx8RbmDf0gY6YWeuNvZ88HVg02ByBrNrYZVItDe3G9g3u7BWAU+9TpdcZ2pEkqTi4meQRa2CiPhKy+9Wd4kQJk7E+heQ9xe4uLO+y+7TpTIhiioYylL+7yyxKuLu9q+5767676vG+i+wYvfAr37MXsatJF.YoHRS0HB7opr0FagoSOZDGAcFwSTOlG+HGASu9DGGRVVZtDXt8N9A6d.V6GCi2uONo056ZIfyaf18SfNC.JhCvq2cXbct8NaJBKzfkCwNXqvkqMIe0Rj21YxCnw8tnCXeKWHvDqFyssmN4jh7pjSfDetpkZGRaFhLM1NsYzFqireOdi+guAuxK7hLZ0Uo7xywG7IeJVdtZzb0UQpDDGpHLrDwUhIJtDApHuE+6BPFFwnAoztca1ZqsnYylztcaFLXHooozqWOroo.Pb0p7Dm8oPoTbkqdItxa8VPbLh7frJUJhZ0pvhKLOKszBTudcZznAkJUhnxgPZJFsiRA9yQgBHoaeZs4FTpRUN6C+vL+7yyy+5uAa8FuB+sc1hma3.Nxi+Xza8HxrYTckiPugCne+ArxQOB85z2ebbWBxZ2N1e6Tx02qBDqHEgGjMMLoVs1sAJJVXfMmB4YXFdPEEcYjoCtBJF2DXWRSz8SSrtaaK2txQnXNsc884JJ1GqeFMgCm0hIKkkhi3CdxSwW6BWgMZ0iJUpwVoCnayMXg4mGovwvACfHeJ.EBA+I+I+I7Y9LeFFNbHAAAbricLFNbHJkhzzTVXgEHJJh+h+h+B97e9OOyO+7HbRpWuNAH7xY.nTTDcFLf1asEMrVB62mJ.eOO1SR4LMAVCoBCggpb21+sO1MFM2w3q2GccvtgBc.5bNBDRuIe2set9qN73tRUDNHYjOROU.fzqHe19hSoyei5zRZzKP5CyVvaOryaXvWppNg2OPXaCT0AX0IDHkDfAzIXyz3FNhQarI8uwMn6EeS9VesuD5adSN5i8nb5icLBHilatFFaJQkqSkJkHJJBkRQPjOcfYVC85MfVMuIas0VbyadS1XiMHYv.uAmUff.BpVFkRQ850Yo4mmvvP1X0UYyVsvYEXLVF0oKiZlQKoiqGEQ0Zkob4xbzitBm3DmfidziSPP.FiuoQKkRpTqJJGDEEvnzDZ0bSpDGxG4odLdqabCV6JWfu4eYW9nxOAOToRzMOPuZqbLhkJxFjtmVfvzGqmdUsuaSg73s6CPKG6291LLCue.SNY4jK5vtKoI+Vpl32A111Obmp4pc6ye2BTPH7xeQfuRyQpAbnbVP6ntSxYleAd1kVf+ia1jzVsnT8ZzqUS1X0aRinpTNJDhBXXZ53BRxXLi0faVVFiFMhkWdY50qGqu95L+7yS850YiM1.rNhCBYt4VfrjTR0onpTldICYs0tItjATSJQz2xyFGxG8LmgFVMJqWr8hPEN8tueWfoYo7V3rTcuqWA+NC7rW4vPfP.CSgdCgo5ssiYuZLj2VzDcWoU4LbTJ9.q1ICVB4jQ2JmHsbBbNCh6Ne821XWEOms32yYknH3JAXDVeOjRk242sFbiFRZq1z55WiMu3awe2m8+.zuMK+vmfSehihvZn4VaRkvXVdoEvloQJkDGG6SEnJjzDM27l2jqb8qwUtxkY3vgXGk6D9Uqx7yOOMZzf3RgTu1bTopOu8YYYrz7KP4xwDD9gPqsXzPZpl986S2tsoSm1zqeG51pMc2XcV6JWlKL+awwO9I4jm7jb7icRZjWsgiRFPoxkQVqLt1so2lahTKYgEWhm5gOEkJGwke0WmuwW7yi1Y4LO6GgjlsoT45TawUnU+ADTNdaV+tME19ju1CpxCucNmtuZvZpO9IWc0jLXMcfhSNYxjU8zds+MCyvCpXuXofotu3986I1swUNHMXsqAVU76R+7XxfbuRzZQAnL9hDZ9vR7wd5ODu7W9ugM6OBUsJjkkw0u1UH7Hmjirxwnaxv7Fmsj4laNhhhvXLLZzHFMZDgggLJetA.FNbH0qWmZ0pQxvQXRRwY091ZVfBjBVey0XysVmiWJlftCnFvG6rOCKqBoQfhVs6hpQDC0oDQoCUPv604+62Ytp.9sY+9fRpPjYfgI9dP33ZH7sONzQ3XAFlLBCNjBEZqMuLTsdO1Xem.8vt4emKxwIufnXxXeejpXZZ.o.izhyAh7a.05Troifdco0pWmK9ZuJm+Eed3lWkSb1mfSchiQ5ngnGMjF0Jwb0aP0JkITF56OixPRRxXqMWiqd0qyku7Uoa6sf.AkJGQ8ieTVbwEYwEWjEVXAp1nNwwgTItBVqlvvPFNbHBGDEGvIOwwnbTYvEfNUSRVJCGNf986SqVawFM2fd86vlatI8Z0h2rYK1XsUo+i2iScpSQkJU..YbHIICQFExBKu.ZcJiFzl.khG+TGGiyv0N+k4E9a9aHpRcN5ieV5HWGmMfJMpSpaRQe6wjc69oCp8c5AgOnUQe6rB3caPjhaLmgY3AcLY00NcEBVbOv6VAWc6N9+aWlr1qWmP3agZFq0215zVvpQIDHEBjVKZqkSezixG6YeVt4a9ZbsgCPEEyvACocyVDGFQBP4F0v4bi0caQPegg9BWpWudDEEwbyMGIIIzpUKem6vXoTT.oiF587JGzdi1zrSaeka5fXiiuqSeJdxidBD8GhJVRrRgCXjNivvR66vXGjFrtasP42sfP3.gEmyPnLz6794Ypwj2VnOLQfdWI.KsV6EKlyf04oayYr3TNDhI60eBJ1Z8li967zKNdHg7IdUdR07owTPd+0ahWuQiK0fqWezMaQ1laRqqbUtzK+xj7ReKN0y7LbzEaPZudzqWOVXt4Yo4mCkPhVqoZspjkYn+vtr0lM4hW7hbkqbMbFGUpUmibrkYokWfie7iyhKtHQQQ.99HEXILLjlM6LNEiXcXzdGCNkTj4AIFEER4xKxxKuHG+3Gk9C5RuACn4lax0u4M4FW6Zzbis3ue8uNmakk4rOwSxoejyP0ZMvJr3aW.PxPKCSFR1vgvnQ7gdhmjAixn4acN9G9O9E4Gt9BTNtBC2XMpWulu+aIx818w8sQ.gar322Iwx4jp57lW5jk+5toKh6tCbuSBt8qB22+IktsGmwj2atF2DucSFXtuSLZEFBDpYwXMCumE9qu8ZjUZki+a63tUQgPP.svPlzfQZQJbfMu+BOw8.RDdwBy1Kt4sS0WcaC61e3SO1Mj2aYyeMEaGaWfUNbFqeBV188eadCbVRNK2NPHj4ZOySjfuchIxsbn.jRvXMnzYTQH3G5C7LbtMtIac80HbNAkDJR62lKekdrzCeRLxHxxLjQJRYIxrYXxLTtbYbNGkKWljjD5zuGQQAXEVFpGQsJUIMwPVZFRY.c6zjqe0KSjxwQhBIZytbhPEe7uiuKVJyPTxHFlLfFKViNVGQAAHcZj9trKtwBdO2XoykMyjXGINSXG2fqE4ig5ONmmIK2tPvwzYTvUbE3d+7S+dm7bj515ZKadgrk28UJ5DI.BQXdl07Vrg.M3x7yK38H+74pr9XYFeOSgEUr+a.G9bz4fjgiPXM3ufMCgThTnwncDDF4cEcm.mSgCAA4a5VmCoTk25.1o.4F+we.hf1rWqvXpGG+Y67T6JxcqcRRIP.HEXEfUn7SvZ.kyQinRjtwljsUKxt4Zjs0l7Z+seMF7BeCN5S93bxkVhQcZSVVFyWoAyUtNAxXeKTPHPqszocOtzktDW7hWhAc5PoZ03jm7gXokVfG4QNCAAADFFhPHHMQOlxXoB5kNfvvXbNPqsiolWHiHy3v4x76bVevfEHpTEVnbEVYkixxG63bricBt9MVkqc0qRyM1hWR+ZzdPON8oOMyOeCpToBCGNjDYBUpT0G.Whl9atIO2i8DbtRM3Juz2luZXH+m9ew+LpXNBctljRm5TLzIPDDhJJDsw38MLk.sN0mh97fuyaQz3jJDiMS0By9faIEyG74+hn47CRN4mUwssJU.Zmw2qobdQnpB7aa3Lf1gzZ.rXrYjgABDXEBr3HTpHHJjMa0jm6zePVb4kx6wZAnsVBj2im.YFlg6Qvhu6ZHQ5Mdx7IKL4+l.PYbnSFwIN0wYjMESZOpoaPTRFkiqfd26Bt.E81U+8vVq2CActsC9xZcG3RruUFyl3QmuwEKrNzBGZ7K1yIEiYQ2psnTRTNOiNaa1odSSVYMHUPpvQpQiIPhLJXLwAgggXzZTVAgp.e1Nz9soPUHRgm8JoLBUj.qIiDmlfXGkrZTs5vYJcb9EdtuCpk82xWY8lzWAo0pvMSGRmaLfxIywBMVfxUphQmhw5HLHvqwKg.ctlYkgBjQRhJEQZVFcR6C1HFLZDs2XSx50g4sVVz4XkjArLv+C+i+Dr7vDpXgXY.pn.LYNhjJBCTHLIHbY99HLVrnvIU48ZWIYVGJkBUdVJrlLbNKgJEBEjJsjY0HrPfz2+ZwZHf.hBBPfmngw1cQfOHNsUiUanZfO9.mv2ZerSEg0j1jfEOIHl7e.7soH2Nulv2E2xseAq0GPky4kkj0AtbKMvZwR.fi3PACSSIMaDN6HDZCkCKgvoxkKTd.W4DFIc9BD.29Gj0guJBy2YFSUH1cDkqOZwBlq7oDzNdaJeUCuCj5nhSTErUU7XfTgyYyCramGpDVK5dCQkkwnVcHLMkW9e3axZu3ySkScBN9xKPZu9f1P4vHpWtBgRElzLTgknR0pbiabCtzktDW5RWFsVyRG8nbxSdRNwINAKt3hXxCJZ5TQ4ip1usgaxJ2YxMPw3UXcqOoGoFCkhqvIN4CwBKtLKu7xb4KeYVe804U9VuLiFLjSe5SwQN1Qw4rnjgnhUDHUfU3YKavPN57ySuG6Qn4a8l7BesuD+ve7OACZtAh4lCU0ZnBBHy4P6rdlcjBzN+JGDBKBWdShdRc4QwJh1cyIcuttX6W2smN91YZB242QgGmM42suGSs8f4CFL.m1vxKcDNyYNC.jjlPonRre2bMC++yduYOKWWWm44u8delxLuyiXf.ffCfjhjRpbHIpYQKohVUKUJp1kCO8PqN7asiNb8Ta+r+KvcDc31OX6Rk6NT4pBa4RNjJoxtjkjkknE0.mjnDIA.A.w7c9limg8P+v9bN27lHuW.J.PQ10cEwIx7l2b3LtOe6u025acP7V8vUVj59+XmaVTAvBqu25M+7yhP464ohHIpLAtBMBmmQ2p92Z00RRXWre7y6jP12q+Gg8LmveCXKtxJ7UfynwXcHs9FybfPQfRgSIPg2mAkgJBEfQ4vXcTTTf1ZwYLDFKIvUZawU86OgmGD+3JR+3tBvhuA.KkBrFGRzbnIaQ+sayQEB9W8vOJgxWluw0Wida2mirbCN8JCnW6AzuwlL+LyyryrDshaBZIEYZRRZfPEfHDDJe8JVTjiYPFEYEzo8Fr90VilNMmXpIQt9ZHzVdjjD90+jebVVaXFikXgm4MOnZOycUsXWmztalirkUVmvOASm0mkDUfjjfDvYoHOkr9YnC7eVkTQrJffRfZNse+nUaAQIf2.IBkDjBhPAVCj6O.Vqi2aHGjUPvuQKAo5U2OglKwV54lkDpTd9srjMqpLRHwgzoAgEAlRFr7mW6pN2Ba4sub0m2cyPsbmAf0XD.7XMnww..vWIg6uyeO5q8FEP1vkh4nqGBkDmk5irxJFhrFjVCC5zFY6Nj2sKW4zuJ+zu+2GFLf68dtGRTArY+0oUbDIIIDDD3qTvf.zEZOHle5Ok0WecbNG268dube228UWts9lisFmyUive3bZOJfq8SOY609YeynVgRoHIIgFM7UWXTTDW8pWkW60dMFLnGYE4bnCsDMZz.sViyXIJJh9Eof0QqFwbp6693Yt904Be2uKm4dOIK8vOBc1bCRBiHLJAgy2lg7YUvwsRatxutN973emnLe8z1uyuwve2Bg.gBbRuG1TuOC7LbBnjB1dq1XzZN7gNDKN+Bns9YjIpS68AwAwa+BIkrTA0oQSZ8EFVUxvMFGAp.NwwtWVblE3Zc1DrBuagOHGmJZWis9FMF95+edBgnJ0dV+j2jlRYJ3+NChTDXLnLN703t0C5JSy.ikBrnrQHC7LRDHDjnBILzqkp7zr5lnqjphfxhQZPJBvYGlPAKNoBqyfRJ7oJqv.ZKIQwbx4mmO5i8tHn0Y3Gd9Kv4u9.dnlIjZCnnKn61lUuZAAwIDF2DYbL4M7ciCsVitHCcdJEYYX04XLYLnnOGoUChGnI+5qvw.9nG9d3i8fmh6uwDjjlRhv46XIAFrRGFo.izgEAJQXcw9HPVKaGoyCBwaJpdioVazTHgv.YcVWhihQmmiovfz3PHzf16ClNoffn.LkS71n81DDRekWFHjDH8oVzHp.w3SOYMv4R7UCCnQMjzsuSIxnQuW5cp3NJ.qwAxZm2W8bhFAHv9KNt8BH0sqHEgR5FsBe5JEUHa8yTQXsH0Zx50A21aSmM1fm4e7aCasEm3webBrZZuw1HQ38YpRS2ToTDFFxpqbMd0W8UY0qcMlblY3DmvCtZlYlAiwPVlueAFFFTSg9vquUfKrV6XEp43Ri5Mv.1PulwXpW+Vd4kIIIgkWdY9wu3Kx0t1JjUnQoTbzidT+2uUPnTgRHHLLhdCRIY5o3dejGly+idV9tequAO0xGhnVShr4DdcHnBIHLDrNrNSYJ+JScLdvqtQVmYjAXuUr7gcseXOO5tymwUxR1nBd24QPgnVT9UkYcYt1sNhBSnS6soYbBOzCb+X04DDpPlDgi6Llk6AwAwunBECoikxKEkku9v.mtmibDdWO1iyo+u7WyfEODyM0bjmZPHFeaV4NUruW+6.aUWBQIQH.ov4yYhyAVGYoCP6rDgDqDuzMBUDDGgPovgDCdlrL5BDVKlbCZbnrPrZXevx3ITQTxPWYBV8qAVpZGfBgxy1ANrFCIgwXyMHx5w8Oyjr3648xgmZB9Gd9WhK0OkHT3HjbghTYFYYETnRwEFx5ViWeSHPZMHMFTFCw3HpTqWQazkV.2mD9PG+d4Cbu2OGsYSB61gFJIRkEQf.WPoLXT6vGjVFTJ6DoW6bNP3JseBGTzsCQggznUK.qmcOSNEZMJojhdEXx09LWDFPPT.xP+2iSJ.onLqVfhHTBKVQkuX5vJbXEx5p2u5+gvmRWCNe5qqRIXIjg.a04D2dme4yTzX9RtCct7cEFr18+e2fn7LwryiuYEiC3ANP6D3bkSRwI8mb4.gViHKCSu9352kW4EdNru5qvDG6HbuG9vbsKbNL44L2LSUyZUUI1dsqcMN24NGqd0qxLKr.m3Dmfie7SPRRB866cI1pdMnVWLVcFT02uFGv0g2NF2faCuMVTTPXXHRortG7EDDvjS56KVOxi7H75u9qy5quAuxqbZbNGKu7hDGGSddNwwwns9YeX6zg68HGkU2bC5ctyxK8i9A73SOCESNEgggDMwjnBCHUaHyTPRRDVio937to+0BUhg7WfXTLVaYCSvTmli.Jaf2B.slMWYMt+6+948+DOAVsgnv.TBuC+ef9qNHd6Z36wZtZcqTCvx50CCf2PjAlc543i8g+X7W+W9ehsVeKNxzKhLLBqc2rHbCo4+t35uS.4Besd4bkZzoJsgVeaqYxIlDAVbFK45B5Xx8LLoBwEEReOASHkBhiZPCgfPm.UdgOUQVKpxBNx3bfyVNdke7KYMqOUSD1OYckzOA4FMS.sCjZDFCC5zmvPEefCcXdfO9R7O9SOCWbq9bwz0YcGDZ.gnACrozO2Rg0PXXHMChHDGJsFQQNg3HFKyBLMviN+D7AefGhGYlEYhrBB2ZCRBTnjBrRAVkBaY54DBGQBEVY.4HwHBvCITPPY5ckFexiERvZJvLn.sv4K9JQfuCmHCPqAWT.Ro.WXDERAo5BRKR82WS5kFTfRRXjfDoBDRbZOqbNYHUfSqSSX4wWivmBamShpNcvBOCVUuGFSZEucNmZH.WNt88ws6nFQUEXfQoLd+RQ33d8c40H2lkYa06ULxeC3asOk4W1ID6LyDs16KU85ioSa15RWhK7L+.HoA22gOJadsqfzTPql9zsYsVBBBHIIgUWcU9Y+reFatw1L4ryxC8PODKrvBznQCRSSonnnNchUk87MHdSwd2FJ1u8Uia+b0yqJ8277bumoH7s5lSdu2GMazhe1q7xr5UtN.znQCVXt4wkkQi3D1bq1zJJlNYoj2sKO7IuO9Q85wke5uKG9gdDNTqIQjz.hhP0rgeeXo3C8mSHfRp78EZfEiSfSXJ0yvariq6hkq2fifOLnUCFJLZxrZu.bwfR3HTH8ZPCXi0WmAC5x69c837NerGEUnWyEE5LhBiNPAVGDusN1uIH3.7UwADpB3C89d+b76+AX8M1fNo8QEFhHe7MB86TfqF23A02SP.ENKF4NSX1W8ZkUzlSR6s6QPbDAMhw0nAVkkAVMCbZxcFJDg3jBhkRrp.vJoPqQfEEFhDRTROCRZmFbNTR7bxXMnbhRebm50IDRrRGpR8bEhj3f.BEfHOEQtlCEEvxSOM2yS7937arMuxEuHmdkqxk62k00CXaflUGCxLHyRI.HFXB7fplV.O3BSxie7ixCt3RzJ2wj85vrwQDzLlz7AXUJLJENkCmxmUo.TnbBbNA40y90qCIfxzEaw57BbO0TPlwhKLfflsfnXxsNxKzzrUKxy0jWT38MJAnRhHd5IoQbH57LbE4XS6SZVNElTBE3EYuThwsCvppJ42qDv55DjR0OgrL0ghgdu2oh2xlhvwEiKEfiKcfUWTLJEF60EU2LfZ625xd94jR+MWkkov2pwjmiteenSax1dKd9m96BatAG8AtOBMEr9ZqyBSOAMhi72HNvSyZ61s4xW9xr4laxjSNMOvC7.rzRKU6Ju0kzK6.zoJEg6mQWN7qMZJzFGPrcu4IqSOXkftMFCJkBoTgRDvhKtLo4YjoyYisZy4N2EvYrLUyVHbPbfBTRxCBnW2sXhYmiG3X2Cm44eAdoev2mlSMk2iVDfHLhvjFiPs9dDVGn1egrd6V7CUEuvMLZeY5WcJomJ6pxL15JSQrW3EW+5WkkWZY9z+q+LD2HAao9qzZMQgQ2VqaGDGD+hNbBe5a1wAcj0kAuCPD3+aiwwot+Swm9o9eh+7+h+CrY6NL8rycCrGL7MptU.Ycyzf09c8uEvp7rp3npxvEkFEgDgTwbG5vzNMkqzcatb2M4hc2hK1dct51av186ixDRyfDlu0Tr7LyvglZZVZxo4nyNGKL0br4lqSjzQPoCsqbNhbRBcVrZWcZIcd9tnTZ8XDdCpVOnvaT0XQX0LQffIjAjVjS6q95LWPKlKpIO1otW17jGlKzdctvlqy060gtFMo5BbFKRqiFHX13DVd5oY4Ymm4azfiN4DDkmRTtgDkBUniLcOLwADNWC5XywFHPF3kAQfSgz5YoyhG.fszFCD0DQXQKbXkPWcNhFInZNAEQYElqyA..f.PRDEDUAr1fBt55avkWcUVa6Nb9qdUJJ6NHhRC0d5olfkmeNVXlY33KsLSHELYiVjDjPXVFgFKIRIpvP5VzGwNY.DovCWs97JkOslRKrif2AgS58R8aSPViR9xc53tlUpeySa3u3CmygSJKIBrrjgMVjEEPZJze.1N8X6u2y.SNI2yByyVW4xzLLfv.INqFiofIZMMNmiqcsqw0u90INNlibjivgNzgPoTjkkgwXqSkXEPo33Xz5h8jwtJ6ZXzWe326dwz0vgV6qTwvPeGWunnnDfWJRTDDFyxKeXxMZNyYNMm8bW.IBd7G4g8d4UqVztaORBTXwQZm1LaqDBWdIF7hOGcdrGGN1IHOufvnlzrQCbgQXDdCb05wuTWsG9BaX2UG43.WtWaWuQXvZbFB5vz.qh75FSqz013gy36w5Vmi986y668+D7IepOA45BjRe5cUxj8+G9f3f3sIwtliaoXic3S+hnDrksHmVsR3I+feX97+E+6Yqtcn4rSi5tb98Gm7HpdTIr3Dd.MVDXTfyEfQnvHCwIC4Gd9KxYtxk44O6Y4zc1h0.x.JR7UkWXtlvhAHYSjbABwyNzIledN5hyyG+i7gngvQrQS.NRLdsJob1Z1Tp1uUEVAHkJLNGslYJL85gsPSXnWf1l7ThcVN7TMAMjm2ldCzDIkL2zs3wVXZFfiAlBxsFTRomELghHmGrmR6PZLLQmNdiwTIIpYSjIJxcPpDbTfNzqwTjJDnPZK8tOmWHVAXJ6mh6jpWsDzHoP.xYmf9BIWqaGdoK757ruxo4LqsEcwedQQ4WkpjcL8Vswc0UIlywjRXglw7nG6X7dO0CyoNzgHJrAldozKMERSQFGT1zY7hp2mhU6NdZlcGvVd+ZThycmM0yiEOxaUzfkT5YIo12QrdAh6Dtc..n7UxlSIvIkHDRDVABgDiyWIVi572iyIvqhguA63RwFP8NnJuUxMhKDC9TD5b3E4LN5r8Fr7zSQ698wzqKh983a+e6qAwQ7.G+XLX6swjkwryNE5rLhmZx5R8eyM2jKbgKPZZJm5TmhSb7STybD3uo7vBWeG.T6tUrLbEXNbiFc+.rtWrZMpFtxyyq+aoTRXnDgSVmpxCcnCAR3m9SdIt7kuLGdokYo4mCIPQVJAQwzHL.cVFQhHN0IONuzy9B7hO8+D268duL2wNIARIoc6hMIBao2dUAvwVBxZ3J3amCW6dfz8alqC+dGU37CyTZ0wFUT.E4Y9yQUJbNGpvP.p6+WBgva5fVKMSZgCXk0WizhbdeevO.SNqe+f0UPttfHgXnxYdOWUOHNHdKaTwTkPHP4sCHnTj35RFYBPRuhTlLTAFGev+E+R7jevOH+29NeKl6PKQKUCuQ8NT2a.ndL2gGCWoTjUjQbRL4YEDnh7okxciE3S0iBgfzzTTJEMa1Dq0Rud875apQLpLMMSRnaQNcyKHd1IvDmvoe8KyK7puFeyW9kPCXiZhZ4CyxKNGAyNEhIaPPP.SHBQVXvNHkzt8o21aQus1je3lax2a804q9xuJeh266j2+i7N3XSMCIVn2FaQVublexIovzCiUiVaIHIlf3XFjlSdggFwgjp0nhCPJ89rmRXQknvps3b4HbVhTBjANJbFz5BJJbzzAZwNicW4oXA3kvfDAgBIIDBIIXCjTDBoRK5Pk2SoDRBjgHQgzHQITHHDgTPQIoBQRCZSg+3WT.oEN1JKE4DMYpCcXN+5av294ed9GetWhK6.QnfjkVjYmeAZL6LXSBgX+D2CUJLE4j2qGYsaiqWWd8KbYt3O6L7M+Ymg2yxKwm9C8g49leYf.hbFZnLTzuKshRnnHkn.Ep3Fr0lqQ7DMoPW3SEXI.QAkrFp7iyKFwo0quGfXnIR6.FhoppWu9dgBAEE4k3XJSq6MoEJcqF20aFfCqIKmaGJguYTCem72uZcXT.aUfbDBAABAshiPZMHzEX6OfK+puLoW6pPRDgNGl7TTkLcohh75nRHHOOmUWcUxyy4vG9vLyLyfVqIIIghhhcAxXz0sar..FoW4cSRU1dA7ZzWe7ZXS3cg+xLdGDGxryNKKt7Rr85qwEtvEXxlMXpIlfjnHLNKANEMTJjBnAfXxVXt3EY8K85L2hKSusVm3EW1uc4.i0ARgeLTouh9pEI5u.bx+cENuYt5bBbV+5iTD.BAo4YzcPel6vGlG3wdDxvwf98HILfICiJUSq3MiSgOHNHtqEtgdrVNN.d2jB5YxX5vHDFAXbbnYlhG33GGDZeqPan3tkWFFFF56cpoo0ZG0qkHIMCTr5paRQTDKdhiy533q989970+g+PttFV9dNAMlcNlXokYh4WfvomBZjfKTgTJInHGUYQsHrNxyRoylavZqbc5r05n61l+e+AuH+c+fWj+sef2K+Ke72MKL0bnDCXfo.IAnBDDIAMBrodieNtQBRk.iyfyARk.mL.msv65RhPbVsmzAmCmUhzZIvnPZsDQYOUsrdEorJmqp1YoL.EBuQXJUnCEnCjnikXTBrHKSTpxaRpD5KXAo.sTfQHQfgFBvkmiKJhzhbxTAL+IOFqLHmuz28o4u8oeNFHA0RyywOzQHZgEQM4TjL8LjL8znCDnhTDFmfR.VcNoc6RZ6t350k4N98QmKcEJVcMdlquBuveyeC+Ker2EexO3GlIcF5s0ZrXiInnH22IPzNLc6yLMmBmRftHCkzC1GgEqSVJ7927a6Z+7Duozsk2gIE1EqM2It6zXYuZje6QeuUqOVm2DTEks4fnfPx52AWVJtrAblexOFV65rzwNAJmkhxluo0ZIJJ.QoGSscmNrxJqfRo33G+3L0TSQ+doDGGODyTbCfmJWqtgsm8Cf0noSbuzf0nZ3ZX1rFGnLsViLTvjSNIG8nGkzA83RW8Jr3BKPiFsHIIgt86ivYHPJvYMDIf66vKyYetWjy+y9Yb7G7gHS6HY1EQ4.q0Qs6sK74MuZlEBGXEFpRb3siOmc6DJ7Fpp03KuZTRzHHsPy14YbpG6Q4W5C+gvh.ab.NUHZDnzFjR0uvwHdPbPb6DCO5gfQMsQKJkvWEc8SAiffImlO1688we1+k+izs8VzZpXbnF63r2IhJ8hJDh59zWTTDNmi7rbn.lJYFTGZAdwqbU97+2+64Y2rKIGddl7vGFwxGk3YWfIV3PL4TyV1ULpV+rXrY3bZbRIpf.Z3.w7yicw4It81rwpWiSN2rr0qcV9R+fe.sa2i+MOwGliO6RrwUdcZF4Hz5PEDBVC444DDGQTnxWkbfuJ9BT3bBrNApRCsTKTjh.izW0dRqCoQ38RPiWj73jkxov2tVbRAVkCmL.Cf15EOrNPhNPfIL.m.BcRBrPn16h8JjXkBJDPtRRlR5sjlrLlXpoX6d8XaikVGcAN21s4u+G9r78O8Yo2DAHWXIZcziSqCeTRVbAhmbNhZ0DQXDfEq03qzbmEBCIYgIXpEUDIrza00HYt4oXisXtNc3pm9z7e3m7B7CV4Z7q+weR9vG6Xz45WCaZNGZ9EPu81TjVfZ1Yo+pqPPi.zk7PXEdcRW4n6U2W4sxwaJLXUQGm05vIjkoHSNp11ui9aVwZVECU0vYD6jRNgy6.sNmCSdFgBne6swMX.4cay5u9qCRIKL6L351EmUSRyVTXLnDA9sEii0VaMJJJXokVholZp5T6kkkMzfM6kdz1+skJ.TiRa9vaqia6ez+23.X4b9peTaMXcZ52ufFsZvTyLMyM+7zuSWt3ktDyO6bLYqF90GsAgRfNuff.IGY144rMS30e4eJc+feDZbrIQmN.YRRo91pz6j.qvU5r6k472sidOFmf9Gd69tY32m5of15DXbVxvgVJXhkVD4DMoGPjJAIvfhbZ3jHGgAxChCh++CgpzCmD.gfGfkCnSWHHjiOyrrvzSRQw.+3H6iO1c6Flx1DSEHqphzwXLTTnwIaRzLyvO9hWh+pm9ehexlcY566nrvi+njO4zL+wNAFYDoVEc50kr1qPd+AXR0ncZrgFLNsuwHGGyjSNISLwDDM2BL+hKBSOAW8rBNzDSP5qeQ9RuzOkMVca9bepOMG4HGl7stLlhBBMVeQCor9DYoMjOnOQggkbAJvJkD3T3PABWosG3sDCmvQkEPIE984RI02fvAdvURW4iUMaZ+qqUBHPhnh0JGD37o3UUNlqqTNEFYoH1E9FESt0hMtAAwwb9s6vW96884e5zWB6TwL+CbJZdnCyjG8dHZ5YQD2.MR1Xq1zsaWJFz22V0JKhKYX.MZzfIa0flQQLyjSwRO3Tr00tFaux0YoVMQe9yyO4JWgM9p+WI4odJdnolhlAQzIufPQ.AIIPZt23pckVfgaGlUA0tMiz2BG20AXUUIICmhv6jWCdqvb0M60UBm2EPbNboYj0uGqd4KCqdcRlcNRBTr8f9DJkD2HwSUsJ.jR1bysX80WmIlXBNxQNR4E9EkNfq2Go1uX+zU0v.Dq.fLbZDGshcF96aTsasWLXYcFD3PJETTTP+98INNl4meQxFjy0tzEYsMVmFMtGBBhnnHCq0afdAgAHEVtmidTtzq9Zb4Kbdd7SdJRGjRTKMxjXe+dx4paVp0qutaDx4alfppBgShzIQgpjQPGYVKlf.DsZROmkm4m7hL4jSx69c7NXQUSDNKgQQd+s4f3f3swgrFJk0SFqy+nT.g3nvkivV546W9pbwybFt54eUVrUCRa1DrFbiQuJCKMiaq0uxpfVHDkc9BSsrKBZMIlIliWXkU3u7a8s3kZuM2268ci6vKyZBXtElmt5B5k1mzNYLnaOr8xgLMRq2w2MIRu8KT3mv8VquEIMaRqomjnVwnTwbnG39o8UtJhACXl947LW4xj809J749LeJNwLSiosEiwmVuvvPDXwjmgxZ.6PsaETXDBbtPbk5qJz4HzVxfkyyfXfCThRmi2VTBNBekJ5J67HHnPBEJClRAlKJSKnvJIzP8jYsJPTVUfdBwrDVdLRjzjKsUal4XGitV3u4a7s36e9qQqicXDyuHxCcDhOxQHZlknWQNas1Una29jllRdZFNqmUQoH.gRhNMizt8XiU789uEmcVZ0LgkmYFZHbr9UtJy+vOHgKt.m868L7+4e8eC+e7q++Lu6Sb+boW4UY4nDlqYK5r9pL4byRtdfexu6ptlL3bxQpNi2ZFuojhvwE0fDtKJhkQSQV0i6.pwhwTPfJ.kJ.QQJ3Lj0qGm8UdYHsOyez6gr98HOOkFslvegcTHxnPJvxpqtJEEEbnCcHld5oqMRzIZMUssHTt17FZc2Nh.QGd6Yu1tF0JKtYLXAfNK22RCBBvHbjlmiPoHoYCle94YsqcUVeiMXg4l2CZbm9IMRGTzuOGZ943Rtyx4dkWgS8d+fnCBfhLZvDkBNrLMgNe09HKEdnyuhUdcxa9oHrZBhUFLJHJYvxgUpPDGirUBm4RWB.hhh38bpGgoB71yfQqQo9E1kPGDGD21gXjE.v4mzIXogHDWVeDWYCN+K9i4hm8bzo2ZrXqI3JlBuVDk68DEucivvPu6gq8cZhpwFBBBvNQK9wc1j+xu2SyK2qMycp6i7olBsJfYVZAhiavKe5ygVavk6s5fHkjjlgDICwEHoP4K1o.KXyLjOHkNa1lM2bargvzyOCuqG+cfvYoe6s4POxCwJR3Gboqf7u++J+u8q7IYglI3xJ7UQcP.BKHsFRhR7sHFbXvWPQNgGjENABmCkUW5z4hxIgBHq5bQVrUsBnJWQeHYV3O.pJ+rBTVguuIJJslFAd.cf2GrPCBHv4PocXDRF3jzboCyk5lw+vy9r7CO+0vL8TL8IePLyNGKbe2G8MVd8UWk0VaMx51mHofjf.BEBBa1BmzuF3DdmW2Z.iyh0AqrxpX0Z5u7hbxicOrzwi3hu94YfPvC8d+kXkm4Y4+3+veG7q7o4AumihdqNjZsnhiwpMdauvc6aGC+hJtqqfjaTyQ690eyLFEbgPH.qEOEjVR60Eqofz9cY8yeNnQSZjDQ2NsIPHQJgAYoHCBPFFQmt8YqNsY5omlYlYFrVqWz3kdNkTtSE5MtkJFp1ukQWuuYu+w8ar+rXYonHCDVBBj3vPdgWLoIMavbKLOsa2ls2d6cseyWgdEDJfj.EL8Tr1ENOW8JWBrNJxx88WwR2Z2NDClkGMtgiMi6X0c6PIkfw2pL.7c68f.LRIY.wSNIyrzBDMQSdsKeQRKqFwChCh2tGB2NsEm5yn8CG50uCPXYaNo6EuBq85WjkZ1jkZzfYZDS+s2Fbl6pWmVYUMNmq1tYpzf0J8Zye8O5elu8pWFywNBy9NdXTyNGyL+RLY3Dblm6mRiLKSYkLiTwjJAQnQWzg9YaR29aRmNaSZ+dXxxHz4XpnFLeRSlqwDLcTCx6Ofu7W9Ky5a2lS9XOLqp6S9zMYoG4d44t704a+7+H1NKCUiXxM9w9UHPZbDoTDhjHQHgDPfH.kHDEIHHBAgkUtFHUVrJCl.M4QZxh0LH1RZhirFVxa3sVhhFVLw9EBrnDRhHfFV+RjUQnqTyZRHKzwfXGYgNxUVjXH1XXx7BRJLTXkHldN99+zWk+texOivYli4N4oXKW.xommKsdadsqbMtz0tNl7BVXpo3XyMGK0HloTfvVfsHm7zAzuWWx62GateeYCYHKN0rb34VfqdgKxS+c9tLneetmCeDlYlYHHNgG387H7bq2mux24ejsbZbSzjdFMAMaR+rzcctfzQsIiVct6a0SS3cz6TLJbIga2.o1qGus+cGitjFWpyF8l1Bg.Cd8GoKxnemsvklhYPOXiMHZloHPJHOa.QwdJP6MX.H8MOgtc6hwXXt4lq1o1qhrrLfark2bqBTp58LtXbe9pWa3+29oEqpmGD6qNPsV6OY03ety4HHHfYlYF5NnOs61wqUqg.8oKJHpQBNmikWXAXs0YyqdEhvhMe.FcNtxNkpv4oJW571afkQNVvNZm6MynNUrNiuGlojnTBjFCjVPVZNKeziwRm33rQuAjWX75XvAp3v2jWaOHNHtyFBmEQcE8VsTxnkCxKJfvDBkVhkVtuitHyzpAIpXjFERqDg0sSqNQtaqxo5FhNwNMz25eavW18kZrA7oAam2qk77TDnITX8U3sRhKIhMrVdk0Wmu4K+Zbr+E+RLy88.bkNCP1nIquYGdwm+GixA48FfKqvWofJgOgnBAh.EQwAzpYBgRAl7BRyFfoHCgyfT3HT.oc6xgOzgXvfLt3UtNycj6gYO9wXaoB4hSxW8YeYNe2NTD40ljy4qHQqQ68TQUUU+IQJT9T3UUMfRW4y8UIHpRAwKEXTBJBD3BEXhTPvNK1.INkza+QLLqUUK9uOmzuHDhRuHzuG2yXuDKAnldddwKcMd1ycNRAl73GmnEmGciXbwArxVavfrzZObLOc.at4lzqcu56SHUPTT.sRhoYbBwgAHbVr5B1dyMnHKm64dtGlbxo4zu5YYk02fIldF5nKX8BMG6AOLO2EWguxS+Owf3HxBCY6ACXhYlFnpwSu6ya2o04remaeiOu57wpme2NtsAXYKa.T6Zi0572DsRf1N7lylvuHrkBe2388Vg04UqmwVlG5aDTjcOVFEPwn.qp9eU13+vfwzNu7CQBVcJgNGIVKuz2+6CBGSHEny5SiFgjaKH2jSTRLxv.z4FZuUGBHflMl.mUPVZAQgInjd8WMLCVFidrKCCDZm8odPN60xt.VUR.swVTuXKqREDVzl7csXc55EGF5OHCjAnKrTjoIIpAMBRvjmg0TvryNKyM2brxFqSVdNMmnEo44dQmJCnc+ADEGSqjXHTwUO8qPTw.b5AHnnzBd8mLG3TD3TnP5KP.UouoguIrpjkkVrPfRJIPoJ8wjwaeEiCv7n.sqXaZbL5Ys1Zw5KBkd8LHMXMEjjqYNYLsjwr15axU2pCMWXAPFgDu7qbh25WEKGDGD6UHJYtGmib7FvIBg2bCERDh.Bjs.KHjZRRzzdqKgvjhqHf3nYHjPe2rP5ud1378OOmwaFlpBq2q5DBzBGp3HFjmQTPHRsWKTpJcegDmTQgTfVAZggIlHBkIiXaAgNCCxxon4DbQgf+yO82ijomm3FywrydXZ0bF1Zy9zoWehSZhJNgLqFUiXxbdu8ZPplnvlfN.QQ.48xHPHI2jSbyXrwJ1NuOD3ne1.lYhV3xJXv18753J0fIXBBlcIrKtDq1.9+4a8sYEskFSMK5bKJiijvHxM4jYzjg0OgxR5AknQpJPEXKS8mpdQJq5vFJBDJjDfrzTPcFGNiqrzO8EliRoPnjXTdFpxhbjEXwH88PwFnPkpoQlhnBEBa.FYBckgTLwTrYXH+2e9WfWX803Xuy2I8BkLPYY9it.W55WBswCvEaNEEYXDBzp.53DjJCQacDG0f0WecuAVaxI2jiACxPIAwAjYyY6dcwojD1pEa2e.a1sGybnCSVPB8CaR1LM36blyvOdkqfdxFPRB8602WYkVGUU.fiRharNjlRxFJqlP2vUp+PESknDOhnbo94Uh+240NWUwSDFFVN4fa+A2usAXoD6tx7JSdL.0anUwaFo74M5ukSXw4Lf0gzZHqSGx1ZaugSFGiIK0SMsvCFqBzTVVN44EL0TSQbbb8AyJyEsJMgittLJCR2rWab.ItYo8abet8hMK6tlvoqFbbEnWmyQXbDVqk9Yo9FdJ63N7lRSBrQXDHDLX6Mo2lqizVf1jVmFRkUhzTAtd2m1UMihQerJ1KsncmLbBa8B3HzT13UsNbNAEB+hyU1uJKca3ChCh2VGd0W6sS.fJ8r6EVkDmSUVpuEfMCqIEqQi0HwoUksrDp0eSkB4kkSndXFrbTUwb6vJgfJIZTs9HwU1U7rBne+tHc1xppSBQAzwX34uzE4ba2kkt26CUTSRysjW3HOWi03+r4EEL6hKvZasIm4rmkW5m9xjllyfAY3LPVluOA1Y61HkR5kNfqr5U4hW6Rb9KddBBjTjkiz3HvIvpcjqAMJzAIjElPwzSxY2NiW5bmCsJ.bdQ4GDFhopOIJ7tPtuCF57EUDlRGK2hsh8ucwhnWyVC+3Mt3+L1RvEZYYEBJGp+9YcDXAkUhxJw4jXDBrgQzSH3kuxU4mcsqgnQS5KEjGpHW33pqsBFaAgABuS4qMDGGSTRSDpPjIIjo0DFkvktzkv4bLXfmcqIlXBrVu9jqBuWKVZhsVGEFKoEVjMmfAJExImlUR07Bm8rzwnQCPEPoZ1pJAYIv2RjtEtE+tXwZnWulEq6xPRtsAXM7841KVjfweC961wtX2nBzvH+ekThyXwp8y5p8VaSms1BPPRRB444XKznD9REVoTHcPV+AXKzL8zSiRotAcSMJKK2JK2LMScqnoped.eM59jg2VDBAsZ0BmyQ2tcqYEpBfUUCctQiFDFGy1quAau4lnDRL4khfcTllFyu4dcdwdk52Q+e2shgALuy9s65+rGDGDukHFVhEU8fSsVWW.OiNle0m4VQis1JKZYzTG572.EJ8Aqf.rNgm06fHVscWdgW4zz2AG9X2CNofdC56MJScAVbnBBnvZX80WmSdxSxu+u+uO+d+d+d7fO3CxpqtJBgfomdZBBBPJkznQCFLX.m7jmj+f+f+.9betOGsZ0h77b.pqfwptgQUubchYmksLvy8RuDCLFHTQt04YzCwt1976OJE41v6e+4bYb5ZdznvZ7F8b45fwZAo.UTHoFC+rybVtR6MYgCeTDgQDmzjffP5zoimLgBuEV3z9V1ioHirrADFpPasbwKdQdWuq2E+g+g+g749beNTJEW+5WGmywRKszMLI+JhGxyy8NzePHBYDwSNIZfexK+xrYmt3TAkYIX2merKBAdavDbuioAqaFaKuYE6G6Mi99.pAG4zFDVGC50C51ETJBD9x4n5BJkT5oCGuFqTJEQQQde.o7BOf5RJdz0mgGLZbhbe3+dz+2aj2+nOe+.qM59swIx9VsZgRone+9dQbVtOq9yZsjDFQqjFP2dzqSWBTJzEEHJYEy2c6ckLBs6e286323RI3vu9c6Xz82CG2InP9f3f3sxQM..3FFWo50F2X72pWeZE61rSggXx1JoQTCTAQnMNJjRxcv4WYEtvpaxhm3XnZzf9YozYPeJv5k8gTfLzCbZPVJCxR4c73OF+6928+N+a929qxryNKqu95zuaOZ2tMSLwDbsqcMxyy429292l+W+b+uvoN0onc610FcZUELVIQCoThToHnwDjDAm4RqvJasEh3XrREoE4kdUkmiNwHV3Z09mcXu5M9itgd9MrutT+UUqqfm8PeSbV.AJ1rWGd4W+hXDALwhKfKLDYTDVmfPUDBmDctgjfPhBCwYrLXv.JJJ7.NsZz5b9LelOCO0S8I4y9Y+r7nO5ixlatI44402Sb36kTcNiwXHWqQakHhigfHlb9YYktYbwqsBhfPJrtpNDbYVO1Ath6s5pauLti.vZ+.PcyXL4NcTVcp6.1o5l6ir9.9KjUBou49p0fyQ1fTHKm3vHLE9F.LNGJD0o7qpJ4hBBQoT0kQ7nauUWLdy.WsWfaF2m6lAR6lA.a+XBabeeFimZXoTRVVFEEE0u+pYwp0ZD.IQwfVyfd8QXcTjlsS5FGhAwpbnONvUuULdq550AwAwc6vaa.6zqYgcRQe0Mt200GCci9aE.VUrSXKed0MjpFGOHHDqyaFlhnXREBd0KeE53f64Tmhs52ktYCH2YpE0sobbKKNlbxI4zm9z7m9m9mxktzU4i+w+37Q+neT5zoCFiothDWas03IexmjO6+5OC+vezyxexexeB850amrVLhVNAPDDR2bMSu7xrgCN8EuDVUHDER6rLjAg3FBn4t0.bYJCEhZvPuQerR.6i1cOtgLGojXv40AGNLRHGKWaiM3BasFSt7gHUpnq1Pp1P29CnQiVnGTPnPRjLjXYfugUq7FIZZdJsa2l2869cy6487d37m+0INNlm7IeR.eOds52ebSL1uNJwfffjlrsoTrwJ...H.jDQAQU+LZN8bjB7xu1qQgThS4aEc1QfoTAJ8sCfrtsAXsW264VIcT2oi8i4pwsN3bdcFIL9GsZCC50GJzDEEQQV1tSsnwaFoVqkhhBhhhpu.04b05yppABOLB9wwh0dAbZ+Xx5VEH19Aj5Mx6qnv2HPCBBvXLjkkUeghNund+lo7hOzF51tMEY4X0FjFWsP0K2ouqiCR29yx43tvbTSV8tc7lschbPbP7Vs3VtZveCbMYcWbXzTDxNrXkmmSVZNFoDBhnmww4txUQMQBIyMKazsKZbHBTd1bDBxMZFTocVfYmcV9NemuCeguvWfjjD9s9s9s3gdnGh0Wecld5o4BW3B7nO5ixu6u6uK4EZ9y9y9y3kdoWhvvvZKhXXFXpFuTDnXydCP0bBJ.dsKdE5q0PPHFKHBU6rcJ189opmUsc+y2i6912icbpRFHqDBdUYQkY075W6JzGXxkVlN4EjCjacjUXHLHFq1hRnHePJEY4TjkSRRBQwgr95qSZVFexekmhFMZvW5K8k3hW7h7w9XeLN9wON8622KVbtw6KWCTW.VY.hnFr8f9T3wbwYu3EY6ACPFEUxf0tYup9yNx4MuULti.vZ3cbCePd3SJ248em8lhhQVpe8g.FM7u4vhlS3.qwT6qF1BM85zARyHR4AT38aOuVspR8mO0gVBC8cQ7g+Mpzov3nPe+.4bqB34NsNrF222vq2U5vJIIw2VczdV8p5IiJojPoWWZgJEXrzscGJRyHppZLpXqxdiG6GGn28BX9Xmc1c4XTfcGDGD+ORgczhVZjBNY3+2vws5DfpRQ3n2nTTJBduf0knBhH2XY6AC3pasIsVbIRcBZmM.QT.VofTcgOEX3HqHGaov3qj3vW8q9U4q+0+57vO7CvuwuwuAas0Vb1ydVjRI+l+l+lr7xKye9e9eNu3K9hbhSbh5sqhhhZFYDBQ8XlxfHzRA8rVjBX8NcY6A4XDRTIQns6rccC5kBEUsmKD+7tr2rEN73URoDM9TCZDfFGoE4b4UVEoJBYyFrcVFVUDhfPjpPxxx8M9dCzu6.egLXLfvwfzT1taGt+G594i9Q+nb5SeZ9Begu.ekuxWgidjCym8y9YonnfKdwKtKVOGcwu+QgFIhvD5jqQkzfMLNVYiMJYvZzSzFJUiuMX336nLXMNAIOJKRuYD2J+NUuGagO8VR7fiFLX.fWaVX2gJbkRQfTQXXHAAd26NLLzKR9g1Nqt3qx2PFEvx3VGGGqa6GPi2H.nF22+d826EnFkRQbbb8LRpzdVkiJGGFUu+Af7AoXMFZDsSaBZzJCrJtgKf1iiUiRC9alrWM5r2O.m0Aw+iRXs1Ru3Y7ws6jNFk8JX2iUDTNNpPER+7BZ2uOsMPyYmidE4joMPfxyJSQt+6RJpGCVoTr95qy8eumjNc5vew+9OO+jexKyu5u5uJO0m5WgW7EdA9k+k+k4W6W6WiW4UdE9ZesuFsa2Fq0RylMA7ULcQYu1C1YLZoTRXyVzKuff3VjacLHu.CNBiR7RDQfOsd2Ei86XfPTlNQmqdeiU.Y5B1raaHNjdZM8xy81liPhHHf1a2s96vZszLoAggg3zF50uCRE7DOwSvRKsDW4JWgKe4KyS+zOM8GjxS7DOAyLyLzpUq5TKWMY8QWLNPagjIljBqifx84arcazVSsFrpHDwetgsrpLeqebG0nQG9FzU2XdTF.FEQaUUZAbC++QYWY32irTW.iBLnRyUCOaKgPfRHQxMdfd3JhKPonWmtPP.QAgn0Z1d6sYvfAr81aS2tc4pW8pb9yed50qGas0Vr5pq56f5kftrVeCTVHD0oSa+XO5VQ+TU9ywvUvyn+ua3D2gd8puqpWa3uq8RGXU62BBB3pW8prwFaP2tcYkUVgUVYEVc0UYqs1h0VaMNyYNCEY49AjxyQq0DHUTjk6KdfRstoD6zrVq7frQqRowIh1gebzWabmeM7RECbC+6TMv6dMq6QO2a3ykrVuucIkRrtweQ9ALdcP71pPHFJK.6d7YoTtKQt6e6iNgiwazyiKCFuQCmyfVmSVVFQMRXqs63WOCBYPglbmw2cEBTDFGUCDpR5FVqkImbRFLX.wwwbtycN9a+a+aIHHfO8m9SyG5C+g424242g77b9i+i+i47m+7r3hKtqwzyyyquWwviMkUnwf.qL.YbHa2uaY02ETazz.6n8TQYSkWntkY36lF6A3spuekRQVVV8XgxPugYu41aQtAjggjVnIJtA4EFDkSRtQiFDD32Nlc1YIMMEq0R+98onnfkVZI9k+k+XzoSG9ZesuFNmiUVYE9FeiuAO1i8X7Q9HeD1XiMvXLzqWOBBBpkYRkTSp1+VTXvXKYVS6SU4.cNCJu+Y4FTclPbkoB1Ic6B.2vmeBiOCZia+TUusrpGBasVeUftGiu+FItqzH0dibikppTYb6fFWN9qtg23Rsz982i6+MbksYFBTQ0uoRoXq0WGbvpqst+0kRBTQHkgL4zSiTopqhvgAYUci8guI8v2rdbae2MXkYuXtZu9+iFat4lDFFRylMY80WmW+0ecbVKhxSrs1BTpHZ0bFBmXFHLjPU.l7Bx60mlSNW82ks52ozaXF9rjw8aO7wiwAv5V47r85bn5s2Cv.cPbP7yc3uV7VKsg65yTk5L2MIUORAlbcYC6o7y.3L387JqsVeQB18Dov5naZWZlzf33XrVKar9570+5ecdeum2KefOvGf+u9+9OlomdZ9hewuH+3e7OlnnHlXhIPq0r0VaUKMhpIzO7Ms8SN2Rjygw3PC05uE.qUixUcKVKN2vUA2MaO6sdT6h9Cs+o14xKIivXojwHGxRsgYsCOVno7Q+9SDFrVGMa1j986htHmomdJZ2qMqt5p7jehmj2+S794a72+OvJqrBKt3hTTTvy9rOKO0S8T7w9XeL9hewuHqt5pbzidT51uWclfF9dg31IiDZm2BIL3YMTJ8RwQXcHEUEAQov8n7AA0+c89i2jxtwsRbaCvp5Zn87l22jSjDBQ89mckZJ4XDP4HuOmamVqRkXougue2t+LdPUkZGan2mwXfxYofv6HwgJEKr7xjm1229TjAjjzjfnDRhawwu2SPmNsoSmNdVaJuPbXCFcX14pdbT10tY6eFWbq7YGce0XeNiOUgT93DSLAKt3hf0WQNas0F6p61u9lqQPPDggdF+PapWuCBB1QqaBekXVt6uVbqil5vQAiNbL59x506xuQGUq+3W+Y2.ZG69lC.XcPbPbaEidcztXPdjIFMt68U0FsF4aEvhTJnvTfTovZckc+CvTjiMKq1+BqkyQIXOA3sGlxp+Ny3yrvhKtHm67mm+x+y+m3duuSx67weL9leq+Q9hewuHau81rzRKUOV9vaOCWwjVqs11FLlB+3aki64rVbVMJIHKcucI6X3p6r+R52DuMS0UUSNZbg+2yQnTgU3nPqwI1YbUsEvXvoK7V.p02cObNa4wIKoEZOyQ.oE4zqWOlc1Y4i9Q+nzdqs3y+4+77O+O+OyryNKqs1Z7W8W8Wwm5S8o3S7I9D7I9DeB9xe4uLm3DmntXoFNqV5rbDpDBjfwTTe+WKTaEDTlADooBw3Hdg0dPLvaU.YcWgAKXHvEpa8rPNNFrFGyCi9dGFn0n6vugmuG63MFCxRJUorR.CDvjSOEtlIzpUCZLwjzrwDXbPVptV.k862uV72BgnNMdUovrJ1OlpFGfh8hAmg2tciBeejXT+G4FR81H.rpYeanTD3AMpIIIgSbhSPiFMp0kvRGZQLFGS1bJVscevtiC1WynW0uQofVsBuqOKEf04J6h7iJd8cNGXbruUetwM45n8Jcc0LKt+e7ChChCh8I1KFrtYoH2OVrDoSxNN4tr1Yy8iUXq8HKoDLVsuOAB3RS8U0sc29rjyTJI.oOyHMhhonnfzxwmOzxKyzSOMO6y9r7zO8SyTyNEO2y8b7JuxqvBKr.MZzf1saSTTDAAA0obb3FNcMKLk+9ARABqgHf.o.rFhUktQuyBBENW0jt249gVbHvu8e6D9dOwP66c6zXjsVKAU1rgSfv4Ac5AiBBi1uf0WQAk1GgEvYAcQAMhiINJl986y1auMenOxGl28i+N4pW8p.vQO5Q4jm7jzsaW1r8177O+yyC8PODuy246ju427axZqsFgQg02WnZeXddFMShPZAqo.gt.DUbU5WuENCRaYVOfa.n5v22ezLa8VAPV28Yv5NTLtcbd.Fim8JwHulbDVqF9Fr0GXDh59PTddNwABR60yevOPfJJlFINLFKat4lztWWN9QOJVqsVOSilJvaVpsFNEhia6cuxib8ITuA.XM59NfZVeFdcw4b9Yh4bzqWOBCCoe2dzqWOVbw4IJJh9866ch2PInTXbVxzEfvKBdsViIKiD18EAHJ+cJAYoXTfU2355AwAwAwaei8hkAbtZfU6lJYuHlkhRFqjPfDxMEjHUz.nn+.B843x22aKYPBiW9BRQoDFJ83vj3XFLX.sa2lvvPum9Y89fk05MPyJOMrZb7JevpZhmiN9swXvUnQZ8.UZJTzLJDgQiRXAgCSIqcB2N2p0I7MK9cRS5cToPu6vXQHcnDRBUJJLtxJ9NfIBw2.s0E.VPW3y8pv4MHUGHiBvJAYX.4cx.of2+6+8yhKtHBgf+n+n+H50tSc29nS+d0ly5G+i+w4a9M+l7LOyyvod3Ghrrr5L7nTJBTJO.YcAnKvZ78Q2PfjjX.KBsG9nT3WEs65TkQrtg2BNg46n.rFN1yKp12uqajwpajYic+7QMQzw8786+4btxVIfOkVIIIPI.AqzKFZbdQ4YcsQEDQXRCP4EGeZZJ4kB6thFz852ZuzN1vrTMNPY60rC8.L2+Y+LJfpQe8Jg7sWoHToT6xOXLFC862md85ARAZsAkBRSS8B6TJoQqlDDGgXHZ18lrG63GKhce4wvqmCCRcX.hiCXp5lbE0vaKiae3AwAwAwO+w3z3Hr6wS18qs6TJNrDOpcnbA0oNy3zDDJAb3zEL6jSvTwIb0t8oQdABitlIKXGVNnZ7iRvQUU+c0DgqFquZoRbyooo6Z7tQipzbYsVLEYXxyvf.m4+O16M8GKI67L+9cVhk6ZtbykpxZq6p5t5cRQRIRNjRjRjBZrlAvFFd.73A9iF9uHCAX8QCXX3EIaXqAyXgYf12FQRwUQxdqppqpxppLyJWtqwxYwe3DQbuYVYVc0raJVsY9BDHu6YDm3r7bdeededKoS6NzqUJXL3kVDUtQRTKF4U..l6g9e9M+Ssmdb9fW8PHQtv5FoQwr5x8Qr2P7kE37NDIQgvEJqNokBJJLDEEwr7o73COfW8kuIeyu42jc2cW9K+y+KnUbKVc0UaRDf3Vor81ayN6rCe8u9WiuxW4qDxtvoSQJkTTTzDkGoThvYoHOCuy.UJCeZJrT+tAWn4CQVxKAIJTDDKUu+3Iiwh1ySaL+SL.VvSdg8rxheVXP5Iic+YAPnY.8ovgmS640crO46a8NzUwXONNlz1sfJ4H.oDiuhWVRAYYYLqHGcZKzwQXKMMcTVrTJzPrvJkN+jWamlGqNqqgSlZvK9aEt7e5sweX2CNUuWs.nj5PDVCzpNSPLFCs5zFmvgPovXbTZMPEg3a0pElj3i8aWy2tEK0C0dg7rBQ3SqspNDoOMNX4qIb2oXKx+uysysyse1rmEvUmzZduphEMbBPVTUfjcFhUUYjrPxlCVi052m6d3HJmLCSQIBqsIyd89pJ2g0gqJbgI5n.kOz5l4vlLcJyxxX73wMYzVc8Fr1SVoooGqlKtnYLFlMaFBmErAZNrT61zucKjlY.FTp5HDHP3CrkpIjph4gF7iifjeZ5G1hlVpv67.NDJQU6hkVIorw5CfcC.rLVC9jHrFCNsBmRfEG4lb5HawviB7L92929avK9hWi+v+O9C326262isu61r4la1jIddYfyae0u5WkO6m8yxW8q9U4O5O5Oh6+fsYvfATVVRRk2DaGkfv6vjMCp3QWYYIsZkxxKubSae3dZvEVBTAd0QMf0SOxOOu.x5ikuIOVVfgqpST8ysK7ieJKP5mSZvECo1YlsWK7Zm7W8iCBVKyWfWFESTRKHJFTgAbylMCDNhhznzgaxABO5ozZPnCZBUTTzw3KTshtu3yOMkWewqySdtu328j.fdVOdhl8S79mFIy89fZ+V6AqEANlFGGDTTmCu0w3ICIOeFkdC9HEztC5zVXcPwrhiUIz8KPnSg8DrU7D1hwYudmeGCXoe9tXdZ1S1t9rIbnml97rnM+55b6b6++o0z8+T5mWySpyxCVvG97vR+wW.xJq02HOBbnbRjNAZeDtBO3fk50kU61EY4LX1Hj4Ef0PXdYPn.mxiQXovWxjhojzNghhBNZ+CHaxTv5HRHoUTLooswZ83MdDNAZBGESmcLgp15cXbgxui0AEkVxmkQrGRbVR.VJMldsZUwwIEJYDNz.ZvqP3kg40D9PV5giPgfIbfX9g+XGO4bQMkTnEZhWzaf0hm87PbF3ulyXPXsjFIYsd8BeuxR7EEAOYYL3cFbNCdAztc6FYU3BW3B7q+q+03nCFx28a+c4Nu+cXvf0I2Xwhf3Vs4viFwi1YOdm2884m91uKuwa9Y3y8E97jkkQbbLJAjFGAlRzQRLRGybEHbNhMFRmAqfjAwwnKKPfugWdVgrZqzx.mwV35+z5qINEgsV3Od6zIaCOo44ouNvGl8IRveCCTleVnDAcmp9lK1vhwXE04p+7KRoDTxFhOWWmgp8BT8BgxpCkT1bTmYGmbv8hOewr+XQuIAUjs14PkFiWooz3YiM2BPQQoCoTgVKoHKiYSGiP5vXKvXKnSutMgvJNQyjoinzjSTrBqqDoBJM437UYlwGxgyaZNpeMDtm34HbG6y+wEvk0UhyTL+n5+m0ZCdqKITLqyyyYot8PhhQGMlXYLBqgtsSHqbLhtwryQ6BZn8ZqgNJEsLAk0iv5vaKwZKwJpH.OBhbzjRw0GJw76q0t9uAjYEmKDPnufnt+gZtxH2bHQHT.BDHCpRbk4ch.3rJP9Gi8GmbvTkv74EKtqaZzZsSydV1A+4141yMlu1GvT4w2vnFABTBQHSyp1rki.kJjJEFqMvUypxkUi21qpWdN.DgBurw6HJIFmwRrNvyUoLnWKN.iDrBOVoEDFhbPrUPhOkoGkS2Nqh05IexXt401hN.hC1gNROiOXeLEYTZKvE4vk.6mOBapDc6X1ezAnTBZkFS23TJFOkNQozucO50JALJhTILd+iXk1Kgx5IFIQRAwZI4kYgjxINhROT3cTX7L7vQzw4Hd7PVC30u5UIlJh2ifoEN7AFEgRjfh5rp1gWVfTUhxaP4q.Z4CDL2+D.rle.ULVyCJOHjdjBeX9PlCPrdcyRaAdkGQTv4.shk3xlR43g7xW8RrU+VL5fGiV3P4JYzA6SpRSmVsCYGIdllWPVgg+4+N+t7k97edt0O883a+272ypcWl0Vac51qOcWdYxsVtz0dQtvUuF28QOj+u+2+uCqRx+p+0+2vku1U4Ct8sne6VLZ+cY0tsIOeB6XlxQ9BlL5.ZWTxV.u0RCXSoBUVFJk.uVRoBJ.J8yiJirBYjnB+QM3o5vi1P2F2BUSDl+5VqErNRzQjOKizjjPItyZBiDDhF5rbxDI3Y09XGhvfGOWPaPNg3bE70yI+RxlU079iy4nZBF9rZerhhsXNozk5Hb9YHUIPZWJGtOEsiQqiw5xCfsjBbNKduEkLFgHjNo5pPnUKTl0.udZbq5rd9ICg3Ieb8ms19vVD+C06dt.fEWMn.pzKEWH2+TJEEVSS8HzUMgpRTU2EyyPnTjYxAqE50BUqVXZFHHdBujI8NbNERgLrysSbcdRswYwPG7wydx8SbZJLeS895z9EVbGim377b6b6S6Vyt4qiR+o48pE+7Bw7rRqRhaZ1TrThUb5iiZ7.rWVswwfmJP3.ua9ltrPZTJNiqxCSdVuWGtVGM+jilftcWhjQjMbHsWtOyJrnSiYvJqxnQinUTLQREQREEkFJckD2JkYk47c99eWZszR7n81kkWdYDdWn1GlmSQQF1INJ8VRakhEA44knDJLyxYxQCoEJ5XMHKb7RWXCtvxKgKOCsDLNGdUv6UdBalS3qnzgvAXwID3EplEucBGNgDmvUEBw42KDLe1Km+35K5bBUT89hpj5ZAOe4bFrdGBmCsygz6oiPxMt3V7S9ouGcUZNZZFSJxY0stHiFOFuPRjHAg0xFKOfwGLj+W9e9+c9O8282xd6sGqr1.FOdLFmijVogyEqCsTQ9rB9w+3eL+9+9+93qRlfkVY4fWOURJ7FLBOSlMlcez8406zgd4GxlR3K7Bu.c7BJsVjZIVg.qPVUqIq.TUycXTOUfOKNe8YUMQNK6ShsG+yEYZ3ixN2OMOK8gwQoOt1hgMpVI2ihhvIfj1sn0ZCX1taSgsMQwQTNK.DTHTXxKvTTRR6tDozTLKij9cHNNNPD9Jz00wx+rZKN403IAWcZfxdZWKmkcVYQ3hO26m62zZEeGeXWQZsloSm1veAiILPUJk3rVJbFR50lQyJfhRX4UoWukB7pqZG.MfPp4GEU69pJCC8b52u+4c+fysysysOd1SDt8mgvueRS5OagJv4LDmjFziIuiHOrduN7JuvU4c9QuOhbCwsR3nGsOchayJK0iCO7Hj1X54iwmUsQ2HH2Z.zHWJlQiNh+ru2eM+Eem+FlMdBkxBzRASc4HRUzpUeDJE1rbD5HxOZBBfU5uBGdvXl8vcoiIGoaJo.uxUtBqu7RjOdHwppPAVwiIPUUXqcKrpsDuPfQHgJvCdAfWhSFBC17F4.3LqXdn+.A0ALCnxaSPMqRqS1tZtGKbdjtf2bR7B.McPym4ZuDem28dLb3DTsSw587381Gcmdjjj.EVZISvWZ4u5+3eB+w+a+2h0aHseWTchI6nCC0pWmlXmEQdFcizLU.26cuE+O7292yFWXSJJxwIgCyJvmjRlNvu4w6sC8rdX7XhJ8biqcY1ZiMvjmU4IPNVgcdwdUmA9+mqrOQBQ3YwgnOJ1hCH+jbQ0m14UsVgXs1PIDPpIsUKtvVaAREFqGoJJjpsFKtRC4yxoLKGEBZmjRoITFBpKMC0kcmZNKcRtWsHmpNKdYcV+8iKmrNqRhyocNAzP7SuIrCDgVQdU1hz.DyBw5DxljAFKKOXc5zqGnzHhhahgc8QcaNN+SHkF02mNO7Zmamae5wNswqOqieqmOXQdvDVL0iWDHGgTFzIoDs.YYNcEZdiqdUdg9oLc+GSWgh3RKi1aOR8Z5F2lrgSIRDivovZ7XLAEJuvWhLUiLUy16ded62+ejIyNjjNJDwRlYxXZQNSLELJOGqRPVdH6vaoiwLZBSe7AnyJYEkFUQIuRuVbkAqRJdTFCdaIR0bwI0I7Udm53d7V3B09OGxfm77R.IRmDkShvG9qxcbdUU0B2noftEle0s3eYtm+kARTPjWf1AJqmzROub+A7UdoWEaQAIkN5mjv8u28pn8iBadIZGjJTHLVT3Yv5qR2U6y9S1GcZDdgCiKGvg0jgV.86zEo0R+z1jMdBwQQjD2J3YNklCFNk81YWzimxMWc.kSJYsX3K7ZuJthbxylQbxBz53D8aDMsWOeaehWKBW7wOKCxVjmUm1wmjmWmEPvZOkHzJDQwrxlaBsRY3rYfTiTnw4.SQY.nUgEoyRm1saxVv333F8e5r.IcZ0YvmFnpOL.QOK.sNs1hSB55j0rvZg0KNNFQkBIW68phhB7.FmEq0ivKfBX73IPTBqu9lDm1JLwXRDVYXmWML7nZ2T0Gm18oysysysOcXOw7MmXNoOJ1ICykqJtXFeHaBao0nJKPNaJu3JC3KdyaRW.2gGv5sRX1d6yceu2i1Qsnc6NTXrnhS.gl7RCBBxqfyXIMRSrTxlqrLcaEWoaUt.A4kPt2St2yQYYXvSukWBm2vsdueJie7trZqHZaJXcfu1a9lrVRBtIiIVIwTVfRHqg.UQZcO1JfVd.gOHzmJWva9JODYkDYEM+Mw.I1ien8tlCk20.7pt8xIjMgSyV4ILuTEJL0REwRUfmVkNhyJY8BGe8qdcdMQJhIigoyvWVx96uOGN7HZ2tME4FlNdBKuzRb8qecRZ0hwyFCIJroRlpsLiRbIRJEdxJxIJJhVwIb0stDsTQjfl1pHhbB7YVFcvPlt6ArtWR7dOls.90t9M3pCVgHmgXomjjnFBtO2jMbr5iZH+9Eg8wFf0oMF5i7.qSHSCK93Op.Hd1NmmCxRHB2xbdOhHEkRO8Wa.LX.9ISv.nSRAu.q0iVovVZHOKiznXRqTJ3nnHRRRZTw8ZRz8z.YcRvRmE3pml2qV76dZG0WmeXdx5jmCZslVsZ0TiE0ZMylMihhBjRYnrK38HHhYyJY13bn+pr9FahWonv4QnjMt3sdmaAdUTk8hUEl6mFXvysysysmesm1lW+vF+V68pi6YlE9sIvWGmyFDaTAHMFjYYzWI4yb4Kwu0MeYJFeHxQCYP6D1+QayCdvcCZkTjBqViMNBSPAjQhByzLhMd5IiokTiYRFi1eXHCCQhJNgnzND2tMc60GUjloyFyNaeWNb+cnkzPOsmo4i3KboKvW3EdAVVIg7BRT00n0f9bE3a0wyHPH3cJoS138NkOrXrxCQt4GZaEHLGnbtJ.YABwWCvpV41WDvQsGrLh.uZQtfiKpZ2iMN5NYBWONl+Ee9OO8ArGMlM6zgGdu6vd69HllkgnR1FFNMnEVGd3gU5EVRHCBERJ.jQw30ZljmQVYAkVGGN7nFshzarXmkyj82mxQiIsnj0LFJlVv+rKsNegW55HlLlNwZTRvZMM8Cp8JWCu89Th8IFGrNFnkOheuSNf7Yg6Q+rbdcpuuwhrJTdQQIjqTzav.V+RWlcu2cHqzQhNFSYABmmjnHJyKX3gGQZRaZ2tMGMZHsa2lzzTFMZTCA2qK4HE25O...B.IQTPTM.ml1WcxmeZu9Y8YNsqumkq+SBL0683IjBx3q.0VA5IVoocRZSAOUpULY1TJsFRRRnHyPrRSjNJv+Jim9adQV+hagRGG10oViqZzrP.Jpy3iPJz5k0IIwoyEsysysysmusmHhCmXSxeTFEWWsMNlGKTfyZQqjHbkjhCIdzSlw5JEei230X6s2l2e7DRRErTKEOb26w9RK8WeiPnESR.mjHumDuDWlAoyQbr.YbDH73vf2FgqTf0YozGJ9vK0uK6+3Gy3G9PrGc.cEPhMmTifq0IguxqdS5ZKH1EJrOBui3XMEVaSQUNnL8BjHmGxNuDeSEY93byJT5fBj+ewlhlj..pPZHPZCZpU8FYEMd7InUTRQUVZFxc.jdOhpr.MEOQE4nmX3Kc0s3ct2l7msyiXjo.UVIkyFy6e62ms15Rr9xqgyaHurjVs6ixWxjCCbyMV1FgCLAIeGqxUIzFdZkjxpcZgonfxrYL8nCX7g6itnj1E4zozx0A9xW+k3hIoDkMgHYL3MXcfWol2enBXUM+x9zvJDehwAqONe2mlGV94kU+a2T4ywiLNAzQzZkArxVWBRSYZQYP+MbBrl.o3skkLZzHxxxZ7ZUcH0ZpkeK.v5z.4bVOew+dZely50NK6mEObAgrGLNN9XEw577bLNKBUHrnVgDQTBYV.UDKMXM5u5.DQw3jJPJa1I0IOWq200S6dy414141y21YAv5mYJdTqUTdQkhtWAVSAdSIwRIoRI9YSHJaFW.C+29M+M3E0vzilv5szzOBF+3GwC19tLZ7AGadMsLBIZv.1BKSFmiwIIItGIIsIQmRrJkXULwH4Q29tr6c9.F+vGxJB3hsz3JsrrMi+MeyuAuxlqS9t6fpLm3JQ1LNNNjo0xphXsvSc4+werLtO.lxKLUbzxsPsXz0vgJOy0HLekTVTqWXRev6VQUgZ7z7jUP6tbXvQItPlcpkH0RhRDnyNjUofemek2h2b09n1aDWMVw.DHymwsdm2gae2ayrxBxJxYVQAIQozVkhtPQeUaz1HrYdDhHTwsQ2tMxzXFlOEiujC1eG19CtEG7f6ACOhUjd1PpnCv+leqeSdoUWlnYSYsdcnLaJduEoV2j7CGOLg0fLcexxwoeNXehnj6mTFAl+dhiAZHnkQhPrtqPmKTpleiSFhvS9adZK75bO46unWzlqYFOY3Fqzm.7BPGmhwThpUK7dGabkKyaOXML2+tnt3FnMFlbvAXa4IIIgoYEr29OlNK2ktc6xvgCYiM1fM2bS1d6sY4kWtorL.znH50gNrVYzqIE+hWyKZMZB0o790jn+rriKJm9leuiE1RATXMgzXtpj+r4laRud83AO3AzoSGDBAO7gOj33XxxxXbkmrZmDyjRK6s6Avpqxq7leVjooLpnjk2bcFmkiHQEJ0AATV38LuVgojMSjr34qnJ6CW79p+DWWAMzIHUDgbVdQ.hG2iXVq8Xfcqe9GUODN+7Cnlf9mmkimaeZ1pAEwY3Q.OPU1PaqRadXdVRG1LoddokYgpWQ8FMyyCbxoX7Xzo5lwdlpPb4VH9f0mGhpyIaE2MEtphBOtfNOg.bVld3i4FarN+q+5+y3O3u5ugu8imvJcEjzoO25QO.ynwLc+CXotqvp8WBiTiHRiRqQnDnDf0Kv38HPgozyjrIgMPOcH4COhTyL5qjzeZN87vak.es25M3yr9.DauMqFmDDKTUPSGKJsjjzBbBhDdxMVbRGZUDBoDWoCgSFzJQWARsDoptd3JQqTTV5HOOuovRGR1PQi2o.BkMPmqQ6.UUYkn06QJ8UppNAsMSCdaX9NCNLNONggXUAx3LJlrGWs6x7u3MeMDeue.emCmxAYGPTu9rmyvi19tbzniX4ACnSmNLaVFwNEZmB4PKoBMVkmx7PHJxcFPXHuXJ6uy8Xxd6RTdNKEEQrzR7zwrpTv+8+leMVSInuySRjByzYzIItpLrYBkaM4wmO2WsNhy6.4IRPNN8JkR8Zdhp0gVzLlP0BnHu.kNMrlr3CuN+9rX+bQlF9zhI7PjVGzkI7HkgX1KiiIdo9z6RWhQ28CnzEb0oJRynQinc6tnhzTTTvie7iYkAqxrYyX+82mUWcUxxx392+9b0qdUlMaVCIxUJUCo9WjiVeX5b0Y89mDv4Y87EAXU++t1CVEklPYsIO.tpe+9zoSmlJddmNcXznQAInP.npJGEQZJcNlYEPogAW6EYkMu.kdApz3P7xW.bn733dN943OCxyQMH5SFFBgPbrUJNKQn8Sxjn3b6b6b6mMyIcUgJqYKwUgGqd9JEnHHnoRP4E38guSryyEh0Lc3Ab81w7u5q7qx5uy6we9cOfwiOhazIlIlBlM7HlMdJO7fCHJJBkVSTRLp3HzQgETKxJwVThszfuLnp4IlBRmNkMZEgtnfXfuPuT9u3q9U4pK0mQ+zeBWtcazdv6UAAVkPcssQ3KEAMaxh.urZisT6oIIwRU.zkyQVVdfmuQIzpUG5rTqP8cEp7rkCu0f0E1fnz5oqnMXcXbtFhtaED.QHCDoOL+p.mh4.zjfW5ozNEWa.aNQYi4sFrFK+E+Jr0O8c3O+N2g8FMDuTRZRB4YS3vsmwgJMIIIzKoKqkN.mWfUB4dKis4LsbB4lLrlYTNdH9o4z2Aa0USWiEeAb0to7Mei2fKFEwJJIswgxFRdLOB7Hwg.pESbTMftEHvKrHPfUDJAQOuZ+RM.K.hjJbNe0fVADGi2aoypqwUd4Wg+we3OjQEkjlnItcGF93CvKmQ2k5SoofGev9rzJKS+984gO7g38dFLX.au81yGbbBufrH.mE8P0YYOM9acVd36zBo3h1hgxTHDTTTfRoXkUVIPn8ISYod8wZsLZzHL9401qhp5zUIN1aZNDEwUe0Wik1bSNvUIZqtE.ybZWSKP78S658jd05ru1qAJUQzqp2e9PwmLrEKdbd3HO2N29EmEBdFzHfjUutnBHffJQoVJZ77fPJQ5EH8FR7djEknQRq9KS7MdIV1ea9t2aWdzjBx.NBXBBxSxHOVQtRRtziPpwYDn8Rh7AoKHxZQUZHwVRKrzFK5Yk7h.e8abC9huvUYSmjN6cHak1Erk30R7ROVg.e0FnkUBKpxIQUqPUdK3CUYBHv2U+zLhShQmjPqjXJ8RrNOkYVlMs.gPg2GD2ZDdzRAo5DhZEgVHwLyVEJ0PDYjUd6wgOj3VMdGLzN5EF7JAdOXTBJ7P6zThxkjOJitNEuUuko6MtIaE2l+324Gy1NGOb1LljMCSRBkZElrIbfeH6kceTpHDQZDwRbpPA51Zlh1TxFIIr9fTD6cD1wF1B3KdwKxux0eIdwAqPZQNsjdh7dvYAukZMmTpT3Eh4fqp8zpnVAEe929kd.V.Pk6p0pHHNBiofntc3BW4J79u3KR16+tbjof05zioSmRVYAoNKVIPYI6t6t7BuvKvJqrBGbvAr7xKyMtwM3C9fOfNc5D73S0.O3IA27rP.9SCj1SdYb1xPwI+b0gHToULZzH7VKCFLfVsB6ZRHDzqWOt8GbGlLYBwsRY5zoXcNJLkzIpKSJs3FMDdoavlW6JPZBtbKdWk7M3AoSfTTAxp5RpYB0Ox2lNNA3eV7f0hu9IC+7414141u3riKfjU0VNpBQXC+jjfygsNKYrzHS4hPU0gTjTNMC4zbtQmdr0m6yw0Waa9Gd22m2c7TZAbHdlkmwTGniTH0drnIxqH1qHwC5RCphbhwSGf1.qC7JCFvW3EuF2b0ALv6IYZF87djwJ7ROFEghirRfSpPQ3BS5CfMzxfmVBkmqEfFX8zo6pXKlwngyv3AYbJDGiUnn.CsZ2MDsCWINigRqgxrRXZNdp36UMWrjplRJm2GJYOxSrtBKLeoSnv2pEkRHVBwRCtwiobbAWNoMCd0ax586w2amGv2992k22AixxC2BTBD5DJhk3o.mu.x83vhx6P4LDa7nlYP.bQf2nWW90t1Kv0WdUVIJgkJsTXJQoB20kgZkVPtjzJzJMEn.gBUkvrVEiuvwmBxlveoGfk0TL2qRRvqjXDRT5D5rx.doW6M4Gd6awriND50iN8VhCN7wLNaFIIIHjB1e+8oc61r0VaQYYIEEEbgKbA1e+8IKKCgPzPT7ZvMvSxQs5W6zddscRvUm1uwI4b0hu2huu26Ia1LJMFVav.Vc0UCgFTHItUKFNbHiGONHCEog5zDhflXU5rr+gG.wZd8u3WhzUGvjBCpz1j6gDw7ISpi.PX2GUGDdMW3D5XWKmTzYO6PgFFnE3rwhWaezAuctctct8O8lSFzFJUUH0vKWHLPgLey4EUkYlp5WXEuaUREnSvVThRXoWbLVuf3BC+JqMfat5.9Q2917fQi3VGdHaW5X2R3vRKS.bTPDPDAraI.cI.pZq1cXyts4FW7B7RWbS1HMEyiO.edNK0oWHzUSGA8z3jBLQRLRUPzPqzXcbVjD33qjPYVKjMggrKz5ETNMCqviOtEQoo3SRwnCdxpz63vYg5wmVkRRrhHAncU0jUeIEloAIgv4AugHan9QJo5nghtUUXDjH7dv6Q5UDG0kxoSwWXoabGRTP9rBJxFg1jwuxVC3Bq0gW6Rav6r2t71O7Qbuw4L15ovlQoDxbTq3WnARqZG6BzC3k60h23BawqtwE4Zc6SbYIxIyPVliMRfSVUKAURPE7PYnzsIfp5M6I2zrSJBZt1ywgGD9kb.VBOXKMgr+y6C0YPgDmThWoQ0pEW4EuNu+VawzexgLJaJcRSHJNlYlBDJIwJMdObm6bG52uOarwFryN6PddN23F2f29searVai.jVC3YQtXsncZdyBNafWmUovYQhldZbvpljoYYYzeok3BW3BjjjvjCGRRRBdum6cu60Pn7oSmhwZIoUJxHMSmMCFOBdyOGW+Mech50iYNWHzgd.jU6LwcrJXdy4YMPK2SJKEmjT6mU6S3ZpZWY9iSx8vuwbtcT+dB3IZyO2N2N29mdawhnqKPKnlLfKT9bjgvigDmv23cKjgEh8dQHIohznD1P1xkmgsrjdwQLnSG15MeM1azXt+vg7vwi3AylxilLkirEjCjOMmDkjdJM80ZVMNkKzuGas7xrRmNrV+tjOZDkiOfURaS6zdXJKPWJfk5Axb7ZIVoBiRgGIBWcE3UVo0WdJwguBjnW3.uFi2SQZB9DMt3Dl37r8AGvc1cWd3gGwn7bFOcVXcC7zMJgU61iKLXU1b0UYotIrZutn8EHrNnzhyXvaBkTHAgSiPaZUCa0FcCNBTByfXeODJK3JwIgnVwXkFrYCwMZFqpiXoA84kWeY9hW8pb+c2is2ce1KeFOb1HJH7CFCzIVxRocXPq1zWGwKt4E35W3BzCXxtOloGtCosZSRuX7NGlHIdUkmqTRDJv3AqyG9Q0QU0.n4U035sUKD7D.uddy9kZ.VPXw15L0vXs3UZDQp.M6Rho+fU30ey2hu296wgGtOwqrDc50khQCIqr.rN51pMSmNk6bm6vMu4Moc61LYxDVas0Xqs1hgCGxvgCaHNdcMJrVdGdVOOOsvH9zxhP3IAXs3q67NVc0UY0ACZ.UEEEQQQACGNjYylEz.KojoylRbRBPn.WOc5TX0A7le9OO5dcQ2NkBWXWmwQwTZLDkFE3Yl33oNbSYy4Y7dzo0V.LGrT0DH0WmNuGYXuiAPVKB9ZgGeNPqysysewZdgCmzgvqZds.nDYkufBYYnW3qH+bMWrByek4fj1Q3LkLd1P7BOs5zBuqfYGtCQJMq3fk51lWaokIGASJsL04vHELyVPbjh1ZMsQPpCh7Vh7VTRKidvGP2VsHNIFOkjIcj1qEdBY4lWovIE3jxlxdipZtmZ4lnhEQ3wfegqybofh05y6r+N7Nu2Og29Aayc1cOdvHCGAX.RRDTT3wVMUUKfkAVoULqDK4a74dc1rcLarx.VNsCsTIHDFnvDjJBunRLWCYLtrh.7d7nbJhx0DoiAoko1wLxNAuzgrijNc6f25X1jwLa39zVkvMa0madgKg6BWJvuM6DrNGtJR2GihTUDshhIVpQhmI6rCC8F5sbORS6wiGcHNukMVeSjEAxsKjd75PxM3sgyUgPzf716CYRpsoJfWkUoOmOE9GK.V067HXNT9.x3f6bChelUHCYRA0wVu5wMgLJP.veQYNAgX9JDf0hR.HUgXLGkfrUatxq7p7Au66vNO5QLw5Xkd8IIqf7YSvV4kltcay1auMwsR4ke4WlY4k73CNhKckqgX6sY1zbLlI.UtK1FJQMQIwMmKmFw0CYhRS7uN1miSAjfWd71R6B.rjUtfGgCmEvAW9RWMH+ByJHRInUqVb+6eW14gOjNsaSV1LjZMROjl1lwEYLNKiRKr5UtJW8luJiJszJNAsUPoyRZjhoSlRbr9z05pJ9U7QAhUc4f3X+sZWLdukZ.V0wk26DUSJWQzyZEUtwCeUd4RF1QY83VkCLdAkBYUolH7+R6BoGtSD5iaCMimGJxysO8Z0J8qnpl4w7wB3U0UOX.CdQIVUANgFDmvq4GaPvwGv2T6QOw66EGewwSyC2PMEGp4cS07Ftv7bdkBIPo2AQJ5F0CqsDqIGgWP21svkUf25waKvigHmfXmmUzQHjJlJDHrdhrEncNT1JERuR6ptP+dfVgUFJNwgJClCkJJzVfCOZ.IBoG7AIYvICRHjz5Q30glLuBiHBiJl7XEGHE7+yeyeMeuGrMu2CNjo.cGzhtu1kXotcg3Xha2ITlaPgvZIezHFu+g7vC1mGN7Hd6+zuCuVe3y+5eVdsqec1pWaZIhQ6xPYy.gCsyg.OJeXtsv8aIdufHUDlYYTJMnaGgT0iYEinzE7pDkNZGmROcKDVHe1LxxGgTnnabDoBy76k9fZyKM4HJpjeHErV6V3hDXviACI85PIFFYxoSTqvlhEdDnCpFhxgBIRUbPfT8dDU3DjUcgp2W8w623N1e+YYtY4SoO47NmUe1mgeuO1dvxKAqHbyS4.bgn+5ERLDbOpz6Q4jD4BcGQGDTMqLPlQgKj8CGav3Bgzo1N0v6rfNXM+6RHCM79vNGV76TG+4Jx9oSawj7BTBEBo.g0BUK9pRSY1zoDuwE3ZegeU14fCXxcuKdmh9sVlillix4X3n8oam9rxZqvCeziHKujqdsWfd85wC2YGVYv.51sG2812l818QXkJZkFiTIvYlq16hEBaXsmmRzQzn4RyuHNVF54XAD+Ul0SUVJppbwb3+UQVNBAr45axktzkvUMQZmTMFSIau88X2G8.vaPgEowPYYn8wXcThlxCFCW5J7FekuNQKuAzpMSMdhhSPpjTVjQqHMlxbhi0UYwmrIl5TsaD7lyrWbiHGWO2JzriEkHvICkziag58kntKeUQSkpz8VJqy12Jw4SJQIi.j3UFrTBNGQVIwnw.TpcLM2QbqTTVKsLkzRGgSYojv.mLikzX8y86h5b6b6TMu.LBPCVUIAonThj3f6SfPber43imAIyHuPRgIGgziRJwgEGg+V6MYgyVUL2CyeGICELdQ0liLNCI5T7kNznv4EMKrMeS6g4EkRYP8wYN+JA.g.kDv6ZHHu26Bj7VFgGEdqCQjFoxCNe.vi2izF9sblB5qBS.IqB2nToQHDnDd7BAVopJi0DnpJ2LRurhOpRDjhPDgBKklbjRHRG3QVYgGgwPR+Un7nwThB2JKyz3H9Gu2c3+o+e+i48cfcotrwq9xb8KsEsWdYHIhz1cnc+djzJMbcaAaYNYSmw3giXxvQ3GOB6NGvsu+84a82983pe+awu6W6qwm8ZWldQd5J8P4Tj1RTh.2qrFA5nTj5TxyKIWliR6qxefR7VKQpXhDQH8NDQfT4waCRigpkkVwyWeR3TMyO6winp8rNysiRSB7kBQHxMVHVoHQDAdIVqGTZjBQnKmCpD2pPstUERXJQ0uO9vFyqyVx.eamC3OrthGuqZcAeneXSWdr3ctPeypWVoT3lkEzkxhPEJAmuJSzaxohel.r8w1CVMOtB3hxC1JvQVAnqJ5khJhJVufY.Pvu38.PMDtZznxv8jFuuo62iCdvXV4xWkW6K9k3Ge3QL8vgzasMnSm9TTNBSYNimMEUQIkFGGbzgjtyN38d51sKRojd85wK8RuDcZmxCu+1LZzHRSSIIsMktiyQq5RVi26wPkhvWEJynnHff3nUTVRb0.PmwF1gEfPICoNqRg04HVqIeVNiGNh1IobkKcYVev.DNAdGzao9Ldxg7tu66xng6SuNsv6L7nG8nv+OQDcFLfwkFF+vGAW3R7495+lrxVWCqJBmHBDpJOKUMH3DgEL.1YAxqW2d6O4NfOw8mS3MuE+a39lqZp34ux7+eG+N8I248hpCrntvoZCbSvJBDo7wO9wfojHqmx7ILc5Txa2iXjjDGwy8j.3b6b6YxL.kDn7MMcqymlQRBHjNJxmPpqMIUK.kkkQZZJP8l6B6P9YUI2q87wI8DvoMhRbh2OnqdNDxvBnGiapBYUrvjUuOg5dpsxC154TDPToeTpJuSKDg3nIjxv4mpJjdKTK+PIqzWJMNuNHMCXQC3o.7gvFhT.QQL5nw3h5PmUGv8Ky4O3O4Og+728cPtwRr0kdQn+RrzJqxpquJKu15zpaK7HozVv3wSQoDDmlRZ2NzYUM8M4jmUhHOmG912hnk2fNiFy1u2s3+w+8+Q767VeN9u523KSOcBE6mC1LvTDzbKQPrnspPhWYDV7dyBTnfpBQsH3keWfIqdoDuxE.No7npVeJp9l0B7l0W0V4E.5P1+U21IoRBKpgEUstQEKOpJ2Omnein5+6w5jTqOVmr2xI7h0YL8rv+j8yNsnsH9PVe5Cy936AKOyu4vS93m2sEanalj.BdbQnHNIEUZBchWma7puAO9CtG67O7cX2QiXP2TDtBjdGkkkHDQzJMESYAO792mrIS30ekWEJMjWTh2Y3xWbK1bs04QO7gbu6cOL1IXq7HUTTz7rMzXaJbzUmP37dJKKa7vE.SlLAcb82Si04v5cMJDuRo3v8O.myyFarAWYqKwx8WBg2SdVIsZmv9OdWdztayrrIfTv3rY3rk3iTT.jztEy7Vd7gGARMW30dEtwa7FDu5pLVGE1Yh7m+PkWjD7eTjZgE6OJNkW6z97RByOtTuN3MVlMdLK0oMK2qORDjWTPZb7S7cO2N29TiIHj9bxJBlGJhLGK58wwofOCUtC0DCoJHxJPSHDbOQoxYwe9OAjCkyZbZclkE.TE.BPcXiTRTUQ9z4pJmJN+bQqzsvZTNaPFY7gnnHDhF.BRwbfBK950dmwgrY025B1LBUf9BdAVoDcq1r6gSX8KeMdu82m+v+p+b9Gt+GvrXX00FP5pqxfqbMVeiKfPKHqnjid3NLZ7TFN9Hz5XLdCdm.kVPq3VztcJIIsHQq4k+ruI6b26yNauCaEqY+aq4+3O3efIG9H9u9252fq1oKBMjMdBdmfjHMQHw6rDIz3crPVcKZvm3EAbMNgODjHgnZi6UcLphbz7xone9FVWDfEUQZtF3ZU6Xy+OV3+2IAQcZ8Gpu++g0w44D6SL.VgGe5.sdd1DRBdxgvNc7RAXqPsKfBmmUWeCFuyN3hz7V+ZeI9NSlwA+fe.Go0zsUahbwLd7XrNBJELBxlNgQGX3G9891bsqcM1byMw6Rv4bztcatzkuLc60i6bu6RYYYyQiWrb9l5ZUsWspyFQmygVqCEd5ISvacfL3VzRSIkFSHCap9N850iKu0kYqMu.BOLd7XhTJVdk9ryi1iacq2mCGtO8WpK5DM693cv4brxJqfwJPztM67n8fbC8+U+03k9LeVxihBSXoUGSiuVzUpKNAq6L7V4Y85mzNFHoSj0gOK1o1uzG1k67ipOmvgP3QKjb3d6y3COjQCGhtzfz3HMQD7Pn+SOCzO2N2No4p59GVXThF6b5NQvIANAnJsL6nIXlXwnLLd+YLa3XhTQgzqeQ8kaAOX8ycqZQbuuF.DUNQqNpDBbl5xyEgcL4BkPFQ05VB8wWT+XfEkymSqALPE.gv0cUXjDgPbJEAfKdePM2KkJJiTvZqv6N9.9+7+zeE+w28CXkMWla95uBYwIb4q+JHSZy3YSYu8eL693GSdYApHcHjkxBJJBGVqknnHZmlR61sIJJhG2Jk01X.q2JkCdTKRZkvNusfu+c2lQ+g+ew+c+m+6x051hNcUjMbLJOj1JFcoGSYVEudXdFsKEMN52KIDFz5kwWfNNM4lUEXUubg1Q471p.P0vuuag1wZWYz78Z9JGueyy+HHd51uzmEgAyEHKcMALkhp9RBxsNRRSfjXbosX4s1hq+Y9L7sOZHE6rCSjJ5m1BUhkoiFx3QSIR3HVKocbDCOXetsojx7Lt7kuZkPdVhVGyVW4pzoeONZzPd7ieLGczQjkkcLfoM03Kl24qtzJjmmiVG7Z0jISHurH.9JIltc5S61sYokVhUWcU52oWSo4oa2tjMcJ2912lsu28IuLGoPvQiGgW3PFmfVqXlWfJMkGevPHqDt4M4M+xeYFb0WfLoBTJTpHPoNF+wdVDzSOO6fSNKuLwyvj3OMP9GCrkLT828deE+Q.MBjkFZohnPpI+nwL5fCQ0oGoJAylLi1sSeFuJN2N2d9xpKqJ0iECBjYclcPScJskRSrpCtLACKlRYlCkPG1X2OmWA4j52GT68pZWmT4AKpeIwbPVBAdW.LkqhdJR0bONg2eL8yqAPkbweyE9+234kJcephFAduag1OEVIAEYWGwr3XlDkx+a+G9Ove46eKV9pW.8FqxAoJV6hWjYdO6c+s4fCNfBqAUjFcTRHy10JLFC53DhRRmuwRqiiFMAiqjRLLX5PtxEtL5AKyi1eOZe0qhdoN7idm2k+c+8eK9s+ruIu0E2BeoiQSlhVpHRnHqLiHcDtZvUU+sleS3V.3EymuNjo1UOVLuN+VGbNQ08Hu.jJYiGxZVOnFc0hd75j22OQDQ9zJPqOQKEJNoeC..f.PRDEDU1y0tO8jO9SC1oESVuTfTo4noSo6xqRq3DN59ayZW4E3y7Ufu+e0eM4ev6yA87jljRqtdr4yvUDpr5FcN8Z2hYiGw69S9wbviOjKc0qwpquAw5XLVKqNXc51uGKu7xLd7XlLZLSlD35SQQ.vT8tWz5PMfRJkTVTDJhpUd3JNNlNc5PRRBc62i98VlzNs4BW3BjkkQwr7.w8kRlLYB28t2kGt81n.hiiAshixywhm31sPFmPIdFNYFLbBr0k3y7k9JzcvlTFESq9KQoPEJTomx84FWAufGp9YIV1mbB1eVBQ3w99mxuaHapl6AKu2iRHPIjrZ297Auy6vgGdHu1MeYVtcaD.lBCZo5I9ebtct8oIawPtzLhRPiJqKERvqP0pM6MdFydzPlj1i3zDDUyoDVbtlGt+SSzKBi+UzDSKon4hohNvXwiTopzCJ+bhz6mOYevgcyyj8Ff.K3cL+IViqFvPC.UWnTuHEBjdEXgRsFiNhYQQ729tuC+EuysHeoDV+EtF1tojLXYxjJt068djOq.oTS29cPGkvrhYLMu.egCkLBgziTDHqu.UUYvQfEAcVZYt6t6x9CmvUu3kXiW7573O3NLa5XtxMuA+ou86Q+Nc4hquAarzJLaZASKyoebBQJOA0xmpvC5OVn8pANIZx354sutp6uJjUOdNHoZ.pT0FU+3lPsJp41zI3k6hgL7iSmimirOw.XsXGvOMURRle9dBArTnviAhTjk4nc6Hzs6hMIgzAIb4jPoi4cKxwbzgL1aXoNcIQqnXhGa4TFMp.bN52uOJolGsyCX28OfKdoqxVW9JzoSGJJFQRZDqs1ZrwFaPQQQCPq77blNcZHgAJJY1rYMkwl9KsDooo3bNRRRnSmNzpaGZ2tMwwwAOKIDL9nf9aIDgLe4Ct+Gvst0snnnfk61CowRYdFEdKI5HLQRJ8foHGiPC4V3hWhW5W6KyVu3KgMsEkpXzwoUI7gNTbSWXWP0hS5b+Heb6ih2qZ9NeLBQ3Imv+XO1c7A0gIWbH7Nz3Y39Ol689RZ0Jgab4KyEWc.BSH6T0Iwf2+whDjmama+hxjK3D3ftwIqzyQGgkNASoArEH6zmkt103no2hGMImIkETXJIVG0L95IjLlE1PyGW6r.rYajuAVzEKXpxkesT0HOP9J2tTKRkdush2OUDstZ9jFNCU8o4jgHrF3ExP1vKrgZYHhFXFNTTph4s29A7G+s96oneBq7xuL6YLrZ2kn6ZWf6b66gNsEoIcQHjXbFFMZLFukj3TRZkPYogRmgRqCu0gT5QojnShPKSnvTvfACX+8Oj26t2kW5puHqb4qwCmkwzIJJiz7sd26vEWcM95u1qPT2VTLdLNbDEoIWPk5yOOxd0s2tpn5HqZaaBq5haZ1s.35ZaAmqTGw45PDJp8Ln7zwHT+Ype0ZfbOgGtDblDX+4I6SLG7VCTwehG+oEyW61x5GSnimP3IscaljkizXnypqA44XrNdw23MosNlu+e1eJr6C3HkmdIQnSagW5vlOCcTDSmNEcbBsZ2FqSx8199r2gGwpqtJW6JagP5Qq0Mjbe4kWlUWdEjRIYYYjjjfqzv8t283C9fO.mywVasEuvK7BySW1JPM0STXLNrUjjexjI7fG7Pd3CeHEEEjjjfVqYzjInMAAwpzCEVKJcKDwwXlMCFtOr5F7V+5ect1q8F3a0l39qfKMgoEVR619XfklqsYUd+qpOPXBqmzyv+Sx80SYR+ZqVMtXgWoI6h7fT3Yotc3+ruw2fW+0eUd0W5kHlvNd0QJbEVjQOeWpFN2N2NKS.DU4glv3TIJgrYfg.HMIFxTHes2fubq1r1256x9+seKLimf1GUoigKLtZAOX8IA.qmlmvZx9LB7sp9y6DzvMHWM1Kuu55JnMdAAaQfVMmBFMdAqBPkSDH5MxEVKqZhrfmsDgrMVD.54DBzDjEGqJBqJh+lev+Hu6ASn+K+BzdiKf06n.Au2suOi1eDchaSrNFIVJsVjZM8R5fwYY3vQgMww7RsV80nwXv3L3vRYtgNocHOuf2812gKr5przEuL2+G+i3B2703s+Q+.9K+g+XdoKeYtVbJF4TJL4UD+O5Xsm0d4aQsVrdOnBo.4I1MosRdNpm2utrn4YgpxQsCLXN9fiExvErORIuzmB.Y8IZVD5ppQRNmKTmfDfWdB9Cs.vq5uipJUXWL63psOtdA6zx9jisiKAHDJPXq0fyJ2hG5rUZK.o.YjNHbkhJEFtcWhrNt5K+JnEB9Ae2uEk25cYTtfUVcIzoQLdeCFmEqwfvZI1IPG2FoTvnQi3vgGwi1Ya5zsECFLfACFvJ8CdlBkBmy1HKC8WYYVMOiae6a2vip5ZBXi2c.bU.pFNLDpwc1YGlLYBSpJ4BBshYE4UCJ7nShwjWfNMl186wgYYjsydPbJb0qyu5u9uI81XSzIcP0oGVUDnRHNRg03Bjwjp3sKkABuuX8O7TH7p26Ctueg2qYB4yvCnmEwYCuNgIDa5GFR+Z+Bw8ng7pRYiZ5qTJJrVDBEgpVeXx.sVStKCS1LFrTe9u7e4+RVpamvN5KrnwWUyrdNez84141SwZjMGAXjBJIvCQkbtlyYMNTwsghYvq9J7RW9xbzlax+q+veDYk4zu6xGOr8MgQ53El8mfGoeDmW+jedYCPG2h7xuI6zDBUUXOqPCop3WzBdGIb9YOdHrV.vYPlFNtbuTugQofJUnGv6wZL37IHhSXxjIjLnO+z6ba96d+6vVWYKXvFjYEjzqOylVP9rL52cIvFJB0ca2AsRxd6sG.ryN6vZqsFiGOltc6x3YSwZsrzRKQ2tcIoUJQVnHq.sRgv6QGkfyK3viFg14X8KeU148detzUeQ9gevs3u6G8Oxl+pet++Xu2zXjjjr6762yL283Jup69X5tqta1M6YFNWX3gVhkCE0RMBPKfVfUeXEo.3JJABAAInuQ.InOJHP8EpEDbWAoUPPR6AkvRPPxkTbwxU6PxYI4bOb3L8LjSOM6tmtqp5pptprp7JhvC2MyzGLy7vCOiHyrpLxpxpJ+ehDdDt6ge6l8r+u+u2iMVeEFc2MYPZJENq2zm5tziZorfFtsy131lj3uN2LQPW+9zhZkzEtuL2kU6dT7dSie7LSqdFr1ual.axZqxmZUyKz+e76VqEqcpcLKCrzXv5v74t+jWl62Wzus9Eo32WFX51IRiYHQcFsdu1giVq8uvU5IK0psnRyHomO5TLEkbgW5E4GJUyazuC68c+NbmqdMX0t7TO0Syjg6gY7XJJLLw5nX7XbJEoIcnaVJkEiYqs1hs2datxUtBqzqOqt5pr5pq50SU+A97URYI6ryNLYxDOSUVKFbLb3PFOdr2sh6sG6s2dgOOhwiG6CS4P9yRRzTTTPdwDbNGoZeNbQ0sCE5Tt8NCoXusAQSuW9U409DeJFb9KPu01fr9CfNcwll4yCLJ0Lu3TcMsFOywFphb7T+klGTllLOCyZ5tw48bUhnHQo48euqvsuwM37q7RTVVR2P1o2DJyQstGrEO5hXuh9u4qGBPfKK+WJMPWgbQQGkBVaMd8abUt8ncwoxvhO4c1Dmp7hgHUcXNStuSl5lungUVlsy7lueWOKK4De6Dj3SKD4Ek9ZAXVW9f8Fx25Mea.Hck0v1a.irNjgk3bPJIPoASnC87Be65O+y+77I+jeRJJJ70H2RC8WY.kkk7tW8J7c+teW1Zqs7QbdogU51EU3jpzXvhT02ZgnQsxpryc1DQz7V27Vb2IEzAXP2tLorjlnYetMQUa3Mle75ZSCvNou+eR3Qjko9wW5w.Rc1gNJBcr451j0hCx3KfpH+6f19y62E9l+EHCHAdwkn+jEK3rnTJLNaHJI7hKzoTU5ugh9nRUbA0KfKAdqLMa+VuAr81b8O31zaPejztHpRDqPYoASQA9jcbIfCqym5EFOdL6s2db66dG5j3cWX2tcorrj9c8kTf8FOBsVya7W8l7F+UuoOgiVVPddtWqUt34nmoFi05eAtr.mofIEETXJQmjPmzDJTor6v8nXxPPqgydQV64+P7Re3ONenW9kgrNzo+Zjze.lzzYipjnwIMFUZ8FVm27Npolg6ErHCwqG10yHhxnHMEEJm20pdAY5PTfVIzMMkabkqx2+MeK9Hu7KwjROE9HPgojtocOwEyaKZwIJpn+wmiCLDbcnOIS4KX6hhxt8XjqjNYc3q+VuM6VTvFqNXZ62L66eOvLvxoX1Vzmss.ar8m4bL57gIGt.aW0k3P0l+.109zzh+7LKqK6NYHFmE5uBW6N2guwa9lza80Hak0v1sGjWxjxbeBi1oPJczoSFkNK6ryNXLFdsW8Gj+S9496x5quNat4lr5fALNHqiUWeU9VeqWm+m+e8+E9ReouDO6EeJRMNv5nzFKEXVrnnT4KQco8Gv129Vr1Zqye0UtFW612gMN6.VqSGJFOgzjLe0TIxhUkmdiswebt4builWuWz.Xq6Nyi096HZmx8KdfjlFZJv3EI335n5Ez4Hp4kASVwnPwgYZwGNLRGQ74BDsHTX8FZM0MnUx9CqVHa0UYOmkhtYrwy8g3iswZ79O0E4sd8uE7tuCiJ2CDegidP2NzyBSxyoHeBiFMgrrTDkO+YEOesVKCGNDq0VU7k6jl4Sk+AcUcyadSFNbHc62qRGVf2fA+1w6xrNc5vnI4LYRNhRgJMgNc7tcLGXms1x6K9zT3oeJdgO7GlOzOvqxJm67LIIi9qrFzoCFslRw6BAq0hXs9hhcciWTwDvGdWuFuGWyMcVo90+iO7Mty9rXSBtPXQ6GOsw9jmW760gVqoSlO+l8c+1uNe1+c9oHSmPQoOWkoSSNcL57VzhiCjoSpqIQOqNJ+ftPPmsBVL7V23J7ceq2AT9DirqXwuEbRGjSpfNnbLmAX0v0Qy+3QgMngrEw.yAgXsK0XJQ0sKpzDljHT1Igqr0c35.W3hWBWZJEkgHUzVSlW.CGNDcmTxxxXznQzqWONyYNCu+UuJ+5+5+5bm6bGeNTz43kekWg+V+s9ax+c+27eK+R+R+R7k9BeQd1ycgZt0zasrSYvHNDQgjooPqIYkU4VasIu0UuFuxYeMLhB6ALL2CiIqmDvgm1SObrzSzn6e4K1XpixMvlrQD+c229vul1.h9T1oTPLioGdH0qKOGhwfn8VxaTfx5.mmUHiyBYZ5dt0Q2qCi1YKjDMWT8ZPVOt44uH698dCHeD1wF1obHY5TRDgNcxP4fbSNtRGSlLo57Rq0dQuqRwLonRT5iyyYus1FbNVY80YiydFFMImX88y4bTZLXLFLkQeJaozYQmjPRmLTIZxKJXRddPclkvEeJN+KdYN2y87rwy7rz+BWD0fAXbJJyxvpRnDKNmWTmfEkFRzJfYMtp5Z8btucP2aeXAUroNmff2smVmu.QmjnP4r7m+M95biqeMdtm64XuQ43DgdoYLwTRpVepWnksnEKDJ.wGGYInvD510IVeyJVgI4kjzIEHguzexWgqesa3yv61SGO3KLaeC9Y5+tJL5Kmr+9atWFr9bKiJ.YVGiFmCoYHoITnzb2xQ7Vev0wBL3BmmaXJY3dCQ50GcpFaowm.WU99gRRz3JUjmOlgi1ig6tCe6u02h+o+i+Gy0t103U9AeUFMZDVA1a3t7ezOyeG9I+I+L709JeUFU3kpfnwmsYBE3dmHThAqyftaFXLn.d626ZT7QdMJrNzI95wpJF8itlmexRSBDyqMRWsc4Q8Io6ECfuWvT8XWw.vRY6tz0f0hbQ3hzf0Awv.bvLPbTPye+7z00TC1BrWE8WOVzJPDEj58yNXIozAJGFQXTwD5MnOcRSH2VhwAc0I7zYYrwYOGWcvpr8MuAit5UfstKSL6xDEj1IidYdCmhB6OxDkwX7rDIBEiyqX2prlOyGOdLnDuf0YJANIh20foIIfVQYYoOWvnTLdRNlsFAkEPRBLXURetWjK7Bu.O8K9xrx4NGxfAX52E5zAURJE5DDwmqqzZkufXqbHZPmHXM9QJMUnn0BwV7r.179vxzvpngxM2jQFrNTFRw+bZ8XALFJxIhhzDE+k+k+k7m+m8M34dtmizzTecpDnzZHU2lKrZwilvW60CYxafDWTWR9mushEsJT7icvst017U+i+xTLrjU612WjbOfd7dXv.xQk4kkQaPJmu73nJsPoAahPtxwGr2t7lW+ZnWIAoeG1aysYjEFLX.JQgQJvE7Ff+ZnOWGlOdL1hRVYkU3BW3BrwFavFarAqt5pLpSWdi276we7ezmmO6m8yxy8bOGqelMHOu.sHHJAT9BxrUbXbEnQSIknyRoXngdcFvGbqaynwkLIE5idF2BV+5xSJrWMOWDtL6e5AtKBahlZj4fNYmmQapCgFu8cwpgAft.qEUETxJisrUTPqUQKb8rWojXFANkQtILpXBJQHoWeRRSQxmPt16FtW5i7Cwtm+BbqUVgst90X3le.r2tTjmisXBIc5hnTy3hvxxxJWAlDdAL5tvyr9FXCYt8c2cWx52yOJiPXIabNOk0BHVepanvZnbxDOaUFCzuOabwKvpW7Y3hu1GE0paP202.0ZqhZkAHc5hQmwDDRS8OhjHBpDENkCm3Pg0OJWQWwXUSMWLOiaqdaNlDRWRuGGiXwp8ia57q+e7HZQQjH3MzEwaz0ZqsFevMtIe9O+mmO8OxOLO0S+zThiIThJssPHzhGcgS7k2YEPhSgXMjDxf6EwgsIPVZBXf23O6uf290eS5TJzsepuskhvfRY+sS+fnS5lFDTwXUb.5pYcQXSuebXclJG.KchCnzRFJbVGEFClrNriYBW8tiPcg0I24cWmSqnTb3bE3z95T33xbJJJnCcHMSyJqzmrrDb1RJlLlwi1yqqVki81aOzZgKe4mmM1XMt9MtFu20tBW7oeNJEAQU5cMHFbhEmnPb9nl1nfIkkLHIiQ46wV6rGO64VCmqrJB.ir.VkM1CWqZ1+5QkQKoV6uGFl2ZrufK3guyNtuvwpGh4cN6HbCyY8UMaWjxYmOmgfO8.nBhqbpnrmy1x4lI2eTe9USOrWPlivlmY4t82Iu+gLeNvJZPlDh1Cw4oe0p7gBbujUHuLGq0gtSJhVSoAR52mMx5v1tax5OcJqt55L9YdN17FWkO3ZWk8t0swjuGl8FFD.gFRRHIIgDUBc6jgnbTLNmDk1WzmcTUuBEQnWudLorrJ0CDyiSFqiINq+oxwiCwUrBVcMFb9Kvkd5mhK7TWh9m+B3V+735MfjACP2sG1jDJCMrp0I9pJuyWqorJwW.UcNLNKpR+Hvb.hKj2XPlgEqpqygoZBIxO2wWn69PLWEBt7v8XwhEMZeBrweO25P4DulRXpVvDUHY54rXChaO9bhEuHXO2Fmkadyaw25a+c38t5U3od5mF.Ft6dr9JqyIebxzhVbx.aio.dQiKSmSQgurd4JfW+0+lbqabcrVuLDbAoA3TdsEEaS2uM8C1SfpHztpy7kvqLKpC2pzL.hOUvTa+6CXRYlzIfTo7r4O0J9o9.gY1ofErFe6yHPgiz9InkDFZf9c6xvxBb5D+0PmihhBuaBSsLdxDVoWez38NQQQACGNjQiFgnUrxZq5qaroob1ydV9I9LeF9Y+Y+Y40e8Wm+Q+e9+Em6BmGIMwed501.hy6kCsR4K4MIIL1X.aIogfXX2gCQN+Y7dLIQ68zvbtlVWz+QcdEuF2LsLTeZ72BKuVGWz94v9MwUqZf0QdRHD7r099Ln10BIXlw8SFObojI2cNnDemvNkCwYQaLjHBIVENkW6NNE3RDOaHNPYC4XDasrCaS5JiUsanJQkUYTUvfMXd5.yQLOo.0tw6bU4KImygyT50ZUUCCdwBJpPkROvjjXcHpDjDGEVqejMhCmwQpJEKVrFKXbHoZzzGUpgUe5mkhQiHemcHck0YiydAxdlWlst6cHe26vtW+cobmsgs2BlTPYdg2.GbfwRZmN9potyqQrQkSBZdRAFHUow3DlLoDWQYnnaofjLHMCFzGVeCV8RWjybgmh0NyYo+ZqR2UVE50kjUWCR6fJMAIKAsNAsRAJezxINKNktplHJhFcn.OaEU0HW8tIzubI7mCvEhf.mKTJITpJ2Fhi8kQeWzz4CEtRPzhOugor97th0G3BFmCEJOU4VAoDrFuKXM1Bl3lPhNwaHMSSXqZsmUNqUXmQiwkjx0t4GvW4q8mwm9G9GAEBaLXUrlxfHfaQKdzC9RbdncSLDR+e37IogvJ4YZ9C1dS9KdquCiri8kjqd8Yqs2EUZJI3MjRWaPynDLIBVkub0f0ReUJCGOhTkxmTMixxvc.ccsfNp2+XkmschXf13GnTnu.B0Ku.S6Rr1C57WM78cH91xPE7ph2PJmOEi6MYSDeopQYwjpvNYBoHLfTxGZvr8D5.LozgNzFRWUW5qRnvlSgsDmNgRcICGMhM5uFixGiHZR61iac66vy87Wl+g+u8+NkkkU4BqW6i9CxW4q704W7W7WjW+0ec93+veZrcxXq6dG5JVVKKAyX+8xjtYXJsTXEjjTJzfqeFC2Ct6vcIqSGj7gAon36ua1Kog9sMVuWbjfgzNEtfwyVmKPWRPK1y49hHGrYIKxNo4YDdL2rolY4lv8vo8+67BCuhbDkyaXsy46S0Y7Dkf0+uXc3rNLJuAnkhir5QbZbeW6.NlROpl+BvRwGG0srM7IzA+SqcNJQ7kT.0zxRfxpprv7AMG.yzosEhIiNGDhJkvKUw0GMhXpEwZhulnVazd9QLgunAaUf1gUzj1OEmRiUofNcQsxZ3VcHt0OCE6tEab10obmMYu6tMi2cOLixob7XriGA4iov3.mw+.i0DOfBlTWBlvQYRJj1waTU+UHckUP2qGm6odF5LXEFblyQ20VkztqPRuNjj0EWVJlzTHQCJcPtidncPPkbd1cpKjcQ4iPEzgLfenTKLsY4ok7fFtOHd3CKm669jy7z3exfAkDBOam+3QBrWobpZiVOjApEKUkO95KUDrhPoyQZ2tTZLbiadK1a3HFzuGJ7QZXq.2awipn4yt91+huK6gAGkN3J23pb0adMJrEfFJM9jErfOWvoidivMkCnnf.pyfUjEq3700KgFKQTwfRXyWeZEqLUqs5Pm57VMF1t9uaPQgpDDHszPBZ5QBYNexn14TXLgT6iy4K0MAllbFKXcjklVE43VqEiw30Lk0v2+JuGCFLf0WectycuKe4u7WiyctywO2O2OG+Z+Z+ZbqM2jNabVRzBYnQLVRrg9kl3PzdOeTTXvV5k.SIfRIXMFnnjjzPR9tw0uXpzIN.XCt.SO0RP2LURHyb8tNVht1ad4ox6oeeiski8e3EqAmvrKWZrxUUQfC4X4QJQjbXZ4ZdhWedQb3LeOtdy4BVScgIh3skvUqO431KvNSzslhyVYHjJMgLaORRxnisjd86yfACvjuFtgqfc34YzdCYxvQTNbLSFtGi2YOJFOBwXQrdQuWhepq1NuSZFIIYjjlQRmLR61izAqQuUVAc+9HY8HoWOR62mjt8P2oChNEcZFjpwpSpRbn6SaBRrlBNs1BVeZLp6pGMNVw+Bbk1HBsp1rfPurZRstFLhhSeYBiwvfACXm6dWd629s4l27lb4K+Bfy4y3+stHrEOFiXv07Nuy6v69tuK.jjjfwXpjuwSxnvYIIQwjxIXkLRTJRPHAe5avV38Phqzgw3P6DjRAUtiNkJLtBTY95G6VasEhHblybF9leyuI+J+J+J7s+1ead0W8Uqp3I+7+7+77e1+o+c4xW9x7e+uzuD27t2g0VcU5HBlI4gJHoihRPoz3bd87JEkjl3M7aPVJZiwy.eZzr24i58+Mu7cVKNX7HgAVGX5enw834k2ShT+Ve4UKilFbEmzvUU6aGENNzpf6BmVJV7idy6xrRSfojDuOw0nPmlfJMASQJSRc352mjUKvUVfqnDS9XJFNFS93fk1dZMMNKVaYUAIFfTIsJsNHoonRRQ2oKI866m1oqe9c6hJICI0mpFT5DjDMkFGtjPtyRlZXU7yh1ebKgRfiSInCFiI93ZsxFi4ldFpY70I8KjtJZgY583io8OEEELXv.tqwv69tuK25V2hW7xuPUQHsEs3wYnUdWkeyadSt8suMCF3qrDSJL9Hf9I3WChrvYUBNaoWaV.I3nK30RUgO4OGSlzZmfxoPOwAVMtf9qDYZ8FLIIgQiFw2467cnWud79u+6iwX35W+57du26wEu3E4m3m3mfO5u4uIe++U+9b1A8IQmRdoAknvpTTXs9rLuSgyXQaMjXDx.VsSGxbAMzcHn4.Xi0XvEETZsXVbp2.qCK2Y0L2XMsi0FVZ2fYp5IszYX4plAVyXHlH3pwbUrtEhn8Z3Q6cWnix.8vIdBTUFbJeXzJVuVEDqOiwqSD5jlf0TfsnLPaqA6jbRmTDx36Fu9qLVLXvhKX7iOBOTk1JCrTIod1kRyHoSWTYYzcvJXUZPq7tpDeCBRfpZch1erEbAHtZLUIBJUR33OTiAiLcERNXRf3emJHdzZu7U2.q5FzdRfY0f2xyPtxxRuP9cN1byMY6s2FvGogVk8Pih0VzhGkgCeMFc2c2k77b1XiM.7O+qSzXKextCVmJJ6.Gfus6dIZNWutb6QigI4fEeDFZLjnRPLknKLnPizqG6NZDVqshYvwiGyJqrBu5q9p9r49pqx5quNO0S8T75u9qyW7K9E4e6ex+57puxqvu6uyuMphBDw68DqnwIZrJGNIgByHLkkjYbXlLh0AVKMCcoAkRnz8naD58n.N0afUcb+3hvlrmLunQbQVhOahnKtcpuBDLfX55SP6N9hDpCqw3ESoyGUKX8QfmUAFmP5fA3LkHEFjxRDiELcQWZI0VhonLL5AuwUVgfVn759J0Ijpzjjj58WtHH5DTIYnSSH2DJFpV.rAgJBfw6y9rzJW.ZADQGD1p2nv31rdMFb1aJpYLjp40s8Mui0Xfwz...H.jDQAQUPVr72COrhmz8FTJkOBNAetpIOe18WKM4s3wXXCo+k777pbymwXlJj3mvgHhuPBKBJQQ9jbVKMim8bmg27JuO1Q6gUmgIofwkEjkEJB0kdWst6vgj1MCiwvVasEFigzzT1au836889dryN6vku7k4Mdi2fs2dadkW4U3i8w9XXrvtacW5lnwVLgRkFDMS.e.PghbigQ6MDadAIVGS1aatvJ8oepFS9PRrVDm.r3.0od51v2V996GsEKFOxXf0hXxZeBntgKBq6hunAW0KsLQCGpyLENezu4bt.qNDdPDnoOnaZfkNwminbFDwgyFGYgMjZ.7hG2Y8kfhIwnRHIwa3j0g1lVEkhZq23pJJZE6LQdmT3PTZbpDeDP5hLb4CIHcZp2PIQiJQ6ElcH4AFuVfnvIJzghWrSBUidQUynHUXTRAVthtHUIUhAbFlpjYMtZZ.CbxfSJMXkkkMSBdM9YIpOs11YZwiwHpAqISlT8cq0WiVsKY1heTDwqGoZMJQvNJmMNyY3EtvEH6JuO6MdH1NNxMIjXmPtzAMfyZI0oHOOmUF3K2Yw1VRSSoWudrwFafy4X80WmjjDVc0U4W3W3Wf+l+6+uGewuzWku3W7KxZqN.mofRaBtDeI4QbfU4KIa6s2dnKKnWhOGO9rW7BzKMA2jwgacGbTP2jzBXVMYcX5v6IcCwdjv.qCyMgGDpXfJZHSSFrNf1GhFFT+AqYzQTCsdUYbmR.SvPEsFmx4ixlfgcJmCUXjgSlLwa7iDcCoEm1WW6iDj3Pg04YhxheTkd2dJnEEnypRs.wHcTTJjDOcw.ACIUXcBRHyw6bNjDurHqbQnHULyEijj3YrklWt1OiUK75+CHrrYUJMMsJ2ioTpprtuWj+BsVX0hGmgR7uSEYwE7uSmnSXho7.9kOY.wAh0gRoIQT3JmvfNYbw0Wkd.iF6yoU1BM41B5KFrhAGVzhvJqtJ6s2dLYxD51sKhHr6t6xm5S8o3W8W8Wk986y3wiY80WmACFvktzk3q909F7K+K+Ky67NuCmcC+xUgTKjQr3vhHJJJlfc7dzwMgURRP.t35q4MvJ2UMP8CB0kRSSFrdR23oiBN1FXYqEMc97EkEMy1wZcFLlGVTmv0u4Vscpo0p44tupsW36wH6qIc1wsqoVpOHFEg0ePhF6K+Ckh2UdRHTbc9LrhmbGwa.jJXXiyWcOcVMnT3bkACj7a6TsWaWQ0h5RDDqO6ZTOiHGOOLTaLGNEhNCjDe8yKXNjU.DEh0GQeHhmAtPkmOlOp7BAyGkfhRED8nphcFmxWzniLYIUQQ3zqOUu7om96pe+zFipxv7Jc0LPIdbbDednNhtCTBLHp05PRIQpxp8VqAjj42Pfr+HDEnJToi6ixxRRRRptOjjjTa+2hV73MpGkvZslwi84Aqw4SpZKrd6Tw.L4n5Bw5rN2TFGGEFRNJ3fBxl8yPSyisCtsnTRXXwHR6jQYYA3L3xGwydlM3kN2Jb8auKCVoO2c7dTNJEcVJcTob10Fvl24tzgNHhDRnq9xl1W9K+k44e9muJ4iZsVt6cuKW4JWg+I+S9mvm6y843JW4JLnquTqgBFWVfRkfUoIIMEaYIi15NP9P5ZKYxc2hWdPJuvybIVcPe155WkUuvE7YA94b8YdRqoJME0XclU+qG7066UDGTaS49DSsEIZ0LO+U4mImCmMjjycSSR3pP+Xhy9.oM7kRhFcdXFiRNDpjWzI5IE6Gy6AoCSOVMWWQ74GJTBJ2zZZWj97nIkNW3A.weCWExYTNwUE8gZBtZy4STotHKR0Ndl9.jd500fq8PRBoHY+ORwTCXj3zfwLdcp6MTJFELHdMWEcqmMX+TUP.DVGAZj+XludqVlXQMxMCc0MtmMOWS1hVzhiGlqdJeB9ErpAqZbTXMjnUjkk.kSXkDMehW7k3ac6uI28N2kr9cY7n8fUWEi.2bqQbl0WyOnsvfmGMZDe8u9Wm25sdKDQX3vgn0ZJJJpz+4vgCorrjyblyv4O+4YmctEIYoHc5Qg0W6ZEQvNdLlc2h0vQuI4jVX3S9ZuLmakAr8lax4N24XX9Xjt8dXeY7wZrTLvpNEh62vpoXZXzeJ5sxfEvUZvJJR5vHwPEXXpV1HGnJ4aFyFvgx0NJm0uMzlf6.ireDXJyJfXCYKdCnDzNGNanfun7C7ya4c7fTBhMEnlAVVTfnCA9mdFCQpafE3qDOJkLy785sZZzARPOUdAal.xz7fkqxYgAF7psuZZzU3xm+Hu55U7L4d2HrliLZ5mEbUWWlESM.6dd20hVzh.hu+zz3pXaHlmnySCJzVgDUJVqCSJjjpYb9dzU2gOwK777k9K9K4at6DN+Jqv0GNhO3puGOyy+hj1OiItBJFUPRHJvWas0HOOmqcsqUk5FJKKoWudjllRZZJm6bmitc6hRoXuwiHMKig49T5iR2AsRQ9taw3aeWx+favyLnG5Q4b4dJ93W94YsDM1cxIYkUXb9jC0.f8O31S+Zt6zz.qO17uFMvpIpy5R76ya40+9ra2k2MxCZaMSCFKfojE8uSIUY2741.Ti+0hZte2Uss7FLQPT594k30nkJo5+DUJIpTRU97lhV2jcpfAVwDBpNXDUXTmw7YUUBDU6+9LiLUEhDR09u9LkYqC9Z27tNdTuuDw7dFZQzWeP6qVzhVb7v7ZS6IYn.bFKooo3.l3JPzfcxDziGxkRS4m5i9w37.81ZWtjNgI251r0stA86jPd9H51yWiByyywXLjkkwpqtJm8rmkKcoKwFarAqt5pzqWOxxxHM0WjsmLYBas0VT5f7hIXJJIQoQUVxd271Tt4s3RoZR2dG1.3G+UeEtXVFxnQrV2tr81aiNKikfI.OTQS6L1GKqOjwCcQtOuNGOJtq6nh31XFFWli6jbQlihQKX7F0zUb142fAGGDX6ZJCX02GhSGxUB1oVX6vGAfNgnjEl0O2dsSIfWiV07yr2.mPIYPb9HyEuqH8j237rqEbioKX3jUKAlqBGW5X9tRMy41z7fkWaVpYhnvZmWQi1hFc035eUlcu4MlPMN6g+q.snEsnE2ennvaTToyvDqgDskrDnXzDjcFxe8W7k4M+duM+A2YSRGNgKljwMem2kApDx5uBYCRQBQSXdddMob3mW+98orrrJBCi5BUq0r95qyt6cW50oOIo8nLufQ29NXtycXMSAuvJqP93b9wtv57I+POKClLlLKzsaWFZERS5P4rk56Vrjww1.qnQAMQcKHiZv5vbQXSQseRgYLNhEvb1BLvp4xqagvrFxEVekxWPIwWe6ThOy5585nFqNlFFDuaDEuqFE0rhF0awlZZUD2AJIrcTB9ZScHsWFpj5wxTChO2bEEtdj8M+KyREaWyvLTsxiSzft5WipaX07D93zqGyNBo606s62sfSuNW2Pb+wwTltNU4F5VzhGCvrhY+nIh8G2gTEbRJbFGNkhrNIjMAL44nMC4+fejebt0m+Oju1c1hytRe1y4Xq2+5js15XxczaEOCUw.rotbaJJJpbWXZntENZzHeMKTKzIqGcx5v1auK2952BYzXFHP2I4L91C4k.9a7I+3bNELvXX0jLJ2aDqu5ZLMaY+nKVTa8w9mVtYEw6crzLvpRD1Mbkzg4hvE8NZ8GxNtve7rXi51+5RkwCT67Zl0oh4qPFdGaHhWB2vsxTcVQrliSk.qb3vHNLRp2cfQCrLwHfHrectpPAFT3rdcf4MzxhUUFhLhZFtnjftrjpH8IxXTU4+VAwba0LLPISYtxqyBeLKZow8VzyZzoLMeXcP2GhkP66EyrVjKnA2Tl0l2xa6.nEs3Xg3qV0aGu5e69SbKOoAcZFk1RLZGjnwZKv4rzKTqV2c6c3Ue1WfO6m7SyN+4+Y7cyGx46zgcJlvnMuKEkvnIEr5pqRVVVkQVw9bFLXPEqUwbQlRoHKKirrDFObW151ax0u9MIem83LYcomSXEGbdfexOwGgWXsUo6t6POmiNIvD.sSSItC812ihZv5zDVJFXEqf5pp6WgP4WIXhcB6TnsJu7drSY03v5TdYg5tJ7.iRvJVQBFKYiOjolowFuK9ll9DjfglVrHV+fCTNAedUvFz+To2.Js2ejhygVYAhgZpffKTPm8FQ4CuzPTGBUgsrO34Bgkpxgn72JsgyCQIgx2SH5+pzKVnz9H5J1mhkJmpnHjf9BDwm77HlBHpewxBTaa3unsuLCkxM0cj.gyknmKO767KhwqnAV0MlO3kTv576Wmqxcn.XeDezZsnEmzH9947vzfqY5fiEm3Srkw2yDpZ+HhXa.Qd3sw2+eLX7OocyXznIfEzYZJKlfMOmNjRpNk0N25bsu+ayG6YeFlj8oYuuvmmhcyIIAJ6q4tasEau0tLtWe13Lmg9qtlO0CnEzIoLbzXPqHMqCDB0HiwP9vQr8lCY26dGJFNDIufmoaOV04vN7t7gHiO6m3iwm4i7JXuoWyWx3bLSJn2fUXqc2h9arAVqCq3PiBq3P4TORV5b1eZbvO+.MHg0x2WpuGMKSGt+rP4l58JG9mUmMz4O5GWKmnHz4sgHwVkNmnPAkJGYZIT.LkJ1HUZARTfViVCB9LXtDzqCL8k3HbAVe74dJWkquh6v8cNGtXGSLjUhRmYYcS6zg4EFgFNbgbjgyExx4to40JuwUNeQPVClhxvwafUKk2HIIrirhOWR4O903Ded8HRsrFedCyE+SERoCNuQBMMJX+mm9RbicF2dRkgOpZBq2WGA0dszK5faJ8QinKrbBtMDjvxBYf9HKWpoZwRBWCUhOoa5IUyeuQDoJ4m5a00U84JiGiF7Vy.r5G+wOW+9yzSa+1x5.Q4.w5S1dg+0.FbX742drJakQ+hxgfErF+Z2PjjySKYsnEONh5smDeluoQVIIITZM917R7QwbRRhOGwIJT3nDeMNkDewrWKJTVAmwuOr.EZ7qGPVbv307tv77lvCaW8ePCFW.JxGQhRvhPwDKZIAUVWJKc3jBxJ2kU6KLYzc4Sc9MH4G8Syu+27qye4NkLZ6wzuSGFoRHe28Xqs2gOPkPxJCn2Y1ftqrJ6kOBzpPga1faRAEiFS93gXGOljI4rQRB80ZR1dSVC3C2cU9o9v+f7i7hWF0stCqoRPW5P2sCVkvHyXRVMibxwpR.TXBV.6bxLs4pTM6wYw2OZNX3kw8t4kldlwaYQhOT1JVUsVK3748JbVTAioDqwarjyfJrdJmCeu2tYM3WpbyyTMK23XySwvAiklH2Emxacnyui8ufpBSEzA1jqXABuQT5f4EQrn7dzIEl29aJaW5poSc2jKHSJmWdfwBmL0LRKTxanxUhg5uiVCg54kySOEZRllZGp8WzPxC5ZgOeUUKSr2.wBub7bRg2Pn3Tq.tP5cXpQX0hRn34B0zcUbeIxzVNiW2bSs3O5xxlFJAKyQtN8QbIb4NtskZ6i3HxlVTVaQKZQSzj84HlmNWhuSopwDUL+4AS8pgkZLX4.SsloNH1xdTAQ138Li38LgSTTJNTZnzTRVpBa9HRKlvqt95r5O5OJe825M4K+12j2JOGM4zEEcRyXjHLbus4C1YKxcFFrwYnvZvUThqrfDmkDmiTkv.bblLMS1dGL.OEvO5EuD+HuvKvqblyvfQC80zVwhNIj7pUJJCCzzFtgKg9okp9q83zPT3c+B+yjtZCZvRj5FOiTAFrhcZcDPchrbGQeu8.KJBiQ+1BW1CwalKRjbyL+PDBJzHpDiFZD3+dlZZHfhYyzrNQpxpr9nKr5fv61vvmqhVvC3316FuZ4XpEbMbFQoWOcM.nz5PFbOrd5YE6d8s8TCqjYO+qsuOrWPmmn0aQKZwoeLaHwKUyqwJ8Pm0oGTnRJEpoFPVkTl8MjiwXnSm9jpJIe2gzUI7Rm8RzoSOVai2muva+84F44b6QkrcwXRJfAJnSRJSTBxncvfAw3YfQaLj.nwQeK3LvK.7Cb9U3ie4WjO1S+r7zYcPuytLZyM4r8GPRnYaWjwGDbNesl0E5nZd5l9IAbP1krLvItAVUtyq47p84HiPyD0gOj578fz7STiVMifsY98pYe.UojYMvp11AbHk1fwNw4QkgVGECOsxTZJOLCrZ9uW+Upo51pgwSRk6Bqssqy1Usu27527XEr42itnrEsnEmNwA0WaSis1+7e7G1PTR6SRz9fSJRemXM9BhcoAkRypc5w14EL912ky0qC+Xu7qvy9zOMu8suMeuu+6w6dyaysrvHKLYRA4.iFOAAea7I.cA5.jAzG30tPWdkKcQdkm8CwE5MfrwCoX6MouNkMN253xKB4PZEFwWaZEzXUJvomoa1mjMxZeAc2R57+ABCVyDIg09797taSir3jmkilr+TE1s9Yt+iw.yTQlpjnH2kZZ4w4EJWTj0DMdxMsHO62XVL5oBHul8UAHGpgl0W5AwXz7LvhvnZh5YqdSk1vHvzwzrPCCup65unqEqevOCiW0NVh545ImWeaQKd7CyFUg0Z64IHiqhvIJBUi1pfQxJFecALwQQdNZqPut8nWVG1ZRN6r6PjQi4k6kwyblywGu+Jr4KrG2d2gbis2hO3tay1CGQYviGXgNJXkdc3rqsJmc8MX8tI7Cboyx.kkLbn1dSHOmdHrZhhDkkPAoEmFeazhNH4EUnuK6B623Is6iQTWO1GE2.dP3gpKBq+RZcCqdX65nYDbcMCFpaHUkwFy4FfHRkntq+PpWWS0nYWzUeW7JROniI2znsKH175QeWbpe+u3i85eedFZ4SWDKl0oYl2bWm4yd0QEdCsdx7k3VzhGUPcFoluKBq8du6A2fiOM.G9x0EwTzSngYqBeZrHUSpnfBGLIGmNgUzInPXTwHJGsGqjo4RY8vd1yS9YcrawSwvRKEkFJrlpfhRqD5nULHKitc5PuDnb3cQYFShSnSRB8VY.YhFWQI4aeG5zaEezjG92UMH2n1m2e+COQc+6QcWDNObXtH7gIbAW5oXZgD1ufYMtpl74lF0ELUubQhebUQ4XnBfGYKp5RfJH67PJVHbCu56L80flSahC55WkgTg+i6dkRW4lNQjPL2M0vw3ie0YYSpN9cAccMmjCaC1qbg5b3AYLWKZQKNcglsU2bYyqy3mjbQXr83puGk9.PoRQRp3iX9DvLbLESFgNIi9YorZmA3FC1xILZmsYbogDmi0RRX8zLjjTLVulXUhPBNvThczPrauMVaAqMHwGzRhfpzfyliIIgjNcoyf9PoCWHsOXUSk.RLXj7sQO68pmzXuZFOrwz96WF3ghFrhyeQcw9vTCV.KTqUMYiZdraArv0q455CC1oYLFIbdGmygMsNhFjoko4wJK.V6LyWCyr75kwlXjCVcM3HfCSqUsnEs3IH7jS+xd3TgjAsEEJL9YBJvJNLhhIiFg1HzckdnkAXxyonHGmHj3LnEG8yD51IiRDe8EzTRQddUa8h0KkjDEjnE5j0EkpKkE4jjkRRZJDbmnEGkNGZDJS.QK3TZLhOJN8Cnt9v2C6imfa6dVirVdXoXfkyERNlMGsybD.ITyJ+ltOq1+Vo11gZLC0TT0K33IhoIlylIhL+5XbSCd+JW0QLpPpYDX7XKbtFOei1GIgOHgsiy3WtIjGtzJYp6DihcO7.dkgZyIxbNra3Umeg7OkM3pQu667G25fqG0JeLj3DOuYdRlTUYZA+OUpN+mdpWOSuO69OQmLywY8iVmygjnqttD2t00f07bwZ8s2gw3Uc1DqBhfFG2yrMaNp6Fa24Mh755q6II5yaQKVT.xHA4ADy0cUsSEKz70FHo0Z84MqQCIoWB4449DzrwdhOP5ls6GmWykc+BckNYcfnvItPVUx2VYNNz85BVH2ZQYM3REToY3vxDk.RBNqOaLUh+2KNgLRmo8esO45.HjCncPV15HNEFI3lxDohspRAu9qDk2kkRveKJ74EpZUii5WWpi52aOpsIWGOnXBap9m2eobpd+BUy+vXYUjE12z8BdnWrmuWvC5N07Mlrn4GeHa1kMSjBxQ6gwlFAT+6wDk5hP0CIRMl+BthTPlJ97XRVslANQWBNuiiCBOoQgbKZQKNchG5sCIwrCNU49KDMVALXQIfS4vn.kwKcDID.ThCHKAvhKLOqKlepb3rSamUKBNrfjhMHLcqSQIY3KaHBhFrZeNVzEyyUJoJJwgfxMlIi.dXoJyVbbviLFXsPWQsDeAqNaHULYELLIlL8lpwJeRkKtfoBWmfQNdCbjJ1UlYGMkEqZEC446N0ilQOyruC6i5r94pMMtN9403ErXZYn1ucJiSwzIwzkaab8OdeoJ5LiGesD9zhVzhkLNLCrNoGTtUbgD8bP1EUFY4MiwFqUsNPkHf0BNGJmusTkzwavSfUEUvkiNmmQpY8xiCPgdZmSXnC1pRSmKTRz.arCqX+S38Ng2HsZ4ewJE+1hSBbp2.q5oCflyaYs8OLMSASc82LyS7kckEIpyYnTu1hOptYpoFslGTMdIr99ElkAr4wl1bEmdisUyyklmGGkykSJT45tfzRmIZmpaIXKZQKdrCOrcUerbhEU0pJNXTGdlrvTMvcevFp.qMDI3ZDM9ZmJfioSEoVaXL6fjie2JB9BAShudzJgxAmxaQmSDDmo10HaX.0s0j0GT3TuAV0wI0KSM8MuWaNypAKSny5pziPESMt.aQgQpDq4dA1jD6zThPEbLMOZUWuU6+.6.Otqn9sVxMs5ZT0KhyZf19XBLp6KY9KO9RcSswsniup5VX722ZfSKZQKNgvgM.zCShEGG3BtATBMlKV6zR0k3CVIiUEjqAd2IBfnpbmnxFXyxQPr7ypE3Ypkt07PgWAugRbiJtN362hPTvCdcVEzYrRzUcx3qysmXWZZQ.OxXf0hbQ3xLOJMW1qZv7jzX8a9YkZ9Lf0b+TY.2AXD0gsbgPFD1sXgadXM.UYJzBLvp4712xcyecmGyisnEsnEKS7vjAKmXIzBr2MeJA8Ts9G85Ws9LTgnMTpbLmKpHpfbY0Nqev2gAyGG.M.RPOUw5HnCAQLdI0W4okoxSw5hk8LMJr0ZLtdjqqZGD7IHN0af0IsKBAuk70G4whx2K9+m+wPkVkBqiJXzSEyPMcQn3OolWTNV+6GEQb5pYG3TcVM6Km662TKp+py3Tyys5LiMiVtNfiqix8m5EK1VzhVzh6G7vVj6FE.VztXa7pnG57tOzFXYRhpXU6KP1V+xySitTzfxAZmNHZdOqSUDvEXvxpiFYoQb1PsIrlLPrSSlOZYpDRr005ak6EnUBEmvXoafkxMeVKh0Lu38zSK2WEQPc.ujNqwUMJKDL0Pn8yry7YTp9uCAbApjmqaJALFyAd7qTppTJQ8yoEEZx0WGnFCVMl+g88ihwe0CZf8cbuTZWrVgzN7WzkqVlut3lc7jspQnEs39GMkSv8Zq5dNbNN3gsAVUvEaGNzWW0fVitqa1qNNgot7S.PgCvDkYRHiEJRBVwhCkO5.E0ToYHBVCHhshIpYp3GXfPRHsZv+00tg3drj8p5mRpJCee3fikAVwGHpxYTANQSb9rrAwarBdZMUQpSCFQrfSbeGlyZbRL8Bbuxd0hdArdTHtu7cQ88glpbdUSiKLVKoRHWkDCriJlg7BUTo2+wgDO27q3rcx27zqFCTMMZxafPzfrFmWAXMUbaM2kK5CyEhSgy4pzNVbYlp3KbVlvldLNm6W2COveX2s8hLUUIOzX.EHhBAMlRKj3BAuSLK4qQitpQr5meU+WS6Fw6+KJXFZQKdREFbHNAaH2Wg3SpkQXs9TJPQQAZsl7IiHQkfszRBZrtCtS95CjadAYz8RTDtnAYd+BwoPYBTF37LQEGNbYbepi6iodp.olS5pzaU3bJ74pizfAURv8hVT0Z+TvpR7CROZXGRXpOUQDiBdq0LSioJUxQ552AI2kGDsEdX8eWszZqlx4+tuTL5WPzH2Xa6R34NqwhV7O65ame+0P2iCN1LXIQKwCPE84bv+ygy0PRNiJA+oCKSNhzTNOViNMfC8AfZMJrOWO5Wg6+8svLOXcOqepkDNsbu.BMxrvqIJHVrssmr0fpVzhmLwgEx+0Mj3QeDcKXDMOs12oYbv4gFezG30gn9nl51u5L+aEEQYBaBDPL0.uXmuUZZYlsrES3f6wqTzfTaZ0mcytbq7f64uS8Zv5QMzzPploQg6Uq9OpFubTcs2wc6FQkgiQAXdevt3xF98+9Gk5hRwDsrQ0hVzhSRLOIZbT0V68x1eQXdI85YCHpkxgQKV.VZFXMOpXWFODsnnQ6zbmiGlwL0Evt6dvZ549xjrOGbdnrVcuXHzAogq4wj3CKirlaPGDinlEPs8o4mgZQKZwiOXtQnNGIm2b.ayCuss39clZdavcXs3jGKEWDtPQb+DJNHeU2z3Gm0djeK6vthdX2GlmwRGVZbn9uctFYsf86CJ3YlZ1nnbNDY0hVzhV7PEKZvpmDayG1dSnEdrTcQ3S52TOpF3z3Gcj292KoFgEEUhG3wxgrOm21npj3bJ3d+7bQHb.QPYqkXsnEs3DDGlKBqmhftevhJUYMm2LAL18PJ.pEGOrTXvZQXY5hvHKO6qyxi8dX4i5OjGihl5nNqQMeAoINHe26bNeMQ7.1+yigp6EiglmAZylFHNxapSTrHWD5ryllMj5Ku0.qVzhV7..KxEg3bmHYT8ihWkNMLn3G2wRgAqVADevXdBMr4zix139AGE8fcX625MNnTpF2ue3eeN5px4g40nxoAA42hVzhVbbaGx1X.vKR+sM0b0zDb8wZ22hCAGaCrbNexvLxTiHhO2lr.gDasVDrnT0VdibLT8NAsVaUm5S2m0dnZNOgbThrh5e9fhxBkRgVqmYd0M1.69iPviRtCYZT3s+h0by0+fLJHdcudFgeQtEr4w1QYYws67t96WvAusNr6EGWMD3bNrVWnXm1X9N69ttMSttpw.ApecDn5Yu3yeG101VzhGWQ82S8uCDdWIP+h0Jku+sH...B.IQTPTY8ch2ns0XTTmllR4vgjzMgwiGSWoKVi0m8oO.bXskNWgiun1pNF3nPfvAs+WX6aGxw2hNWVT6PyyUfvrQy9xrsqCSBJOrIZIZeQzFCZz1ezFCq0hnS.G6643iCdjHMM3csyISGZKxx+lOnNueyx7k2Essz5os.cPQ+1hl28xKSK533zRjB9f.Gpl4ZQKZw8LdX2Q6S5XQjJzhSV7HgAVQzzxy5Zp4jbeVG6iBV2AGtq2uMrzzXp5SmGMvmDiTq42Wj.JeTFKZjoOped0hVbZAKxM8KpLZc+fluG+f982Gz6+p18OQ2Ks33hS8FXsnHdXY9P7AE4EG1uaY+.97NOiSiMLUWGT0KiKwiokElmwU6iouk1d6gCpbgXSpi4n8LPKZQKVLNn2eVFFYsn16dP8d6I89eYrcVFd2nE2e3TuAVvCFWDV+y0MV4gQjWTeeVWaUGzwXyeScl9Nr80gwR07P6KmsnEs39EKaFrdTEGmq.KRCxyr8O.caE0RWKN4viDFXEw7cQ3xmEq5FV8fzPh48xfwX1253E1sm4pd85szYvZQai60He7QATcsS1eI0ok8pVzhiGlWvjrLwhdW8AU62OL1+2OayVMX8vAm5MvZQtH7AENrHB4dIhEmGh4.EW7emyOxNgJCopGIapfaAEqezKKCiAp69w53vtt6+MG919zLZdurYzR01PTKZwxE0Gn1xhEq5sU8vnMmSp8eqlPezFGeCrpxbiyJ1awANwhfFD2hq2dhk6GZJePoAqCx.FmygUQ04VLi7JXATyzzQyjImx4qF5FUssuyfx.Jrf0h.nEGIJgNIJRRR1WJCHe7PJrkjas98gjgQAFDrVGowzrfCT0Djuf0Wo1cGLaU0+tU.MyN8gK7Wm8S8veOnYxU0EVGa0oqShzyO65dfhUUZqeWs3wWbuXVfxAlCe0Zwo.zzvul8s8nNDr3nd+a9otZelvxcvQt1+tLvRiAKmygsz.EkX0FzoonEMJQANvY7mnJktxcLd5OldyutQLVqqRP2wkGw7dvXYnUp4ZTwbJJlUqmRXhcBhRHwEL7vBBNvYwgijTMNkP4jRbNGYYcw4bXJJQxxXryQgyhFCINAscBoVCmseFmckUIwZHe3dLZ6ay3wiwZkPt4JEsBtvYWC8Z8YrB1bbN2onDiVnPoorPgSqQaAyjBzNKqzsCh3nXRNlRCpjDvs+r8tIbeQBF.hHf3S6WNAJsVlWgT9ftldPL.de+xtq.0LiDVgm9uXtSKXbkX.ozelIVrnnx3LmBbBNmDjzfLyydyG96ux9LlqEs3QGLu9Zl6Xtr9AIqBua3B4KHE5.y5g2ChxWP4aOOx99z.yo19w4VZR.pYaJGcVjNZCZp4lKdd3L0OABskU6jxUkmDazNWbv3GRu8GVsh8How1EstN2LCZ7n1+2rqy7O9Npr4crMxyY7FLENOsgodBKByCGh0hxEdFVbXMSyIVVigzjDJKJw3DRRR.yxY3CmPtHzVYknXsfn.U7FkdtLATui1ihqmN0.QvpDrVP6BLEA3bVDAJKKQkkhVqYbwDL44nEAMIj3D5nDjIk3ljSuLEW7LqyJJXzGbSdu276x26a8sX26bGt8MtI241aR93wnjD51sKocxXsysJOykeddwO5ODm+xWlmd8yvHUB2c7D10LgIEFxzYjkkQVhBaYI6sy1nUBm4rmkc2cuizHWqy7S8oG1soiRd05XgZLfJNo5.Sbpvrsg0wRytSrBy03x8iY3hjoMJGYPqEs3wCr.BsmAha5aDRrTTMmVQdbggjmTwoc4c.M8ZzTVpp3uRltLOrU+NCm7rYszMvpoFVhVRtnthmxj0AGQeGz9qoNslwG+GgGRVDcoGkeqH5vn5htcyg04SeAVmihxBTBn0ojkkgDGInwB44LnbBq5brZmD5oD166+d7k95eE9Few+T9f276AEkf0hpFWIhJkRslhLMacCEu8W4qxexuw+bV6keU9T+09qwG9S+CyK7bOGIabdd2adK1c7HzqsBVwxc29trVuAr1pCXyM2jrrtG54XKZQKZw7vzfDILiFAKxo+tnaQDmDtM7IcirO9kJmZe9vb2SkHhaZHUiWJiuXNq9WV7wv77o78ajVbuJZ7piY.rpfWOM3bBHNzYoTTVhHZ51oCTXnXxDbEFjhILvTxK9TWfQ6rM+A+t+K3ey+p+kL4l2fd85xS0qKCVKiyLX.m8LmgU6uJJkh7hRxyyYn0vadiqh9LmAmU31u6U4O5O6eH+Qm+2heze5eZ9w928mlO7G903u5l2jaemMQ0qKqtw5XJJ4tasGoI8NwCU5kp6.aQKZwoRr3jIZKNN3jlEol8a93j1rNMfkRsHLhlFOMOFrpa7zogW9lIkObeDshJzfyS4n3cBLNw+EGPVZFEkCArnrFFt2t3JJXs9C3rCVgW6Lqym6252fe2eqeS176+tzuWGd9m9h7bm+BboytNWX8MniNgNIoAeh6nLv3YgVwm7S9w4V2cK15CtMa2cM1r2Zbqs1hW+262iu9e3e.+s+O+WfO9O4mgAC5vabkqR14uHYc6yNatMqsxJXLEbT0gPKZQKZQKpgCMvWVf3sNk.Ap5Dep9vN5Qt3og9vOMiklKBWzMi5hradq67x8SgU5H5htGttHbei.P.PCJGNzXBQ1fVonbxDLSFwZc6ykVeUV0Ux+G+p+83a+m9ugMu5U4xO+yyK8rOMY.azuOO+Eu.kiGQ4vwLbxDJJJvZAkNkNc5fzIiISxYETbtyeATqeN16B6v0u4Gv6cmayMxGy+r+d+OwUu56xO4+g+s4i9hWl29Z2DWOGm6BmmM2bS5mkdnmiGGbXWWaGsTKZwi9n46wmj49pVbxf4wlUKNdXoXf0AciXJCVym13EE5n2O4+ikYm0GYWDFhfABLYIR7AUPv3E4t3ihAwTxZc6xEWcE195uOeguveL+Q+F+eyYO2F7C7pWlM52mydt04Ee1mE6nQ78+9uMEiFisnjhhBJJLXLFLtPE.2Ij1a.ar1Y3hquNqj0ky1sCImcCxRDFr2NryU2g+j+9+8YyM2j+i+u5+Zt74uHe2qbMLkB8WcUrSFWITvVzhVzh6Eru7mWMs29jRGzGT6m6K5COYOTtmQyTsyx1EgG27D4i53D2EgG0aVMYgJ5Fwpe+QPCVMm28JtWe3RbfD+MVW04tKZrHZxR7rXIVCJyDVuSW5KV9heou.et+G+efy+I9AYEkkKctyvy+LOMIH7M+leC1cyaiBGJyzPkUqSQ2IkTUBVmCmArES31u+U3C99uC861gm5RWfyblyvE0qhSY3G+0dM9Fu86vewu0uC+ZVE+c9u3+R9nuzKxq+tuGI85bOeM5dEG0zzPKZQKdzC00MaSLOuW73FNoGb5w0.jiTZbnsM3SLbrMvRolZfi05SPlSWlhl44Jq0Fx4Pg7jxBD4te1yZvUcL8Au4KJ85Lg0T760aPHZ7x7RFa0W9hfmwJllSMntwd9nqwTTxJcRnLeBenm9o3ewu9uNet+A+C3re5OECjBdtKdNNypqwMu9MXus1FwTRRRFZmEzJRbPV2NfSwn7w3LkjjkgBHy.jovlkRtsf261Wmau2lr1Jqx41XExuwl7u0Ovqva7A2luy+n+o7+2EtD+M9Y9YXiU5w16dWVu2.LS7miYYY3bNlLYBNkPmNclYzn0y36Zs1eFZrSYlr1nViyq90uSBQtqTpPVvOdLFtxG9byz+A0dtvZsnBmG0me8o6O8gT66KsyhVzhSmv4bn09ZapwXpxKSwOaKrHNednqd.KASa+VoTTVVRRRBCKGiVqwVZQIJrg7E37xASw8+gc7MObuLv9Ca4KpcqEk7oON6u6UzLOQ1b6as1Crc2ipDNNn9WOns4QMK.TecuWj3yz80rREJZCwgc01Zsn0ZJGm6yWaNwWd5RRBA.1wqU9SDMXU+B1wsuzSJKrWlaWQBIrsZ4ToPPDhYRNTVRIk7Qd4Whu9ezeH+9++7qAYZd50FfNeDRgg7s2Ca9DzVEZRPY84zod85wd6sG6s8XzoInUIjjkA.44iHAMhRvHJLZH2URowfYnk77bFn0TNZDWPkvlO+Kve7u8uCm8YdZd0epOCCe+qyjwCIQmgVqwXLgFT0fVsOiKqafwiJi7431f1h98KimsaQKZwil3zB6bGp7bND3qnHKyinGcvCB2SdryRhBGrUvGD8wG0SviSxp7fF0wQUD8G3+gjppezaVrgbgkmSEC3LzuWGVuWG19lWm+k+F+yfO3F7hO2yBauCmqy.RFVx36rC18lPlUQpUi1HjZT3FURpQQecFYNM1QETrWNLwRJIgnVDroJLYZljJLDGaMYD2cucHUTnxmvE50mOxy9b3t56wm6292DYm6xG5rqionHjY30dccYsjjjPRRx9Xep9+UrQ9HLNtFHdRmhKZQKZwoOLKiKVee.XAmo5y0m2A9u0cr92KSEl62m2mq+e77P4t+9+zBtWsi3AotuNQSC0KRqMKqSv62NHWlhvLZXUyTcPkAlNHSIb1UVg+0++96xM+JeEdtW7E37YonFNjdNvkWfYuwHSrjZAsAjRGhArSLncJxTYjoRQ6TPgw+uwFLJxgw4.kFRRgTMhJAGvnQinXRNYNG8njenO1OD67M9Z7U+W+6ySuZOVoamJJ7iz4GoctIEsGEJxebFOIIb2VzhVLKp+9eyAp2rcgGTsUzTBGGz9s4w7xHKl+frt9MOLuq6woy690C51uOQMvZQrWcuf44e1kw1bo7Bf3quQ5vmikkEmy6xPwZ.qgDLr8stIes+z+XPYIsXB6b8qwye1MvNYB1RChyQpnPbfszfszgxonaVFkkkLb3tXLEzsaFqt1.RRUjOYDVbTZs3LFbFKZqhDmFkJAstC6NdDCyGyV6tEiFtCuzy8TPB7E+89myluyayYWcE.XxjIHhuNLUoOop5GlrOCjOsXrwhZf4fZv4fDl6AsOZQKZwSV3f79xCSCqhH50gCynplFdTe4G01MOMiEY76799hl2IAN1FX4X+2zlmff22u6AjKBue2m2SPB04tXRmqlQVoIJN25qye9W8qxvu+6v4tzSACGx.ARrkTLdjWHhZsulFhiRrXwPoXX3jbPIjjkRosjs2cGty12kBaI8W8+e1685IIM67L+9cLelzTYV9pZ+LcOXFLvwAX.HAIAH4RiXHJtKikjJzdotfg9KP598RER55MjBpkwFAECQF6JshhqXrbI4RG.gi.D1AXbs2V9zmely4UWb9xrxp5pMClo6oGf7I5NxpxLqL+rmyy488484sAJSkHGcdHyC4dT4JnPgqTPGmfSqXXwHTFX66bKd4OzEY3MuJ+C+U+kjn0GIceZsd5MsmDIjGVDsdVF+vdd+gMv0GjF.ZNli43cOdXRL48iwCNNwhGDouGze6OJfeXSQ3GHzfk2ezSxGWqNuawC6fvSiTD9H0f0IjL5YSVX850oHKmu9W8q.iGQcqllIVVawVz+fCnvWfyBNKjqbjgCmUg2pnP4oe9H7IFzMRnS9.d8qeY99u8qyt81m3FIH5vIQsKjZw3BvVB5BAmyCVK9XKRhEuQXmsuCat7hzTa309JeIxFzGkRQbkv4KKKOb+ve+UH3GzHV7d416Gz12mi4XNduGmz3fOnHY8z3+y988f1dNo8geT.OHxjmzwmS58+jFuG3CVm7jNyZ2BS98eTDdzgzAhBuH3qhlkGAiySiHC27Mdc19JWCSsFTzqGYZgQwZhpEgH.ZEhnnz6PSkEHnDbdOI0qgRKLXTe1a+cX6c2BvS6kZQ69MwVUsgVTnQgnphnUksEjUjSoxSTTDCxFPRjFe1HZnUr2stMi6zEaqkwFYIqHmhxRrwonpzkUrsxFC72eTJq9hdlBJd7Df4ThiOxVcQ3S8v8cc0+c7DNC6ywb7TGJgptQwCFy95dEXNxGviSqi4Yk4B7S2WTxgaUyN9w8UgchFs3PDPDPI5pwFl7AcX55zhepFkTb3PkhRO8y0Hky9oOc64gUYeyt8ovERdhJLOvjBtJ7dz3m9YZvSkNcAbHnArdY52maxbGUU.ulv9mV.QbG99z9vbVPk817923fdU33wrC4ql420BG4bvjWexqUYRSOleaZNL7IOdWC+tlfkwbLOPRNzuq7kkXslYZz5G5EQZsFoJ8TGu7+EQBG3lQCPyVS7GYh9Y9reT3cpefb7uq6+EUnzfyAffwDAJnz6.eAFqPMEb6u22Gt8c4rKsHQ48IKqO9lqvvRAWlGqyiHJDeI5nHhsVDsmrhRTZH1jv8t8snyN6v42bSJJxXv96Su50YikVgrw43bJTQQfVSoWvgCkxiAEJsi7hLL3I1Hr68tKu3EeA9h+f2l23q+04y8a9eM2Y2cYv3gXZ1fBJYbVIMZzf7xv9uE8Qrk.QKU2D6pt7TE92DdWhOL.jd1iqmvwR4gey4rQF8jddCgaxThDFLYx.eDtAZR0QNwFJv4vnz3KJPKfSwLEoPUpdmYRfG70UZTJ8ybDLmi43cBTvQFaEBF6me5E1JDAbhGOy97yL1nRPojYFmNPxv4bXTpodlUw3BhpGQVVFFiFegDFe3DtE6322+fhTgVe+O2ru2i6STy9dEE3U9oDRLdv30g4vpX3XzpoyGAfVTfWe3lrWGLRZkpxuvbT5yQoDhMFLBH4k3JKQ.LQVvpnPITTli16vRXaI2URQYIE3QYznLFJbk3bNRiqJFo7bZTqNQFK85rO0hhvnzUyUpAuBsG.CnLfGDqAuEb5HxUPo3wqMX8dhw.dgLkGmVvqMfRg0DgVTnKJvH9.QRkmRimRkmbsfnTDWQ.6nWS83Sf16OlOEN4y3XyU+.iTEGRxZx6WPNx1jAU3XC9ou9zqMbNd7HKMYQ0uyv6Y9f0CByR553+7QX9eL7AkHd4kpSWdEhdxMhU5YRIjMXH81aOHqD0nfmXEarLxWh1XwnrnECn.iIFkHTlWDFzBHxXYPu9LnWe7NGdWAh2Sd1H1cq6wx0piUaCDqDIbytwfQL3bdDb38knDGB9PjnbdTdEJuvct10XTm8vhPbjALFDMHNEkUCbnpX5ebpPydg8yBX1suYWwHG64+gFOBxfywb7iKXxhZNxcTGqjxdzig64c4cjuqgV.kJPffIaMU6FSzW7IEMoI9GkS7j6JADhEMJsfRBFts2KH3wDYHJNAuDjfQ4nw3QgV7jMdD4JIDUKbTHPtu.mVinxPYhnnnf7rPAT4ybzq6.rXwWlSdhgXikj3XRiqgVaBQox4P7NT0afTTDplbFCQQnSRvXTXLVJFWfV7fRWkAjJBMNGkdPWEELsxgWcXj0zDl66YgYokYdb1sG8LK1dRTsNxiGYieRjoNR7XOAnqBw2i211yrDr9fDDQPMIMRUZRSoTnUZxFkw96uOfhxRGTTvB0SwMNGSpY5wgIL389P+KToAq0hRo3fCNfgCGhQGM0A0KJzzqWO1qyAzt8RXiSHKuj7bGQIoUgL1iWJQ4kCSclW.mGw4QqTb66dGFLdDlj5n8ZbhfRowplSlXNli43Gd7zbQxGW9BOV5qsZ5GsODEFEfn7H5PDrpTewQHX48JT5Pz4A.qFz9fk4n8DoTHJPKAePbXQNBFjnHTZMpRf7RrCc3JEHNkwhibmmbuCGJJUFBNpkPoqH78VVh1.0r0PDGI5XZzbYxJyYTVNp9kfzEi0hI0hNMFcjEsKi33HRShI14C8z1rAHFMtHK4pRLJEZsEKJrt.oDu3wgPg0dDYTn.LBXKHPFS9wWiJ8wAOwIXcRhc+97DqYHY7AYHhDhnk3QoOb+d3vgXhhll5zj3XxyKQh7HdONuaZ5PctPCc1FYvXLART6sGkkgT1AgzdUqVMxGMls1ZKhiSoc5g1sfnfHqFeoC6Lz30XpBceUajACEEESiLSQQANsAiMAiwDRiq+YnPTMGywb7LIlcAzvSOxUOHhTOVRFg.2H0LAjvqpR6j1W8y5JBFgz+oTZTJOhT86ZGFEgHWgf2In7dbhfWf35MnyngzseGJ8PDVLhBsFbVCGT5X+QYreuNzoWO5mMhQ4YjUVPgqjtNG0UpvmGPq3DRrQXMFVbwEodyZTOMgUpu.MRSHUqP6EjAEn8YzrVJDCQIJLJvZTfWnzWRYQIXz3TJzRIZwftJ.ANTHJAuVgqJUpZ7XCARKn8Wfx4qC+ghmZDrl7esVibB5p5A828rd0NLkXXkX5BRPnZa1Dd8wiGO886J8XUZJHbg5DG006KmtOaLloQuZ6s2lACFf0pwZsjWD9rhiigXnyAcXwAindyhvwWT3JJQWI9xIt1qVoPHjVPeYfjEnw6Cg414cAsiIG1GmbNG56OwfUO9AaxvywbLGu2.oZAbGOJRePXAyF4XjqTgH23UUZzhIB7VpFvFThCOBJkih7wDY0jfEphPj2AdMHJCkc5gOWHUhn.E64J4F81mq0+.1tzwabm8nmCFUTPNPAguyRBJ9QAzLRXbNLDHNOCadFdf3dcnDXQKrwRs3LMayYSavykzfKktHalzfZ883cCoitK5ZZRZlPbZDVkPo3oTGJHpf3UKQIpp9CbPiz43pTdj+P8s5kCqkg4Drdn3INAqI3DEwlRgH9oq5Y1Tk8rMsp.lbCnRBBlyfBmurhvTnwO67dJJKwUjiyjfTVhyIDkDAtC8dJQbSaSMFiAu3XvfAgneYLTqVJZsNHT6JXsVzZMCFLfC50kzZMHJJBmK7YEoMnjhPqRnppO79PjpJKKwiDVoTQA53HzJKZqEs1f2mG7MUsthf16OGimi4XN9.DlsB2dJLN9CpHXdbH2MoByzRfPDvzptyAfRP7tpUA6QiDhZk.JwGpm3ROVkBEgp9lpE15JEx8d5OrjA4N5kkysFzkWa2s36uWOtg.C.NfPg.k1rN0Z1fZMafMIFkIn8o1saGZFw44gEp6EJKKIezX7NGct2coSoiqtcWLa2kkAddfOdq04hsZyYZzfUajvxsZRhQQ1vBJGNl5wQTKMhLSfPotRtNdEAMboDzFOFoR8sxgGu7J8gVQjnmuV6GBdpPv5HZt5XZv5gcevGDVAzjpQIhv9htR73JM38RfDTTDTFtP06g7wYTKsFk4EXHBz9ooODU3ucT+QryN6fy4HIIAq0F7nJIz2.EuBmGp2rA8FzmBAVeiSQsFsBaStBTZPb9PJKcFDkPg2QdQI4ENJcBQlHDkIPtJxMkDm2Gr1gCGg7vRNVlQH+ywbLGywwsqmm1KTdVIl7NadCMAFBg5ibJ4pvxQCU7t3QKZTd2TwvGrv.HlXnvSg3nv4w6DbEv3AYr2vwzyn301Ya9F26l75YB2iPTpZtrl0V4zbgE2DsMgjjDLwQf0Lc0rhJHujtC5SZTBadtSgQoIazXJKKQ6E9PuvGAYbA4CFvfNGP282gePu83azcKzc2hWYoH9vKtBeZ0o3Rw0YoxRpm4QEWhttmjlVJiDDifXzHFEdsfHkfWiU.K9oEyjWoQ.JLgiByq6mGNdhSvx6C5Q5ARv5I8FvSADZ5ygzyoCkdRnBRzJhRSItVJUI.GOBEENZfgh7LDcN5nf1nJKKwFErUfACFvVasEoowDGGSVVFEEEHhfwDzNUddNsWnAGzcaFLtjEZsDMVXQLJg7RGJipRCUgi0kRvc2CBpLH18ZMaPbZRPPj9hPeMrnDk.owIjOtX5fIGhCq9meznLEli4XNduFePXAxvgBYehn1gPDq.vH9f8MHdTN.Bjs7pJycVrTTTv37bFVTRIZFV54tc5yM51gu3MuNuMv1.IKGw4d9miFKsDFcBfghRMZcLNkJjVvxfEMDEEg1Z4x27s4t28tr7xKS8VKQZZD4jiNNkDSD9AiwniodSKosaQqyeF5k2i86uOi6uOeoKuGuw92ku+UtKelkVhe5SedtzRqhxC8FNjVolPV9TZ7FCn0Aplhf2WPrxL0Kr7UmOKzSzl1gzSmiSFuqIXM6A2SphAChB7n5vZZ4u58DEGM82gidS4DmDmYd86SSVuKWhzI0DpOQC07A82azf2Gh5iD7.jXikbojw4Y3hio1Bs.sAwXQYhn+fADU2RylMw6zjkMFQDRSSwXLLb3PJKyYokZejuqjjYOc4IIIAQYXokVBuXBgNd7XRSSPIvfAcoVbD1HK1nH1amcQhhwjjRmACfZ0XiMOMPXkRiKxnwR0IOaLwwoLd7XhsIgxw16P4EbpC8MjPVdOYWO4A4eI2266DdtGmi+GtZ0v+0UU8nRBjbU9fNx7dGdkfQU4qZUESvjumGV6.53qJ93Wq38dLmP0VdRWSMGywOJiC8DqvCdePORhygRonrrLL1V1no+t9wXp4YmC33229fJLpG2wO7AG5bpy24QvIxg9jj2g0CwnIRL.dxyJw4JwDYINJkhBg98yX7XO93D1sLmu9stN+M23l7Z.iAV7zs3EN+EoQ6kvqinnhrhnLHFOhViNxRbTDQwwSa+YNDVZyUPkZHNNlFKs.Ma1DwDFKK1A5BGpROCnjwJONsl3nVb1ZMIY4SQQ6Nzeq6w0u0Vb082mu196yqdgyvG6rOGmucCZ0qOqTOgTiMXN0NG1nfeZUTU4hgiNA+wBiNj5TI3mVwp62VCdmMl2IOOwg9bl99lWdVtDSjLyrAsYVdHS+6jY7mSuGkuxW2pZQbdek1638Vce+TSCVuegGEAoGjKz+3gillL0j6TgvRgzFbhmm+EeAtR6E3f98ntJDAoZoMv68LdbNfdZSVNKKCQDVYkU3zm9zzpUqG52sy4vIPQtirrbxyKoWmtXTPszTzHLZzHR0JLQV5mkiCMCGmAdEqu4FfQiW7nzVJcNDQBlvlHUjqjotkav8ymbg66LSk6IADQpFfbR9KqzqY0yi5XCLybROywb7r.d+Tqsyl8DGR0jqtpNfgCMdLBjpsnyEbiGi3f33DHIFmyyA8Gw9cyn.MiLwb0c2k+tW+6yW0my.f0+HmiZm5z3hhvIQzyqw3MDEkfvzdIIC..f.PRDEDUVqnP7r7pKfn7nvf1pvZiq3xXPDGKszRTqVBJkg3ZwjllR6kZgRYnXv.pE0pRKuNztBxJKvmWfLpfrRMsWdSRZr.oquNc1aKdyquEu90tEejNGvm6hWhO+ZqiLJi3diodRDMRhCB+WELNam3wqBKR0qprUHznDEV4vN0xbbx38UBVOMBg7OLDr9g56QN4et+3QblKbdpu7JLbqsnY8T50YWFUVRrInmp.abAmqXJibiIljjjiTAhm31uJXaCFiAQGVYnqn.aR3ueX+dH.NQQlyS2giHdg1LbXFTuEm87W.TF7tvJLKKKOz4yq94osZfoU+Yvj4BG+dO4v2Oz3wgf0we+Svrt56bLGywSVLQJEOMwIM99rOWYUp.kpHWMoEio7RvxBJJQJ8XDCDGA5HFONiCFMhd4Nxr03dkE7Ut9aye8stF2BXoSsHm9rmCebBFcc.KhxBwQDWqAsZr.0zQH3ozjin8n0FLQFr1nJoc.BdzhmHiFEJRhrTKMNTseJMwQ5ptFhPZgi3BGEY4LVUPlI3mhamMFsNhlsWiUV8Tr956w0t9U3qsyd789Fea16ravmZk04EVeSTdEbvHZFGQRiXHxPd4PJzPtNnxcqHn8Bw9vhqcGymvliihmpFM5D7zT.jOJBTmT3j+gMEOZpLospAR7.iJyYg1Kvlm+rb429MPmFSw9N1cu8X0UVhZIAwtmmmiVqmZOCYYYLXvfG42+j1zfnBUenUawljfRAiGOj7hwzpcaFTTP298XXYIFilRfMd9KvxarVncLHdLlHxOVZYEen5YB4gSvPnDhOrma8rcPPmkD0Opn4u4XN9fFlXkMS+42mvIR3BGhbXi.R4CQtW4AWoCs2.1HvFwfw4rS+dz26XbsZb6Rg+C+iec9p86hwBW5S7xjt1Z3UV7JKixJoVZMr0pixFQj1N0lhzZCpzjP65gvgm7hBJGURdddffz1aOMiBKt3hnQS+98IIIgzl0wU8YYKErEdTQw3iKgRGFmihwI3FNjN85S+w4rPqE4huxmD6cuI67VuE+427dr6dGP2RgO1Zax55XTY4.dRr0PPiWK3s5PwRIfQDL9fuJ5mKBqGJ9Q9HX8jNcPS0Z1LMZyoPqvWpvVqNu3G+iyk+G9pze7HhpUmNCFRZZJVk8vHEU4SXSpjuxxRpUq1w9FOple7DRommfwfZ05Po7VjSQ4HRhiQaMr2Nayv7LzIora2C.UDe7W8UwFkPuw43IDIrfquKHNB8iKUkqEOqlpdF5FpId10zdsZ0Zjm97GiF+8SnddDrli43IEdP9g0rDtdZfGjFeThqxA2qruvIiiTEE7nnHzpH7YNNXXGFpTj2nNGLZ.udu8326q+MYGErxZs44tvEoYqkna+BxLkXaUmzEaftdchRRvpzXECVsAmBb3nrzyvhQLp+.5Mb.CFLHzVaFmQYYICGNj1say3wiY2Ne6ftaEglMaRZi5TeoPQJkTqNwIogdcXbLw4dRK8THVhq2lrl8Y2c1haev9jlDwoVZctvqtBa+1uAesasCW+68co6yOjetm+Cwx5DJxGSsAivzLJza+Tg1ni1QPxHNAs1iVL+Pzg99wG7TIBVOCMe78gGkHIebwrs8Gk.hVg2CYtRjjHdtW3hrzlav9equIMZVCWQI2cq6QpMhjnPkB58dxxxlZ1n0qWepH7l4a5H+lwV435JKNmi77b7dGQJM0pUiXqgC5c.au6Nnp2jnzZL9d6PsW3k3E+HuLYE4j6AhiApDUHRvarNx9mCkeh9qlIReuOyO4QQvZBIq6qpUmKdf4XNdphiOdwSiE37vRQ3DecBXpIQezlDbvNbvnXjyQ2rbxpWiAF36b6ave7abcdSfW3UdA9vm+Rj0Y.2a+NjtPKZztI8EGQ0RPYCx2vFaI0DAdOc60k86zgacm6R+QC4fCNftc6R1nQPY4DAuxhqrBqu9oY+86wst10BaXVK0qWmwiGRiVMn4hMYo0VmkVYMZ2dIVHsIMhSoVpEwCE9RT0qwJmYS52qFC1eO5MnOoFCq7RuHEVM28ZawewUtL4CGwm6Ru.a1HghwindRXbR6DhxUQvBlmZvGG7TyGrNNdZkhvmzZvZ1IsUd4H9BhREZXx8FzmUazfO8m8mh+7qbExyFQRjk962ktC5yRMaMkfUYYvnRiiiIJJZFBVS9fOJgq7wEA8WoBdWkunLLPQjEqUyfQCXuN6SdQAoFMixygEZvO4OyOMsZuH2b7XbQIDMwDSMAyKsrrjjnHvezpz.ck31m3UKOppD7ILQlGoFrNg2+bLGywSWLsaW7.hn0SKb76+0nlJsCyjJSDUvJFHHAiQCFxHO3SS3tC5w25lWiu4UuKG.7e0O4qPGSDcNnKlZ0v1nFiTJhqYY41qhXsPU0t0cXWta+PqO6d28Nr6NGvfNYgdRXYUbfLVhiqSZTLFigkWXYVs05TLvwdIGLMpZRofervvw6ynsNfd2bO1YgaSyEWlEWdUVYsUoYqEX4kWj9cNfw8GPrVndy5DaUzqydr+vgzuPX4K8BrP8EXqu+ayey8tChxwm8RWjUiMXFNhj3HrNp5CtASG0aTAOyZNIqGJdplhviGMqmD2nMaioDHTV9U+r1eXGSeR2z1SnuSgnQvM8wIqjYptrNAQr60BEppJOQowoqjvo1fV73EM5nXNXu8oU8Z7o+494469U+G3Nu12l5oMPEOfc26.Tk9o9akdFaDnnnXliQpI6fyruBQI0v6C8zPCfMxhV73JGSu7B51sKCKxQWqFC8BEYYjb9mmW8y+KvHTT37nSBhs248HUWQThmXitJEjgVJwgsdGIXnpJEtYbBKYxA0Ye7XmhmbraR2nGk+HDg7pitBSl0FHlg85jOdGgxaLz25mrcFDgeUR.NxJmENZ499NBp4lq5bLGePBO56vqbobUUGEQGDMtAHRzXLIrcucYPbBia2fu0stA+wW8t3.9z+jeLFmt.QJMiUdJDElzDRpWCSTBEdAqOzaZ60qK2912jabqaxN6rEtAYfGLQModsZznQCZ2tMKtPKpUqVH0j5v3cqs1ZDEEw5quNooozoSG51sKRYAC5tOiGLjNc5vdC2lst2NXStAqbpMXoUVlyb9yvpKuDarwFjMnO4i5iNNg3lsXr2Sm9CwmHr3hKyxufi8u7U4+7c2h7DC+Je7ON9A8YYeAVwPbjPoVvoBhaWqLUYznxs2m93SWbrYHOxOOaDImTI7SqH9IyEwLM.tGx5uO9m+iyd5SsHXEbo7fldTUaZS60f5P7YUZcXRrCysy84UQy1eBU.5pIccpvT8G1CoBsz.i1.kNzNEJkgDcLVTHkB49RJMkTHdznC17wjs4JRKZSvWkzpJ+UpxQyCF2IfoDQanz3wIZT9RbhBvhAEJacJhGw1YkXRi4y8O6eJ+IcNfNuwavot3yycd8WGUoGunXwEWjj3PTqzBUdh03fv2M5p88PpGEBogTJ7XrA2iunHiTCTOIlr7R5NnK29d2Fcy1HKzhhLGbpyxuzu8+BLqbJt7stMs23T3L5Pk9FYwKpfGsDaouTf0F1ushBiWgVDTkt.QF.cbbvj9PBMGTA7ybCmRold7BIj6dSU56rZKkdONUfvzzd+UvpUPSfvzDiNUItv2kHSugXx1gbbhbJEdcveSLwVxKbnSrfNT4MJigRwGHG6mb6RPwlpJ2Jdx0ZG5eJpoUaTXe6juEadTxliOvfYWjwLd.0zwaQcn2AUoQzvaUvn0TVFLc3IZGkp6YBUebP6Nkkkn0Z7Ed7D5PDASUNILN6iHWFOPOPrZa9gA8wd4Ye6JQSoWQRrEIujxxwXSDLZE57RztXnvw3wZ11FwW4x2j+vW6MYXL7o9TuBiZsHWe+AD0bApuPazIQnzZLDiUEQjMgNc5vMu4M4Z235r6t6R9vg.PsEZylatIqtx5znQSZ0pE0qWmHSnXkhhhvZsjkkA.aTeiomS13zaTc70yft8XvfAryN6vVasE6t2dLXzH15l2jsu0sn61awpqtJm+LmkUVdQRRawnA8QoqypqzfD1EiySm86whKeZN8lmmu0W9uiqcs6.sZyu9yeIt8kuB+DW74fjXx1eORZuHhRy3w4nhopQWaBKVtJiK2ejJOzauNx4mieB59N+5Nj.0gqzd5CJlPeXx7LGddW7BFkNztil45AoZNCeUPIlLWy88UehaO.pYBPvi.uuHx8CMktmTLdOzZALZMhN3r5JumReYvQZEnz6IJMBCd7UNd9zAVpZJ0A6fJXrZtpjQYlPFznPq7UsVff2fH.Lo8UUsu0ZoUYmabEbYJdtO1GiO8u3uDegc1g6b2sItUKxFMjc5rOhVwhsZhdRSY16oVsDBsXGOtRGk9fGoXMwUNCuiwiFhQbrP8TLHzs2AremNLpHCmIhMO244Z25tfxxO8u5uFm+i7w4FGb.oqrJkZUHZPUorcxEahLoQmF5WjdQATcA4LQUJOKGmFPGDAoxnPqsUGGMnjfS9GBVkDpsR+r9qkZZ6ovq7SIYoqDcpgv4JsnqFHdRzxlbszQOyGHYe+Cr9.wiUudXVCMzyDhige99MZu4XN9wEbeQA9XlA46W332+OMxDG64KEOjWh3bnMfQqP4JwWTRQNr0N8XgyeI96t7aw+gu2+HEKzfK7IdYFjVmcFmiocKhZzDuwPQdnnjpmVigix3N27NbsqcM1Zqs3fC5fNJh013zr95qyZqsVXA0ooXsVhhhBKJz6w4c3K7jWle31e03fGwDjAp0rA0azfkWYEN+Et.GbvAb26dWtyctC6u+9b2abK5rydze+NbgKbA1XiMnV8VnyxHOaDKztEY8FfyXXmgiI1TiM9neB5diKyex24GPyQi4W6E+vbuc2kFJEKr9ZjW5BVVQjvjFKzzn6W8nJLx8SbY.M477znRwL5paxwoYdsIPQHnIN4nu+GDNRxXjCI68ntB+oBAqGzMZOrU5+fbo2i7dnJ8dU+hBvHJlzvMAv303pX8TRnOKgFDs.FPJ8HtxPTgDGJiEzxT1oNkAmpJBFp.gCpVImRCZhp5H6BFIzZZznvIgNutGPvQi1snnaO5lmwm7y+yR93L9p+Q+gTe00PFzkAc5xMu8Moaulbp02fF0RQbNbU8vPQBU1mpJYXBgdjUw3wXvSRbLdGzoaG1Zq6Q+rQPbLlls4Z27VPy17R+7+B7xexWgLkPeWIMisTZTfVgthThlfGrXqZGDpJEjapNW3Bc.5oGehiiNRTdJKBq10KBdQHMIIHZ+JGxUbgysNeUuaTWU8k5vJJmDIKU0E9ZkBielAVnJxcOEUW+80HY4CFMi74XNdRiYIRISBMvLu1yR3jluQKPAEfwSjwf3c3J7jWTxvRGCakxOXqqye826ax9.W74uDMVbE1pWWr0qiMt1TocDEEQTRL8GNfqesaxUtxU3N291fRQiVs4zm9zbtycNVe80INNNnmppGEQvUYxyS06JbzisGCUKgEzJhiRHsdMVncKVb4kX00WiNc5vUtxUnSmNb4qdE5Ob.ixFylatYUq3wfyqwEqo4JKwftCn2A8XsEWfzSeFtxd+.9yeqqxEO0ZbNmmhROMKJwfhgtvBq0pitDyi1uZmc6tZ+Q+9y0DGQqzOkz+M7Tff0CpAbNop6lZyASDX36POnRldFM7PnxPLGNgbAfGDkFwpvYBjCbZAUofprxORzfVYAitJcbgTQ47d7nnTErVegJxWRnSpWOxhQp5F4Uj77Bnqr0csBFkkQiEZRAdt816xYVZQ9n+r+LzePWds+j+egHC0Z2Ba8TFNbDW4FWmFIorPsTpmViHik33ThSpDLIJ79fMNXBh9hNGzkc1caFNbHpHKQKrLRbLCcdPJ3k94977K9a7aPOrr8d6QskWgw9RrIgnMoqzpj1Wct5v1kNhJbSrRc3wtIjuxFkcXJT0ZrJCZSzzAdKJJ.zUsFhPJNAPYBlqp3EPBoDUzSh.UHxVpIw.r535zaZUD1lHbN5wNZUuKwwWU9yBUQ4bLGOKfCaQIGUiiOKb6wwI9M84vgMxh2UfxZPYT3GUPQgmbzzUCGTOl+u+y9K40wyG54uDKs9FbutCI2nIoQc7kBRYI0qWmFMZPmdc4Mdi2fqdkqS+98IsVMNyYNCm8BOWneBVu9TCcNKKiXmaZJX0Z8Qzg6rDs7d+8Q5RoT3kPgQgyinUjXinVyFbgFMvqfUVYEt1MuA23pWis1dKFMZD62sCO24NOKu1hLNq.uUSjMhZhhrQiYXuQzrVSt3G5bb627F7G9k9Z7e6m4SwZKtD6u6AzZwEHurDehkPsmq3XSC+N6byCAOqQR+cJdplhvGTOeaxOOq9pdrJidkfWcXNeMdEJQGZNk9fQe58BhxToIGgBkGmxQgDxiWgpJ5IpIl+lMjyVGSczbDEFkAqRMSLACoHR6.sOXTbHUR9dRxfUNLZEFqh9iFhMxR80Vg6reWTNGe9e8eCpGa40+ZeE5c4KCwwTqcaJ62mNCFhqnjd85QrMh33fUNHlPD4xJKnrrDkSgyE56W4dgn1sQmjRNASqi0ViO6u7uLuzq9YnqSXjQHYwEnzpPGA9frIPTgpmwJJTdPWYLLhRgu5tGmRgXCjkbJPiGwpAwCU5SBuDhJXU5QMJcfbKPoTQVynwXiPazvvAA0ZI5Pjp.TZOZuFE9oU4imJBVUZr63Zt5IEdPKPXNli43Ye7f7+pCgKnKXqFwKTjWhyoXr0x934O86+M408dZtRaV7bmkAY4HJMMVpE8JxoooFI1Pzf1c+83Mei2l27MeSJJJXwEWjW3k9vr1ZqQ61sQDgwYYUFJcDwIoLMxNSm+IPtx4b3btPECdB6CSFORaLDU0VybdOEdG9wknqzw7parN1zDpUqFW8ZWiC1cWt7UtR3uORXgkahVJYXmQnUBqtzpLpeO5jMlEVcMhx5wW65GvKcsKyFejeB1PaHKKCu0fRYBYDbRfET2eDqlfIY73o8nnSBfyjeVqBY+X5hkeBuBfmNQv5gPr5AgGmTDdHBBszqLUQyPO831rQJKrJfB7JOdoDk3.Sbkf3pVElKGuKjJKuyA5noDunJUWgFXc36AufTke7vr9gbaoTAC6Ta0HYB5nXTHTT3wrvBnxcbu984+heq+a3bm+B729W7Wxtu12gQc6iodMRSpQ9nQHkkjULFFMNnyIcnx8JEOdGXIHzTSbJwMWfwhG+nwPq1T+bOGuxOyOKehO6mkQVK2d2cYg0WijFMCN5dbTHFuUoATEJKuohJmpK.0nXhMeIUdgUXOUvZ0gT44Bh1ROIhjSrWBsETk3UgtztSGDeXgWQLZrmPh5mTsmfFkGDkObiwIl9gm7qQ9AZThOSr974XNd+Gy1FsdXo058SbeaOpJs25JQ6hBRxnvSowRWQ3sNXe9Kt51jtYSV94dd5qDbdEwo0XfRi1FiwZIINgt86wq8ZuFW9suJJkhyctywFarAm6bminnHbNWn4WyD6NXx1SXaZZQBT87Shp0rQw536KSKdLiNXwDZEdmPtqLngWUP2q0Z1fKdoKQsEZxkey2h6sy1b0qcM1q297g+Xu.Ku7RPohhhwg1y1nQzoyHFqhn1YOMKjMf+xae.al9V7O6k9XLneWnVJQJUnPFfoB+93lN5gAL4XG5eF8Zj2qw6aN49iaJBeTjrzS5kTLI8RUWjVcwJtppZyKnkRrJOViiFlvEzspWGeoCmKbAqVzUc48PjvxyJldwtqzgHpf.9pppO2DQYG5C2b7Ic0BfySi50orzSmw8Y4EVh3Eh3x2YKF2uGm8C8g4W+zmiu0W5Ky26q8UoX6sYXYIPDKrXKjhbJKywI9ooqSpThdsZKvvgiwIBi7t.gl0VgW3S8p7wd0OMqbtyyN4ELbbA0VbYHJkBmm33fKxKUVy4jiydsZhG2UE12IhJuRSahGkKvkzqbLtbLFkiXkgXSLoIQjXrX0QnvP298Qo0Tn0nUBYhPNdJb4gb5qMUGxpz31jHe4mo3YEcvDFppTDMpP6kv+3HP82aw7HYMGywgXRA.MkXk5nu1yJ2sbxQxJ3F4HkDUZorPvUpYj1v05d.ew25MYHvBarFRyljig3jDxEEY4dp2dAnvy9Gzkqe8qyMuwsQq0b9yedN+yeQVXgE.f777flY0FrFCJklhhBJJJvZMSImNs5KmDcpJuI7jx7yj8mBuCeoLQWJgH9aMnQgQqXX+AD40DGGwFmZSLFCw2nF26N2kst0sPhc7wd4OBKUuM5DghwdDaDpjZL1EbZ9UetKx8t2qyW3x2hO7ZaxYSqQ73bLQQnUFlbZ+jxpvCJxUSj0wSZWf+gcM3SiwxeesU47jf8pKDLFDI3sUn8n8dv6vpJIR4I0nI0FSpUgq2P7EkSM3Ss1hIJIXMBZCQsaPdgiQ4ELJ2SVgK30VJMnM3cUDdnRz1HA+zBePLzhizzTFMXLQ1DVesSwtauGkY4boOxGmNacGt9A6QMklO0u3uLejW4Swq8O703s+lea5s6NzaP+fIz47AFOlpKJpztTu82ChhgVKQ6ybFN+G5E4LW5kn85afowBr8nLJMQPsTRazj350vWIzPiTU4eUtetWQkOhoCQMRzgp8CB1SAfw6CgTW.Okb9MWAuuDkyipvgj6fQ4fKHx80p2DwXwasLV4YToi9tbFWliy6PHBGUD1PgnbUhcWvTUNfJgoaK5pwRLd.7gTY9DD2mlql80dlY5i4XNd+Ay1AK.llsh6qyI79HN91wry6XpLLSqG7NMkXXf2vk61gu1AkrxyuLpFMvGGgPLCxcPTJ0RRgBAWQI27l2jqd0qhHBm8rmkye9yyRKsDFigQiFEHPoMSS82DaqHIIgYWP9IIt8IDtNdfGDQvgf0ZwId7pC0vkHgNwQ93brwQLZzHFJCoVsZr9lafMNjUlaeaG6s8dbql2DyFZR0gF7bsEVfVwZNXv9r+d6QikVfUN+J75WeW9hW8s423S9p361iFlbr0pg2TcEfDx9gqZtv66bvrxsPN74eX38BRPG+ZwIYU6oQzydWSvxKG8BiiiCyA5IwBuJzxZ0QXwCfpJjoGorTOdTtTPQliEZ0fs2YOZ1ZITBLb7HVd00Ym6sEMShwkOlFFMmd4UXwjH5s8c45e6WiKeqaxceq2h8t6VzoWWPYvaT30FZtzxr7lavYuzkX4SeJN8EuHqe5UY+wC4N6tMiyJvjTmEZsLc6ODkVvXTLJujZIQDGaY+81iZwIXTZpmVCQT3cvRsVhhBGcGlA0agwXYzfgb2QkjlzfW5m7mkO7G8Sga3.t5a9FLZXW5cv9re2NLNOCQoHJIFchkUVYIVncKVYiSwhqsIosVDeTMFo0jW5HoQKhrVLwQnLF7kdzVCFiNHncsl77BzQQfUSt2AlfomV37DYrHE4TyXPxJnXvP1bokXk1KRut6vsdsuGacuawMtxU4d271LX+d3yJP6BozqYqE4Tm6Lb1W3EXsydJZs4Fbp1KPYTLcyFyvrgTqUa5LXHwo0vDESm81mXaBKzrIk8yPqjJAxWE1bufXjpT2FEtNTMgHnBlVINfW7nlQfnG+ZP+CXPMT5i7bywb7ij3cvjLGuB2f6uvOljMhIS1qNVZuXF8tNcr72E2h4JcS+7l8+SiPRUDfTbRSl6IOOiHwiZbnIKmIZtQud7m88tLFKb9W7k41CGQ+rLhRiozZvZ0DqMLdzX1cq6wku7koW2tbgm643Ru3KQ850qJtGvXiNxwnf4JqvIJbtxftdOFNx7fyH186OEmUheuhsxDxaS59GSR6XRRBEEELdbvSEqWuNm+7mmVsZxq889V71+f2hjRKuzEeQx7ELLaLnMDaSncZK5tWeN+oNMcu6t7meuC34tys3mYoMPFVhNQA5fi2W5bgnnEEjSi26QYzGtwRUjqlTLDp2Yiu9fpjxIu1wIReRe1S3iLaQCLQuaJaDHUi+68umr.520DrTpidS1w+4mDXxMlg.5nnydcX0UWGmHbP2tjmmystwayB0aPwftb90WmUii41uwaxe8W3ug27a7sn612CadFw4iIRDrZaPv1JMk3YmacKt82+6v+3e0eITKlUuzE4S+49Y4ke0OIma4Vr+fAbPuAbvt4XSpiQYvXMLN2w98FQ6VsHIsNhyWIZ7fIVZTJJ0BZsfxDQoVWofLCt7bJJJnTLnSEPa3i7S+yfqLmh7rPGV24B87cSnKmaSLf0fNNAkMlA5HbFEhNBmIBINBkMBk0VUgJPDZLXPqTTjWRRbB5XKkUV.gCgbIGDOFDJKGiueNmdwk3RO2Kxn8Nfuxe1+Q95esuHW9a+kAWHhUnLDYSI0FQDgUR0aq6wVe2uMeqRGr3Br4G5E3i+S9p7o9LeZ9DenKwk2YOdqacMZs7JjMtOwRC17LmlaeiahzSHwDOMhVBfRTgf3UcyziRmhSDM58sBvpz99nJlhiWHFG403YiUnOGywOthiSrBlIskOBnkpDQ3.wILHqfw0py26N2i8.Zbl0nPrDYSAaLnUAInDYfBG48FwUu50AfM1byfGSUqF.SI5XrQG467PMfGRQ4D0rNYeYx1+gu+GtH8mZRyyPBaVxX444G88WQ3LIIgEa2lSs5oY66cG18t6vNsVlZKzBIRiTTRZTJEY4LvAaMXDwmYc5dks3e7V2jOwBaPaBQGylFEZxzUcCDu2i1X.yiVBGOtF14GTw6YoH7DKg82iwrQuZZjs7JVr8xzu+.1YucY4UWkka0j82aGZEAmZ00o+ctK+E+8+87U9O8WP2qbcVJMkyzrMQoZN6YOMKsXSpWqIk9JuxxnnyvwrcuCX6N8IWKbmu62g+ie4+d9J+DeB90+s9mym3S9JTznEu1MtKIMV.aZL23N2lEWaIVc0U4pW8pbp01jhgiAYRYrpQWk9MwpvqU37ZrVKXiQrY3FmAJMFqEapi6zsCJenE9PTJhUlIcZBkQZbZeHZMNGZSDQIoDWuA0SpgS.s1h0Xwn0XPgQG7OEsniEkoST...H.jDQAQE4q2lv3hBx7knh0nMAcXEmZY3A6Sp.erK8bn5Mf+n+W+Ww25K9koraOFt2Vbp0WhlowrR6EY4EZyB0pSr1FRQn2ynQYLpHmNiFws2eGt923qye9W8KwW+7miy7xeX9m9676vuvm9yvqekq.9B7Ei4FW6xr1paFz8VtKnwMQc3JJTfE0znL8vvrUlywtX5cXgTb30fywbLGGBQjJYY7z+diGjgl93tsXIn4fB7zy44NY470u1UYrBNy4tHEkBVcDninv6wW03lG1a.6r0Vr08tGqcpM47O2ywJqt5zz.Jnv4qruGnpdnUnlxlXBAqC0s6IMdzihr0jOtYIUM66y6bXLlP0bKdbUomLxXIodKt34ddJGly96uG27N2lyjFbPdkSH1DZUPiIl8FNlycpSyf81k+gc5yuR2wrV6EwMpO0spCMJ0pEBKhDLqaQNr1udrNi7iV3ceDrf6K5UGVQIOYHYM8mADuFwqY7fQb1MNEtxwr6MtNu3yeAZGY4a8k964O8O5eKc+BeAX4U4UeoWh0p2jkq2j0WYIr5Bp2HEsVynQivIJhSSw4gCFMfwEd5kOlc51gKeyqyM+ReY98+JeM9j+J+x7q9a8axm+m5ywW767co+ABmYsUXXYI6t6tzt8RjUTFXxippbVCFooVqQiCMZrVS0MJZDpDudjBJJw6jPqywUTIDeGJenEUnrQnLfS4QYDzVChwB1HrQInRhPLZr5C8UEsZhkvEHmIJvXhnrpBWTZEVqM3fv4YDkZQId9Lu7Kya80+F7u6+8eO19G75D6brbRMdkW9kXyUVhFIVVHsI0RiCshHkNrMpTTV3o6nAzazXdt0VgOxYOC2a+cYq81iK+U9x7u512ge9+4+F7Y97eNty9cXuQC4LqsFiJJnnzgVoviopPBLDTXeUZHTpJSj8w+Zlo3ALv7i6eeXvvGq+74XN9wGHbjI5eReKxIQvZ1Ry+g92JfwKj6bjozLL0x25l2hq.Ta0UIs8xzsaeHxRoyGFqwZHuLi82cWt4MuIIoor5pqxJqrBJkhwiGGrggn3iHZc4DhzFH3kJeNfilZvI+9jz8ce6eUXVINbRu9w+cQjf+IZLnUZVbwU3bm9r7144r8t6P5hMXiMViXqAeQAVaLw0aPm7BxswDuzZbuN2ku8suCmeokng2DzIjO3CWytO5mPtZ5d68e7+okc679EdeUj6u6Qv7PGlkSZbMRUJFNXDu3pqx5hv+O+a92ve6+G+9Xp2fW7UeEd9U2fl1DRDgkZjxBKTiCFOldCO.edAkY4njfi7lDkRMslMWdEt9MuAJrbtW7ix9m5B78e8e.+f+S+m4a+28k4ew+8+Ovm8W6+RdiadC5r+APRLCFNhSelyyA62kZQoAghKGRg2ItfinimXaZvOqTfXTfNFhzHEE3JJIxjhyW.4EHE9P6lQoIxDix.ViBhLUgnEPYvoqtoV6QaipbcdMdQphjlffAGJFMozgsVpEGiurjxgioNBKIFN24NOeg+8+w7+2+m+Aj+5uIab5M4kO+Kv5KzjkZ1fEar.ZuGwITLXD8F2AektHLlfmorzJKypmZMFlmwVczDqJok0vAEY7Mei2h+3+G+el68FuM+p+1+lb5yeAt0AGv985SbsEB2jp83TlIQUuxTTe7tBIjO8iNP0Dx+J0i1HDOoUENGywbDvQpF7664exiG1hjdTQnVAPQP2RcsQrahlu70dKxQwoN84H2owgIHaDwgKT+3LbvP18f84f9c44uzEYkUVAq0x3wiIKOmZ0pMsB.CMidAknlpo3C0PrupBtOb6c18oGlEMLcenhD6Qdey7yZAjR2TRmFBDeb4E3EEFukUV+zzOeDW9FuMas0cYw1MncbCF4KCFxcsThLMYm85wx0agX2luxN2jO5vyyGsUJ3CEIFZMJqlHsI3YhxiVBF+nNdWSvZR3.gCW4vzKL9gHELOxuuYYuqDPGg0FQ1nQbuabK9ItzEnsR3e2+5eW9a+C98Y4M1fOzYNCq2pI5xbZ0nFar7pr+96ye6e2eE5FFFWLFkWHwXQ60HkNLJK0hSHJJgM27Tr5lmhAiFhJNlW4EdQtSiE30u2c3O3e4+RvZ4m5W5eBe2qdUF4EN2lml6syNzndajJ8C4pp7NQWhW7UMkZgHsGsBJU9JBVFDQgyn.avjN0XwljhRTn7UU1WUOzaTYNZL3jPI5hVg0DiMRGhvkvz9XrRTUB+tx4vTAasv68DYhvUVx39coQTDatXKZEo4K8m7mx+9+W9eBTvO8O6mkrs2lzxw7bqcN1e6c3dGb.9Rgh7bFMZDiGLL32KhfVqIMMkaduaixZnVyFr75qwktvEnSutb86dW9U9jeRdy6rEe4e2eOx6Oje6+69cHpvQMiATgVpiuJFyAMXEdTWEx82IWuLAytJq2ISDb7A7dVoJoli438SLIEgePCZATENJbkbPjm2r+A7FNglm5Tzb40o+nB713pNLgfwDla3fCNfti5QxBMX0MVmzzvhjcN2Tgk6bNxyyCNDuJL90znYMKAH88Wz.OzBF6A7by93Qliz6mVg7SbI9ouOuvn9Co8RKR60WizCtKc6c.c2aOVXsHv.YhPFfMsN6dmayJsZSq0OCu4suNe6suGWp84no2ipzAVprE5C+t05StWs9i5ZuZBdOKBVGWE+OIl743StIhhRQnX3XhUFdgydAj9C4e8u6+a7M++5eKW7CcQVJwRy7wzV0hVKs.6t+A7W889137vRqrLYiGfwEHTEoLXjfiHf2QtaL4ix4s51ijZ++yduYAIKommm2y+RtTa89x4LmyY1GBfA.CHIHHHnIo.WEkLohfgnCqv2ZeutSQXeuug15BeocD1lzzRW3ME1lhzTTzfJjHE2LIHA.IGPfAyblyZuWq41+hu3Oypyt5p69bvYvfAyzeQTQ0cUYkUlYk4e99+989890gAquFQcSI1GwJatJub2D9ydm2k+Y+i+GS1uxuB+3+B+84q+38Hqn.uEzQITlWNe6VJBcQPur1rNsd7tJhDBzZIVuCav0t.kBYrjphRDTmlOj01UfbdO8a601ntYI6vSEVuAmHXSDPvoekRBMsZ.uWPPa2ATWpjTJylFLtz7BjkV1c00IZZN+k+4+Y7u3+xeEh2X.uzVaPRVNuzctE6t1JjO7PFdx9b+8NBmLBs3TwjKSOk0rQkYHDJ7V3frgbuieLCVYE1byM4kewWfu9W8s3St0tn93N9y9U+0XRYI+m8O4eBLdD26jSP2c.dgGmHv9lPTewIMsHmK+rrySK+xqFpmzy6fFndWGWGWGsikcy92u9NOmEp7DLwdgGn1aCOlJ9SemuAy.twNaiTGgYVNjFgAGNInkBJlkyQ6e.YUkr6suAIoo3bt.XJg.sRi05vZqE6sS21TAOc6VHQ3c0jMsvj1VXrp1Our86E0e0xlHXyyNmadOOTJznzBlUVhtaJacicYu2dJGdvdrdmND2ekf2MNYFchRIRFiyHHs6JLC3ub+GvO4c1lUvQpVMmIsl9167ue9dR72umDO6LX4Ou.6Z.W0Pe7k94uxkXYem0m7HBhfV4gsGziM62kei+G9mwW9292hW8i8ZrcRLqmlxVqO.g0xa+VuEiyxYvfAHjZxlNgzjnP6ww5.iMHLQoNzDhEBxxKINNlYylwvoSXk0WiM2dK79UX3rL9D24171c5x+G+S+uFbN9w9G7Ofu58tG8SSXV1DDnw5OsYGqHT8eTK9ZmwQjTiPEg2Uc5ElRIBkJ3qINQ.ujKvbk.EpnPkGNJaZnjniTHkBPJCsIgPSYrtU0DLGUD0nrZXuQHw5bXrdPKHJVROUBqkn4M+y9S3e9+z+qn2pC3NqsAazoCazqG8R6v69N2k8ev8HNMg9qrFVwohHWTC9w68X8dh5jRoITxxQoI3bNN4jSHKKiU6MhW9NOG26ceDee6rMc97+H7U9+7+K9m2Mk+S+O++BbZI6OMGWa+3xKnw498mebqfl.a8ZWTUDRc0tHV9DrVHZOqx.vTu2VCR85353CGwSCbH4EMv8Gfl6w4XKhf2H5qeF.qWQoTxIdA+UObBQcTD2uOEtRbDRwmAP4kHbZJlNggGOBmRvV6rMRobNCQQQgJFLH4ifDIbXITA.JBskh5AbpaqZsAXsrpHrszFVFHqESmXC.mlkMtUyjtsObIDBjZO8VoOGO4DhRzr81aynG8PFdxXFtwL1c8sPIsTYqneTBC50CSkgp3HD8Vg2Y5H12XXSkjtnHAAkNGVqGiRGbR9FFspyfi.QM6Ugryzzp6d1B2BO6W3+a96k+cMuG2dIeCMKyhmxeU3WdluCQC.qveedZJunYwbFwvIHXy+Bl+n8IERYPzzMOzJ077m2KMB+zwbq0Wk+3e2ea989e5+Qd9c1g9dH15X602fYSK4A6cHSxpPnhvYAmwRjVirlMHmqtROjZ7Hox4Iqrh3zThRRBtVKBlMdBO5cuOEimxKs6yQbdIu1laBilv+O+p+p7fu7WlajFS2nf8FTgMz7k0InbR7UdTVARBUihLRSk.JcNbdABYXYkBMBmnl8JUv6phzHhk3iE3hDXibHSUPr.uBbJOBkFkNBkNAkLE7QHrZvJva8g9zHMNhNLY3X1Y6sIuZFGObO1cm9jc76x+q+29eCISGxqsxJraRBazqGUNOu06ded3ACQEOfNoqfRnQ3ASYEEY4XJJCtle82gsxgxqPiFsURrWSpLFaVE6+383cevcYiM6gpXL2NVya7weM9x+O+qyu+uw+B1YPWvWA0slGgwwpc5gIq.kHPCevWrB9VR6yYDh.nVf4BOEBksrpFPXTC80BWX.OuEZYRdWDKWAcSD5EWWGWGeub3a8X9qsvcLjRYXhZVWXhn0g0F7g64LWKky84plWuYxhMW+EGGO2OlZ97OqgzWOeR2ofLLdCUtJLlRD3PI7H7NDNKJ7nbgB6ovqXpLg6d7LNwCas8yQmjtLMaH9jRJHmnXA9RnZhiCNZJVmh6b6aS2jXJqxw4CSXNXQAVjRQXxt3QQPZrRB9wmvaQ3sHwgjvxOuSgT6GSserHno1Kqy4v5pv5pBlasvgP5QpX9iv1mAm2fwVhyaPp.DNL1BFkeDBoAaQNQdE27F2AcROd7QiH23wWZXk3XxmMFk1yvxwrW1H5u6lbOf+n6+HJVYcJljirvPhsjDkK.rTGgwUeuUmAoyfzTfvYwg.CLGna6XQIbzlsty6VAmkotqpECE9PK6r9Si1v0V5RbEVCT63C7SAu8Ar1gPHPI7ncF1Y8Ar+67s3e6u8uMcWcU7YyniD5EEyviOgoSmRYkIXK+xPk7Ib0tBtwNeVBgKLC.7DZEQoITYsTZN0o2w4oLufYSlR13QrYudz044S8ZuFU6c.+q9e6+E538zUIQViN2QsajGbeJD9PuiB.mHnaJpYeJzmCW7Q8IUyWtfQa5UdjZARsHvDSsnsc0FZpOfbMLqMmHz.kc946637r4ZaxCu+Cv4rb6acChkN90+u++Nlr2C3U1cShJynKdbkFN4jSXRdAw85RZmdjW53vCOjp7BRihoWZPbmlhRpxKBZYqdFKBmGesizKsdhPhVIXZ9HNZzATMcLCzB9X2bW5LnO+u+q+qwwO9Qr6lqi2TgvZHVDgIqfj5JzQJO6EcMmL2VKfKFOSTUujACtNtN9vT7jTUWmgg3uKyX0U88eJCO0oECW8XegTCl6fwkNt+gCoBnamUBfAiDXkFLThwaQ4EXJLTVXINsK8R6TqkjyFsYRpM3HmyV+2VBFlr8LfqtnGKirhKJ8eKldvqJEsV73oBg1gv5.qizntDkziJqfY4knjRhjBzBvKL3TBLJ2bOt5Q4y3vp.iXXsfoDuy.D.VIqsnnPus0gx6P5833TO758l3BXuRD57Hyi1.xZ8c6VxqMe4Z+OOkaueW+NFW0ICsOYq8qEtXATVGazMk+3e++c7v+n+8rV+djnUf0gVIXx3wTjkOWz0MnhaVmkkkmYlXPfh2xxRxyyqEZs4Ly1ppphoSGyjISHVGQjTwM1ZKRzJ9K+c+c4s+a+5zKNhtQAaNPVewUX1Eh4ntWpCD2XoB0y56pdn0ZzyMQzy52SKVhuKdwmv4wYpP6EzKIkURS3q8k+y4a9u42iTcDwQQzsaWP3IuXF44yv5pNk4QmgnnHLFCSmNk7774FYWiSN27c0dFZsYlb5rYTTjQTTDiFMhphB9re+eFp9peU9+8272jAQQzUEL1OkDJJJPq0mSqBWGWGWGWGOIQafOVAjUUx8e3C.ftqN.iu1qAawTjPHX1rYjkkQud8nWudyKlmK6wUAf5IEfUaFrV70tnu2qZaq4w7JdzZoSmNzsaWbNGSlLYdpNOu37kjpk7vCOfilLBmVCz53kmfIaCyyVhzScOr88RfUevN9tN.qkEmADvYnC7rfiD.IROiOXe92867uBhhPTUv5C5iuJX6BN2o4CuoOM0bxjy4Bl7IAPSMfsZVdsVetu+yrMXpnLeFZAL93i3Eu8s.7767u7+arYYrVudnEVD3Q3Cr4nDxEVmmGnUacCcUOZurKp2nKB.R6KtJxlwVqsJw.EiGyuyuwuEjDyJC5QYdNqrx.rVKEEAVpRiSHOOiY4SQGGQ+98INN9Lf6Zxy+7T3sft.ZuMzsaWprF7BGSmNlGdu6yKd6aQ2W4U3K+a9uj68M9lr8fUPSMM6BAFWPR+my20Vx920w0w0wGMComfdTgld5V3dJ9P0H6bNHRQtwv9ixnS2Dh61iYE4T4BUWXy3HVumYyBUHc+98m29YVFnlqBjzSyi1qiEW+WFHtkAJ6h1lZqMq33X50qG.Ld734o3ENkXiFxFh60g8rvimLBWcWPwQHaEJg.oyixApEFF1KN8wG1iOP.vZQZNa+ZKBZn4GXmKzFWVONhu1eze.l+luF69b6B4YncV5DqnrH6LTH2lIkl0eTTnh2ZNwSJgnHEwwwyYmANK3tFliDBAtxBvTQw3wrYu9biacat6exeBeq27MYPZBwHP4CZ7oQncRBMe5Eo29IET0kYy.sAasHPsyGNRzJjNKwNG6+N2k89K9KIpSuPJ4RzXcUXbU.A1279yxDUyyZslNc5Pbb77RTtppZ9uUs21fSYlLsWWLlP5GiSzHEdN7gOjO4q7pv3I7m9k92vZwwjJk3sVhSzTVVf2a+HQY9dcbcbc7zGMistnHva+2VuCqPxIEyXLP+0WEzJxKKv5.uPM2GmBckhPiatSmN3bt48avKh4n1ROYYox6IItHFrdRY5ZYLes3+2LFdy1aZZ57B6pYer4Xf26m2FfhSSXHv9SmPoRfMT9.zXhNMFoirFPaaQl63iFLY8cc.VKBnZwnA8b6kugYqDkFJx4u32+O.7V53cjJ7TMYB8SSP3r3cl.Hgp597mwblhIHKKKbi9zT52uOZsloSmx8t283Mey2jISlLmUKSsVrlCfABh+rpjU6zgrgGyZc5.EU7k+C+2iOaFIJIxPGjD7g7A2rudYo35oAf0hWv9Dy9kPfqpDkyv5oc3u3O7OAN5XhcNzdHRpX3vg.fRGbo3phBzZIp3fiFGEo3vCOj+1+1+Vt6cuKkkkzqWO51s6b1AWj4p1CLbzQGQ298vKB.25lFyd26drhNldarIes+3+XlbvAzKJBu0hPDZrndo3bCZrr8+qiqiqiO5FBg3LheOT.N09CkyQtyvdmbB4.oqzmRmAScud8TOiJTbLk0lHZZZJEEEWIqSWEiUOILZ8jvH0E82OIqefy.hprrb9jkMFyYdulimMjTHiiv.bX1Dx8VJ7gJvO3P9AlqTtSSQXS7QE1qfOf3j6Rec4027BdevlDbAgL6bAE+2Thmx5a9mFo4Aes+JN3teKRWeUlc7QrcrhXgCSdAJoLXTktPCL1IkAQhSsIn48TUUQbrFgPQYYNiGOk82eeN7vCY5zoDUqCo3XENuEm0RbbL3B.tRzZv3IRoX7jwn61ijt83a8W8WygO7gjtwMvY838VZ5cdgDbYQfDuWP6ZoYQPAW07bZV5ESQ17iss.ndNFu7dJKKIoWJtrLdy+7+BvAcEZRkNDdGH.iqBiQgy0z45Ug1BsOXdcSFOj28tuMc5zgh7Yr95qSZZJoooT5s3bdBB67zT91r0VTTfy0ijtcHexTjVKRoB2jwby9qv23g6w23u7qvq+49BLJOm7pRhRSPHCVLw00w20w0w0g6Bprqv3MsATbZJCcNGlHM6M5XJAjc5vrxR7JMd.iyfi.fhhh.a7860innHlNY3bImvkvFUacvtLoa7jNIvEIh3zwwOkvg1K6hDVrT.V0udkwPjH.EHKKCYjld85wAmbLUUUjjDcZkfJkXZxjiJBOvgkYLwZXcuEsTh2Eprakul.Pg6TomOeyrt.rt.qS3CKw20Yv5T+x5hiEQb2nQpDsh24M+aHxTw5ooTLdHQNGqOnOiN4XThVoXTpNKqF0Tc1saJJkhoSmxCdvC3se62l82ee.X80WOHzc3L5wpYcZLgx.VqkLc7D5FmPj2SGk.6vgL7QOlDoGMfnFTUS0r.K2IaeRoNd9wuKQ+QMW3cQ5xRHDjlDg253dey2hpQiIt+.RQfsHmd85wfU6SkqJnAq3D5jjRYYIEEEDEEwvgCIJJhM1XCzZM26d2iuxW4qvcu6cIOO+LBjbwzypPvM1dGdvCd.VqkzzTxylRpRQ4nQrQZGvY4cdy+lPpLEdxymQbZRX+vc0L3ccbcbc7g23pjIPsc.NOUURXdUs48dPqXXdNFBUNdVUIHEX8gBfZ93VUU0Y5HAkRMmomEiKiMqqhsoKSj6Mq61Ou3qs3XgWz6s3x0jUFgPPYYIPvNMfyZEGssaC.PE5VHybVJrNL1SSIZHGq1P1aVRrn+E9g03YlAq.n1SOnuLjyh1uWqeXmqSH04SEX6TfEEEMOWwMmL.PUYIO3seaJO9Hp50i060Cb9fIVt5pAvQwoTYLXDNTIoAGZuphNQQrwpqwjrIbvAGvie7iY3vgn0ACWSHTr+96ypqtJJUnBR79fVixxxl6MWHETTlgVBSFOlpnHFjlxIylwc+FeSdwO0mAkziSBf.jgLT6.PzTochysu2DMZ.6hhkowfk8ds0AU6eazJEqNnG+Ae0uJbvArRuNTMcDq0uGUlBpLA2fOVqwTSWrVoP5ETZpnamdysvh986i0ZYxjIr2d6wQGcD25V2hM2bS51sKEEETUUcpmTUUR4TGau0tTlmivZnWudLaxXbkVVassnmNlGc26x3SNgNcRnvDpVxhoUzsaWrEUKc+p8w.sRi2UDzMPb77AFkRIdmYoCX0tkRrLvZBgnVgAeDYjhqiORFWlDNVllJa+5sGueYSRFgn1CoNc8cUemKFRQvvgW1moQVG3p8Qu5sopZMDo0ZlTjw3BGwJ.slxbaP6UxZwbKfjtIL0NMLAyNcHOeFNgCuzOecu3wikM4t1S5s4+upIAdQGKl+50E9yU8YVF3KOA.TMZMtoRzqpplmlv77b1ZqMvnJYzzozSGF+bxjILXq0vnfQypHqxfWoHaVAc5lfNRQkwgivwIq2iPD7Gw4+1UWDBOI+teQmKrrIWyh2Kn89dKFGeZIy3am38LmbO72mem4I5DH+omrcgmH05BySyCrgxYSAmCg0DRYUrt9hnR7BnxTQ29qRt2wjoYzsSB2bmcnX5Ddqu02f6+vGhwXHMMkc1YGpppX73wHkZRSSWJvklvIBtft26IINlTuCqyi1ATTxw6uGBuGouFYO0L1U6B4.K0MxaGW0wOQsKiurk6pYvwAxvrVpxlAEkDklh10zToCC.5DxkbLPRif2axUebbLoooyuf0XL7Nuy6vd6sG23F2fabiaPRRBiGONr7oITYcAG8sNksduK3iJlJjEkncNJmLipxbHIJj6eqMnihpmLiJ7bf74rmOICFp0YVd+R.jdcbcbc78NQ6qbEN+blqD0xLwKkTVZI2.kBPEGQGcBhnXbRCNkGCdhUZRhhQoTXLFzQAgIjmmShtSswCeJvp12GaYoC7Y4F6WD3iKBjxkB1BB6StSshBHjsl3334UTnVqIIII72VOU0e9xxRPBFCTYrfREHcv4qE0dM36EmDpyiRD7AqKa+5CCw6IZv5h.g3acP9x9rM.rN+m87q6FlEZrNfwiGOGTmwThT1A.JLFj5XrHY7roPskBXpJ3a8s9VL93iHOaJZoLznkqOIswikZ.vcZ3HjdO0Y1FMRv4sjFEgnTfor.YjBvwvSNAecquwUCpRJTHvFzwjOn7rK8TqKSH7v7KtuH1qtn+eN.XojhhBxmLCJqPaArNbl.3kfISD9MBuLP4tzislQNq2MOkoMBhrskMzTgl6u+9Lb3PVYkUXs0VKnWqpRbdQcuTTDRKes+yPUIl7Lh8BxmLlhhBjqtRns9XcnDJ7N2ybSC8BakNKY.xqiqiqiO3EKCLQ6ITIVxxN++k0sbqf0IxQGcBmLYLUtbp7Vh6kvjnwL9gSY7nLLwQr5NqRTRLJk.iygxedlqZO9W6u6m0wSN21OW78MW74ykxP.ioBjm5agkNCSlLAiwv8d22ks2cKlNcLXcXKlgnzfsHitBMdWfApRqGSUEh3TzhZC715PHZfW3vKB54UXCByJzQQjf2f+Cw4K78DFrBOeJqUyoB8J9rhEvecNp9Vxq0tJFpppHeV17ky4bAwOKfRqgz3DzpHFNYJJiijM5hIKbhyniNjdcRYyc2jtzEmMHvulTc4bNN3fCNG0pmYeUFbCWiyh0GJa2YylBoIfRiqprUdzOceM3mvgsUm+xci3mDFrZurKalSKt9Z+aki5JjIOGJqczWCAPV0uuyIP3Enb0Wf5D3Q.0U9WTTzb6YX5zoymAjVqYqs1hhhBN93iIKKis2daFLX.IIITTTDz3nzg2KvSiWp3wVUQUYNJojx7h.KipHbhL.IZDgNbyUzKq70ofXYSBHLHyxmbv0.qtNtN9febQLeLOsc9SyPAC9+I...B.IQTPTQvYzeEsdVAQwh4i4e+G8HrBGQIBpdTc6toJhHUu4L2HzUnrPjJ8RSs0hhbeY2i6a280vXaWbJDun6a07ZNenHujZERzysdBScFWzQQr+idD3L09HFn8gV+ST+t38ASJ06sXpBeFUc1NB2ezM2JFr3Q4s.QgTCZ8Ms00yEWjbO9dw3CDUQX6KBl+Z0OZyjv7C6tPtu8VWX1G04c05ACB7RENoBqPhEA8WY.kUVN5niHRJ3Ue0WkwGtN6s2iXu81iN8CNyajWQdVnbbkRYcy6L3cUmdx4oBU2gfJ7XkRrdAl5zkobt4Famy4v38XqQr6Qhed+55pO44oAf0hyP5p.b4lCNzCdYHig0GWC.CC6efGk2iyQcNzC.uBoPzh2VEZAEJAQpnZSZM3NvSGOjrrLRhTbycedVc0UwVUvrIFh0ITZsgF3rOzDrch..Nq0RUQ47sEq2goNErglecnBTDRI7T1vPaeQ6hBIs4X0hT8ecbcbc7A23hXtooHpjMxPgyxfiyERwkQ5QohX8UVGmWfJwSmdcXzronEQLPLfM29lzcqMYTwHNZ3iv5sDw4k2R6+dYouq8+un1iun8qK5079kqAqKk4pl+WbJCaMllp0ZYk0VkabiavNO2M4d2+tTUUfonDeUNTTgzYXkdqP+98Yz9RDXOE7ZqsCW3KcdKwYdlFb9Pm1wEtweioQ+gw3YFfUywkk4yRO8qqKl0E3zSPZlUfRoBZ5IjzM7dOEUFRhhPphnz4YxrILXsUItSDUUg17x1quA2X00Xy0Wk29A2kwylNWGVM5GRJkmoSj296uI7dOkdKJkDuLrMnT0MwzxRTJUvnM8A6XvK.j0sfXgnN4fu2zvSurikWj117NAFgEUjlzjDPopaJzgAdpb1PuRD4bML3qmxg2IvKrHizyasPMFTWyr7lMaFFigs1ZKt4MuI862e96M2M98RDt1BhslAqZFJCBtb.BTTUE5eXNGXqrnkhmn5fcwTFT+GmdbnUJAaNd8g0K3uNtN9vVbQxKQ5OcJrsAY07dHfjjD7.YkgIaswFav5asIRsGivvy2uWXLwwdR5Lfjd8XhcJV7jDEEREl+xmH1hSd6YYRaKKEgKNw5yvR0kX1ot126VJlC3qQWpJkhacqagRInJufhoivmWRU1TRbmlMIMPjJFoWhyZQUWw9PHCC1VIxz6CLBFlgNXUm95KNt6GFlf66ILXcQ2P5I4lTgO6xEG3xlUP6dCUnRG5AtPCN168LKujtocHVGyrpB7BXRVNIwcnWudXJx4vC2mUSS4l6tC8Wa.28g2miN5n4kj5hsF.3hDIn.mWfTonrkorYMgkc0UW8zkUD7vcAh2yy47EMKmKRGVs2mpbFh6DXvinH7sJOXi0iUKgllUsWhvKBUPi+ry.pY80tRA61sKqs1ZbiabC50qGiGGzRUZZJZsl7oUfSGlZiqoAf5nxYmKR9JqgztcPEGQQcEUpPf2XQFGG5Z6W0wlKIEgy0K1EnAqqiqiqiO3FKNt7xd+Kyq7z5Xhzfz.k4ETVZQDqAqkrxY3vShtCGs2QLY58n+M2kLxYz3QzqWG5n5LGf0xzy4xXW5YIEXKCf0xVlkwn24.Ywoi0IHX8QVgg77bdzidD2+wOhd86vZqsByFOgImbDIHoJaFSlliunKXgXAjniBSh04PfrN0g94oHzI.Yy8xCxvZdeJbYZI5CKSv8Y2lFDLuzKEBAHEm8YXt4f1DBe3.t.YcYZddwA1FfUasW0bRhTJQpUnRhAgGi2gzKvTTgn1ZA7kNVY0UXVQIyxmRjVi16ovTwIiJHqXFqs0l77O+sYiM1fG7fGvAGb.Q5DhiU0VjPf8INiVcpqvLDnEVz.EEULIaFUdQHka5D1XycCmj4CzfJkNfPSeV4cHEAMXcYwhWP0jXwEO86xnjt8w01gS.VqCQrDc2TPGEDse8EsBqCc8GK.txguIUmtZ6lnxfP3IpVX+ylkiwXX2c2ctEMbxnQr2d6g0G5QUSlMCkPfRkfo1g.cNGhPQ8FDTo2Qg2SkvS2NcQEGrpildJYIdjZ4Y.X4f4CltrCqKirq1sTn1GKaphvqhB+qiqiOLEh5INcYQ6228jQh76QQfwauH77oFXoCoOTUyMU2L.dus1uqr0iWVOFIt4r2TMMiDGzUEVtSlNFQVLoZAFeEGkeH6tysvJ8LqJiARK8RSYbl9LYTooXdVDf0hF8bX65oexaWDKcy006R.ZtLfVsecgOTzU1RKE01zfU5HRKvo737gI4pEZvXoXVAIIIn8JpDdhizj4snUBhkZjdANK3kJrJB2SV3PWeeaoPWOIZvWOt8YXaTvybQK8zFdQifWl+Jy0qTyubmULOt4uySxuhOS.rDTCbpQST0ohIfGIv1jRJ.enoOJUxfy3Vehlivmy4EHbtyLC.uuYOPh0QvY2kZP3w5bHjBPqXyatMucpl3zDN4Q6wNatCxBKwZAZiG6rbrdSHsSJPojHiSBVBfRv6du2gdC5wf9qvG6UeMtw16v96eH6e3wLaVNqudvsd0ZMylMCmyfNRhqpD7fYxX15F2j683CQ1IEmSxiGOC8MtIas8sv6jnzJLRAVrHvhxKQ673sFDJAVAnNC.nvwkliwsilS.C.VE37mZm.KlB0lWqcStFXtOPYvPTrloEyX86bSPYYrTPmDMUEkTMbDqtwpTVUgCARYB3sTUBZolzNw38Y3rkLb3wDEEwl6rKau0tr5pqR2t84dO3A0UohNXh9dGBQnxKMVC5N83jSFxftwzMQx96cORRh4vISHqLi7pJ9Du1qfiPUWVjkQbbJI8hYR9XzQQ0C3uDJxqo5VqUjmkMupVrkUHExfF9Zc2glK10ZMTmd21y76bCRIt5B435353CxwBi5DdsVfrZlvliFV3WjYj5NswBWHDR+e35iFoRn0ZlYxPq03Lt4St12ZcJv0xWoN65z6cA6Ygvy3q8dKgqlEl5IE4az.aXBaJYPopNiAPTWwfd7FCqq5x1woXxyozMAW2f+FlXSQWnnDGQxHz8SP6hAkAqoDsPF.0YrP83.BBSVz6aUcxsu4cqTTN+3TKlGVT6VMuV6mm+9MGybKmY9lnYL+Ee8F.VNimzjDFlWgWawqgLJPGkhJVQmNcP6U3ysrhtKIHYZwDhUZhDNbFneOAwREBqfzA8IWZYDUTo7rhQPrCTdEdkfRsfbMD4JQHTgic0GhDtSqn94mK17+xyyDX62+a2vIbmS+2y6ikg650rjmt.hmbPfOyS9noOCMOmuhS2bDhP6tQTK3618DpkkxlKJZ.Mz9jNmyQo0vq+Y+Ag3HN4jSX6m6VjkWxwGODWkg9c5hv6QqfnHMJUvfOsdGU9PZEGrxJTTTvC26wbx3Qzavp77u3KyK7BuDau8tDoiwXbTVVmdpVNSt2XYs98X1nQzqWON5jiI2ZQ1Iknzdr6MeNLNefALQ.Uj26AqEoyi9ovjJayB3oWS5mmu7E0+VaW28hz.fzCdafR3AasAcesWlQiGQNvgiNIr7VCRmEbglUsTKpasPBJJJPHDzoWW1c2c44eoWjW34eIVc0UY1rbt66d+fn9cgToBfutJSbdANALMaF8WY.NmiwCGQuzdL7jw3zZl4rfRxt24NHhhIOufznX7VCUUEDmFgSdVAt6o04fBXQJBatXZwmWV7854++5353oIdZtYU6avrHHfE+62KBo2UOt2oOC02Dr0+234fM2pVNert42EuVKrASuL1IXiN8IB.SIpHIEU4XLNTnQ3kjmmiOQhW6nvjE5EqJ0b4TbUt09kc+t2K50fMwxlD3hwY2dpklgKPff0ZwHrHSTT3JwKEDoSvTVg2BBuDSUENiEoDrtJj.ci0jnBZo0TZw48XTAviB7ncdzVIZqLj0DgGqziSXP5aAndgme+JbBBfzOSwRc9BmpMSW9E385hh2SX28xtQzhmf8zBvZQAt2Ncg4kUr0y8br5K7RfPRoRviFdByrU3kvrxY37geDUBOfCm2LecY7PtwiEEHhvXEjWXwih02XSd4W4UoSu9XcdJqL3QhPDgyJoxFppt33TFNbXv7L8NrRIt7B14NOGqt4FjappEK94Ol.x4sugycbSb1GtK3wxNdcQla2xx8uv.lBCas0N7ZehWGxmgraJ4dGixxov4woBBzuzVRk2hKBrQdxohbmiAqtAO2seQ1bqafGMiFmQdgAgViEvIB8aQqWDdfXt4k5cUf2gDGFiIj91ISoS+UY7zLjasMuvK+Z3EPVQAQoIXLgF3cnJOe1hK5bvqAWccbcb0w4JpomwBc56TwEc8rR.at1ZDAXyJniNdt9OqqebFmOi33XT5XpprDEkPjJFmkkBv5Is02.LWSwMOuLfZK68Vb8sHXtKB704.vgmRiAYjlxpJ7VGI5DLEFzBIcSRIOu.iwBREY44TYMHzQXMAl6VqaehTZDdG1JSHiUhf7e9nd7LCvp84sKSyOWj1pdZu.rM.qFAzUTZnTp3U9LuAze.CGNFRiwHgiylfCKdeENaElpLpJyCT5BHp0+TVgEzID2c.Njb7nQr+gGSVQEwocoa2t.xfEEHUXMNpprPsP9LFK4Ekb2Gde1XysCtSq2xq+C7YvpjgdzTMXJQcZt7VOB+S49unEs8s0.wSQOrp43+bcrgBkPgqxQTbB25kdYX80oJRidkU4foSXbVNVGyMf0rpLJbETpMXiETXcfHBoJgpROiFMlISxv5EzsSevGXvx6D3pmsTy+CPmHM4SNN338ZAGdxPbZMj1Apr7w9zeF5u15LqHzivTZAVWEZsBu0dodH1hm6zr+ury+tniWWGWGWGWbr335rjI28AkXwwAUHP4Er0fUIAn3jwjHEnbgFbeo2hSJHunBkN3r4FiAEBhzZr0sgmEIMn83usAGsLfPK60Z1VeRdbYLacYqKmyg04v58TYCUMeHchRh0Q3JpHVmPTTLEUkXbVPHnpJXqOp3PU4GArdmNzotpCCzUHPiGsWTm8pyddP6+68S8V8ci36H5S7h.S8sqUNrLwA58dJcdr5TdwOwmF0t2.xyXyacSFVLi24duCcGzEcjDcHWXfoB7VTBIBOXsdhSSw5DLcVFEkVjpHj5HJJp3fCNB7xS6mU9PuALnmfXhhRHaVAU05Hi3XlMcJa95uNu5q+5LyThQ3m2o2CjgJm+PbUpIcgvQMaVbJfK6EXTlK6X2xpLyTcBtRGEkNt4y+Br4m9MXxnQTDqoPI3wGeLyJBMs4n3fP1ysYj6KvH8XcdlLqfiGMhY4UDkzgt8WAoWx3SFivIBZwnNEwm6AA8xUTNkwSmPVkgnAqv8d7dv16vm9y8CStyxnoyHJItd.DKIwZJJy3pjZ3SBXoKhV8qAZccbc789wEk0DgPfx6XkzTVCn7jbTFOQZMFmkRaEVgH3+ddGp3DPDrKlHzHsmeBZKCTyEk9vkA15hxryxd+qZcuruiys73wR.jkfP0.JA7NGIwgwaMUNLtZS016Pp0AczUVPOfc5zAo2SjRPjTgRHCZttoOM5CBp1xYkIzGEhuimhv1.o91MG8MrV096RHBZ34nYYbiW9U4UeiuePoXXQAFkhSxmxQCOBu2gTBwBIQHQ6En7.tP6fwYI.PxwbyEMIJNr9qLTUVFL7x5uWq0NuLTqrdNb3HlVTR+M1j6evAf2yOxW7GGYmDFUjAZEVQP2Wy21QQPc0RdRy3r6BdtsFrZzcU6GsYrZQFaDdPKjXstv9v1aym7y+CCBvTUhnWeNd7DlLdFUEkDKTjnU38VLlbpLYgpAUpCUHhygszFrPAg.krt9RaaLr1ffTwGDkZQ1X52KggiOgGt+dD0qG93tv3L9999+g3FuvqvvrRxMU0dtUUcCFGr1pm3ygVVbY9rxGUF.35353YIVDPQ6TZ7A4qgDBARQnmZzSo418SPTA9oYzQow4LA6hwYQpTjkkMuiUTjWQjTQjT8Dyzzx.3bUfrtHFuV1qurTFdkY1.aXB5BO44gIQ2IMEWoCkPSmnNjMc1bV3LFCHDDGGjlgIujcjv1c5gvZPIEHkfVBdmI.xZtp0afZbVa84C6w6YoHbwCVsYK4hnp7Ia86OifsaWgbBohI4U3zo7o9reNh9zeFpN5X5r1Zzo+.dm6ceFNbHUEkDoTjnjnrdnxBlPCMNOaJRuizjHzQRrlRpJxP5rjlDgvYAmAguwsZqaTkkUbzQmvgCmfQDgQGiurhm6G5Gj67ZuFGmmQkPfPGAx1smlyB7A3LMgzECGmUtcmQWVsNFs3w91Ou3r1ZCRsrzPbZWp7vLqkm+0+jr5m+yCIcnBAx3TlMqjSN3XxFOAkGRjgAmD3vaMDozjFGiDHa1DlMcLdSEchiP5rn7NDdKBuK7vYChazawZx4jQGynoSvIUXTIbRdEce8OMuwO7WfbDjYsnlydkiXsFSUAQpqtHXuny0VF.+1mmdcbcbcb0w4FO+aiw3e+JVFCVBmkdJEuxN2fN.1wSHowRfDNJMACid5zoAistSWxyyQqiHMNEtBcX8z.z5pXhZYOZl3ZyjXW7+mOo1VuePNx0EkFdDJ4b.jcS6fonjHohj3XFMZDdmCqsJzOXEdhiTHcVbEvsWqC6zsGdSNdeEH73bFnJTHWm6dSddhj0wGVhuiagIWER7mjnMKXsAGHkZR5NfGezP17l2g+N+L+bPbBhtoH61gbmiCN7HlMKGbdjBQn0qXsH8RzpvrPD3wYLXqp.qEuqBusDeUAZUvBEThF+5JPU5rhb1+fCXZkg3Aqxj7BXqs4K9y+2EWZByLFjooPTSUGFpXCIhP4oJuhl7bcr3OPKV8aW0EoWDSgduGu.Jxqna+AXjBdzIin+N6vO5O6OKrwVPVEcVccJLdN5vQL9nQgK9PRGohDoFu0goLmhhLvYoWmD5mlf0TvIGsOBrf2gvYpK6U27AgA.sh299uK4NG81bKFVYwkUvm6uyOI27UdMNd5TbREoc6PksBvgVGZP0uWHx8VGPduacccbc7QnXYSL4ChfqVLDdP48jJE77qsI8AXxDhctfjRTgIbKkA.HBgHvfkoBkPSTTzUBNZwsgmD1rtrz5srkaw0+E88sH3RHHvBoTRYYIZYDI5HrUlPZ9TJJxxvW2ZOblRz3IQHPUUhD3l85wpQwPkAq2Psqeiv5NSee7LUcpGD9uyzAS9fV7LCvRoXooipo5GNWZoV.rzYXjZofnjyOopw99adOcTTcUqMCuLhO9a78yq8y7ywIGOjRolU1bGNZ5Td7d6yd6sOk4UjFkRhNXZkdqCuqBAFjXvWki2jixaPJr3ckTjOCisDisLTdtwJN73C3cd22lQYyn2FAPAzaE9h+B+BryK9RL1XfjXTIwmBzwCZoBoBbNSX+VIIIIAfZSMstYYZCUig0ZCdwj0hyXZ8vFDEd8wp4KWMaeMGiZ9Mn42m1+lzX2D8VaEFlMERRvGEw67vGxse0uO968O5+DHIEh5fwKYZVIiGkQ1nYXlkC4UHarvAuqtR.CUEn2UEtPTqvYxwYxwVUfspDsTPZZLdrbxnQbuG+PFcxH13V2A0pqi4fi3S7y9yyO9O2eed7vwThBacS0tAPUUkEgTOWblWlyHqTJbN2bQ56bNTJ07i2MELQye21X.u1jQuN9ndzV9Av4YG+bie6O+37uWDKlEjKhgn1fNVb6tYeoYaVGoPhgHr7bqrBaC3GNEYQIoZEUEYDozLc3IDIUTlkOGj0QGcD6t8NzsaWppplOFSy3qZsNz3jMmNN9x11ZO90SRp8VDLa606xVOs+bMi623KhFiAgPvzoSQq0r1fUvTVQUQAwJ87BBa3vgjllPUdFBSIqmjf4jSXGfO4seArmLhNQZzQJDQAifUKUAo3TGN7y88Rw7sI641mVJKiOCZ3dYFIc60caPvMGyZJVC+SgMJcQw6qM64E2IC+nKuv2aYu9hqKsNlacmWhCd38oqIme5eweQjlLdyeqeSlrVe5u4tjMdLYO9PlN0vVarM85zCgVg2UgTDJUWAdDxfo4IDdDBvW2LqVYv.b.6c7g73COf7xJhhio2pqw3pJv33G4W9WjO4m+GgING93XxLVRsNTZIp504b+By6wfGoyiov.BHJJBgPLuDgkRIQ5PeQz6Csxf1mnXsVLVKQh3fi1u.nplKtZd8kEdfYkYXvSbTDQc5fwTRg0yN24E4G4e3+w7G9q9qQuAqR+N8Y7g6wwu88Xk06ypatJ5jXFzeUbtZCsyYBNCuGTRQvjYwQRRLxHMVim7xBFdzTFNYLixFiKVwVe7OF269OD7Zd8eoeY9L+X+D70u28nJMEQjBgRUegRvKVj.Rgb94NWGWGWGemMVbhKgwj9tebU5n7x1FkdGZED6MrlNlWei03cO5D538rwJ8Y3i1Ov1hVgzBU4AVyWYkUPVXYxjIg0SqIuBPQQANmijjj4Sj6baWMfgthChMK6h.nV74kwdX66g1.5p81iPDxhxrYy3N27VDEEwniOh0VYUhhS4wO9wXsFv6vWUhnLmM2XERqp3voFtSeMqqzz2YQo8g9ZacOAt9t5.sMF6u8iks++rFuevx56K.rtX8trbjnK6h41qq1KuREN4FslBmh05zgeveheRJJJ3s++6OAY2ADqhvkmynhRF9t2kz3Nr816xlatJkyxQXaNwHzJar0.Zb9f2MczA6wICGyj7BbdODowqzXQ.CVk23G8Gmeferebl3fokUDu95HprH0ZjHPKDAaYvG5FgdgCm2iuQSn9S22j9Pu1SRcIt1HJbBKmRF.anDfSJnnLz2+Z5F5MrXoqqzikcA9YhHE3U0UGhBgNhgSyYknX9ze9u.9IE7m+k9R73u0aw5auEqEI3w2+c4vSFxMu8MY5r74EFPRTTfYLNclZJklbSEYSmx3oYLKOiJqGq2QoPgpSWNXZFDmvNe+ed9g9h+jDuwNb+SFR5fXTQQHzZTJARgDD0kFfWEFc5CXkB90w0w0w6uwx.Y8jK+DKl7R5p07od96v+5iNgpSNldqtRPlHENT9D7JnX1TPIYqM1lnH3f81md86gEKiGOdNnJmyU2j5Osc5rn1WmGxm9wuNC.pkvp0hKaa1FaOwakVwzYyHMNgMVcMxylhvG5gtGdzIbxIGUOodOlhYjHgM6zA0jQDC7523FrtTQ2HARo.mzGZty9PCvVT27xpUGSHcjT+2Og61s29W568Tdr686363.rtJZ7tL59tHcZcF.VZIGevgLneWHSv2bu6ystwM4m9W5eHeo3Ddqe++sfyhRmfVFgSVxnhRl9n6yCN7ALneBwZPKiNy2o0E5B3Y4FFNZLnUzcys.cDyFOAiyAI83y7S7E4+fetedxEvgylfreWLUNVc8MHqxfVD5OdyALI.uvM2t8izA+DorLjq+XkdNUyk4EAlaZNMpt853ZnWVoHpWZMXsyRQty4nnn3RYvxIfnDcvyWxKHR.8R6R9nwLbZFdojenepeFhRS3O626KwwO3dLyCCt4snJeBO5fiINMJ.vJNlDcnDcAp0rlfwSmRowRVQUv+TRhINsCIwInzBljMEhi3i8S+E4y+E+Yw2a.6MYJcVeCz85fKVhTAJgGoHbIp2IHzBWeuuwYecbcbcbZrLfKW1Mz+tUzF.yS9MdcnzB7lLRc84E2bSdAf25vgHVeHazMkISKfpRTwRrUNlLYD862mXUG.OCFzCuzy3wiIOOmNc5PZZJSmNctvwa1lZhydb6pu+3h.zZuNDs9+1f4ZCppYR2MY.YdFNrPddNuvK77XMFxmNis1bSrVKO9wOdNybJgESYF6LnGoNCyNYBuDvqt0lzw4Hx6BReQExXgUxba4IDxZ.UR.+oEy0SAHKtf8+mzOey4DKBJ86zw6qoHbw3IYmbQT+mmsKK8VoGkk4Pjhd6rKGmMkXcB+T+R+xza003cey+FN4q+lXyKPMnO82XcxlMgrgmPV9XjROQp3vIT3qS+V.Ljt2.Xv.DIcXFRXzTPpYyO8qya7C+ixK+Y9A4QimRoB14EeQNoHmIkkLPEgYRFwh.KVJoqQQgHUBrxPU.1jy9Fs.0jW7F6LvZLjniHRElMjspJPaqThTqYbY9bgJFEEcZeFrN2+WF.KHX0AJk.sDjdAcSSQU4XVdEScNJME7Zegu.c2Zc98+s+sX5W+ulBsj9qrFBofJmkJigQ44PkAbAviQZMRUnBJEJM5d8QJUT4EL15grpPtY2ZW9reweJ93elOK1zAL0HQl1EhigHUnGTJbfyVClJ3kXduJzon7tkW9kWGWGWGumFePBPUSb0VsxkeW7nXEQJOJbrQTJetW9E4a9VuMi1aet4K+x7NiuOUEUgwhh0TVVxIiGQmUhYs0VixxRRSSYvfAbzQGQYYIwwwy02yhaaWU1ZdRiSAXEx5gqoczUuG27sDL1Y27refPDrfhpJbBXy02fU5OfhQSXkACHMNg2892iJSAdo.qoBgyf1VxNqtFl82CGvm8VaxtwwDUTFXuR.BUirMpqPQWneS5CanOwrV8dYzFr82MX758EFrVjgpSmEj3bHuW7y19yrXtngP9tGLnGSl3Xlsht86wjpBNYxTzatAege9+C4Nuxqvat6M3te82jpCdLSFcBDqHd2sPjkgx6PKjfJvNhwEbHbgNhhxfCh6yx.cL7b2lW6MdCd823Gfsu8cvDkvIilvfc1lQ4kHR5vpCVkCO4DVavJPoAsLX7ZH7XHznp8xvIf4kV5jjRbbbPTjEkfKX1cwwIA+IQHIIJBIBb5H715FicjBUhm7pRJKKIOOHByFfVMzUew+33vXCsbFsKEWYE9RGRgFcZW7VCSKfY1b58h2gex+Q+Gw67U9J7W+m9GyjG+Hzc6gupDo0hNxirq.k3zFWs2Inx4wqzT4AaVITXfjDRu4sX8a+b75e9OG230dU75tbzrRRFzin9qfQXw3.cjmlVLqvIpyHnJjd1O3Md+0w0wGJiOnpAK3YHEghfI3DoB1N.EE7we9mmcEtYoaC..f.PRDEDUeq2lu0nYrqG5GGy3hBpJKPDEX.Z1rIjkzksWaKdz9OhACFvZqsFVqkoSmNuBmSRRNWgdsXbUakWkFrj9yWM5Kxj0hhmurrLHlacDu3cddlMaFZsls1ZKd38uG6s2dzaPeN33C.qCkoh9QZhwS1LKaA7529Vz05H15pyVgFjDr6JOgJGuN8fdAfW1p+PVaV1M+8kbhzUqAsu8OK7CMZv5hhkU0.vYYqZQ.Um60jdxJxQkFizY337YD2e.I5Hdm8OfARA818l7C+S8Swq7ZuJu4W8Kyc+Fec7YSvoUXFWEtouqVqRN.DPsq8RTLLXU5s01ryy+h788odCtyK+pT4k7N6c.IquEac6mGRz7niNhMt4Nzu2.lNIiz3NTVNgHQPX11ZZSsgho.q2gTovgetX1ihhBUfQnaZxye66fyXnpnbd4yFEWyTUjBbdR60Eu2OGjUUUEUUUyqZjKKTpv5PIjHkJbkAKrHJoCE1BrJOUFOUdAC5sKu9Zqvt29Fb2uxWi8u6ayw28cwZbfor92CU3JGqGLFnWufuioSfU5Q5pqyMt8c3i8I9T7bu5KQ5NaxiFMjBSIcWeCHoOkdGxnHTQRDhv.TBefQwvu6hfwlNenkOZTxuWGWGueGWFvkm0av8dQbkLXcERToppfzjn.CPEEr5pqxm90dYd6+12hi16wrZu9X8NNtHmJkDY+tTVVxvgCoSRJoooXsVRSSYyM2Diwv3wimOQ21.rZSzv76ecEaeWjtpZd1ZOsJNW78V7yaLAlqrVKc61kUWcch0QLtxvJc6R9rL1au8vXJozVhwZw6LHbF1bi0nb1Tz.ehM6w1c5PRdIczZhzgl2rWHBZTFPKBZl00ZyuAP0S6oLOqoHboqu2mhmY.VsM7RHb.rQLahZzoJ3bdgA0u+Yxa9B.otnKdlCxR5oe+9bx3g3JgjjDzZME44PTLqsyNPdAYylfVkvVu7qxNO+yyvi1iGd+6yn82mIOdOplkSYYdnRPP.RAIocIoWeVe2cXiabK19V2gzACnvK3gCGgNMk0t4ygOoCNkjRigm+keQlUVwAGeD23F2fSN3P5GmdpKz6bXEgVliQ3nBGq0qKlY4jmkSrTv++r2aZSVRx0Y58bb2iHt64dVYs26cCvFD.DMH5AfCFP.xAjRlFMCE0Wjos4+f9IL+.3X5SiokgTzDkLY1vQlQwEM1LjCH4fghfzHIHVZfFnazcWUkUkUtm2sXwcWeviHtK4Myt5tqp6pZlGyt4Mu2HtwhGQ39weOum2yJc6xRMaic3PNo+w7W8m++GGt6dr8suC6u+93JJPWpnvXTr90tJaciqwy+7uHqeoMQ51hiGNhAiFSZQAhtQ8LHbS7IM7tSgngrQiw4UzRGgzHBPgwYoXrCiArYdFMJiQiFQOAV6YdNtzUtAtAmvg2da1emc3N25Vr2d2m7zfSfFQgWovDEgJIgkWYM155OEqu0VzrWOzlXFfl27ctEcVechS5BwID0pA1rTJrYrTqkIKeTnsKDPvfJ3KRHqOmYtaB0yGzunrK7hLN7B6B67rGzgcBHm.mBCq2yw.Rwrid7gq4DvJdZ0tEtQBsxJXobKeom5Y3u50eCNXm6yJu3pjlGwtCNlrQdLsRvW34vwGRj1vK9bOO6r2t3bN50qGCFMjAi5Sgyx3rQHk8EoCyNDmpbbsp9hKaCkRheuv2Ineh9J9KIDj6.HjbR9SKyQKJ7jE9.46QqncukXyKuEGczgDEaHJVysu0sIMMklMayw6cDMhiHKsfXfM50izctOKQ.8p1VGs8dRhCIfjWIT3b3k.pZhRiRYvUN2Wu3PHbdWcWh3UmoyVS0adnc5bP5Zx1awK26mThdp3E1zsMUHp8tYxb++CxHJefbvxCj6BHw3nTumrN7EVDsGwDtnaEPJ83RHHMCZQgXpb85z2fbVDbuZ4UumVjSylMCeWIukhJkzRPg2XP0nKEpTNHKMjPd8tBquzkYSuCUZFTjSZQvydOATcLwQHlHTZMRTLNilARPSlTZCpjX7QBp3HbFMFSDCSGiWaJI43HZ1nMJQEBKnHHIMAwwfhQXUJ5zaIF0e.MDOwwBqmzfVEo7i9F+Y7W8M9Fb3stCGc2cP4rHtxhFJdrBLjP65Qcavqo77Ma1km44eddketuBu3m9y.qzkc52mixSI2CllMPqLgh0oyg1AdrnbJRhRvfTlrhdr1Lb3nYbDtw4AELNoCVcLE4YbRdNVznS5PzMeF15pOEa7x+L3s4Xyc0WG7dOlnnfXqpivYTjqzjoETFE9HgtqeMjnFnhiwDY.oflMi.cBYYYn0Q0cF4jpDGzFPyh.GD7ScOgvjRpjyJ3TfNVG5rKNFQzTT3PoLXstIjjetLso5yUDCc9vbKhfbptAtvtvdxxVzcuS6mTMhK9vKUYleG3LZUVPGDRYkRUlUbgmACbZMPii33XxNoO5lZrVWPNbvinjYF4RlaD2ZNpJxoFWnhv10G20GPSNtqyhZ+jgPqI.NfMNhCGOjd9FrlHv9GgtYK9Ue9WfeqW+Gxn6cWZrbOZ1tAdDFc7HPTzY4U4928dDqzr0UtDENGGcxgrzpKQTKCae66R+g8Y0tqPdZJEVWI+XEx7AMPLJJghrbTJCk4dcHBGT5.qJjXTAGKBNwn7Tubm3JIRulFww.drES3wqRovVd8xZCbkMoQSVdkUXokWgBqEkwSutsX+6sGGbvAn0Ijmkij6Q6bDW34Et5Uws2dLNE9xq2hWXi0owgGwxIMQzJJTBNqGwqwnqp3JJbVKhtzIZwEhHDdbhfWJqEuhJLg+oQmRMAXEUolTAAfZl991v5bZD6jx6CpxB+44gENeIRZxLHpUsMbkjXq7V6S63lOba9CRbSdnGhvpgblW2Kb0CHVtd9GNwwOf7UXFBNQBYwlwDRETkFQavabHQw3iaPQZV.lz7PXAMwZbZC4ln5GVEiFiwfpT+kzQAU6EcHSLzFCQIInhSvGowqTgajJuYnVnxbgYMHNOM6zhbaF862mVK0FeiH19sdGVMIlmZqsPOZLemu42j+x+n+Pt6q8ZzLMisZ0lqs9RzNJhts6PqVsPkDb3H05Xrqf25NuEdshw4Vt0e0eIu125ufK8LOGu5uv+Pd4u3WhUuzl7N6sO6evgznamPn0DgFsZ.NOYoiBNkH9RzHKeftrG0FU0RQDrJENQgUGg2DSQgk7QiQorX0Fb4F7J6LyTHWjfCVQFjXChRgDavEYPahwmz.wDgDYvaBYLHk0ZPDI7Ode8rWptsRpmyQ4O3TyAQcF++E1E1E1YYOHfPM8.NmReiVH5wy7qmc0894DzwGtSXodRQysYCgcJzeluralHkPGqiXG77c5wq1sC+616HVuSKtx5qwqcqai2qoyRqR5fgDYLr6N2mFMhYkMVmjVMIqHmtK0CsVysd62g6sy1r4JqQylcYznQjkmiINFmRX3vgzJJI3rjOLoTWU1tQIEwXRX0DmuN5PUee2VswichDL3lU.SMJC4NasipKs7Jb4KeEDQ33iOh02XY1eu6ystysYX+gztQa7YEnrdhUdtT2kYczbu6dHOu.uxy8bnFLjFNGXyvGGWwrJznvWcCjnmbqvDX2vIktPOMTVyYmBspyHTuAgs8c+9kp6Wqht17K6rtkOHxDm8yDOH9t7gFGr9vfTjJUHjWpROjC0fox50TTDQ4QjYLHooTnxvV.ZwfxZP4B52ADxFBUYEEVq0niLXhhPJEzSsVG9bTLEkNggnCdiWV.mqBOJNOwIwbzIGhnTr9xKwQ8OlwGkym9lOCWqYS9we6uMeie++.9Aei+H3vC45O8SwKbkKSbQAWa8MnYjIj9usZBQZbdgTqiLaFu3y+bb3gGx81cOV9fi3N6sO69FuI+t+e7+I+6+C+i3W4+9+o7IdkWgsO7Ht8N6gpQC7hhw4YTjlQbjFONbH0NqFpchBdQPGEinKQfSovqEjhh.5i41fCjdOpBKRzDzqlLyKBn.FaPhLAs1JxfJxfxDg1DinMniL0pKOUnFMi5Q6qEFuJK7.1i16od2PS8B6B6B6wWad97BSEdHmmXD7XvYT3h7jG4wocrYut74dtmkuye0eCCty1z6EdV1b4kX6iNg7zwjllSukVghhB9Iuwag0Aac8qRZQNdwwx8hwukilpXFbReN4n9zrYSZ1rEEVKEEEXjPIpY5i0JjmBSRehR4un94BYOXoiYkJPtpTWs7VH2UfV64fiOhVsZwy9LOCqr7pjllB.Wdqs3d29sY6suK862mnn3Ps3cz.vWPKR3ZIMH+stEq.74e9axMVZUT6deLwQPrAGm1Qi46m9ggsnjf6IA6QtCVKxwpGlCVM8fukeAdQBnMIB1bOJcIpRJgXcHzjlnHb135BQr2I3DG0ngnzHhGcTPulT5n.XJkeuSUFdSsJfJS.77f2x1.zkNAHRSgXYbQNQZAwkfIKmU0JdgUVm+j+0+eyu8u9uNEe+uGq9huDO+K8IoWjlKszRbiMWGa5Xr4ELZ7PN5jiHy6vozHJCJkhdMZvxIcX4q2km9pd14vC4Ge66vac+cY+23M327W6eN+m8e8+U7b+zeZtw5qv86O.m0QuUVgCN5v.2l7UY5A0OvDlUWP5HT.dSoRwKBFkfXU3TJhM5RhPFR+Wo.PbH9f.0ElIiOfjnF7FSvWTcT.xasAozo0pPJHvoDfuGGd35Bmrtvtvd3ZeXgf0hHAt3gXugbukBDrQB4wB4twDqS3Zsayuzy8z7u5G8lbza9S3lu3KfC3c14.TXfrLRhhoe+9r8ctCljXZubOzFCZshUVYEVs6J7F+3Wmct68HRqQDMo4Y30J51sKiGOdlL9qlutd+ox.7SkvAhfKu.cIhUSK0OYoABsmaK3F23Fr9k1DsVi0ZY4kWlzzT1Y66xa7C9w38dZ2rC3rL3niQWjxk51kq1rEp81mA4o7Ke4s3ydkqS9dGvxZMFih3FI3Kliz9Ojs5y4oRtsmj5G9CEDrBNYIyfDvCqFI8TtusnsoxDxRNeY4uQDvnEzQFDeS71bD+TNVQnVJU8YcbBh3QTFDUftggXJqwoDb5f25lRnFUk3h579fTLn8bxvgzc41PVJGdus4Et10Ix432+23+M98909ej1sayy+Y9brdutrTTLWeqMQ4x46927WSyjFjkmx3wiYP5XrdOXhvDkPh1fcvXVoaGVZ4kIoUS1HoEMt50Y0ls3NGdDe+29mv+5+4+Z7p+i+mv+j+69mhRo3Mt68IKOPp+PnMCGuU.B4qh+ueBGmvaPYBDST40HJKNcUstxVRPpBztP6SkCVh1DbbUzfJHYCNsO3.afvank.xUOPDMbA7g5QocABVWXWXO4ZyOf7z8anbJzNAmWQtGJz.MzPlEc5HZ6fe1abCt012g+8CRIa66xVqrL8Od.4YdxO4DFJF1XiMYTQFu1q8C4RW8Jbsab8PkzHoAJivK9heB1ZyKycu8cXuCO.u.IMZvnQivXljMz0TKwOo9xljjLoOn4NuvCZcDUBfPZVnLqUUuUakzlM2bS1byMoa2tABtCjmNl6s817l+32.w5oQbBJqiCN9XFkNfqtbWd5VcYk7Lt+vi4mF3Kd4qyVovvzLZrbaLxrbga5i+E0V+90l+Z2SZ8C+QZHBqb758+FUEb5YQM5RPqoTJcM46s3wKAhXpz5fqREp.ed7JPbk4qwTNboMf3wIZDkuD4HPBwhjhRjVDWYNtUgnqRvKVxcfJQSVQJMvy01XMZlWvewe32feu+E+uvM27RzUo3xMawkWaMLZ3n6dOFz+PFNZ.Gdv9Sc9TNKrhbFWTPlUniNliO7DNY+CIoUSVaiUY41cIOIlAX4S+LOE+s+j2l+reueOVd4k4K+O5+bt1kVme36bKZsTObl3ZdT3KcvRTA1jWghUE5eduN3Lk2i3BsqAEu2f3DzdSoVU4mwAKOVvqva.kIJnjChJPZVa.wQ+zI4PEe8l4A1OZCQ37++E1E1E1GbqJTX0edAbz5ChcV+9vjz.atCQhvJVrJgnFQHhGaQFssN7mbL+m749bb6+3uIe26d.WsYCdp0Wic26X163g3T1PQM15HazXNZ+C.kPud83JacYLQBQp1zpQaZ1rIc2cWN73iXvvgLb3PzQSJmNZcf6uURqy6pnjJBEkEZ5bmstF11oSGVas0nWudb4KeYFLX.Vqkd85wAGb.e+u+2kc2ceZDmfVEJmZi6eBEoC4RK0gq2sK5iNhCO7H1D3ezq9yxxisTbv9bsKsFhOCkhfnppatv1V3gSXBeRMzfU1GI5f0C+Aol5BfppptOQR9kRNEITRJtY7JVghf1TU5dQ3cUohoIJb9.GkTJEZkFwnPIZDsPUtDHdAsWvTBwqSrgZDHVZzHgi1eGVtaOtxxKyexu8uC+N+K+MoYdAMFOlm9FWi0VdIFe7Qb739jkOlwiGQZQJIIIHZEZUfj8deHqMKxc3EBYlhySQtkCObe5e7AAguasU4y9BOG+f24c3Sdks36bq6vevuwuNsVdYdku1u.6uTOLsaSVVPQeqyJCojDkhpxaqP3C0DZSIj0Ghyg26PUl0FdqGOZTkwZTgJfHlTlUQdAwHnhhBsa9RHeKcosxItvkvRdHnUmdVRyd09Qt8vFw0KrKrKrObsyBAKQBTzUGYB8oXKPqEzZvpcD6EFd7wb8UWgewW3oH6G9S3n2YaZu0kv0HFq2wPql824d3LJVpSaJrVt0stE850CQD1X40XnMTMN17RWlUWeMt+t6xN6sCCGOhQiRq0sv7RmkLFyLg6aZgBc5yCQDxsVzZCIwwzKIg1sayRKsDKu7RztcaNoee5zoC1hLdsW+03d2YaFmNj3HM4YiIqviqvRZwXVuaGdokWglC5ynCOhU.94uz5byFMHa79nhrzzG5u1ZDFk5PLOZCQ34c86IA6iDNXU88Or19U1hJmNUjHTzBhVWp4E95rXbRhdxoFLOjEoA4oU79RTo.e.BqxvnUF6buBkWP4lf7iR4HRqHMa.caFypcay1+3eD+G9+82Gdq2hO2q9EIJc.8ZlP1vi4viNjrhTJbgGzWc0UYbVJV7jYKvk6BZLhHDoLHQBo1RHgMJZF2lhzQb7I6isXLcyVlK2oKGbvA7Sciqw28G757a+u3+IV6Raw0etmmW+12gjNcOUFZTW8yKSWVuTVJFJMaIQ3wIXzAHt8JWMuFfI0hPq2i1GJAQRE40QgmoTu+oHzdc6ekdwbgiMWXWXer0d2Pv5Cpcd5nnWowgEswfx4ghzvD37kxGgAVqYB6cq2lW8YdFRys7+0a9NTbq6wRWaKz85vs2+DNZ7.hZzDsOHOOJihwiGy29a+s45W857z23FzpaGJxRw48b4qcYVaq04vCOjiOtOCFLfiO9XFNbXnLsU5TUkvSOsJrO84fHBs6tLMZ0jkVZIVYkUna2tDEYv4bTjWP2kWh9GeHae66v8191Lr+fPjE.b4grNTiidwFtZ21rJvvc2mkAd0qsJewW5Sv8d8WmadsKSCUC1cu6xVarIFslB8id7Yt.AqRaZmSpHpWXfeS82Wyclx2qt0uJCHfIgj58x.qSutUYtV89ynmI90RYpZVwuHiJb7snjKd9vQUoCIN.u0WxGpIjmzWQV7pO6bDmXXvgCXq0WmVJ3+4eieC1+6+83oetmgri1mUVaI163cIOOCq2E1GQZDkhAYCmztpTDIRsv0o7NrnXnMTDkiDgXkBUR.kqwYivteAMa1latwF7524t7z27o4Mu06v+p+k+ux+C+y9mwFsawNiFQT6VjjjTWpczZc.92n3Po6ob+Oi47n0gzxUAPkhwW5H1jz21UVOAIbc2I37AjC0RorLLUac08E9pzNVMIzgmxAZuGiIJ3f2bguqJD.mULDWDxTKhaWKhyW9x8sWd7obgbgcg89wl495ynO24+9YSrH+rqWEZKhq9YxJxUqTAwNtpFo5KBcFW06bXx3mlIlduGqahpCU6fj2eJ8wZ5mGqN9lmGky7dRDo1B.OMDCthLv6PoE7JOwQBMyDb6d.u50eJxcd9ceqaw8t0c4xOyUHZi0HVaXu98I63CIIIIjg2CGPbbD6du6wvSNg0VcU1Xi0X4kWF.hiiY0MVkM2bSrVKoooLXv.52uec41o56O3fPTIVc0Uqq8gKu7xj67rxpafIJgnnRIExD5mNazHFNpO2+s1gi1eO16d6P5ngzpYS5zrAoiGyQiNAOvZc5xy0cUV1a4v24soIvWXkN7Ud9WhC6uGKe4UQ4xIhHh1XYFKEDkZokXnfpnTL4ZUMvEk.MTcsX5w7qudIyt74utW86YQ8C+dbx20TNobbgJwXs99oELNxGT6izRkyCpsnrG3AsQnBp0Y9to+r8rGhLD.vInr.kj+lRJaUFUqoEnrJfdpbzZb+ArYudzRo3a9u8Oja8CeMZ2sMKEoHRrXIiBwgSWpwIkENyBeAthxrKwGNls.5xcfyKXUVhalPp2hy6HCGJbHZAegCWZJ3DTwdVtQSZkzfiuzkXue3Oju0+t+s74+5ecNYm8I0ZqKsNUuz5.Gql9AjYtlT0U1705PYl2PIp5und6yzyT8cG92ou9O86gceP7UOknR68mBYtKrKrKr261hF3qdheeTb.89zVjtI4pnZqMLoakqT.RKoMQ6tswZ8LZzXDz74t9MwztI+g+3Wmu8abGdoabUtRmljficFbBmjMhnlsHJIghrLJrvvhbJFOhSNZuR5arLqr5pzoSm5PBlDES2tcwUXCSz1Fn.xq8899jkkw5qtFO+K9BDWNgxVMZhCgbuf1XBgKrHTsMN5nC392aGNX+c4jCOBJxIwnYoUVlwmzmcN3HT.qzJglsavU5sDcFNhS199rLveuKuFe9KeULiFhBPaJQ7REhBgGP6K6GeJPEdTdca9Q6eRIxFO16f0hFTcFRGKgrfqtAutcOnL4FLyt7vhp+bg57ycs5RYvhxbMg.2hHHeAJufcJfdDmCeZJqt9Jju2N7M+27uAt6cY0M1jDEjDoHsHibJvKAGrbDH9u0AVqKvAKYBhPpRr1DevYrw4oXEOVuE7VzBDGGj8.WVACFMj1RDK0nEG4bbi0Wi89a+17G+68+CuxO2WhNshIsLkdMlI2NDEEQZZZf6WS0NTecQITUDlmofSol8Zzz+t51up0o7uN4zODM805E4bU0125rkgs8zOvUworKrKrKr2e1YIyAU++C6P58nvlOYXpQrnbBwJkftziJeYgcypBXoY8NZznAjmBoiYkjH9TatE8sCn3GcGt8aeat5lqyZarFQTvae7.rYiwALJKkFMai2BoCyY7vi4f81k82sCKu5JztWW1X8KQTTDsZzfnjDZznAhViQovKBwkbrUKBK0sG85zgCO9XbEE3EMEEV52uOmbxIbzQGxImbBCGbBCN9HFMrOK0tEiGkR5fLDcDZaAKAzHVwpsZxlK2gg6tCGevXtJvW7xawOyMuNKknvO7X5o0X7drIZRUPSumnojlqE4bUn88Qv0vR6IEmqfm.bvZZ6rhm97KeQHgL+5oJGPW4N+p48zNVE3ZUU8UTvVRJ95A8oZa4v6CNE0NJg3bGuw26GP+27sHoWGTE43vST6dzOaDd8jPpBTK3lFig7rrZmqDOjWRxbHLKKmFzFMdwStKj8iEDDbNQqqgV2YyIs+PZr1xr0UtL28G784u9+3eJO0W4qwIdEiFMttswZsDGGGfxWEU2AkTRX8v4K3qz5JsLYlLtPGWpxrPLzC1jkqPl4gRm5zcQunYLedHXUwUr5qUyDJfG+G.3B6B6wUaAf9LEBVO47r0hmj1oWm.T6pxRjhmwY4n7RvIqBKi62GGE7RqtJO8W4F769m7mwauytTrytb8sVi0WdId86dWte+grRqFLdzXTQFLFCdQnv5YvQGxf9GinUb6luMwMZR61soSmNztcHaCaznQPBGbNLJEooob396ynACXu81igCGB.8GVgZ0Qzu+wjmGzaQiRnWTLtAinMJ7HXGkgAX0NsXsdKSGCz+ctEpwvV.+buvM4UtzUoUdJEGeBshzzTTnUBVUYRdUFwGWXnfybhwyystOHW2l1NsC9OdaO16f0rY8wryhpJzcUn9LsUCnwTgrZZXsqTuek9cqPpDnAeEmdJoRD38nEP7kY1GfSK0O3prgigUZzD+I846+s9Kg9C3RquJt82CcitXco0ZQkHB9xRcfVDhMAGaNMOeb0NnnAZozHZE4hmARF41Bxx8T3bnJbzLtAo4EXsdTNKombBOyUuB2c62lu0e72fm5K8UnURCxGmh3BEjSaQAJSiPwf1UoOVppVwx1QEdeI24D2jDvrJbckeVDB06uo5Ot97QlzBOy02oVmEc8+IYROdgcgcg8gmcd74UUMHfyGzlOmtlitZJ0GPsJvUGCzHNgt1BRGlgNyiMpf+S+r+L7m+Z+P9qOpO2+t6QuNM4k1ZKVa3X19vConvgKq.WjFkwPhwfWovYCQc3vCN.k9XNzDbByXBkksnnHzZMiGOlwCGfMOiu+286fHB862uTM1UjMNu1gWiHjXLHdGpBKZum7ACw4gHfULZ1r2RzqYCR6OjiO5PT.uZW3y8buDa0oC1QGSZ5XVRGQOcTYVUJzvoqSfKqdRxc4Xx3vKJTdOztFN228jBJVO16fEb1CrZI.cZXb8ytAeFhL6mj8f3qb.3A3XXpnMMMQ40RfyTNjffMTFtKrgLpqkIlity17N+feDjkAoonvRqVMXz3gHljfrD3q.6QgAAWVHsciKq0S00hOwUKqBZuCUQ3gqBsPbRLXRH2Zw6cAURGMiGNFMBsa1j8F2GIaLca2ja81uI2+d2kVW+oqmsjVqC0pwxZWUU6mHmd1CPHTg9xya.PKyDZNYlvAFbDcAQyagjKex9d1q+SutpxL6r1YsolUyENhcgcg8AyVT2p0DV9Iiw3lEA94nXPU8oyIAd058gHDnbgIN57B53Per41BLFXi1MY3vBMhST...H.jDQAQ0wr+82kUa0iu5m5mlqcu6x230eCtU+Qzd7XVd00v2tCYJM8yJXvvgLJeTPTpMZTIQnjHzMRBpHu0xnQipyfvp13oKl0Gd3gyvoXkHDgAkKTCdUJONeAtrTJxCI+zFsaiOMktQwrdudzvC82YWRyynKve+0Zwm5xawUWZIroiYb1HZXLzqQSZo03DGFGjTVhMJTgnlXqJ8tKfgMOLu2X9IXeJDrdLuO9G6cvZ9PzMyxlZc7kxV0L7AhJDmjZGqTS4MfqDNpy6RTUFNV4DWMDykKuZ2FjwcYRImQByZvfvsei2fg6sOwlHRGzmdlHPCEVKQFAo.7VGFzDahQbvwGdH6r6tfLIiLrB3TAGrbhiXKrrSQw3TbsZP2KsIMVckP5GqTnTFxGmi2KDmjf24vnzbz96wkVeU9QGeD+3W+GwO0kuQPdUcdhShHOMKjgdZ8BZapv6qpVCt.tOcNsm0s6LgH5KJDgSip045fkVWVfpcmY1Ncgcgcg89ylOy6VDhxOIXyO3rHAjqTkzxHSEnqf2CdmBiWgxAVmGUjgLofgEonrVZqhnSjgltVrStii7C3E2bCVc0U4+3q+C4Oe+ioXmc4pW6ZbRtkDkil5H5OdDiyyHuvFHytNkSHTcKpJ0M5x5eakfidzQGMiviFUxSVu2ShIBI0gsHGagMTHk8VDukFZMsLFZhvxKuJq0pM9wiYuc1lw.OSyk3K9b2je1kZP7wGPwt2mnFwrTutXJ6i2AzvJDyDwlFbjqJcvRKXPNE+1paueH126Ebv5QjsHxkWeQjGfKjUCfWNV6LWapbXRnliPy+dUVC6pUR7oPmwCBkZSUIRMNTnjfLJHdGw3Y625m.YiX0dcwcxQD0LgzrBDSDduf0VfuvQjN7PkMufiO9Xty12lnnnRGrbXUkyzRBjKOx4Hu.FLdLdSLWNNFSmt3TZrYV73PkWPi3HhhiYuC1kjts4jAGv5W+J7it2dbu24c3SjNFcYXNMlpxViEkVufNVqNOmz1WoaVgI8M6m0LktZwDzCeP6Z9rbtJLazxhxM9P0nu5HSHvMLY58jvrAC1ATRz0Eremb7NIqSqRgbE9Gnx5yE1E1GGrEMwkvjVmz2qepHAnJ66zQYejUOKM0xeuQdlGrm1llqpmZKHpfyGnopxR.15BjV84fWUS0AQzjjXXTwXx8VhhhHVhPkWfwAcLFF6BkYriO4DVJNhuxK8R7BCFxewq+C46bqagAHNtAa1qGazcMFjkwgiFQ+wCYbgkNJEda4vSVKNWNEtPewdATNGRoFCFYzjHAIzIKKiHkAiSHpr4LJJgn3HhhzzoQDcaDSTgkw6e.u0t6fA3oAd4a7r7zW9JrkQPs+1D4xHIxPjVSCI3fm37XysfLoOyplUkuRdsCxtQE4QbBU48UMhgUe271Gzo99dVhFpdu79QGKNRJOrsGJNXM+Iq2Wo7rRH6zhhpy9soMQj5vRUEkmEIlZvhIhtfTu9R4mmuHAaqQsp7Xa5PcUd2PUi8hdWD8zBIP8wU3ebXcVJ7Nr1HPEg3TXKrnsVL3AaJGev1vf8wkrRPf3QQdlBQRv5J0MLslLWAjmgVqo25qvMTdt+8uOIIQni00hPmRGPtQ6cXrdFejikV+Rr4ktLduhzgYg34qTXUd7JK8SOAUhlgiGgsPnXjmHSaFsy9Dmawn03rdFN3DhRhXbdFhRiQEEbZp55RktjnjP8KjP8WT7SdPa5WJj5+eplvYtFetV4CpBSUJc7dbtPMPz6mNDgdp3ClmpNKM3Kw+eBxiNB0WROn70EZ5xMNZUPE5cNG4YYzsSarEgNzzhmhrzfZKimG+oY4E1E1YaOHCTUQW.QjRt+Dd1va8nIftR8VwEF.yJfVAMUZhjR8uBOQnvV1OsS7kzut1Em58oxOsSURY03vN0D9phHQIpSdlaD645q14KcxB70hUnfsZBadO5hIZoUfJFBEE4nHfhCNW4j50XSTfCZkGb7oiU3nzTFkmR23HtzK8b7ERGye528M3NYiY2cGiBnyxKwl85RqVsHMMGcVNNqk7BG4tBrnHGe.Udumlws.HT.mySQxyoGZhMQXrJZRDIlFXZEizvf0XovmSgcH8O4HNbuT5AbSfO0Vqym4xWiq1rKICRI8jCnay.0RLIwnMFLPfKthfNViyKjV1dG5PWiWEJIbNuCQqqKwZTk7CkItjHBZckL8DdWS43qkx8fadTEoZb+Si537q27+lJBROSDNnpBt.d2DeA9vpW6G4HX8tM.5hlswCysOLmSUgezotvU4.2Y89rgsJ7thPn573BEH5fdADpCekdJOdTeRGOJ33ms.bgRa.nHJJlQiFQTTIgJQH2V.JgnFIzakkY+i1mzhbhDGJMgYg4TXJ0MqA4iIoaaZuTOzwQ3QEJWOt.BMh2hi.Y4skY2n26gBOhEFcRe7iyP0HFwYwWVTl0QgrdAuqD54oZCBh+EdQJKMNkN4N2LHq.P5C503JagCFTVCIC6PG3ckO8rnTWXFMzn73VNSN348dzHHNKw5.G0vElIqyWTN68G6AA9B6B68s489PFvU9rWbb7rnZW9HYf1B.9oPvpD45pnA3g5Ih8d2NeTrp3E64exTgaR4D0jPlfKLoOp4mLdkYPB7yBOduBqDprGFsK3TBfWrzvCENHxWf3x4+hW8Swar6d7Z24t7iG5X2COhQGdDw.cZzllwInLQ3ZIHJMVIjE3YdKVmijjlHhuTY1SQ4cDGYPKJzENVVGjAhrhTFb7gLL6DF6BZlnA3Suhhmc004EWcCtRbSZOXL9c2lHmPuDCpHPYDLJMZT0bMVUwArpw5TyJzmRYaUUjJltMSl58EYU2eTccqt71M8kpGAgAb58PE.JOpsGJiNbVwG88SL5Our93ChsvLP7Cv1o1TBXKOtEOgovMY.+zr7vr9TkYih0hVDr4YnhSpQFRoTXmRvOiiLzsaW51MTpaxyyogtAhTfHATuxxxXbZFW8pWhkVYUfozGp7LDiAAOTVflU3BJjtMGbVDmm986SQQAwR3XI7fYT33oFouRFNVkVfSkY.0o17CZ60iY1YovyUG2UWWzZMYk0JLQDb1o3l2E1E1GiMsJDofJ4aYhrz7jAWXVjBuW+Lte1yhJmEpQ5np+qoV+IiQ4vXTfxSrRnvaPiCqVi25wEqIKOimc0k4RqtBexA47V6tOu082k8cvvwCXmwCnfPOnZMniiASDdiptBUHDPKxRFdeAoYAH6L4NNYz8AfwDFLuMvSqgatwZbotcY8VMYknHVxDQRQNJeN5DAcRBwMZ.1BLxjJnRcaDSEVsGPPLpV2yKaBqAr371VSkjTOoaefcvZx8eyNPkRlknxSayrdpogEL7adXNn77N+8dkrbOHGKg0Iv4pvLoJczpbYNOfnH24wTRB+7hLr4onkI2XWEZ0hhBzk5Nx5quNCFLfrrw0NVU8.dddN53H5rTOhZjvnwiAQi2G5PzYyQ68fqHfgtK.wtKOihhBbNK9hhRMMoL3YU08JBg.TK5SetV9J.85i14ArnrI7CpSxuWLiwf0ZwZsk2uNQyxLFyEbn+B6uSXdumwiGW+4mTbv5C5j0m1YrEoCeRfGDD4EZXDxoLgqDPHhiFOBkNhlwIr7Rc3Zc5xKs4lr6AGydoi3sF2mir4LbriAVHcTF4jQAAdNEQ+IiiV9RG1knAVp70ZchXy1cYq1c4RMawpwMXIshXmCFNDW1In0dZ0pAM60gnnP3+LkQgX59VqFioJiEq4b2B3CsuLZFKpc6At88830jmjrG5w2XdDrNqF5GDjtdXL.5zHh89Y.52sGPqHIe3CSBKm3KqyQZEpHCXs3b5ZmnLhhhrLPpREWGd+DoQnnn..51sKsa2l77z.+qJOlxxJHy5Xs0Wmj1cv5bLZ7XLlXhMIAHjwGB6GPvAPK3BYcRQQVHDXQQ.JbNnvSPY7mjajm44un7AsP4cYpFOLcB5QY1irHTrffCVEEYjllR6NAAADJytT93cmCWXWXhHTXcztc6.h4iGSylMq4.0iK1zgfbgKu545xwCTUiETsbN64JMMxUUryVbkfJHS3yYjVgwGB6nFHVAK0oKmjlxIC5yXaeRhZxpQw7zarF8EOun.GZyXvvwb7vQLJcLiKrjaKH26HOu.u2gBAsQHVaHNJvAqXiPm1IzINl0SZyJQIzqPnYZAwmzmnrbZIBMRhnY6NnRLjqrjWjQlOHsCFzy3fTkSbmaxksfn3T8My3L3CnySKZLhOLmD8iR6gFBVv4LX7Y386YstuWdv8AGgoIGiePtnM8wlqFLmpX66NEpNNTznYav5vqzT3bgAqa0pD1cv5KWyxT0sh7+gvQkPud8X73gjllhQMAko33X1XiMBBMZQwrPWGoP683JrAdWgEPv4JH2Vf0lSgufk5zCQoHsHHXcZitblKNh0ZbEtfXiV9HTH94B15NbjGZbrZQ17NEO+2+A0CmEpsWyMSUq0xvgC4F27Zr4laN69+B6B6iwV08+W6ZWinnH52uOqrxJOV4b04YSKtzuerIIzzDWHpQuSI38tZGTTJEdqCAGZQvHgPDtdbLqjzfTKjlYIMOirbaHCAiMzSof3VXazBOJr3I24onbR5Vanu4HcoSQpPU9HJVQlcDJmknwCH9j9nK7zRYXYcCZ2oIRdAMhhIIJBqxSp2Qk.QazRorF8tOI4yz4pyp8hoxpvyaOn93c+nenifUX4xo9+4ic6CqAvNKTqdP6fXQ+9yJyFBYnhCaIGrbdOhQQ6kWFLQnLwT3NliN9.1rWGxSKlRvSm7.r0VTOCfgCGV6f0t6tKdOAUYWoY0UVmFs6v3rTb9.w38Ekk5lJR06s3KCKoGWMWJxcV.Oqsw5niiHyZChpWUY6wNYVdybNJd73PTksIuKMiuaWGeuzQ8ipY07tcLTUU6u7kuLW8pW8QxwvE1E1iiVUHidwW7EoWudLXvfZT3mlj6eTYUht77RAAy84Jh0WkrNNu+bIa+jIOWtoJ62WW0+eY4+x4BT.IDlMBY6nCDukHkfIRgQGgCgnhbLdGMUBEwJ5ZUrrExcN7NOEhGqCJbVRysT3rzoSGJJi7gxEx5RWQFPJ5XAh7DYDZ0rEMZangWgw6Hpvi3czoSarY4Lb7.bJGlnH5lDinTTmlOmG3GSSampWUKhEe4WDorV0d51zJa9e2onti7AGLjGGrGpjb+g0fkOJZTm2wn2ONZcVNVEz6EWY10EzMIK1vCo5DZ0cEHJAIoAoVGGONsNyFUJMhRWe73b1xZ.nptTIr4laRmNc3fCN.YpPWs5pqhHBiFmQTTBIMiIylQddJZsAwZKg1Fptk1pfB700QwdKuLwMRXvfAAwQsjL9Uols9b7gpB57Gk14gnYXYOR28gDSv6onnfUVYEVas09XwC9WXWXOnlVq35W+5zsaW1d6smPyAkoRPjdhyplLeklGdtq2LiUMqRv6PUK3WBZPrkHDYJKhzVr44ANuZ8zJJln3XrVOiyRYEIhbqmBmkBG30fWhwlTt8cdbkQ0HJViFAmMjUmJSP0bDkMjcl1fHiJhfJRvHFF4RQEqvznIUxFsJT5Q.kPgJvipEADhy4Pli76SatRGJOuNg8Sst+cQ6CrCVd+DmTpBwUspl38HhpDlyvP5UjDt9FbuGeEwt05PHml2a1vOXg2D7t4fzC5xmICJla+O89oFoMeU4SXpsuyBkDFz5bLNOmANOO0y8B7mu5k3n6sMXglsayvwioHcDQM6fHf0VD3XkRQbbLJwWW1DFLX.sa2k0VaCty6bKrVKat4xztca16jiHJJT2BGdRevInDAm0Bt7.gHUJxcVx8vn7BRZ2l2712l3qbSdtW5E3N2cGZ0qCsZ0JrdNWvwOITeDEQPqDDsTOkvpqcyRA926s+ua175h17WWpz2poE.142+Uob7zW2p1d5pZMl2SbbLFigQiFgRoHJJjMk6s2dzpUK9k+k+kYznQzoSGhihovVfVc5VfEd+6E1E1SflpTcyWZok3K7E9B7+9u4uICFLft8VlAmLfDSRMkEBHOHyjQZ0eOrvm+d2f.axyOmNBBS++ADSBkbrpLualIBU6PT41qDU+nRInv4Bg5yacy76gpHqrniSEZcPIu7dWnP2SvoCm2Ghlg2iWApXMw.dePBFPAQMh.qCzBQnCxoi2i0Ok3mhFJ0uqfycfDE5yQ4cDgGwqB+Vk.ZAmRvhJTflK6g14CSVV6CnPY7BNGHhGJq2rSijkeAgDz68yHaQSe8YZtWsHqte7ps64r9AB1Ky+k06uYF2+Lb9KbtTEkkvq4WyOLPf88mjj7wH6AkaXue1tdTLLyxxarEc2XSPTzYyMXuCNf6u2NniLTTLmiUJE444jkkA.ZslzzTFMZDMa1jm5odJd1m8YoWudLb3v5NDpbjPUh7Ukytdfz7L7Jg8O4D1d28PZ0jVqtFMWZIRZ2FkQSdoHl5jv9bgnS493myByyAqpPeT0Nlllx1auMO2y8b7xu7KGfrun.m2UWBKtvtv93p4bdTBrxJKyW9K+koUmNryN6PQQAsZ05i5Cu2Uqp+8Jm9l+yyOI8oWmyabgpHWTJ0xfWiSD7hAOF7hBmnJKaYgWVQgsbL8BjxHIXAo.up.n7kXCUCDADrHXQINThEgBPJPT1vm8AgdVivjBalJHjqnvJJPzgp.hRiUGNNxUkKiSiP0CJeq7xhWm5Rg1Te9gcjpNue67IpzGk1ENXcNHiMenfl9FufCHpIOfgBkWgpVGrT3EMoVGMWZYdwe5OSPhrRRHoaat296RZQJJif0kSV9Xb9BDkGO15vzAPddnvOGEEwZqsFarwFjD2jrrB79RhJ5J6nv4QJ++pYOpzQzebJGze.NSDibPpRyS+ReRRZ2Ak1Pt0RdQfH7ZkYlhJZPduploRHiGQbAkP+Iba5qwUsWAjsBNYd3gGhsnfu1W6qwK7Bu.vDks9wgGfuvtvdTZgISDPZ3m+m+mmm8YeV1YmcX3vgyDMhOproKQOK76K4MjWEdUitlRNymgWnSVkd7TscpbZPpjwFk.hNPRcQvIFrhAqIt1AKmN7pPEb9xUVS+rJGNwgS4BgqSSsJdJpIkysxZvFn70kNMqVBNMo0AkpGMJzn8Zh7ZLdcP3qITeZshPtVQlQQgQp4RlBIn54Kv4JQjIBIa4mmoMepeyzgB7T2WL05cVW2NK6AQ0Al1Y4GW5a9uy6fUkcdgeb5Gzl+FuPozoRQyK+9.q.CkQ.SLCyc7S8Y9Yf0VmS16HV+pWEqnX+COndaWTTTWM0ihhp0eoJDUzZcc1r0ue+ZAIMREl2hqvhMu.WQQHyAKr3rPgySTRSt+9GfOJhtarAmbzwXEEu7m+UvVVFKpNmOu1kG2t48ggM84RU68zYx4N6rC23l2jeoeoeIzJMiFMBiwfvrU59KrKrONZZslhhBFNZL27l2ju1W6qgVq4jSNghxLWFN6Ip93hMeeWAIVHT9dVTe8y2m+78+O4bsjr+UkeKwW6nguTiCEkAj.FSdpjAGHHXVSrZz0HjsfZQU63S8KQlwYHmRSgN3.GhFQzX7BZWPxHLDdopBYlVvqqbLSvO246hdM8w2oZWOikUGXWkLymOus0hrEkk2yuMdPnJzGU1ENX8A1T.lIHd4I.Vqnpm4xnbKsVYM9jeweNHcLGMLk1K0iCNoO6u+93bNZznAJkpT.PcybyS0M5AGrFyfAiHMMGw4COr4IHCDE1x3oOYlZENGGOZ.GMXLQc5R+hBv54lu7mhMt9MXXVJENKnTnL5I7kyOQTMqNN79.43Cbjz8nN70enXSifU0+Wgd23wi43iNhO+m+yym9S+oIuHu1Y27h7KBQ3E1G6MiVgwDJSTZkvW+q+0Yqs1hSN4jyLQk9nb.uoQZY9iky584K0KSPKZBhUvoifgn7kqlqLbd9.Y2UfVJewjZwp.0gxSW1uswKX7Zhcku7BwS+45uq70TeuFcYsAL3bkWUhdEZhDvfmHuGCdLdOJI3dmW4AUf+UUHj8.6bkRpCM3zKupc+c0wL0Dzud+3j07ayGWcrpxtvAqRaQdGeV2nL46CPuJdnV9dqlUhDto2hfyX3jgo726u+WllO+KR+s2lnlMQYzr8N2igCGhwXnYyl.PZZJ444yfjREg2ihhHNNFQBERarN71.AMUHkbuxfTF++wY4bm6sCXLf1f696Sim644K9U+EH0IjaKKUOFMnKK.mSEproyvlSSvvGuu49Awl9Z7zNXkmmS+98wDEwq7JuBKu7x.PiFMpksAkbwiOWXe71b9fuFMZzfBqiW9keYd5m9o4jSNoFE8mDryB88yJxDKZ4KDYKkslSThxFJd0dW46gtH0tfiTwtYekPnuZkRAZCRIBTxToNjTF1uvj1m8kB8DTujvZU432zGyJ7nkfiVJwFb3RrnD+jey43f07WiOcTb9fsdmmcVWuVz1Y5wmdbAUq+N+HDK5BwCBDwS++UOPTUnLm923ECENE4dOM61ku7W8qQx0tBG2uOcWZITJEGczQr6t6VyypJYAPDolKUVqmPVqnq4GTsXj5BOnXTAxNhMHloCFLfSN4DFLXDM5zkiGNBVcM949J+7rxlawIiGgxDiniN+GN7DJn0khT2i7Tu3CQa5LdpB0tJjDO4jS3JW4J749beNz5IsGEEEDGG+Q0g7E1E1GZly4HuvFxVNumUWcU9re1OK.LZzny829g4.bmEmdpcxnhT6kHRMuiDNYpvYcNCpO62ETRJQrnn.EEnEKZIT2W0hGkyh16v3bX7NLtPY0I1Kn8BhRUygpBsPgVStRvJgWgnfnJ4OkpbcBuPDLVH1IncTF1PeHC.mhuVnBgtT4cD68j37jXKngyVG1wEMd2CRHBe2V160u+rryiCVON3H0YYW3f0GPOcmGDGOBhLg7mwQFRSGQy1s4t6sOu7q744y7k9Jj2OknkWmdqcINt+Pd6acGN4jSHJJhjnXT0k1lhxGVsXs4jkMlw4iI2EP3Jj9ot.7uhCOEjkOhSF1m8O9HNXv.7MSPZzB2vT15k+o4m5K7pr8QmvXmFSRxoB0kHRc0RGJ63ZR8.BwWVlbduvRwGQVncp7C9R9M3U0oYLLkSuSov9UbgnhyChyixWBadY3XGMNk0t1U35exOACA56rjlEPKLNpAiFe9Cvbgcg83tUMoPuTkUYSLGAMvxlmhwlSCJnkxwW3S7hrT6VjZm77T0X40E5dmGsqTHPkol7o3ww6GSsv2ko.SeQ.pOuyBSmMgg9OOiLFbA7iZ52C+uFXQxHiq73wM0wV3fSIg9q0Hnm2QlJoIP7SEpSW3cwOk3cNY7EsHn8gsmZpiEupZaL00HoLSy8tfycdPvUS97pP+cV7lZ58cUhcAUGmpxgHT3m464cUs1qVu4eu9b4cgavm05vL2oc9.Cn7gwMlcrjx8U8Oap6Id.G66gpNXUe.M0IeklhrHqFkmoV24Se1pv1790InyJVsmmGzmGw5l0SZOVWnlApjf9iL4X1i38jmmQm1QjkNFcqDNH2yK+O3qyINCeueqeKVYqMY40tDiFzmsu28Yzfgr9pKSrQS13ADEEtDUcyuEAmWvJdzdEJqihrfyV5XMiSGxNGrGGMnOY3YsqeSN15o+f9r5m+KvW8W4+RNzp4DhoSTBVuGMdzhIH.c5xx0iySt0V67kRKf3P6U37NTEUsqtYJP2y+9C50sEAg+z7.aQOPEbHxgxGz6PuH37ZT.txasEQvlmgxYwnhIqvh2KHZCVGfSvlEfKONRiyUf05QSfCccu4SwaLZ.Ff33FzCvjqw6Cph74ctbgcg83rI3.WPGlBC6GJfvdBOi3j.kAZmj.GtK3RgCNjKmMf0WYUt2vgr1RqRD9R5aKf.VrzvQfb05P4AKVaPY83h7nhTXyywH5YF1yyjIC4Jc1qR+oTSMHW36TU0CjPX3ldTFgv.SxT8qLSIgITpuPbHR4pVUWUUUn0G1WA8appBV.3qD23P+gBQDxp5x8kNLo2Z5q5bkU3ivwjqhKqNGdwg24JG11VdrVl81xjSEozA0vxq3maI2tJ8bMLwPl5UY62Tb3M3QUPqI8ks2pRc7qBEOQp9My880s9Zv6vW4PUsN.FHLupLooJOcCUCDe41U3TUGjp0MjrAS9cT4P54z+e3c6jkW0XM05L4be5ee.4wPMdTiyEZ6EU3d.wEtfoqn+S4Ydsp55maycN1C8Rky6UKbyzCGXDO28wB9tGFa+pG7cBAjSJ0HcuDJqAwFAaVdPBFLQjCDY5vM9T+Lj+q540+C9cnIVHJg7hLt2AGvw8OhU60kNsafxqBO7vja3kpJCnRnUy13sNRSGw9Gb.au6NLNcLc1XMt70uAaezIje66wZe0eQ9G+ey+szGC2a2CY0KeMTMZRVQ5Ls+SlI5b2PC0d0qbp5E+vNO5l1Y52KWeTdJ0XlvC3ZW4wJU0DK+LHXQo37UgBWUGXUYWTXFkZFZK3O669c3Gc3d7BW5Jr00tNdKj6yIJ17wonkdg820Le4e7RcwRN3NwDaXZFIwJX3.re2+Z19G+i31e2uOTjyRqrJVmCs3BOeUJ1uhVEnqP0fsSgf0GrGWlE8JOJTkCvdlyh+7LeXPSoxKKp1TgonAKn+nYFKQgHN79Jm.NiwZpNwmt+rvrCw3OcVbWsIpbzRffhoO04dcei0ZS37W4X10CpcZhJmmXBwzmwI2o9MSPSZZ8xZpjehoklgI++7sBKBvmEU5bd3zc5T8yKUxlzTedtchTc8q9uKncrxm2JtVW88uKGIej6f0z1GDjpdPr2OCb+AZ+IPTRLiyyC5MhVy3zwT3f01bCV+K+knY1.9t+Y+oXu+Nr7UuFsMZt8a75L7NaS2UWJ.krBhhRvXLnUQnLQ0N6bvdGxQGcDCGOfjVMo4RqfQKLrvR+exsgFM3S9q7qxm+q9ODuJlgCFwZquIsZ2l6ezwznUxhgH+Cw1oEse+PCEnoJSDSPMszQVsl3lMneuVt34G...B.IQTPT89bxa9lzJ2wn0uJFePRGzdUsRIegcg8DqImc.ThiiAeNE2aa9d+seaFc+6ShIhka2k6TXwaBZ3jW70SVtJ7aUBc7SDVYzGpC4XUeBkkFGgR9vVhRRkhwOy5N0mElcR7dUUzblEosZjWXZGrNMZ9mIGY8tEt74W2yiH59ErdSmQfg1g4Vuxie+baqyydbZLlOrrOxcvp5ltSEVwyXP+2Oa+oee5s+CiAwm9F242tdkDBCkHnMwHJK4ooLtHkXkg3lM4S+k9RzrWK9a9VeKN7V2hCUdRtzkw3JHa7P7YonJ.cQFdeJhUvZcjaKqGXIZTFMsWacvnYf0QdtEhSftc3y9k+Gvq9U+EwlzjsO3H5r4kvBbu6cOV6Jaw3rQK9gtGRH78fXmWL1OuP79v3nqlyAJAWH3.0gaToTjjjvlWdKF6J339mvgGez++r2aVPxxU588866bNYlUUc0a2ErLXv1r.NjblghaiGpvjbBRQIQ4GrLUDdQJbX+hevge2gewNre09YG9U+hb3fVgkTXaYQaYaJINZ3h3vgCvfAXFfKVt.3BfK56sWqpxky4yObxLqrptpt6K59d69BT+in6rprxJqb67c9V9+88wFasMNaBkAOo1jOKjLkqvJzBoNTMhFHQh8ZuC1aeJGOgs2bKr2rGWaiWi23c+.td+dmP3ad3OvHHcCe34CG2CUcUjYNkAzoJdEeqtvO+X6qvTkvZpCWKSN1Yhf3KP4oEsbQjVe91dy7a+oc7LuxYm11N+6EYwJ0+YIboqfUClmOVO9fYI4n1tz.Zf7xBP.m0hI.tdYXRSQ8AN7v8n+FavOy29uJa8LOG+f+7+D9nW9GR9A6StAPRvj4vEBwt0NfDDb.tffWfRwiOwRIJgw4PU.dxmlW5uxuHeoetuNO2W6qwcOZL4iqn2laiWbToJarwvHuI5P5yt3QsxUWVVxLekGNd7TKLxZnnpjd86Snpf7CGw3pBvAFi6yHUBrUXElhlf+HZjaxilTRJUzykvFCViAAOGTEP7AREKNwfw.ZS13pS6hDgV9E8vEAgi4G41PRoy99ikTR0dtpUIs5PwEZkKU+8ZL5r1SVsDNOzTVdlFRutbnpgCYMyroloJTI.AspsutRi7mkX7+TuI0YcFW6A4z4dlU4GZ3fzbbQR5bAYpm1ryFlvNgSr60Gp4rWihRcNHm8f1r34U97Rye9RWAKUm1LdadOzQgqK.qfl2hpGUdnQQPbwRMWPTBAAbIXrQdYMQE7Z.eomgewuH+1O6yx6+M+E4u368cY+29VPvSX+8ovGnHn0gY2fylh0YQrFppFCiGGIa3MeZdtW5mgm6k9Zbyu3yRuM1hclTwX0wfM2Bx5w3xRt4S9Dzq+Z71u2sY8qs0LIVPWbY59148nY2kyXw0474iFNWEK.d0oqcjVJwVqgyxjxBNX7QrNB8GL.LPdUAN2m6SB2U3wYzgtI5bqN5AKnWVBLoBoJf3qvWTRwjJ7E4zykf0TyQz4Fu9nTAqyK5FRutqKtr98Sii2LyUsHu2snPD1pVRmuaX187odLtn0YlolYs7HQrHOXoypu0wJnpXjiczMU4qiebeZNH4y5gCbQ3RWAqEgKRWKeZgH77dSu6.wlr.oIuH.HIIAuVFUxxp3MfFzXaTnWON7nQ3RxXR9D7E4r0y7r725e++CX2O7N79u4s3id22kP9DJNZL4iFQUdAAe7bIXfm3YdNFr8lbym5Kv1O4WfAadcLCViRSJ2uxfoWFLLkRaJ86MfqOnOSxK3fC+H1bigfwrzAuOR4B0b+1OpPSWdeFAPJsbsH.30.k9JvlhKKk.PguBiaUsvZEdbGKILTDyTrDA.kp7ITNZBIphUgTMlt+WUv7dDowSUKySVyKUqY7di7ttdzpwCWJfX5HSThYTX6bLDyswVi1lQ947GfQpz2PP9kMO0zMewdBpaIAZgngb8KyCVHcJ+C06GiTSZr3FEulIyPKi.zV6wNlGyputMyggY1Oe5w6msU55RWAqSZB7KqI3OOn6wbyq8nXsVxRFD8ZUQNlDK82XSRxFfDBTkWPwQ6yg4SXznBxVeK9ReieAdouwuPqBVE4iQq7XECINGVmiRSIYCGPRuMnDCiUKAL35uFoCFhZcXSy.qghfRHeBhHjzKi986ynppicbeYhtBJl2qlKxCVmmi4tY1R6TElY2m6t+d3Cg30OeEEUknjgKMosxyrhl6qvi2Xw9YRHPwDOog.YIIXMBgI4TUof2GKMBAE0nGSYfKJNzdU.MFc03w74k6Liw5y84c8f0hjU0TFBVDA2OKz1na6LaQ3jB03wBSWsRPs5j1wCmyvcql22Q4pSBOdR8mKFbtUvRDZyXjl+7L0MwFcZrqEotOu0YhSmyMyCncCWk26w4b0e1hI57oe7MmaOe.Q2A.K5g0FOf367HTTg+owEOIIkfpTTUFqyToYw5eh.gh5poqKir01fPRF97IjmOBTAm.RVOTqESU+1PhohgRCnRfIFgwdERSfzd3xFhlM.uKkzACPMVDKQ24agXSTOd73bIsCRZp4UmEdQ0tMKoHxcVU7YQtXeQut6wWCBgvoZ+iHwJduy4Z6yiIIIbTa6.xz9rl26iaiNsdl0ueeJKKiVwZrjmmiesgHFCU3i8CrUXEdbF0dbQoNq4nN.PBw5vmVhHJ4imv5YYP9nXUFf3Xnt7thNxIEQZ6HEsMFZIlAt8rVBUgZGkzILVsGRyqvwhOzkFELlaCZTdvYh8W0VcHCSqshPz6zwueyNrVtsLc6Al5IKlimTxreulkse8FOjqc9M5bNtr4zVtGql2yPmrBVV2bxm5nriTu+zNysFaH0TWSnZnKwTEGatGIhs9jzOqxfyElw48bUCZ3u5YQ987XdkP699ntFS+tZcMsB83ISfHBAuuUuEfo8A3KHiCtz8fEb7IiOM2jdRZ4+v536jFLrnistqeZVEFqTrpIRxGUfzjdXr0bVvZIHVDiCi0RvWRYYNXLDLBAWrs3.PoDK9cYoowGFrofKEIsOj1GSVOv4vW2VcLFA0Va0goo5894U6JhnIaoftUXGYoByaD594EBZtBeNAMOWO+50oqUpesPntdxc0I7fWV3AkCnKxyWmlJFmnQtmRER+z1Wy6gpPWEFq2+M5etnuaSo83r3osOuhqDJXAcz9btaNc0nbdhT9nPAqFKPVFQ4otGccLW.WOKrXhUb3Xoqqp0hiPLeTvjXwTaUkZMT4DrkV7oIDBUXqJHDpPCU389o+Flnx.IIoQk0rNDSJZZFljTjjLTqMxY.i.FCFifX.jZ6TkOqGA7SGFMN4wxTVtanN9rTXOVgU3ri4JViWAwxTyooRg2sseMy1NG2rNF2sZ8bEQNYFzY4RjdxdPpUVRiWhZbzSsBqmlGnXt47Vz9WOoTIn62etiIodksm60KiYVYSQXVqCcnocilWV4zuemvH1rtle6OmJ17JiBVMXdkllmjfKySQOLOdNoeuEkQIyakhoNv1FbDnpS4+2fn9ZmIYPECViK9.syfnI3HCU8sYkSDSIlsEGfAwZPEKFmCrYQEtrloUkbqVmUHJhsl.i742Xi2fFBrJJXquW0HPVfYRq6lWGamHmt0mqvJ7XM53j6oCCpU1RZ7ewhqf3WXGByExsiiG9RvZ3fU22uLZTbVuRzbVMe+LbdzkhJy6ALU0SV4p4+MWfmlzl3.Vil1hyxB42osuWjgpedFW5JX0vOqEsd3p+MIUh8gJIL6C+MVL0LQMT6ADbDz.hXhgqSBDvCRrGao0q2XMnZ.QUT0Gavx080pVqIUSclr0IMbMNTCHFhsqmldxoT6wrlPx23d4OmqgUSKxooYw1HLsaHgElqWm07ceDertBqviLHAlm76sYMVS3g9Lv.f4C2eac.atsqU4llJ9dSAFsd9o1rNb9u3b5NMqGcjVEsNMrHtpdV3JzYI7cp43JL1XXdyVG578OKSYbUed6GU3RWAKXVNNsrLCaQba5x3l37ZpqnGSIw3wV8qOg8A.pknxUAh0xJUwfs0szdeERvVqDPaeJu9aavHInhEwL0CZw5JVj18VwhF0bfn1VxLDO7hpJH+3JZBuaTOzXGluwiVlZuZ08dt4LKRbEVgG2Q.sVVVTQjNMTmq3gL7hBm2LU9j8vkrv0tLrvx1vCnBVcWWTQ4i6HitJQM0ChKPwLg5nhL21dFUB6yC3JgBVcw7gHbQgKrqxUOpBU37GSM+UEh06JEOHfUMydNDlaPgZp87tR.ORRrAQ2PVfoYcYjP7IFyz1qfFlIjUpBAIIVSsDESaA+qgYhALRzE9Sq+K5TOBeJ7G3yinKwUaedakzhU3y0HpX0Ldupc8WtnkqOymgdmwsaYXl5kkHDpa4YRiS.p+ELHyr8yavZqGqlmuvmw4sN0xXTme6EuQyt8vrWaNVXGaxgz5scYkJVUl1Pr6hPGmpIhrztcQy27y54f8UNErVFl26VMJ37vVAqkYAvTEnjn0bcTVYFWqpQR5KZzZAgYsNngECQWOSTYJolfgBD7Jl1v4YInSejWzXHAiwLWi+YTBhfQiUkbOAvXoIgUZcId8wavDp4H1bmmsu+ju9ZzYGhL+f84eeWWx23onKBLkTkMmOg50WW5IPZcmebCCXZSJ8nq96RVSCQZq0b74k3eAwfHfPrB8uRE0U3wdzxQz4VsL8ylhouITqvkrfO6hBmFGrlNVWYQ0yKSn1.VlJiXpLnvIJdSpO2iYarYlF7bqBRsd1yLmxUyG6wl4pZ9dQhGf5a1f18S2kAUZeuHZ65EwRydqoeLtnkVZVJys9364DhZT7vbYe1znnDjP69aQbw5gKLzTTuAp4PaGk+TS8bc93ypgY29HBs2qgN2i5foyML6b8Ga3wb3BQAqtg3KzTegVx1DBg5rRn46PKmWBgPacM43dL53dy5rnb070Ii4+LeccvnqBamV1IN+usIzPzoNaSyuQ25nU8x.Z8fVsVrfgXBlHsaSy0GiQlyNAaGieL3z3RUBw1wS8eXqCEn1nPfolT71XluTKXPDEUVdW06jpyUF0.pO9vYCYHaFngs0EzsNLShVD00RJWXpW45dcsYo0ZWZY6nRUbhqM7EwPnF5j0lF7d.Src2PVJpQnnXBNif0.95ebunTgRkHfGrpEGNnTwXSnhJFE7TXi04MBJI0kciUXE9rGllHLwITpMpzFSnl33cCduhyFk+opF4.esQMduGiH3bt1ZZ33pw3rIDpTrFGdppmWqdb+bGEMi28y8d.LZ.qoghoxTsM.Z7MhRT9mVKaHXhk2l39QvX5XfXPaKCNRLs+hFQ0LgZcjEf5IbkJzPYrbCqZL5DMVx1PlCswSPgZC7B0Gh0x2TKFsY6WvRg5q8MjguY81NJJJSuUcrk0Fpq0pNHy9914am65dy9V0p16KGaNQiM97.lox3auOM8Xq8Uc99MW08yGzz49MrV6LI3UjmwwiYiFutKACpeZAytsT6nS85pXDDeybLBH0Y2uwOyuYit.wyEej9MJLi25jkoF1wwktGr5VBFLFS6qeTnA7Eg2uLmwvr05xYoQQp5AxXas.agJ1sHRI17QZzGJpps7rRMBhXpkIFhJyIlH+hXphfMG2MDqWz5is4VRsxSGe8ZzRH83wbOZkjGnwSZ0BFTvVWfJhm60mOKK8iOEknitfNpbYbEAPaR5.S6OvTE75buR8HAOXssdQzaZDBUmzkc9oCXvKZmq8qBc3J74MXN1q6RWhtxxWpL7Yc4UWeMblvwyjt.J0dUncVuFkTpWnS8hUS.NCcpGCSkg6a8xCM6JUw2p7mRipdpXPPwYLwVdSHJbTQp29tdpZ1yz4CiXnUtzBVZiKqUGqdW5Z8bRX5owmpkmUrvLPrUguylxFOn+FvwclwrNYoa7fVfmjztdqx.ZT4Voy82lmT7M2aZU8a5SkSuVMccgot75DwUFEr599GFJX8nt3jNOlWvP78yd92rMmc9k0D1qFMg5z5DD.hVGI079p1ozw2qBpXQCNBlZkhXIKC0tTt65IJbKXKmIN6sIoXsEDD.6bISbiEmAfJc4EtyK56SqxrkUXEtXwCrBVeJ1+.KP1o1wKKMJSU649vTEbZnWQsCqNNWoTSasrpNFCQkrpkp4EnaHkhN1xSPLX0PsxYwDIJpXUf.RzyOpfQBSKCL5TCyZnvQvnKU9W2y+kstGjqvmk6GGqmNtDdacQikksiymcisbCqtqAbUWj9ktBVpF8U47WfunTDZQgW5xRIKX1GblWnz7JWclFPnScabfXiXEhNyQDgPsvCC1HugZduF8JiQknqaXpxQKZYL7hytdLU3k4hocGKDhBwpoMYGtnE+LcZMXQd3S0w4GfdVCw7JrBqvYCmVMC77fEYfZLzXS4Vo1DpMKPS6tR.j.pFp6bn5zP9zxOmNsaGUid9uNLPFSimohgLJ9qFqCXhuIVQQYfMj5VIPvP72hX0POZTaTwJKS8mU4r9W3X3jlG3A4p7xTf4AEOJL5cQI11h11tEt0qp3JiBVs73JDlo3pcZMyxy7uwBV2koGMZDPznf0xxdxSRXUPhQDKTqfSSMcJFNPAI.VrQKn5pDiJDp6AeFsokEuXzdboSsVsY8g.DrB9ZqDM.kc3em.fapEbMaSihVJwxSwoNH5XW2574MUA+ZQfcExqbJe2UJXsBqv4BQN2ZlZvRWt1FVVNn8oCGWlXceqcNOY0IMfXlv5T6EICSWcWQ.M5W088R8JMZcwUMnLMTeFL9XYG1HFvDpcOUXlchZffZPBFDwfmXkQOTSsgSRJjxBjANy0DyYp0c0Rsg47HUim7V193XeuNa27TC47hOKFggKcErZv70AqGEgH7Q8DrG+bZZU7coVobJGiQR7Isjyr82pQPhFqKVJQkrBHw3GqDI2dslOKmCVMQZOZonTWxGD.DOBgZulUGlxNodjJQBEFf4HKe.SiBeywkoSSgpOsXkGrVgU3gClYrzE7XpSJDgPzHsFdWIRaAkncaizZH16Daq4xcnHEy9x5UTW7ETOwpuSzX3XcxKVpF.pIBukXW3HfTGZQoNzfPSDFhhE80bicp7tF9it7qYm1bgOH8E0OMxROIOL0T02uHvI4gskE4o3quZKC+JiBVyGK+KpIVupEhv4Qzpui64poavI+8anvkwHPPwPcYanVff1jYhhEuIJvI.DjX1dZrknhtTErTeLqOMHsKa9bCPRnomUEEVnAgfIL0BsZNVESxlFqMiVcYCwtcuo0ZxEGhgkcu5A0BpUdvZEVgKVbL40cLj4hTF6hCQH0J2TKeoV0pneqZ3tS812g+S1N5z3kJniGul9KDocgstdOE87UfVejo.pEu3ZyNPITUamYETSjdip3q8xVvDnBSasDynfK.1Sfw4yjkiyet28DbIXdNLM+9XY4OdqmqZde6s2GsgHb9jcZ1xzT2izql3RWAKiIlxuOrTv5pJlZY1rq+AU4OiwDK9cD8TkPmPvIwBjWz.u.dwPPT70D4T0.NiBDExn0Y6W2kQNLnnXaWpcyJPMEmWi1GJwzRNTG9uPSsVQpGfVKUQYpqoWlUHcGHsnqIm2mOVob0JrBmezUdcSVf2sr27P+2OzQIAAlVBFheX2hdPjpB08i0NDa2WKmyKRT9ElXcwKHjllBDYYpQo1H1H+sBBTDj5rNN.pf0GCqnEEITFuVPrbDDpsFNHBdwF42AVjPfyJ5dM8r58pGl2GdXKEs44nE4bjFpqbUFmaErzYlnrYcSmbro1VIbbKPDiYojbu610t8eJ7.QW9bsLtXEBgiKnnyw+IgS6g2kcLtLKKNo8+7g4xBTow5HiotFrznfUSs5vihZDRGzCenh81aOrIN5uwPBUJlROFhYRSzyTytzHVTBQARZsfISLyDMJjfCTOhOFFPqwQf.UUg1qqCRSwGBT5qh00DfPPwlXwZ.03OUBxdrAVcVeWNXsHXLFBdZuWZsVr1lTddwSDzMj0FiIx2hlDSXAGSK6d1JE4VgOKf4GmzL9nppBwBd0i26a4fUC5VWg5BU0XQX1r3v2282XYvTOuQa0Tml5tmRjX6Bl5ZnTpKgf5Yx3IrVVJ1rLFc3gTIUrwMtFilTv9GbHYC2jzgqwnJAOvcGOgIkULZbN444n9.Vqkzzd3RsXRSHKwxPWFCL8HyWgqbB9hIXqLPHPugC4n81kw4iYiabC7VKGLJmd8y.e4CTX9lO1lmlW7a3v0wl6s4Z372aZtWz78a7bU86WTVFtHuK0r7jl+7rPKllW2kK1MqKDTnte8JhfFBngPTg0NbAtKLlX4Znkmfye7bAKt9gtGrtp6IptBNtJFBokIbpAGq5KO26S6mxjhbt+t6fM0x1WacpBdN3f6iTEHKa.V0VqPkbrk9x.pDpCMXcQA0n0Uldv3BTgGw0TPAqnnn.mMkACGxAGbHdeLwERrIXsVbNGAOLoXLZ5IqfxhV2IpXyY7d1U8mKWgU3wArLCfWzjjG66cAIdso5jK0YQspQ21qJjXsT5CLYzXxxxX80WmISJn3nb5u4lTHU7Fu66xvqccdhuxWg6czHdq8Oj6r297Su86y6uy8PSRPbYDrBpJTVEkw4Ck3bVVOKimby04EtwM4E2ZatQVO5ifUM3O5H56GQha.a3F.4dRRcrYudLY7DRNmoAvocIr68m4MR8AQd44EWzQf3X6qK+opWHdjFhvEq.yY+B8mVBpurs8jTt5pBLMd4iiyQIUUZ4NdmieOSuNs68uG23FWijTCGbzQPYNYNGtrDxVKCM21PeyHWrlaoyQMGshu2fo88UFkcYLZ+Xcxx6KhtqOEbRESpNhdaNntMDXHTTxj7Bx7JIIYDq9ywTnl1SiOcI4vhF.unAdmTFKtLz5krq.JbuBqvUIbRQV3XbmQNNWauHPr3f1P77oC4UMPUQfdYYL1CSxKIayA3LN1au8X+CNjxTCa+k+pTfve7adK9Ku0axs9j6QYZOF7DOIYeyuALnOYatIoquF1zLpp8FuVVv89n6v3iNjex8tO+327mv59.eksuA+RO2KvK8DOER1gXPvTUhTVfOuhhQGf0ZY6ACXhujv4ni7omRYZcYJ1zb8OrD4gyy4poshrGLbZFKeQBoNDrMKuJn00kNGrNML+jsOnSxcZDmdQtd7SyuyCKbRGGwGjLQhHncZuAB0gLKvlaLDM3w58zWT3fCnvWhfRgIEU6S3DdL3jJSFdmGx7j1KCeUESxOhDSBCWaCpJp3d269bsMU7kARS6g0YnpzSnxGsFzwBsf5rL.bp6ni79n6d4jB4W2s4zbud2emEcLdEUm7UXEdjgkovTWiRlWIqte2yiT1XAAsNbQTmsfBS4zTMmo.HqWOFWUw8FOB0Zwr95TkHXFNj24n8469m+Wvq7NuCa8rOGOy25aylO6yy0ewWfrqecl3bTYgJwPUcK0RUESvyyWUBiNhit683tu0ay8eq2kW+t6v6+idU19U9w7a+K9KyKd8s3nctO4GNluv1aAxPJ2cGJO5HjrzGbsV5fGDiDeP1OOTUB5hd+cBxnur0w5QRHBOoAemkmtha6h8.148FV2XH2c+9nCmrKhCgtJETWjPacaUcrjqIXtJy1HSMJLree9fa+trYVBuzy8rLPr3KxQTOSBAp5uNElS+wfEdMQpnvOBWBT5SnH0hw3HqWOLFG2bqg7Qe3NjWTPUUA8Wa.8WuGkdkBMfDTrAcZZOeAbuTVvySKiSGmWA7qvJ74crH46Wzdn5jfWZJyM.ZHlgf0UOcEEw43nwiHasgjj0m6s29nhx5auA9zD9C+I+T9d+3WifKku9u6+17b+7+br8y+h3Gzmaeu6yGUTxjhRlDpHOTEq6W.hQIQDF5RH0jP+uvWju1y+hX+0JYma8N7Fe+uOu7q957N+Q+y423m6mme0u7KxFq2mc2aWVyGHYsAPvSNgYagWOf3ztBeZyscr6Qy06Yev3G1xwx7j0Eg72FOVcUwoHcwUdOXsLbVuXtz3+yhIn27dw5xNzgmzwfH1XO+SpIMZWE10HID9v2+83KdiavSs4FL9t2m26seKbg.q2qGGLdDSRbTXaH0d3XKIHGa8A7QRtSECRcTVLAw3vkkx3fx8ppn2vMYym3I3E1ZCpP3d6e.Gc3AXGNDm0wjhJpaxDWHWmVVHB61.sWzmeZXYgFTth3B5UXEtLwwBe9bgf5T4354P7pRSSnHpPkQYJOQqQouBwlvjPfw4SHY80IayM3N6bW9m+idU9W9deDO2uzuLeqe8ecdpuzWhcKJ4mr69r2cuGSPQSRPsFv5HwkfyDqIfw5cEDJK3vhRNPK3fzLF1e.8+ReE9Z27l7Re6+M3e8+z+.9e8u7Om6ke.+M+k9EQNB1euc4Y2bCDmqsIE+n.m17YOLL37xHDgS+Mt7kOeoRx8yhRRyy6pE4woyyw0xxxgqJZCunPz0br4QASc+xRj1zUVUsMsjWu+.dladSd2ezqx+je++A7wuwavZVCRdAZ4DRRigXTBFTywWZwgZBfWZWeP7HASr1vjWE6B8VGZhiRwvXMPt0QYhk+1+c+6wW+a8qx5CVm2Z+6.bDRVFRYIIoowlq4ILd6zR7fVqWNguyx9tOHXYJYcU44jUXEtLvxFWrLkqlwPVcFpi9f+aKf2Dk54BwWKgowDHHQEr5u9P1c7XFodVa6s3C1cG9Se4+RdsO9d70+a72je1uyuEI86ye9suCEhEIMEeZJWa6qwjISv683yyITNAeQATUgF7XTXsM2BRRIXrLppf26d6gJAFlkwFO6Wfe8+i96we3+K+97G7x+PNXxA7a8ReUd9m+o3f8OfQ6tCat40VnW2O6WCNE4Ocu.qcdeixHgEG8fo22V1uam84C.dT3vhYbfxC8esSFObTvR6nTf1P13N87Il1lVpaavsa9CpKOO2GpWwmfbQ7OqcIz1DmiqwTWkhm1a.+Rewu.uwewOf+G+u8+NB+E+.9Y+N+l7BOySyd28tb8gCPKlfUa3OpTQB..f.PRDEDUJ1dgisL1hcho7b2kfIVfPGWw5qsAUpx9iFiasdjNbcdmO4C4G7S9I7O3+l+qYv+k+WwO2uwuIq2a.UFGp3njRLVYl1X37mmye9ebXvPHJiPX5ycwh20zZsUyecHSOhE0TWI8koc4dac1G4EnxDsPVESzaacn1wJrBOtCE.ab4zIBhYMrBskhEBFTLwJQdc8P2EBj3os8b0t+jiWNUlYLbLiWhFKW2.4OOS4ZBl1PBJpAu.AqGuFGVOnWeHHjWUQ1MtI65r76+c+i3s9nOl+5+G9eLOyu4uC2Zui3nc1ixzdTgvZarIpp7Nu8sYmO4SXxni3n81kI6uOlpB5YLLrWFooojNbSFr8Fr0MtI81XHqMrGEgJFEJiYIX9D967e1+o7m9O5eL++8O7eD8LBC+FeS5Ybr9MtIUdO1Pnsr3ncoLhDlVDlqqUWwP1YvUK2Tjl5R3oiEceIzb+R6nzTMlO7fyzgOthfFNX0z.QDZ3l2oebN6G2U+DXwT2oYtwi+sWFN2JXYLG2R91BMmwzlEbZ8CQMqWHViLhM9RKFLfJD70ZMKcisJHgYCoieZthD++Rhs7hpMGyd7alY6l+62EKqNZcdvChF8yeNDCwFT38HYYTUEHQE7AkipFyVasNI9J9+5+o+9Ddy2j+Z+1+Vr9FCnZ+6w5TfNpjf2SZZJh.EEk.BIINBAO44S.DrVScEEVIDhcvbq0gKwRdQNGVsGgfijTAQKYxte.OcO3K+u4uB+A++9Gwu++C+2y+Ie0uJa+BeEt8a+gH8fjA8waTnLzVCuZOOoikumxnjXEkOJl1CfZiYFSUbDmQTL0UaPW81Dj5IKrNTxIM0v3p.9pBbUdT0xjfRtAxEAuHjErj48D793uVnJF5fSfyWqvJbUFUBTPTNReOw9ebhmJBD.RDP7VHXAbTJPgDka2yKLPgTiPAApBkwwrVKpot8ZIfT28FBnDpowfHQYHRSp3zLd4T7R7wFWo0MhdDRph6qwIPvYvnARCFB6Ol05uAVSFSbo76+m8mw+5wS3eu+y+ufq8BeE9Ke22EMaMbNGINKRkm67duC2912lO5CtCGczQjmmiurBqPrkj4qv4bjkkQo79HNKq0aMt4MuIO2K7h7jO8SQvY4nQ4npk25iuGu3u52h0RGv26e7+6jZ1feqe9uAGM9Prk6v18SwVILonhfyhZiyYlHFbDffu0XuRQQz.IUwhIcreS+fFknoycJRTlnsVY21q6Dm2sc9aUwqZaIwnAM0Sv32Yw+Jm159zhf.nwmkZbNWaeoTl83IpP+zkP87nASasXSjoEI2nRHloGvRrgd+fhyuGrZuWYN1MSstilyBH4d7bvNS76uZD0zGefPjiTlfhODI7tDjXCd1XvZsTL5.pN5.xlLgdiGynI6y37wTDJ.f9I84SN5HTUYvfADBAN7vCQDggCGhHBUUU389iEt1fVAVh00ppDBg.NT7gIj5fhi1kqm3HeRAiGOl0LVRS6im.SBkHVyoZGvol8KZXpP3Zq6hC5rsgJM1E6UDQa8pkJlnfcodCpggodvxKf2ZQDAqBNM15LD.w1XMym9TrdEVgKWXnh5IA7DG2DKSvDvgGstHdVO4i3.QVdlEKmONU8oAFrXBJNEppybZOBASza9Cy5y8936x0dwWj+g+neHu7G7g7s9242ia7M9E3key2BiMEo1P+xxRdq25s3G+i+wr2t6Va3oPZhCIMINIbPiETUfRmkBsDqnbvni3v25P93O7i4K7EeFd9u7KxM+BOAexNeLGV44nQi4Fe0uB+J+0+axe3+a+exVCuN+Ze0W.qYB9PAgxBrRrvkVD7TF7jXcP0TeZEj.difMPsLICZcW43SKTiPSLIfNNlXtsKVmwlRCkta6kM5dr1nXkxTOYsHXzNpKMWD2N4GhWrQ0KCWHUx8GjT.cYor6r6yKexk+3B5RxzHexZxzvnBVGs6QbzQGADs13fQGPdQN1LazChVKCFLfxxRJJJHIIgabiaDq35gPcwCMpbUSEPupphQiFw3IUjjjPZZJAaWErTbl39NKKipQkLYxj18WPKmwRhy64OMOCt.WfqKPk8YddTnsk9DEOOqqkMZy03X0A9r776JrBON.g3D.tELMTSOKso8ICwxffUET0SgSYhwi6bNFnKwj+z.0V64EeTdhMzP98XK6pJwgZb7pu+6we1q8Z7y9s+U3a7K9Kw6emODmMkzzd3Cvcu6c4m9S+o7Nuy6vQGcDoII.QYlMxWMFCYoIsx.mTVfV5wXrjZcjWLg69QeLGbzgLpHm8GeDewW7Y4d28CY6zD7FKeke4eId+29s4ewq8i3K7TayWseOpNpDBgZRzaoH3a4FrNklT.lZZ0zrhSO.qm57nKg6Umq84UHzvCqExwrGAQZ3BiCVKqFR0LQZWBON0CVGWwKlYad74F4kENsLyQDAu2W2dGRwLwPZZJ8G1mppJJlTPVVFIIITVVRRRBYYYLd7X1YmcZ2GMe+l+Vas0X35CXuC2q00pFigLqAePgPQqkdMGideTvgwFcCaLDwmu6wMJX095Nm2sdGsy3nYudU6F3121XG2TdYEaPrwRIQHTgnwPDFz.BAbhkSimoqvJbUEV53C11gFM8oOvKJVpffGSPIoRgfxXIvXmxfyWgH+bAs1nHqQPMfwq3BMFDAAikwh.auA+e+O4eBo2357s+c9qwm3Udy25c4E9peMTL79uy6vK+xuLuy67NHhv1auMYooTTTPYYIYYY.v3wi4v81GmywZqsFqs1ZT5KhyUYDVas0HKIi7xBd629s41e7GvuwvdzKqGldo7w2eWDqiu8u6eC9+3u++y7c+w+Ht427KyMcVRSyP8BdeIBArVAOdRVPgDM1UMhmymJOiNk4P0krcySwl4W+iSyMe7RHB7nJVYW3jb+30bnieC4r58ptdkXEVLZ5SdciNrpw9wUSX+RRRnPi8MrPcu4pptcOndkQiFwfACX3vgr+96ya7FuA2+92GQDFOdbqxShHjjjvlatIOwS7DbsquUqhLkkkQEpbVPpfPEZgT6UrHgPCgP78CRiguLTh8BHDasOq0Dpvk3U0kmQhwYIBxTg1w0p3rBIhgxPjCWwR+.07IYklUqviuvPbB.a6rrMjrt16I.pUASLD4odkLS.qpT4TJRT7RSXDubf2zzM6ig2zE64x3ECdwRYZJu16+9ri.eyuy2g8pTNnpjm3IeZN5nwbvgGxO7G9C4V25Vr4laxVasEiFMhcqCQn26w4hSSt+96y68NuKgPfm+4edd1W34id+OeBE4iwJNrFGIII3CUL5nw7G9u7Ohuy242fQg.t9838u+8op+.d1u0uD+3+n+E7W4day5atIq0uGEGNhp7bLYI0sSrpog4RiIsiQ53rhGgJJzEOHJY8fVfSeXh48Z0ihe6KzPDdZj.edxvOiWFl6673jFxWln452hpqSc+rlP.1r9FEh5m0ONXND3C+vOj6bm6vt6tKIIILb3PRSSIIIAiwPYYLTe26d2iQiFwG8wY7Be4WfrrLLXHOOGHpzWRRFo86QY4GizqWqPpFWeeVKwAm5.TZFnb7mgZV1MMnWjkYJ.0YHkN2txJJVMfUCHZntF3.pElxdgKQy3WgU3SIjFRh2XG7LFWz7BCHJFTx7PlAxTE0n3clSjmKOrQiAQgfR.kDsNj9VgJLjKF1SD9S+o+T17k9J7j+reMd06cOracCb8rLYRAe+u+2mc2cWVas0ne+9sFd1XL4fAC3niNh6cu6wG8QeDipoawG7Ae.SJmvy8BOKIYoXrPYdAEk4XrNxRRw4D1Ym6w+pu2eBeme8eMFllQ5Vaytimvvm8YYsm8Y4Mu6mvy1uOWavPTl.9xXEo2Zh7psirKC0deQmF5voJZc9v7IC1h7bU24kalu4pd977Y9PD1fEENvtbjYdkpVoj0YCM7ip4ZpwDIuciRESlLYlLkLMMEsL1I6aTLprrjO9i+Xt8suMEEEb8qec1d6saCMn0FIMu26Y73wbvAGv96uO2e2cvjX3l27lLn21QdaUUhOThnPRMuspppnppZZidNDv68WH2e6N.pMeKVfR6KSAToCAGi7wpQdUTgJe9DBE4DJxADL0b8no.ltBqviyn6y+ciVPyqzltlboGSdANuEWtGQUT8AOqpN1u+ovAqSy.q.0dqWhbEynfMXPbBdii2Zmc39VCe4etedtWPILXHdLb+6sG6u+97tu66xlasN8GrAGczQTVVFU1JqGppLd7Ht8seGty6+9fpjjlhpJGd39bvQ6i2D3IexmjaLLJ+axn7VEUCUdt1VWi25m9F7UewWfW7Y+hrVVeDqiJU3o+YdId++3+H93CGySjTF6QrhV6g7pXF7EhYjYrjYDybPQDn1ycZM+Q+zhkc8cQgNbdkqdbAWlGqOzKznOHor9pPB9fiPHPPm5UHAAiXPToUonjjnKmEQv4bLoXBEEE3bNFLX.uwa7F7du26QZZJOyy7LLb3vVkfrVKEEETUU05UqgCGFck93C4VuwafHBewuvlQWpqAxK7TQLjjNmip7JJKKa4wUgVRUUERxEisuc8fUWxyeVIhdntUULOaGDfpIETNNGMuLJzzGWpwVp8Exw+JrBWpPk1ji5X9i0GfJEJqvWTFS8phJjROTEK1lWlCC7pRrb50PDyXn7ACUVK+zO5iw9DOA23q9Rb67BFqBhOPZ+d7m9O6eVTdTs7MiwP+98QDg77bBg.u4a9lb26d2XFV5bXsVxyyQCArYo7d25VDPoWRObpTyIrX+QsHuBQDdlm3o40+QuFekm6EXu8N.e9Ddlm5oI+vC3NUA1YxDNrzSlyRhAJ071PSFj5Z8UcYLJ5sQs0ygmVEv5zl+codt5ynyC2kCVOJT75bOCmwPKud5VGIlwKUcxzsl0sn5TUa8yZNMjWF2Yle+N+1rrPmM+2cQgnr4bp4uEsMWD06n4ONdP+qw6RyT9D5bbmkkwnQiHDBjjjzVYhWe80oWud71u8ay68duGat4l7k+xeYLFC6u+9.zxYJ.Rpypl77bJJJvZsr95qyy97OO6ryN75u9qiwXnWudTTTPud8lISDa7jUi62apeJm18mSCKx0uMOKsnqMy+bnVysAZdtyBhQQHFZPGv9excY+O4SP7ALAsl6JN7gyuE7qvJboBiPcwHblVsUrxWIjXRg7BJKpnrvyt6tOSNXLodKYdK14FeQGJ.zLNurrrcbXj9.I389n216HecQximmRIG6OMV9UvnXRLDphjNuWudb281iae+6wS9y7UYmpBFAjr1ZfX3k+QuBiKxidwuSsdpg.6iFMhevO3GvGem6fpJlZZRzXjZSMdDA9f24c3G9i9gLJeDqu4PBZEESlfIDHUE5gkw2ee9K9S9yXyAav0275ryGuCar404o+ReYd8O3CwacfXorJGB9VRtiQ.yTte1jUygFS7lKpOye8pg+rOn+cRW+Wzu0CSrn66Mq+zvI87kp5L5b7v.W5jGYYSP9nxMjyege9aFOtitJY1b9zj5wMg6avfAs0.KfVuccRJn1fM2bS50qGiGOl6cu6gHwroAhgm7pNrhAQrfN0P.QDRsN5kjh3CrVVeBiy4n6eeRDnpnj8GsOolzK6C+UXEN2Pg4ZT7QlFJAH+vwvvMvac7texmf2EaGVlRksFrAF+bdIYoIRxIaT6xvBUppyelZxP3UEuQQsfZTJBdFWVgr1.j0FRt0wDUnpJvd6sG6t68orrffV0ZH5VaESZmae6aya+1uMiN5HR50izz33beYI444TVVhVmnPjj.hvA2+d7Z+jWmO3C9.50qWalHpUUXBAjp.EGNgQGbHZkBAghfhYisH243f7JBTWm8zn5SSu+DSjIotanHZS4jQOyUw8GVXdGQL+em18uOqiK8l877Wv04V2hpiQWjXQdlZ9iul0uLB.dUFgPnUK8lG3a3A0AGb.GbvAr81ayvgCorrDH5sptjQGVFG6T50uGat4lL9n6y8t28XqgqUagZww5L6WlXYB1iVA2rMQhOJJ3rBYNKi2aOdiezqxnc2mm5q+0Yq9CoWpihK+gNqvJbtQ2QCFEJEos37RvPhj.XP1bc1QK4Uu6GvGUTguJZHFAs0L8t7oZQdau827APIqSZRXKPBNzfO5QGQwXDTiPdnh8mLA2vg3FtN4AAeMIK2892i818dXrQNq13g8QiFwG8QeD2912l7CijYuLOu8bRr1Xw+jH2Wqz.NWBUUkPkmCt+84s7QYtW+ZWizTGSlLAGJIBL9f84f6uK8ehm.mDKmMYauM64RY2QinJYSxLFhE9x.hncJkwRc6.RIV5w8se1TowqvUMbo6AK3QuWqdPvIID3wAEsZTpp40PzCVdumiN5HDQXvfAs70pw6UMt0uAKJLsppjmmyvgCY80WmwiGynQinpJx4p986+n6DcIXYOOEeVyBApKUD1oaaHpjkUU5m33v6eOd9m4Kxu925aylqM.S.FlNfbe9itSjUXEtfQ2ZnTiQFll+W2mXM8bfpj87OC+r+N+lb3VCXxv9za3lTNJOZTBcKc.Gm9Gmkv+8oARMeqr0gMqBOUVEuSI2WwAiGQZ+A3FL.evfSrXPXzdGPQwD52OgPHl7MGd3gbqacKt0stE40YJnKKCSc1O2njUUUEPcKhIDp4kVfjd8QRR3v82iW6m9S3Mu0sHurnklBYooLd7X1e+8igNMMgfUHaysoJMi6ezgDHFxJiDC6o1jkjsjJMRzciV60QNcNX8vFm0PM94U74dyvOKJz038pKRgCWln4bonnfgCG1xShFWkWVV1pfTiRYKC444zuee50qG6u+9TUUQu9YTVFloOUcYhSh2.s7qSDr0VIppBUdzhJd1m5Kv+V+t+t7W8W8Wiu7W3lXKgIiFQxvLzJ+pNkyJ7YCT6HJemvDBRrYEhBO0M4uxe2eOdxuzyvq+SdGBev+Tz8KdneXcZxerRrewUIwd0mXidvpLDXbQNts2FmMVOqrhAJ8jezgDzBRRGR43RJJDFO9HT0yZq0mC7QORYLQEoJTOfhVUhGv6Kv5bf5QKhgxyjT2BgxLLb3Z3MANJeDqu1Z3k.RhghiFygiNf.dTaL2AbCFPHMiCFWPftywDpCcas.F0LeW85XkTlKCbZye9377jWD3JgBVyXgCOdUkXeb.cIoeixUMBt52uez5uppY3pU2PhBKIzo0aeUUU69rgn6UEiZIH+kINomkL0BzhML5tMXZORvGSNppR9s+M9N7zauAU4PJPud8QIzVauVgU3wQzT+qDns9jHXPa7KRMWeL8cr63B15ZaxS+232f2bx+O7texNXSGBhEnZpwKMKkimV+KN4k9zOAr.HdMVI2EgJTBhGqAz.ToArIow9NZohXE7ESXzQGfQUpBE3bQB22uee9ZesuFgPfW8UeU9j67gQiPyyoW+97zO8SyZqsF4iGSRRBIIIbvd6ycd+OfI4SnZRN9fmm94dV95+BeSlLYBGczQTEBTIUXcVx8Ub3jwTTe80qdbhA0kxjpQHhfuxiwDi5fW0XH.WfHrSqBt+nBmlBvmlWr9rtBXW5yPbL2GOmmhdXmBvmFGrfiWj05556q5ng6UcCUXSKrw4bnpNSMopQQqjjjkxShVgj0YoRSIXngaWyGRxqJXQgovffo47oihkVqkLmk28sda9nO7C4o1ZCFev9LXiMnoypaWYHvJ7XOBQN+zDyoVOxFyPNSFjqvgYFNhQbCqk+72+V796uKO8St8wMBqigbmnhUMJfcJhPOQNXoLstzYjZReCdiRvZhYem0D6fEdAmIg7w4nkEjjZoppfzjTr1Xibd80WGU0VRsGu7DXiM1fW5kdIdxm7IYxnQXsVRRRX26ce16t6fOeBJBd.myvvMVC0pTDJvWEnrJmAoCvaUlDJHmJBn3Hv3fGbIDTEECgJOVmhXHxsrtm9s0rrn76fDvpqBC2UYbw2pbpWNeE90zL.tYtr5ZtBclrSm68.OzqGGmVX+t56MsoJwnDutG5pepDSkYBJVDbHTnJRPI0FIgo0IsoNsODUVxXsDBUsVyNy0gtu1GPLBC5kP93Dl3KXTYNdoBahkJqmRr3MJdCTYhaevnSarxmK8TapFLMKCyttZhLDu0NuvnvLo.MAEUETwfIwfKKk6d26xO9G9J70d9WfqciMf.L4vCIaidOF7rwJrBmQT+n7L0VoZYI6r2dr101lbL7QitOu5a8d352OVG6pG+z1hoBwh8Yi7cGSShjls0JRmPQdJGVmvXr3uQrelloPrkIF6Cgp.AST3RnpDsRPRRqMlL1D5GWVPdom050iISlvctycHT4YTMA2aJ4DUEEL5vCYGigO4S9j1R4vgGdHUAOdfzzDpJhINza8lQ9WswFaDutnZSW0tUgo3bMJ9xBL1PMk2D7Zj39FEjPHdOP.g.F.q5wSffDZ4n04QJTyslFwvOnhiupwypSq6kYV5ma5n.6IgGrN2w4RAKAP8w3faqqkHdMfZhVT.wAHVELAEpsv.QpenQidOXdxQVuMyjQJlN25kNVHE7vIkppyeAct2amoY+d7GUmY.d62cpGbV7D2K96uXOes.EX579SyKYhHfIzVWTJI.Z.esWqLAkA1TBhEqWwTEPqJwfxZ8GvjIivZErVgPPqIwYnMzeMd4R6TLSUcp2fFLX.DDxGkS+9YX56XrNgdCbTHELJohRAJcd7t.kYJZhRUBPPIDNmzXRiEwGUKapoy.wJUOpqNiaTLRJNaFURB9p.deIpDqmLpQw3ErZBFWOxKiESzzgCwczQ7m8m9Gy+t+d+sYTdA8bIzaigr696vlar4kNGHVgU3bAUQwizYhRSSKipVD7SbsMYmC2ksFtEu8mrC+f+reLW+FWCU7nZzq2U15F6tJn9.UNEqyP0j7XuNzZnrvSRpk7wSne1538Jh8r0ICWVjFr8rjezQrtDMlRqDzAo7gpmRimQSNfmrmE+tiHjagTGkVKE4FxK7rVVJSlTf0lvZ8GxjiFQlKCTCQpWEWVUT1VPR8phIOmCFOh7TvWYXb0DPTRcYzSRnWuLz7XisOKsO4SJQCBaLnOhuBakm06kQnJGcxQDBUjze.8JKfxivoVRsInMYOnIfwGvP.u5Qk.dwfEsNLszNu3CRVE1LOcS62QzYC+XCscpeTYgI6zCJlYNwyv1urHFIM6qZGHnZTeh1PSGlR+CiXPMFntymL0QNmU0S+zEMlyegFctcRiF5A.LR6mUWK3lAWkis7YGmdLlWTAf6hNiIi9tI9aDDPMcTXsSMUwXLXbBlDGIYN5s1.lTVv9GcHUZfjdY3xRQbVbYoQOhM2epQ.arPdVlWvjilfO2yfzL50qW7GpzWO3O9fYLEim9PpTSp1kaQwYAsOcUubYhVLs+M+f0t0QlnJ7Rzc+R7uxxR9I+jWmW60dMxxRoHThm.YY8oxWwEvPnUXEtzgJg5wB0iSDPEkxPLDbWassHAG+q9C+S4f8FEoGfcZH0CDkKXp8PUS1uEG2W6QoF9NVO777RwBUfJQidpRLPHJtw3UrhPuzL7E4TdzHRDvXgjrT5u1.ppBHpCqygJ1VYzgP.eclBJQpjgULXLNxxxX80Wms1ZK1XqMY8s1jI4E0d5JRXMS8j5t5dVpuLfQMnZr15MHa.8bI3B.EELf.lhILneRr+vVof5H3EBk916MsdmODvFld920qKmm4SZm29J17uyGZ4S5b7z7d0I7MOiGMcUx5r8r6CMNX8HM7IeNNNzF.BcpcMLK2F70tJOX.uDnzpnVazxw.XljhVGSQwkPkB4iFiHBYYY0dNjV9IETEs1ZABJ8LNLDHMIgMWaab85w3Q2GIWIMyRZQL0n6WHLnzxfBKA0PUPp6wVObu9nZbBhOsY.5fAC3V25V7G+G+Gy27a7yA.UgJ5k0i7hw3976idqvmCPbrhfQfOYm6weveve.UUUzefaF5bzjV9Mkkfoe2yx9e4a2oMORk3QR.UqCUYvPPAq3XqACwMZBE6cHYC2jQEUHYYr45ag48dOrhCq3nRyoRI1r2MVzZkLstT7Annpj6cu6QdQAiGOFShCWZB6c3d00ALGVikPnDCVRrowtXQYSR9X.Ozq2.FNbCLXITo.ArEdL4kbiabSz5Bepqt+C40XOk0Ter0cBdosvit3qSONVyFWFlOJPsdnBtxGAgKLEr5lwHe9Am9rqy64ptKie946HPC0M.zZKCM06SiFIAIFEuIPg3oznTEznPIazpud8FfpBUEU38JNiCmKkPUfhhJRrIfIZEmZTTuhGesUoAJqTRcozKYMbIYHACRvhMXHwaIwaHk5WWJ3pjXAHUD7h9Hc.xBCyvo735latIu669N789deO989672ls1XcJJxIsmaUVDtBelGsIFSP46+8+975u9qyZqEKlvSFON5ml5PSMeaGYQSt+fTE2OMDZ32j0PIBNfJmPPCjPB2n+5jr6H7GbHYquM6lOFinr4laxfjLNb7n5P9EhUPcWj6oZmBjbPCr+9GRYw6hJAlLIGrBtjLpxGQR+9n9pn71P7bO0lBVCTTQurLJ8kTUFXqM1lgCVCegGsxSpyQ982kzpRdxs2FiFav7h0fZBXsN7LMxAsId.fknApFQpqk8G+d1mEvxTtBdnm+aWH3Bw964Ub3wKksLmi+t7Qi2YZcmcf1NttHwZBSy.zRshRpnTqvW2hH1d8MYydCw4MTcTN1Jgs5uNa1acrU.4ALkJIAKIACopiT0RFIjYRH2Gf98HoWOJBAFmW.pgTWJNSRcGf2fIXp6H7D83lFaH0OJvBEneFEx2zKEekW4U3UdkWos1dMIeBVyphf0J7YazjwwiFMhu6286x3wiIMMss4HuHpOzEOrJxny7aXDJLJSLBENCkJ3Bv0SGP+IkD1cWxTOt5FP+5quNat9VDJiMz9FBlGav8AB9nGjljmSZVObIoTTUQddIMEfU0qf3vOofpIkTNI9Ygx.UAEentJrmjxf7OZQ...H.jDQAQ03hB7nr0VaEatzSxwYsz23Xu2+8YcDtVuLbzH7NVVcLItHGQwT6cMpiViASPvFNthUKxSVmzeqvCWbt0RXY2iVcyKhE8v7CRqh3DgZHzxoHoUIKCg1ksaJgXZLiGQ8ngJHTA9.aObKdxabSFjzmi1+.1em8nbRNYlTbhAavfV4wOoh7Qio3nbJmjSYomMuwVrwMtFoquFp0PEBdifhgJuGuJngX2tWklPVF4NQ0C63Cxh8Z0Cx08wiGyvgCYmc1gW9keYDQXPuAsS7rBqvmkQyy42+92mW4UdkVdJMYxjVO3tnJ19YAmW4ewLsK5UiJfbiRnNl8Vuxl1D1RRnbm8PGMlg8RQDkjDKW+5WmzzznhL0gzLurf7pRrINvXPbNJxyoprDeUUjJEh.dEeQA30nQjFCItLbVGh0QdYAEkdBFKSJJnzWwfAqw0u90i0dupRFjjgNdBS93Ogmdv5zSASvC0YHXoublqSAzXdDJ0JXow+ZnGyxTr5wcL+yTKSg8qp3BODgqviVDjtIFP7El5Ti1f.9.nMs9EvUmdPl.fGlTNF2ZNFt4PDmvN6rSr.4kW0VOXzPM4O89VO5j0OijA8Xsm3ZD.JNxi2A3L3KgIUdD7TYgREJsAxMJSrJAWfRWz1LBzRZyGlXQCHOKgHDf0VaMNX283Ue0Wk81aO1ZyMHKK6gzQ5JrBWcPixGu8a+17du260N9utDu2hlZs2oMA3hyFvOcbvpgL4ZHl8zhBI1DTO3JCLzX44u103St6c4nO4tzeqWfcJxonxvS7T2j6u2t7tezcv4h7PsrrDmywS7zOEau813bNRrNLFSSEa.myE6CgUUHhRnHxaprz9TU4w5RIXrT5CXxRXTwXRRS4lO4Sv0t10HjmShB8rF148+.5kWxW5YeVrkEXzJLFv3LTU5wTmTPAwPPhESUslOVFQiLsXABv9r1bwmTsp7p9Y5ENIR9r1M2yKN8xzvmdznXkWLsOoEUtxfUiDgrs3YVqvkCI5d45BiWkQYm8uO8SyXqqeMt4SdSt+96wGemOj6u+dQBVZj1k86mwFauE27ZWmAauAev8+X1euCo3fRFLX.Y8xPqp4okyhWB3ko0.qJSffUhjsWCX0q1caFq0h0Fyxn24cdGtyctCas4F3rtXMCStJezuBqv4CIIITTVwa8VuE6u+938dFNbH4Ew9MpA6I5AqyhRVepgDhJgnwViCRc+DUErdOtfxKdsaxe469tLdm6xvW3KR0jQTEBbyseRdlm4Y3Vu+sw5qmFLnzu+.1d6soWRJCFLfPUrHLWVFMtLIIo8bHKKgx7InZ76MZzH1c+CYbYAEEkjj3HeTIauw57D23Fr0v0X+COjDUI06492917LFy++r2aZSRR10Y58btWeIhHWq8deG8B5EznQC1.fv3.PxYDEALRpglIIS5Ch5CR5K5WgLQZx3O.Yl9IHyjYxjogRFGMhiMbHoH4vMPBvEr1.8dUc00RlYr3teuG8gq6d3QjwVVYjUlUV9q0YmU5g6db8k68dtmy648vSrytXyxQTO1HASjEMSmPPCUL3pjjmxrGrYVYedEmTgU99EVKFXU413JHhfWCJDdjcxLcnoNc.LQFmToj6MIIY8mOiuS.jiYTNqx9jl7F6n7f7nXO4IA2z7FAwZI24vHFrpk77LR6DSTYXC2r2F3R5PQVAZgCqUH1InpCer.oFFo4bi6dS5k1gtasAux0dUh6jxd29Nj4JP7JwcRoaRJ4dG6em6x68g+TFNrOIXoaZLZQAC2Om3HnW2N.dR6j.4JwcR.KD0wxMO31z8RcP0ROrcLdF1TmVZhv83f9mXrVb4SVNfv4BE35RtnMuUcKhfqbe+3O9i45W+57Ju7KwvQCoSZxY9rXoEs33f77bRSh48e+2mACFvVasE24N2InugQQ3yzxLGzUOOPyJ4fuTS8r1photVmwg9iYoJVTHQE7dHwXwiAWtmHuAKFJFkwVcS34t1U3u4lWmO889I7nOySyGt+.9f2+c4RW5p7luwqw+zO7Gvm8YeFW8RWltIIr2ctCe5vgjllhy4KGyvVJZyi+9U0gqrXQqdnv6vDkfMMEm.ezMtNatYOdsW+UY6jTt0m7IrargsDK28i9.t068S4a8EdM76sOQoIjDEpPDCyGFJ2XhfEanFJZCYhnVWaizxDY5v2WVIsS7dXeuWvRMvdp4da940ywOiO+nLGcs8IU+ToCVx346UUQLgDFnRmrlk2AOpnMMnNl3j9EzEAeIelJ4MdPP+HHdpEkh3YTTB25V2g9GzmjjNjqdJxGQRjgDwPt2iSBqNy6x4f7LxyFxv82innH1c2cwVTJlfCGvs1eu55r0vg8YqtcHRE5lzASRB8kBxx6iKKCu2yfACPLoTTjQ9ngrUmdnQvAY2IX.s16D69y5BhH07NY3vg0aqEs37NpRpiQiFUWaQsVKNuKnJ5mhI6SkFaY0f3nJXvnR4OfEGQ4Y7pO8iy24u7Oka8t+Hd1G6ZrkQIoWG7EC4odrGE7N9wXXu6tGt7B5swVXsV1au8XiM1DUUbtpIo0IxXRi0hy6QjPkePLQLJOGm5oSmDdgm+YYmM2fK0sCW+G9CHcmcHQU9G+a9q4kejqwk6zgMcNRLlvBdKo0dkCKDwVmLP9RaqdPRqHW13jm2WeZqAVGSbZZfUMLRs6jqWgkZv4g6dPerIojr01Xh6fKaDDEiMJEmOmtXINT7EBdxw4QyFxHFRlHb2O5F0ClzzKOIFCci6hM2CEE3zg3GUPt0g2kgXgtQor6FayGcm8vj6HVEFt+ADaU1HNhbM+LeLzq7hp264fCNfCNHTFMNqUhHZQKNIfwXv4UFLXPsAVg28CFXEcFIapiPBCAVZzkXJ0YJui3bgu5K843e6O76y0+G9d7ju4ayO9iuNYRBlNc4QtzEw3U9w+3eL25N2lhhB1byM4hWXGxxcii7RCOmHRotT4IDsfjNLZTF27y9LxcE7XO4Svy+7OKOxkt.ItBt469S3o1cGtVbL+i+Y+oz8f6xK+huHWJIktYiPTGNI3EPUpJD8UjYe70oKH7gnh+LmnfNKrTCrd.LreGEzZf0C3ntjQTJHcF.uH3wyHmmdc5RuKdY1y4356uO6r8ln4Yb68NfXqgNIwXwhJAxPU4N0pAShihpMvphn6HAxdFEG3AfIwfVXYjJHQQj1qCohGu0vs62GNXH13Nr4law6+ytAIcsz6hcXf2eleILMC4QddN44gr6IDpa8LuAhsnEGGDhltOTvjaLtP3yjS09u9xrR1TKmdkKDTTBI7mGi5P2uOu8m643iu0M4u3u6umKegqwU1dWtiyS+QCIoaGd5G8QY6dc4G9S9w79evGwMNXO1XqcHJIFAaYHNGSmk.o9gtc5vc161z+52DaRLW3h6vktzk3pW8pbwc1BW+8YzfCniKmGsaGt0O36S+e1Oku3y9b7bauCQNGh5woAkZ2VVjpsRDlxLvtRiCUwWuB5pZQX3OOaXjaKNLNGXf0w7kKEBNdsZpxi1fFm1dxH3lbAOZC8uxhS8jodtxktHa+HWkOYu6v28m8t7EdyWm3nH9r6bWdxqbYFt+9DQDFaoHABXZ3opzzz5ATLphoz00ppTndJ7YzsaWLRJtbGtTPiga0eO9rabK9nezOgNuyOOac4qRlyRmNaPjUHuen1AZrlyz1XUYf0zxsQkAm1VKrZw4XT4fgJdSATG9p33X7Ymd8dUAblPx5fODjPJSFGeYokwJddjc1ja7duGekm7o3Ve5c36+G9umuxu1uAoQo7w8Gvd2cOrQwbkKrK6r8avUtxU3C9vOj61e.CG1GUEbgSLhuA+eTkQlvXl6t4Vr4Nayi9XOFW4JWInyn6sOzeetbRDuzS9D7S9y9y4y9d+87kdpmlO2N6vUiiwr2938N71RN.YjfRL3zPldSYoFhFEJZSPxcpv8A0t4dFm28P0xv4.Crd3EFkPgG0Vl8fdAodUbvHWAiLBekewuIe1e++H27O6+.+I++8Gicmsv8Y2g285eLY62OTlcZX.QyPAV4wFHv8hnROZEJaNELJuePM3cAA0iNVnWDLZ.b28gW344e4+U+Vz4JWkexG+Ir4l6PjU3ytyGxF6zihy3xIUSCphhhlP6eLlVqqZw4eTw4nlFXophwZwyoWGXEHqLIdiQv6pR5IOpUPUOoQFxxyPtys4I14h7MdoWg+2+y+Ove++9+X19EdA18QeL7YV1u+AbqrQrw1ayK7rOCW6ZWiad6awmbiaRVQNCJK1yTzXr.qkHTt1UtJW7RWoLoY7nYiviRhufqbgKxEhfe7ey2gO76883yu6E30erqwkjHr6cWL9.EDDKn1x5Iagh3J.uPhILdiGfRN25a3O.6Yb6WVpAVmy4xZqAVGSbZyAKiBQZfP6RoN3IXwQn7V7Y6uOu3W3MXi+q+s3e2Et.+C+U+Y35lBWbG1ezH3RaUcgL6ufzzvm4CCklUkAFdO3KHpyNLpvgK2BZGHUfTKwo6RZ2t7q7a9eAegeweQ99GLfaNJmjtILJeHlN8.SL5YbA6rh+UUFTMQFtJ1y7g3rEs33h5rrZpL7tYlieZ.u.ElfmrLdBZHEkZEk3QjfTvLbuaySs6EX3s2iGq6F7K8FuE+e+89t79dkGciMYqKbA50IgabqOiO85WmNasAwIor4FcYmK9BjWTvnBWYXRCiIXMwjXrrUZJTjSdgGU8zINFqwfVLBM2QbRNe5G7w7C9q9K4Mu503q+xuD6LJmKaLL3f8v1oKp0TVtBc3UOhySDFhDy3veVOMRYQ4V7kdt5rsLwzRx8iITopDTF9+UuPX.J0QRrU6Wy60pYlt1TTN6qdXMvosAVUmcqH3EOdiudfOUUt4mcWXuC3Ye0Wm+6+RuM+fev2GEGwdKNsfL.mwi5ETbAkAV7HXQLJtBEwnHXAwW+4FIBqArtLxFlSjjRTuMXOcDGLrO6lDyUtxUXqq83big47Sd+OhK+nOEJJ60+.t3k1h8O3NjXOaaies257dL1JIC4ACUDtEs33BQnl6kMqEbdumBUwTNA+rHJgYAcQVOg0ZbAP1Kk0gUIjEdUYZ2v7L1dqcQLIju+dj0eHu4y7br4Eu.+u8m7Gye4+leedzW4k3odtmmGams3tCGvA4YLpHiLDhTE0HjFEQRRD1JCZ7JhpjMXHT3vfRr0hdv93yyX2dob0c2hu+e4eAev266va+LOKuyy8brg5X6DKtA8YiNoLnw3HF0f5c.AOFFahwWE.AwSk4HOHw6pyRjbuooEU1h3mhQPyJwADlxPvivT5GqY2TBy25Keg1XBO784NPLXKKaANozyJxXOBZpOANDwDJZklxz.olKQi0jJS8UU4jb0gtorsLC8KZkP42YkZJMqm2yNKRBaynG2WzW1wuLWrBEEAsjRrBC84nFHUrnhP2dW..9nbga58XdxWLXvj2Pj3QTILnjJDFrJLnkHVBjtLFvWpjwi+biIBvi58jnAul4EXaCrM.dG6C79ezsIWrbsKbQHqOE5P1rWLYCGDJJpKAKVnVWt03yRbWqz4DuOLX7z5qRklnXsVbNWsF93bUoltbpy8tVzh6GP0w7uppRNXLlRsexiQB7+DFW5ZTmGswB8hMg9QQQQTTzmnx9UhTooSy2KLi61NcofAr3gbKdiGSYJDphAgfFFIXQkTx8PdQN1NcYaQH+S9.tlX3+xet2j+su+Oku2O4eju26984od4Whq9LOO6kHby9CAqkQYCBk+qxwErphULXECQhEw1C06IB3BoV1s6Fn6Uvm8i9m3e589Y3t8mx29U9b7FOySyl.1ACwoBEoBixyINpafT99PaNhnxZ5rPtSCdqBCHRnzmoDLxSCNsPUYYyPLi6oGciZlUo3I.ygFi8nnojhznbtMi4vm9bTsMeodVoZHQipdWRK2d81LR3djmftWYK4Qm2iiP8hTLFHxVtZhRdUWpYaKRKrpbdzhvZx8AMtAT96pUuTuhmv0Zo5hapqadMaph9fkFeb1.SNQu2ni2pO7xetHTHkJViyW5Qlh.Q0KJVhvtM7Phv4rpuhypHuBPTTGfxWDUchNTqRGvY84S7coiWE68Jl95dQe2snEsXRrPOUMi8s41BKv6dselAq5Q7Mkx.eiuWSHKHQQLg9yQ9fQRI9Bh7F91etWlGOMku2O4c4N+0eG9Y+zOfNW8Zboq8HDegMYj0vHTJbJE9b7ENP8HkhVYO0wV85QJNxu0M4ie+2iQe7GPzA2kK5x3W5c9JbwtwrQVF9gCw5g3NoDUVcHpluab6ub77RdzVMQOMMhsbeJiSw49vrsNP0RwUYRaL7Mb5CrX6OZtb9U8d9Id7YlWQmbrZo1hSRz7deEWhZV2vpxRPX1FXUUrWmmGBa5ImoUh2EcbmUPqQTsnEmevz1pUO+So.gJkh3owXXCuf889T9mu0iwO+a7X78+3Olu2G7A7wu++.eVmeBEatAcejq.8RIZydzsaGjj.w9cNGlLGw24Vz+16ymbiqyfadc5nE7rW5h7pe9Wkm+ZWgNdGw44HYNDukXaDIZJ9LOtBCR7hmTeQio1Nx0Yebeg.LSajUs2PzGvHb0C3nNrlFCS610l7qnZavjkxnoOWy6ua5Yp0YoV3jnTCMuuyvqmsCg0hVbVEqRDOpGyvTMF2XdUZcJc.hyJXCikzKdIdxsu.eZdFu2suE+zacK9ru+Ol7DCihiXPTPtFx0bbtbLYN11YomZ3o51gG+4eFdxqbItT2tDkMB+0uAEdEq0vFocHINTdvJFFJYWIwojuhYg4zd6upDgcVaAqsXRb1lgws3XiJtNLqv5ArzPDtrvmMKixZNPPUo13rJlWMH7nVuqZQKZwoCVTHJqP83TVA06C4qSjhc2T52+.5OLGSbGt318XW6F7nWXK97O50PwP+rb1u+AbvA6EpmoVkjNVh10xt6tMFCzwZHwXHEGw82iDUwXERShBksGU.GjW3nnvQbRGhS6Pd1.NtTbnEmcw8ECrlNbfyax9Vr9QktMUgoMZn4mOKilVlAVKKDgSmJ2GUiVla3kq97izY6vXQFRMsW8ZQKZwCdnRw4a5odojeS6mO.aGgs2ZKTwP+9Gv96cSr1HtV2MwnFbwQ31bSzt8vHJFCjDEgFozWyvFaHxZv5AJxQ0BRLwDarXhhvW3I2kGHdeRL1zX7ZDCFkwpliTyl.3sgI7rNNwMvZdbsZrAVsuhbRBaEYJmCYwq3hELaOXMtniNoF3TsuMMfZVFXMc3HOqwIqyZsmVzhVr5XU3GeU3zfoRSeA19h6v9GbWt6f6PRTJasQO1o2l3KTbNGEixH1ZvlDi0.p5HOeD44GPVdAatcOx8NbiFhpfULXhBDTOCGtBGpXvGIPjPTrE0ZoHGFkkQWq8vx..MIdsNYCmFiOSKMFNqi6KFX0Nk0oGZFBvJzL8Wm9e2be.pqCgyx.qoyrvlG6bkGgo11xLnYwxzv5Eqhm6ZQKZw4Cn.2Y+8Bbjp2VfpTjkC9bvEnWQZmTbNGYtb7EEnhhI1hoaB8LcX3vgDGGSZ5l.gZ1ny6AaPlWvCQIo3.FLxwAYCHJNAaRBwQInEtEZjX63POXiisAVd+3IrcNGzviFAsOItdR5oQXh3RApXNXduXUOI6wj3yyxXfYQl5EE9rE19Vgu+iymuJBc5hNGyxCTyiWUyy.moOtJi0lmQaGEzrXKeuHiBUZ3Sco9vZq04DUCkohpBYKLlyZwQQ059yr9dBqJtcwCs37MDIzGLpr+PbbbnFbZsTLk2sm0hvLD5eUM+PZRBiFLjXiF3hTb75ocNms6ZHuA.MJ8rAkxJVhQTOnUkZrxcvB1HKY3wGI3hM3MI3LJEDzmJQU1XicP7JYkIFClDDqfW.eYMQLqn.LVjDCwkZZk2mi.DanVKEm03qKSu8tejvOqiie9IE0pO+1zQho98sF22lNhYppkZrooThn70BaQUDzpeW0ZBEX65Dv63i0hGrltC18qr85r.lGOclU3xNKhkxwoyAOCWzyfldnah8uTNKNOb82hVbZg4U0CNKvAWu.TIXxZfSSVEDQophUnUU1BwhHgRHsHRsAZPTX5Z0CZPbl0xItEUvWYxlNVbsUy3pdRo1gNANsuuzh0GN1FXEdWab5+2zHqy1lVr9vplQcs3zCG54vLVIzrjRj1A6ZQKVLlEkAp19z6SEVWFXsryP83vyXGUwRVrPAlfgUJX8PjGLdAQEhv.pKXEjWnS44wErTBwXviAvU1V7fOLunJghP8jVQ4w1ndr3k1JBw4Yr13fUSirzGhxRvlFW0jSSUa6rNVFGmdP3Z3nfCqkLyYf+GRd+sEs33fUgisKiRAmpPMAUdmfGlDgZwT2ngxphPDRoWtL9fmurXvqJdMTgRFapWvKVd.oLDoPkGrJCoZ08iRs4pxdqEQMkV7fIVqjbuU9EZwYMrLR1WwypoyVRYN7FrEsnEKFSygkpEe1T.eWmySzTGrlUw5cQGWmhfmlpHZdcn7JgZD7Zk4WgKKWcQSI3cJgwTInliWyxvHMTQcEszmWkVmsHSnZMu5AabhlEgOLX887BKXSxyeVFyieDmmvLkohFxLwzD8u4JIeP3YXKZwoIVTHBqoM9TTlXc0uZUDYzEcrhWGS.dyjgSzOkGlBPBFIF9WgZgXIuqpB2mRI2yDH3QKKV0iSUrdPKcYlxX8rpIZGy47CN1FXUshjI2VXELOr7hxxHJdKN6fCMAvLRNipOq8YXKZwhwxBQnQNrmfaZj05dwbUFKMcX2lED.iqTlDLRfpTlPF.pDLzxIZI8qNbICyndPyQvGNdwFxdvZubYpMvz5KKOOkMGiWwSU3Eaw4UbrXXm.kwnN.UfbIX4tCEwe3InbG5azLYynRZaUCqrL2dJhEIiDOHLA8rV44CLg5cBlqZX0R42Iemp55z1HaeTM7t6XOaM82iow+tEsnERCCaTor2nFFuWMR3yK8FjJkoO+ZX3QuL1Xpo+8pfJCo7ksMeYP4zxyaylX8m073EJMtRpu1lURdoRkAaBJR32BfXm6h6dPX9iyrPGOFsVNNsWndNCuDBwqQmzKnA9wUsgIqBIAp14q+2qxn+GKOXI.IHDoBNAFYB5ChJdRLVrVKFuBdM7xY4KYXEhDAClxWD0wxmkHHXJUXHAiLYsr6PqFZpRwxQ9ZXIjR+dkv2KSyllNyCm2mOKcPYZcRYdG6rNuKB2KjbeVsuiBIVWlNbMM2npzyJQjv.atwcGFqMUAiyEwfpF7NvHA8uJ24PDKDkfy4vFYPJyWZUc3UGpFRKaKgkiJ9wZqhojiFJVLXq0TkVzhyqvXLjmmi0ZIKKid85QVtarQDFInf4hfJJ4pmHuhnPl2gyJ3yKnSbBi7Cv1IAxCUYhhF5RzrvR0AvpIOoLQ+XJCfVP2SUA0DngtNgANPXVHp00QCJpH06qA.IJnsR3Q8gwcpNKRoAa1IlFVp9uR1zWQAAY7mW8QqIrJ5j3h+74cdq+Wy8bsHOTNtcs5sulb4q9bzvCpgjSxfHJpDt+aLFTuGOA6MTQQLUzjyg3AqwfQLLPK86nAPb3wgkJ6O7Tavk5ql5Yo3XGhPiarkbtpUmD71ZsmATBMlpOq4DhBRo0lk+VfwDJrcxqkgi68ny5dopoQWGhSTkCDFd+o70cUFOd0gOaydq5jqnd92QZ8XUKZQEZZZPMJ87SEYwqFyGBaySnu15QdQazVjI+8pdLUyMMqgACWCM9fZhv2zkGkQZY5PkpZolZs5smVbuCojSaFEz5WBLnhqzTYGJFDBktnvahg+1ni8U0jdUUa7BdSuZUlYnqP6ZMvAqC6JyCwqkobYZyziuZ6qZpo1pMQKFOnZT5zO+WlWvzRllJqzq42an8csVzhiGV0nCzhSFzN9U.g6CGN4yTLSER4o733L+2qNVaYQXkG.7MLnxzv.qYs+ySF8md+ldeBFn09xyzXVgJb4t.dU3szIGVlKjW3.yqgl17t9ZG7uEsX8f42G9r+32OroSfm2v7d9LuLDeLgUlUzJN5QvXsDyCQoljv0aqYrS0I22ZtrnymL3qRVc0NIX.yJ8nOubuYdjsecdM1bfyyS26ZQKNswpr34VzhSRLsWTmoftByNo5Zx485+1frhwh93yAq4PR7oIh1zqVoImZTNbXgVjmMZW0vXbbM57zdPtkQx+Eebqu2CNsuOzhVbdEKKgeNKiGFzIvGFv7BQHTY+jTyIO+QXpkksaq0ZQXEpyzJsrBiqgLIrhHZhDJTlhDTIWkwdOnZ6SOw6gLdq8k7CgGjF3ZZz7E+6mWGs78qEs3jCSr36l4mxCHgH7AcbZSAjypn14NUIk2LtM4gIT4+6ErVMvRXbFYUokPKpOT3hzOgwRGJsLY7jtyiHzsX1XUtGcbSi2SZLOtXU6AqSfwGF600VzhVbRgGDVn7r3a0o8XhsX0wBCQcogUU0KxfNYASybpIiF3QiUUG+PDZB5YxvgComwvnhBRRM3btflSHFrhAr1f3p4B5MDlnfdTXB08MqMnSQUdyp9hiYOAKDzHonknCTVq8XoSTqqNSy1.gv0vzcbqytRUm4meTZ6yy.kYk4mK5umGV1.PMa+y5Xm1f5oO9ERB8pzltNoJLgxeAScubEZ+yxioU5tk26IJJhgCGRTTDNmh0Zm64rEs3AILY+SYhBftWC8gmlahyijvSavzr30Xy9qNNbFCOMmHWeWaq9mW88NKc9qIZpSeMQ0wsNa+2KjreYe+SeOtRiAm96ZdyObRe8M8eOA0hlZr8lKLNref24BxpwL1WUULRDEZANmCm2hMNlzNc.orVQVePMjBnivB6OVFXUU8wiLVPmT.vjFkVfw+b3Kv0AVZ3DawIFVjwpSav37NtypnckpsnEqV+fwY16Y+90GEbZON0IMGvNsGi9zliaUzUxXhBKrvDVTMpAwXlxPpJ8yZ0wZIDgIII.MdHMCqEEuhwJqj5mdz99OrWZVFuZl93ON7YUOo....B.IQTPT4ztC3IMtWu9pWgvRVA3w89WyjjHrx6lm2U6bed+YXKZw5Fs8YNefYR56FNE4j1XmYEgll+1eLqTKKCgRajASjEMOGQj5nSLVE2u2wZw.qzzz5vYYnzsolPiaZBhUWpSViDbbddvZYYlVKOtN9XZCnVTH8tWbq7B220vX7mFDquEs3AErLYVP3zI4TtegS6qokQAi0Q66zLi8OskwCu2WaSRt2E7ZUbL3CtBpR218vgL25DuVDBgPT1oSGTUw4bXDAm2iXO7KChD3akwr55HwxvrdHrJFV0h0Ol0.sKxvpypF3VmzFmAaasnE2uwzY0cS5cLKNNN1axsiy1hEilKPul2eyn9zdRgB0iQAq.EEEXswPmNk0fxllP0jCVTqHBKyOQGeRtyXOXoN+gbqWEwyNjk355KUcWEirl21ueQx8yqv4lePeqBQ3YYNXsN4BXKZwCSnxCVU+6x+woXKZ8iSaYNXVe+q6uy4ENv6Gy8M2jkf6OK.W8gBTd02WRmTnWOPkYDfDgYJFoK.qEOXMMGrT+3ll26CLx2Ah2fwngTSQUbdGR7wqILqIHmWVVbTLDaVG+799OOiUIKTpv8RGzyBbvpIlUVM1hV7vJNJcANuOV34QbZSx7Ywwp6mxgQ36PAQPwPZZoAVKh6vpYk8KzZoVDFarX7NLdEimf9WQPWIbd.bnNAoPQLJhn37dTsLtlxrhv4pgEkEaGEiCZw8FlUmgYISCyJTgqxJTNZbvZUyxC+R2uViqZQKVMburv0GTvo8BrOtxzvxvgB26TiieRe8cT4XVkTUUKYUU6W4g3EvTG+NeIGvaLVuZp8xpGPL4.f0GAXwm1E1XCvJ3wChAwClplTUaSVMarVKgH7JWba1LJl3rB786iIpCQwozeTF8hSwZEhMwHpEiSA0GrPLtZBQIXjUUauTCKDgZWPWyC.MDBROUYo1783v7dAo4CxSaivpBgVURBT0tpzfolu7O8KiGEct5ndcNqy6ptZmk0oYZCtVELSC3To70Ge4pJJ6HoFfxTsUr.U2iK0fMQAQwXAQmTuwpZaM09GQDxyyIJJh77brVAWQAQQqsZkdKZwoBlt+cU3RpWLDgEIU0+n43UFiAbGNDSUQsnoVwU4gYwLUU5XICArtxx6k4c8UY7uiR6acMuwxFy83Z.3xlio4y04wCuEgk09LlnIZmB9InTzX9vF9aipAsSSUv.herhrGdWRA7f5Q7ND0i50FTUBTM3sJviM0fOu.c+BJFAxtWBtxUXfKCGdLpIXjlBfIj0gDjugpqrEQm70RwdtWmT1HMAMOGiSQ8dxKS4wP88IX0nUAiSBE44pOqdxLGdewDO7NOuxnJrnqmyaWqqcLyZXvJ9JsrbuX0hVzhVzhSeXzvOppXzwayKMJyMM3R03w18f3qOFes9bFr+nvMDvSDVDSB9d8ftcPiim3bJJnhAmXvOwTLKdN50BGr1dqsXiM5wd8GQWBgHLOKCaZm4xQJszZRuOrplpzk7PMXYZYW37KGYNuc8rNvo08jViaaQKNZ3zfjzmz3r93.m0aeqKL2DkZIuhMq6OMq6fE4dRihCUYFaDc1ZKHNFq0RdyyyzeOqXAJ7X4AKo7DrUudr0FavfAGfHTVNQZjRi3Q7G1yT2KtXbUcc65BS6Qsk4gs60y+pt8Vrdu2rH4Xn89eKZwhwgjzj6iDTtEm+wr3X8ziKW4YqU4bLA7gnsopRlWwIP2s1DrVrR7g28lmSYxeOObrCQnAnamD1ZidjObDFEhsFLpGbEf5l4MlCQdsFcTqh0+xDwxyacha0doiFZM.pEs3rGZGCqEqSrHpBU43F.vWVwXTpUpfl6+rbLhAKpGFU3vEao2t6BlP4yw1v8XMMjxW92qhTdtV3fUm3D1c6c.0gqHCqHXMBdWSiqb.AtVMlvwydBxYQhuihWudPByxib2KJd94UbRK5my57258vVzhiNNOFhvVb1.KxIMBTywpYsOyJZdg8wfQr3Tnu2Cc5vVW5hfDRRJC1ffnKfmI0EqJirVFN1FXI.wQBW4B6RGaLECGP9ngXPPKxwTxleTsV9Fl9Bc5r1ZYDb+9cXBawYOb+vHnVirZQKVLlWHBOufSZJhbdu8sNwrLxp4aaRIkrlXaL6ww8kIXmwDgWEFYfnc1hzqbEvXwieAFGERPJO9S1PDZHTlDh.1dyMINxP9ngTjOBCy1CUSyAqpT.9r5KH2OeAtMDgsnEs37.ZGGqEqCrr4XqypvobdiYElZVkPAc1oVJrVXydvt6.FC07mepySo..sxRX8wNKB89BRLQ7XW6pzMNh8yxnmMB04w4b3JxQPQslfgTR3GgfFbHhNI+qLlZNXMsNUE96pu2.kyhLG8NxGECillGX2qXU3O1r1m40VWlW7VWgYbcYD4Q47rLs2p49nkYgp0FzAKsvg5ULpVWWLiShofv6L0ZyiygHxDIigy4HOOOjQqhoV6epVDP06j2KWSsnEmEwxdG1ETJZTUonnfhhh5iw4bnNEDEGNJJJ.239IduekDiwpLDa5w5qNGKBmT5TUywOmkNPU86k84UyeL822pN1w8502phYU2+lEcUlV2EqNtkM+37lGZ9T9Y1by1HisGnoSYrFC3B+aSI+qzxyqTt8lNtP8UGePpFDigQtLxhiwt8Vvi8H.JVwRFMJETy3ZaUdBd7sdvGjrqKtyNr61aQ9nQjmMBeQVH1m9FuLV4UKox6VGtyyz23m1CWSSB9GlbQZKVNNNF.099RKZwj3nXfRa+mG7vh3f5zKheVa69INjCHHPxc7Zcn.q3b0hdWbhHngAuX3.0SuqdIHMADEECQHGV0nzUVgF.NtxzfBFBqR4JW9hbsqbUxFMhhQYgUyTZHk57ACs7y2.noCcXysMqaVMsn9gYCrdX+5uBM63eb7d2Ca22ZQKNJX59GKpuVa+nG7wrTu8JGcrthtypf46wqwXobuxOaaLxUXfnbwm9IgznR8uxL13nFmlw77Z0Do5iYHB8DGYXPQF6jlvib0q.tuKE4iv5cDIoA22YKKEIpAEGnlIr3r4EPS2EuJZTzIskzG2AINsaeOLvEhv8f4jElqXo3XVFV8vv8tVzhUAKhRCgnSLaOdzhy1XVd7eVFPO81q9r6GOiWlCWp7pjV9uUUqG1u9umi8PENOYdEe2Nbwm8ogHC4JjrnFTcoS1yx7Q0ZPI2ETeNFR3Qu10nSRJt7hvmIZvfJen1+38FzphsXogUZieGtwoSZj0zZgUUHGK+aS6jfOTCQj5A2q5XcbLLpcRgVzhwXdKxcVbpYdQjnEOXiY40xo4j08i1PyeW0Nvc3sWwCqo29ggfy4YnqfdW5hvi+XfwPgXvRoQVMV6dMo2UnpJJuLbrLvp5KzXLT.7HW8Jbgc1hqmMjt9BTWHUFqKVnlwEcwPUBcphAppHxjOPMyY0P0qVZIto77dm7G18xhwXvih26JKTsMHpoHKkHhyKjh2uG.oEs3AMLsGr.lviws8cN6i4Q+l484mlI4y7BSnTEEL+3EYG7ZUoMD53iW0l+FbZvCVOxS8TvE1E7dHNHwndeoiplnQDLqZUm083GDUUIMJFE3ZW6ZbgKbAFMX.EixBYYxR3GzrV4yz+tkCVyGSq.9S+y4czLKcpLjGZk7hVzh0AVVWnyJDftEmLnJAyNslWYYBM97EQz4+2UvKfJV7pxi9zOIrQWJTGRoemzFtnppr.Vdjqb6+X4AKEPKJPRRQTkKt8Vr6FcvMnO97gPQWvZwgsLs4KScU0SjixRjsGiZqsxr15SITsq0FSXF9c3xzXpR6RGKBG2WFNqyAqG1QcG9pvMOcJFeDe7M6m2Uaa06X0hVbdAySFB.oTvFELJAUu1WFhFA7pVOAiT10zpgiSEAOFz4QNlVbeAyhuUyxKUSGVv6KbuhwdJxKbHgJ2n.hGQ830JIUPALX7QfVD7tEkN5QnTlnJc3kXH2.CihYiqcMnSBEMrpRp9eB3H7SpEvaP7PzJL8xwlCVDGT8z8u6dboct.eoW5k4O7O5OkNdGwFvIBl3HHJnEQFUIBgDeAZtCujD3pk2CEf2DB6m0ZK0SEaPV5Kef5qdfWTpCG1SVCXVlAR5bbY5grhdpsWcTl4r+qJNO5ktUIKkp1dQQApTp2LF6gRQWQDxyC4ERbbLCUcBsuxX.qUPwghinHC444jmmU6crwqZaZtHnnqR8RnEs3ATnM5OMsGpDQ3fQYzoSBVMBSlhQERLkAVI0PwPGEZAhyiUKy9JqAuFl4xJBkEdjC88F9NVVK7js+2rxTtIMDIz1qB6TEpmuZI530xBhzzG+zyGspKfedieJS3BnpmyMZclv1Ck3tYcdmu1YM826rlqxvj6qJU2OBFB4pzaKQ.qL1HKMzlDeNJNDiEuCD0f3.iuxnevXrjaJHunfBbnkk8lAN3NQIju4kPd8WCrFjnXr3IOe.IwcCgcTfBfB7THFRrPbQXwDRjtP0b+3SxcwfBbgs2AE34dhmfqt0lTze.1MFgZiv4RvE4PbJw.37HRPLGUMnaE0dvpLbpUOLJNzKXS5AqkYC48qXFO+WfOLOdZ8p08O3bNbNEWdFEEVDQvZsjWpgZV6Xi4WL7.BiSGkkmAIsnEmmgWfjNoDGGg5ivhf5zPRNIBthb5ZMXhrjDESpZPkBxQvUHj6JnaR6XgOLiwlSMIpLZQkCGFv5jYRAQ8H3oPKOaZo8AphQcndAuVPgeDEtBbJXjHTAxMJ648r8S73v1aAQQ0F3EYKMdVUTSvJCGgeaAhKI69Lafy356X.EOPjDH59S7XOBOx0tF6e28nHKG0Wf2kiV3v4Kv4bTnd7T5xN8v0gvl2HWFGrtma0qI2bNyzFcFwpdVtesEm7XiM1ftc6RRRBwwwjjjPZZJlnvZKxxxHKKCeoJtW4cKq0NV02aQKZwLQk5tW0uINNhzzT50qGau4VkKvwUuOU6mw.QQG+.n7vDNKN2wIMmrZZ6x7rMXbHpCpVfpgJKfSJqYfk1cnNGFDLhfngngsuqfm9U97vNaC1xnffPhIo1hJozyqlxZP3QAGaCrTuD7HEfqvwl81fW3YeVxGzGwUDnhu2U5hQEUGqL6d2jFW03tWirJb9oI7pXfzrLxYc5IqUocLuW.OK1g47FxxxpGXOOOmQiFELnJOuNzgMIJ+hdmqEsnEShXavHIWQA444jkUP+98Y+82m81aOhhhvPnj2TYjkVV1oRiienOIkVULqPCdVcLpShmcSDlZMjgfAuXoX7.dYrdXId73JcvTnz7YEvJBIFIvcJUYjySdbBO9m+EgdaBXKMiprsWFNRQCE94PPsKMxRp1vhuNO1FXYDgXaDdWAaDYoSD7Zu7KQ23Hx5e.FuqTs28H9wFY47J4d2gJENMugNqrW3n1AbVw.994KmyK1zmU6bbdCYYY00fvIDv1nvJs6zoCc5zgn33I7XU0DBsnEsXwnZb0nnHhis08o50qGVqsb6wjXiHxXQDss+0JhYEIjGTl63dyHqfAL0kjFwyrUITOpFDubwqXUv3BFdophy.EhibJHTPbThEHRETum77bNHOiNOxkQd1mAhr.A85LPW9RSpJuDLnXQGKaCqXMy4XafUcQejxTYTfm6oeJt7t6vstwm.E4PQQvaVktrSEnP80gGrpL5TUUrEevJU7J1ioGrVVXFOtXUeguYHBePoCx4ADUZHURRRcsqrxcw444LXv.FMZDE44TTTLgLOzhVzhECUUTmqteisQAQWDg6t+.52uOEEEHFvZC0PV7ZqAVGQbVbLoiqzMrr.t07baTpq+fZoWrB1NHXbisYPUWP+MwQtOGuu.TGFuhqLj1YNOCP4Zu7KAW6p.dJjfXLHUlQUcMoJF7XwifF7hk3Wo7q3XKznhBZdAVaTPA4U3ZW5h74dtmge3ezGPwvAPbDRbTv5KSb8w6IbCooQVMU1qZuXwgoxtZVsGnSmQYSyEpkmkGKFyJriyhSVy0.uV2fehhhhBzLghrgLzCiFMJDZvgC4.UqyZPfZcaq0H3VzhUCIIQ3K7TnNxyGQVlmadyaPVtB4iXGHvAFeNdUviGUBFgERToEKyNsHf4k8fmUCi5JqIZhugQV5DaGF6jHoLrbpOXjSUAd1ni8RjUU7kYImW7nFOd0gUfHuhqvSg5YjXHyJTDGyS8FuFr6V3cd7RLBVDrnNBx9PIY1sDdSs1iTRUa7jrT4nFvJn44XDCCybDGYY2dQ7RO+ywe3eweNtQCPRivlFGHQl5nocqpFb6VUyzPoTLTZXUkAPZsgQiSc9vGrXCjl0C5SBBmOsQT2q7xpEqWTkgfFigzzXR2YGt7kuL8u5U4xwwTTDH4dtySZZJVqM3kqRNkjlldZeIzhVblDFMrnDTEq0xlc6QdpiKcoqfWsH4iXiQGvEu3EYiNcgQCCK3Qs.JFiEUKXQYBd63js2CpQUTtpUpgFQlpzAMVM33FONDADqIvSKrTnJEJTXhvkZnnSG18YeFHIgAiFgUBdux.jmmSRzXmAIDXnkCCTV2BVkGKG6z3vkkgMIggCFPRZGx.hA9hu1qvE9W0kCN3tXShIdiMHxFwAGzms1ZG5OXDVqk3zn5v1DrHUPsgU33.DazXWDZpBD43avqxqdMSA+oUl1YglaeVgLZhOeNFRUsONm6PtQsYlStrXzddry0QQ5LZZ35rzgGUKKSGScaphLjwwwj4JHNNl986S9ctC+1+1+17Fa2i7abC50qCwww7+w+m+q324242gB+GwS9jOICGkSTTzDd3bZkhOzdN+87oEsnBhDduOJJhhhh5eaLVxyKvXT1nWO9q+t+M7Fe9Wh+G9s+ejqd0GAkHLiFxUSM7g++9+CW+O3OjGY2cvZr3K0tvh7brQUrEl5wkCeuqVopZddJYk8fxRvx8PzhG+eYQHop4cT8P0ziIN+y+hu9m0XwyRGBmGl22+rZ+S6XCQDT+TgIdJ9V4b4Ao0odNzJyaFS8GuWw5kxPG5wSAdo.ON5lXI+fgLp+.50cKRhRYuBGu2ctEO9W5sfW5EAmCamtA6MJeWLIMtTYdBdNqlS6DbBjVtsk81wwSI2EvlDrxKJJhHqDRZPfKt8l7Ruvyy+t+p+ZtxEuDTjyn8O.abBCFL.qDGtQ67nhDTz2RNxTcyWDIDSUCPCBJCqtqQMM3DPSrpcfOow4QCnVmXQOeBFXu7A.ZlvDarwF7DOwSvq9HWlMdwWDUCF.+t+r2im3IdB5OLqzKVYAuq1JUCsnEyE444jIPRTLO2y8b7Nu8WlKd0qAX.mGt00I+RWh9QQgvDZJ.henYAJm2GeeYyeNK8ErogbqjCRfwZkYUHC0JsoRq4+ssjkQFI3EKEOixcjFGQ2dawACxHKMhOMaDi1nGO2a+1PZBEQQnVKlRygJ0V0wVSIiaIkwOKz9WAJre70AKIXJWk38pZPxFt7t6v67leAHaDR9HrpRQ1PrhgQ8GDtI6E7NGZQfjjRII0LPcJRVgiqgPSmMhGExoeduSxCJ3d4YQTTDVqEu2S+984N24Nb26dWxIL4v96ue89UTTv96uOdum33X5zoyIvUQKZw4GDGGGzRt7gDEaoauN.dFMZDZ1.na0eOj7QYHdsNj8KWbeaw8KbT7Z0QAyJ5MUQvYr28JkUAoI0gBR6z3+ND3OTORYzeffSdbUgJTAqFjEpJtaE3UqfMIgQEJirQ7YENhezGide42FRhvaivPPNQhXhXjU61Jkw1ZUFOsEpf6UXMqzadTWNNIhdFKu5K+R7TO1ixmbyOkG8hWlzHKF0iSAedAdanCZktV38AEdWcNLhDnqkXoRs3C2Tu2MzZcJtnv4+UmbV.KyE4K6IvvgCQMBQVKR0JULgUhHhT6gpNc5fpZX+KeOoYI0oEsnEyFU8UJxxIKKittxzrWDX3PFMZDtxLFzZsgHUTpAhmzk5rVrXrrP7sLrLijWz4OXDUYf1jCeLhRsnfV4zkJpePiHSTmuaUpOfTN9MfQrjkWfT.j1gLSLGXr77eg2.txEgRpHYKcYk.XaFkRg5ZWXCsRfwVWcBqCVU5YRcbUUPKB7o5wu1U4K9EdMt0MtAECGPpMBedAo1HbEEnEMsPUQcAqZqT92.gHmutWcTkogl+3aXE78JNsCu3Ca3dwCV9RQMrRA2q9wRvqUc61EHXfUkxRGGGiwXN1YXZKZw4cHpRmtAkaOJpjurFCIooPbBTpAVU86LFSov+Vf1lAgm6wrz4xpwwm13rI1molZc54sq1OmIDlvf2tBIbQUY2S0PhT37vvQ4jzYSt4fgLrSOdgu5WAR6fCOd0i24v3TLNFWjmMBdwPUZ4UkyflILaZwyGc78QqQBD9p7lUTTDUKJYinHdyW+MXiNob6O8l3xB5MD.9BWvhyRipl2OyR.Ra96kgYYb1Q4bbb04iix4+dIDlm2whxBzU44Wbo.h58dFNbH862mACFPlpzue+5skmmW+NWyh7bKZQKlGTxxBbULMJlnnPBKgywf98o3fCfF04ypEiqEAdO9vPF5dVe78EoQjqR6adNvXd+rnyyhZWZI8gpzHSiV54TUC7spLDgUZoIkGaHpXVTikBLbi81mcehGmtu1qC85RQYUFHvuqRUgux.MppVNRC9VczLY5XYfUv0YB9FOGrFHIJBsTd4ekO2KvW7K75bqO6SY+6tGhBYCGTWUrm1XJb9wtBjC+.rdaq36ly5E50wK3qqvM1hSVjWVRbppyfFiITaBEgd85Q2tcoSmNr0VawlatIVqkhhhw5kUKZQKlKJJJXvfA0RZxlatIXsjllRzFa.EEAYPobAL.HhVlYXsbv5rBNo4f0zeWy2vK+DbuJjsf9Ybbtx8UGGjNuBpq1CVhWovo3PwFkxcNnOlzd7E+p+7vUtBDYQLQDIl.+qlxnBeiel3ZZUM9f0Qoxow+dr2FTnHGCJO1tWfetW80oXu8oX+CHUTxGzOPVM7nE4PQnnPiarr3akvErUAwUJE9PsgY.qjQVSafUUVEdVYEDsX0wgdlM8K.5gectpnM2zqWNWnNUMZzHbNGCFLXBOWUgjjj08kPKZw4JbwKtKwwwgIwhivHVbtf2hA.qf0HDaDRhMzIogGkyFc513awwFMSXgE8Syr1eZRtGJTykgEzq0gGLDNNoNKBqx5vJa.pJQNBkN6w.NInDAQEJQdvTTfOufQhx0G0mnqdIt1W8qB1DxxUr1XhLkdYsLg8.pDBhx1.zjd6GEb7TxcMzTRDKgBfXo0jpRpEJ7NhLQ70dsu.+u1aSFbiOEYmKQTbJwFgg86SutoXsfQivHdTuC0InlfUk9AYXsQX8BpEzHMHX8Qkxufex30N8btUSrVzfyUhTlb+pNSqQaZPc0fAAWMN1.s5vIsja5hHUOsFuspGVB3cKlmOKMSaVR8PZdt+s9yWBMiNt5nxraSSbFVbCnhSiSeSr4t3EzPIOO77QC+38BhwPQQA85DiursZsAJMFGGiykyFarAVqEmyUyijJYcn1frx+tYggN74Kt42hV7fL7dknnHxxxJ+6flXMXXFFig9iFf2mGxxpjDTaDdmRmda.tLv6fhQDaUnXDEhCIsC4ENr1D.W8H.St3oI8rwb04plwyoAp26kz+7dUmmVWGuYdMzp+bNITU040rTmDTtelIGmtxqPSbzGQ52.f+HJz2SP0CjRikTLUdopQCRTv4c0NVwhfntP1D5ULdEi2CNOibZI+1UTQvV3w5bzQMbyAGftaG9YitCe0ew+ygO2yCGjSzEu.NmGQTTMHUTiExbvNQnAqZSAgFcr7.cBSx8CaWmV1.BoLo3fqs617K909ZD67L7t2ExxYX+6RjUQTWHsJwGt4og59SkEsQH0V1ZqhuZ028ZvCTqZLxmkgDGkiedXYV9eVGyxMuK6dx57mC60p4cOqQLzmvn5w7s5vFO0Z8TKZwp.UB7uUkxzWu4BJUOFUoJXK0Imea.DtuiyZioYDsr3zLNI4ZVGkqktoRNRIU6SY3.EePNGTI79TgIHaClJ9XU3Xis2gOY3Az6odLt3K+7v1aBasKhwfAaXA4RfK4pngEqCi+tzwKZeLl+B9m35aMduZ1P7bgKrMey+YeC1rWG1612FuKGedF3Kv4xov6nv4H26Jyvjw+3KuJmUFDbTMv53FVvYMo6YcRLtL7fd6eQXUZ+U6SUnDqd+5A8q8Vzh6WnJTOIIImKF237HlkgUmIL1RD7hGuLdtnv5eGa.yg3ecUXFUMjEglxsSvAMdTJ7PgBC8dxir7w86yi85ed15UdAH1.QVb4UMAYplz56820rNXcXXMFR.dtm9o3Edtmm+n+1uKoC5yFatIYYCQ0zfxqZELFPs.hRDYnZLwwi09B06Cg8QkwJ65Jh4cSaUbw67Ho253AwxjBfSauXsrqwls+Y4F3iKNJY5I2Ce+MMvppz3z5AqVzhUGUiATILuhHApG7.fgVm1R0y8iiuI+SWGemqcXBym6mJbuF.WEmrzxLHrLSBUenDo4jPcGjR0.06U79.2s7XwEa3i26.76tEetu16.O9i.QBZgmBmRRZntXdRsnf09r2JzPXtB0pOAXido7K709ZzMNhae8qi04fhBTeQogSkZfUiLJzi1HKCc38Nv6Qajog2qoE5Je8LCuk0baKRhIZ0QoSdLuUerpcXlkAVUDd+L2.QsnEmwP0Xf.0ZJWqGrNagSRuWI3um+ABg0yyXVaLsmppjxIw6JCIXoQVkNcwiqt3NqpKnsZZvCV4dghzt7y1eet5a7Zry671vFcQMQjKJwo1.u1mCUfVG3D28Ht7fB91IB9Ju8WhW9EddFt2c3faeSh79fASk7vRUW4MdsTD573bgzqWaDdPUUTmKnkVqHlWnuVUCzlNKHVW3AcNXcubO8n7ywIDlG0PDVoiOsgHrEsX0wrLvpcsImsvr7d0o+BHG6.BmFzzpvl0ZtWIdsNgznowU9wdzRUONbjq9ZpDgSHWM7YENFs0V7LekuJ7DONCQXHAaGuvC..f.PRDEDUgrMrNwAOAoDyIdHBEQqqYhWdKK+K9l+y3G7SdWt0m7w7natAEFA0JXhrfyfWLfXPDCBN7ZAZTYFbIR3FpyWRDMOhc0Sk9ocU5pfEMY64gIgOubMzLDg2KnIGrBdvJTaBaQKZw7QyPoWIqIBO3Olx4QbVLDg9ZtVUVdlKMZxpfVQj8xjaS7gjhS89P98odhbgiKGE0nHNCFGfSnvJb8rBt1a9E3I9xeIHIkBWHK.MFgbefNVmjusd7MvRYgsvjnHJ7fyoXiE94emeN9+5e8+Z9K9696PF83nRv.Km0hXjx7OTQQn.HxXv5DDS3FnQottDJdErKo4s.CjBdq3d75tDqBGtVDdPmCVMwIQG36EC.O5bvZrmxNKM3SKZwCBnpOSUc87AIbRKCCm1G+pbNNcQImWojS0Zo4D0YNHfN9eWqJQkp0dDkhAp.NQwpJh2fSMjIQbaK7E+xeY3keEPrjlrAEDgw.YEdhOgme83e1aRt3RKsB7aLLgUQQAQFHwJjMR4Bcf+y9M90IEk8u0mhNZDFmRhQn+cuKwB3KEfNKB9hfWr79BPcANa4B71pNSCqUH3IikZklUM+ltrRBjVyLXr44soAESy4pZWUtDT44j4ILaK+1+g2mkwAso+75rxXFkmnkwwrSZYZ3nfpqgJcqZUPUActp9CVo6YMUw8986Sud8v4b0k2iVUduEOL.qILNWdddceDmyUO19nQip6STTTffPgqHHznh.k5KmwXHezHLk5RWTTzQZwiqat0VgUc7mUc6G0wuV0ieYTW4nzFp1dy4Zl96XQWyy5bMu14rN1om2QUe873ppkRiPXtdzJRt6pkwo51GfIyi0o3TOibdrlXFkUP2s1ke5MuE69xuLO+uxuBXhYPtPLQD6qrSwUVGCO43v8Z07soaNhnDaMndGQ.IVkTfW44eZ94+xuE24S9XLtbx1eetymdCt3laRwngXPoSjk7rQ38EgPBVJnX0O3Jsfc5aBy5gYSiGlWMN7AYL807Y6Urb1CYYYjjjPmNcX3AGPTTTcwqMMMks1ZKTU4N24Nr+96CDLHqMDhs37N7JDGEpDB6e26xnQip6Wr4laxFarAEEEzu+.50qGY4YDYiXiM5.iFUKdkUn4DtmWF+8gYzz4C2KOOMUxtc46DUFjTqR.UYM3TF9Tng4wShhYzfQHpg3jNbmCN.IsCe7A6SwVawm+a9OCtxUQIBwj.dC5nbDfzn300sgEb8s1wjROpwXvW3H1.wHDC7jWba909O5eAakFyM+vOjDT7CxvT3gg4DqBCGTVuBqLrpvgujX6pyg5lewfdYqBntktFWgwoMlkQVqaODcdDhH3bN1Ymc3EewWDH3wJu2y96uOCGNjQiFwnQiXyM2LTq0HXT1oc3aaQKNoQUIuoa2tDmlRbbLCGNjabiaP+98QUk6d26xUtxk4Ue0WEmyQdQNyZnk1waN6gEYTzJYvjq.w6l3Gi5q+Y5+t41qfHRixkiuTXZCVQ38Ek+LoRC3zv1xyJHMJEedAt7Bz3X5GY3Gs+c3Qe6uHO127a.atEiJDRi1.7FH2gYlUYv0ONVyPTGOzZ4qPCgGrRUVAPURhsAKR8NxxJHF3m6MeC9k95ect4G9gH4EbgM5wc+raBNGV0yc+raRmHavXJePNGZ58o5JfseRQHM7UN9EioC41ztGcoWiO.YfxYs1yCJPDAqwxW+q+04xW6Zr2d6Qud8X6s2ltc6Rud8PDotjg3JGbn0.qVbdGwwwLX3HNnzytau81r0VaQTTTvnq3XtyctCu0a8V7xu7KSmNcJKt5JTF5P3jQi7Zw5AMCIXyssJFXMqvoM8wMuOyfGCUjWm5ZNnTc9J4gUE+rl1KV4pm6L3.5twVnCcLb+ADu4l7giNf8t3l7l+F+GCO9iApgBmDrKQorHi6wSAmzFYs9lgPqxUv.jRpY48ETZKJnNhPoe+graRDeqe4+47rO9iyG7idWL4djbGFuhlWPjJnNOhpiiOaQNZgqd6yKzevCOtedddtpEqFTUwXLjkmwa8VuEu4a9lbiabC1au8PUkrrL52uO28t2k77bN3fC.BqnuUmyZw4cDGYIKKCu2yf82mO5i9HFNbXsjlbiabCJJJ3q+0+50YQX2tcGW66lSDCtWl.+jfiLOriisGrNBeOSO27XtT4GWx7JMrB0UZ6vTmGwGz9JUCDaONl8yxH1YniDy9E47ti1mm5W5qg4m+sgnHF5ThLoi4vTZDBJNe9ItNUsFxhvxVci4za9XIzoxS1fQjDGiTxcEA3Ee9mieyu8uF+t+O++Be5G8Qb4m5IXT1HFIB6r6tLneehS5fX.OEgyqXPbRnJY5ETmabA+rrgT4gpI5LdnlcHTh1kbK9AgNwyhSCs7bX0PUwqM244we7Gm24cdG9276+6y2867cBuaasfKnIaeqeseMdq25svZrjmMjjjjooXRKZw4J37J6r8V7K7K7Kveveve.u6O5GwG9duGXhvDEgOOim3YdJ9FeiuAQQQLZzH5jFFylB2L4f0JTB2Zw8ILs2qD4no0iyiD8MO+yZeTM3ZpZmkTooUUbtxEnFjTsuMlKOTtmUbJDsyV7we704pwaQZRJu6s+LRehqwW727WEtzlPrAqjPr2flCY9BroAi5RLm7Nh3DSGrjp.EV8fCOhMjTkcSiIuHitQI7K+M+l7m7W92x246+OwlW3Bnc6QgWwrytTLJCiDgXUBzgKTEqEg55UjAClITi0IMtZh1zTFcT8PdQXYuncVxaQUcNldaKDOjOXW08m33XDD91e6uM+ve3Oj+w+oePMYdcNGO2y9z7a8a8awq+ZuNPHKBqVwdKZw4UjmmiMMgu025awAGb.+d+d+db6aeaTLjkkwnhb9M+O4WmW8UeUhihovUPgq.iFUqfNyJDTM2dKN8v7L.p5yVFVkJdw79NpCGXIepoLTgZo.jWEkJiRMOrpH2t26IGHS73rVh7BC1uORZLeke0eYR+huB6E4HFSPJFTPK.erALJBYDSHTgmj5s9IfAVFLyXV63zdf.8O3.51qGkYfIO0k6x+o+5+p72++zeK25i+H18Idb7tH5e6aSTZ2.41APbHBXk7P.YKJEwTqE0IfXPEEwSnhXW8vojmLloxdk1N2s.Bp4ad9HhSsLJaDuya+k328282kezO5GwS9jOI44Nt5UuLc61ktIwLJaDduuV0paQKNOidoIbq6tGWZ2c3+t+a+ug+k+5+FLLODxvO6V2gbuiW+UeErZHbNoowHXCya4pFicxIvZ8h0YebT7fUyE1uJFaMww6pLfpjxODL5x5UjhwdvRQvgPoisv6g++au27fsrqqy662ZuOm6vapGwPCRLSBP.PRQPBQHBvAPRPPRQQQYSIQRMaMwxzJksTpDGyDG4XKYWoJWwItRJkDkJNpbJKI6xJkjbnrisDshjkhlHEooHAHI.mvHQO2ug68b160J+wdeN2y89tu2qazcitaz2OTOb56vY3dt2y97sWqu02xDg0O8FruCbPN8IFyiexSP48cObyu22KzeYB3AUwEAuANGTV5HhRcLhy6wg6xWiF0DPrzEOF4RqL4RXIFizYVK4KpFLbYLCJbdDyPMgG5M7Z467s+f7q9a7I3PG9.Lb3J3iJUiGQouDyxZ3BASCHACOFNoG3BPgjNQIRiKk0VJmdqLeA8zew1bIuqCyZQDzY9gRCAMcG9VHj8QIxQQK1z3HkT0Tp6wO3b1Du0pKwu440V6TERNs2k3116e2vrEDP2yEy95615uSVEQiNklUP3yy6xlG1qW24boeGJ4yMoCpodcx9hky4flyyjtfKFMJJ5gEUJ8EXpxK6HWGuribc4OfNPRGqlpzubRTqVvQeAtxGyP9YleSalx9Vc4TTED3ZulC0123twa7Fy+aEuApoX3vPQLGR9Zdu2SHDv0a.iqBH8GPrVQ7koqaY6i0L63uy95sGuyD09Y+2Mle5NMo5y1LPrSiCtaay4kQgsiKt53rwyxZvrEav7JTGQjoNu0rbdRPQsc33WZd9l6Y07vrtoMEWsRebXJDb4z9oFkNiBSnPkjCB3DV2BLRS+6JUQFqzunjUhd1ZqZdVKvluhak24OzOJbq2MiNwXFdf0v4JSQRMGnJmkHl48KSjT3ftXhy6st0lttDRg8yk2zS9a12WZmqP8X1eO364678y8euuZ9L++8GhMdKrpwz26nZqQoxuLSZJFCDppHVOFqtt01FzPLQsMKRtt+vXVCwr6q0Hf94YblmM+sa1AwEpnjsagXed6qWLqxwcZ+LMousOH3EpiscKUvmqncvGqyens+6EXAtZDM+1u6xTUecouHO1swX2qLVb4j7NtXg4QtBN2xhy7demq2ea12qYVpc2nwzewFwsasZtByX73wTECol2bLPsZLr+RLr2.Fs4HJ6Mfm5jmjGeq041dmODE228AlmA6+Zoj9SB.jnfnzzqX.OF9K5+B9hduHbOQtYNdW21Mw29698ve9m+Kwwe9uIq56AQkPuATV3vh8v4z1SHZ9x6XHLsisakHdIUJlpfJ5T+vZJQsaSGIEQjsE45K0WDd1negtDFm8BpWnj7tRIEponQYaSOchLsersWXmh.27l01Br.KvEWrW50ZmFWbVrajqNeFe7JEb9Nl0dEAq8b+a.M2WxLvlnuptaWmYTRLYMCpghxXmRbfifEPcNb3odbMQQoWTPQ3XNimyAG9dtKdkuqGFt1CAXXhiQZjRmeR5nEAwT7.l3tHq9pDtjZjOhAKMnWaou+V+1dS7C7g+Pbzm5o3LG6nHw.V0VnUiINdD0iqP63d1ppsQupIRVjEKGc7Vic5hnc5BrykH.sWQv570Gs1sHSsSQvp48d1hymncsWQvZ2l0zESBKmqe+M6yMu22Kjs+Br.KvNi8JZ+y60NehbBb000t6z8atPDAqyEzr+lzNbLLKlZVMVDeLhuVwETjXfnEnh.ZOOaDqn1hzqzi2fQarIw5.kqtJO5wOF9a8F4c98+8C29s.HvxqvoqFCtIwXMQvKIcIAvgg+EglR9k7HX4wwpCGvV0Qt10Fv69c713e6m7eOe8i9bbfq4ZvY8fpJpEgXcEEk8oLGcBqtldCVBgHJdDL7DQPvbtDyYURBbWAmSPX59SnJwyYROWNh4Ql4rAWnlgyE5s6kBb1RrZAVfqVvt968FIulsFml+M4J8lyyqU1M8UsSO2hJUbZrSZo8bcB3msqe6yle6NxqatvyRVxf1JiGyRQrxET74dMXsEo1hTghFBTKFtpZLIhWbTzuO0QimuZKdZG7.u8Gh9usGDFtLir.N7nkSxfg3rj7gPa+MoKaJ5mKY43EBtjGAqX0HJbBCJ7T.biW20ye0e7+Jrud84K+4+b3qGiOFRVrezxM64TSdtttNYQ9gPaqzIVGPqCP1PRa6ig1j76Nqvt2sHKctnGq1OWW.IqsaZ6ZdoCb2hp0YKNWV+85y+kpHXct94+rIBUKHWs.KvEeLuq8lWTrNat9duzG5K0wY6Xy6D1sLubVOdnj0EcLaECY8VkLIgTCWVrHRPyctEknk+Ci0VdEJLgpyrUZ+Ob.GKLlu7oON29a8A409s+dfUVAFN.+J6mSGpwKE4xhqq9Ym838huFBuzFAKQQPQqFiunOQ0wJ8fu827ajG8wdb9E+U9k4zO2ywxWixvhRnDhUf5S8ANm3PiQ7lC0FCVDonGQx+vvLv4wbRp7FcFNWCKVOhy11OV1qYL8hM5Rbp6ysWZxpc1A6Q6bYmlgyYK1IsPb9tcOagHoJ1bV8y0HTx8ZLfylnVsSO2Br.Kv4GRiODm45qtSJa62Db5w5l8Zy4Ip64klwoeuuvw7FeS5r7h63etWfa9lyMwyx22N97aa+qScbIM1IfAhkB1gKoZVPi46SEaGu1vPkTpCQS1qv.7DqUpqSp+4narAOqKxf67V4s7g9do3UdGfTPz2iHkDs5rcOjtsuX4GXsGTbg4698FWxSQnqWOp1bS5U1iRQIFEbdgejOx2Cegu3iwu2e5mFyITze.ze.0x3VhCEk8QBJ3pyegGApAQPEIo+JOoJIvzoxGa5+rToDKSeQ7NEkn4g4EQlYirztiytYgMOMMMuP2N664hMlc+2MhfcOd51VYNaH9c1hFBV61wzEj8wBr.KvEVH54L+icJMg6Vzl2oI68h8XkWJvr2GaamW1iy+M2i4ExjNalxqXJXJtF2Z2h3LCRFy.JYxOR1k1sFBXPbqwLZjPX8wzu2.FUTxSbrmiQGY+7FeOOBqbe2W519qrLatUMUEAVorjJEF5RDzPjIAqRRRaWI8Q+h829WzSQ3NE8k1lsrYza3.HFvhA54RDiFfwe6+y9o4FO7AXyi8bvlqSbyyPeyXPgvIe9mmxBGgQagUWgUWCZ.MVSLTgEpQqqPTESS80nT29NItsh79uqeSMkH7NOBg54Bl8G7caB0MGOcOl5ddsauWbVRMMOWWqmX1uS1I6o3bAt4bdr61sY+sSDC8d+zUAZmyCMdwxtgyli+Y+7NuyEM624oisylzBu.KvUqPl4+fbvBZihrPUUEjGmHjq7aXmiF+tIGhlGO6XOySNBy6Z9caec1JCjtGG6ljH1q82ddtcWN2nptsw8mcaO63+MqWqDa1CKJZd2+X5OeYqXJ+cMQEQSUKnYVaqvowVO7h0ooNmN9p0Hiz.aEqYqXMUlgoBdUXE+xL9TaxJ8VCobIdlM2hyr5xr+238xs8i+CA85C8VABP+9CYoxBDfgB3zbjzzlV1ThRkhQfDYtK13RpFrZQ92PNQnTfd.qMrjqY4k4i+y72fxPfm4q73rjSX7YNMabxSvfBOUm4LPHfFCHZMDCPnFIlHZQTQi0Szm0T+kabzZXtD.uPcCzy0KfuZ6F361mwqF97u.KvU23RuWZc4L1sw.mU2sMu+t+64Q.qYcuPF8t4FfAMospj1qBsZhVzXxg.ZHuIf5EhENLuinHImcOXr4Y1jCdfqiyTE341bK9FiGy09FtWdOerOJT3AeYNCFtr4jBE4TK5a3T0Dpp7gllisl8hfStcIkfUaZvEG3SRdyz.dfABrugE7tt+WG+D+.eeTepSxW+K8XrjWP2XCFfQ05mFsdDZ0XzpFiGMfVWgEqSQypNPLTgVWSLjdtl+hw5ohzxKHA7c9dN3rjf0dMynqTw79rd0DAyEXAV.8p3+1cL68ClazxZhdjYS3QjerSjs8bMO9r4dbmK2ep8OiD4pNMs4TwlERDqB0Xw.TWiESAXRyQTJBDUkXznNJDvwyuwlrduA70FOlA29swa+G7GBdE2Aa47o1wg4.73xjpJTnzZH2z4dHBXsZ0sfWLn+bIVCVNLQ.5DpyXhoqHdJEGanJ+venOHO0y7b7O6W+WmACWlC+xuQBi1.y0CMacnwb5jcVAl.tfj11BfU.DPhIcZoMjnbtD4Jmrse79h0M22IsBbtn8qqjwtE5dXg9mVfEXAt5E618BDQR99Xm26ru97hxU2Wyo6baf6743S0jlqRoKLlJ.M0vhgjrcZHEpfhRHpDs.ASQTI0qAMgA6+P7MN1I3XTicK2HO3G4CQuusGfspFCCVEy7Hc9.H11jjaJWgYZUo+uiIpu5h68QujRvxjDy0TmDTSBNWMzXfT2btj9VpRA+O4m3GiSb5Swu4u8mjdC5ie4UXvp6OYBopgHV1N76kZebRhotKyXUDGj02kScsssGSEDY6cD7WTqBN1NQqtg48kxX2F.4rgn4Br.KvBbkJdIy364d1pYFVSVgzjCsik75JSa7AqjFsvRZNWTEUiDys5tT2orjPQAG2ab7UFvWKTw69C9cvA+Ne+oniMXMbzO02KcMdbkKUrfRSD6XRAr0V4ftjWXlOrc1E2J87RdUDVilOc5RcFnhb6WzRmJJJ8DFE3Pq1mex+J+Hb7SeF9j+Q+Qba28qk986SPhn8TLugiTeETv.mkx7nlpfPI0xGI1dC6hbeIJcBtMhIyH35yWr2W.M+bgOa0aL614kJQ3YmRI6BxUKvBr.WsicaLPKmBvc6dAy60l9dJm+iwZVSk+EwEI46jQE0hIpLYSEUrHZLS9xTLEDSvEEbpg2.ScDQHhwHG7kOww3DKOjWyG38ys7A+.vxCoJBda.wfAdAqsYwZ4Lh0IJVctsoIIGEvcA3y7YKtjRvJYDXEnDxdwdFhjBenCHprxfBd1SuI2ysbD9q9S7iyI2bCd5idBLA5s1AxDpH0igrDwJyI3rrX17Ef5wWpnXnpGuWwhdjRRQ1JmtPWmpZ6EiavOOcVMqHEadsWpFUqc5b8K09bt.KvBr.mKXdS7bl2wb0m67jax72V6gUKrWVwfzohMsFsVESdPYl3Up2CpP1DQMMlzkkk0ekANMEcIKBgnxXMxIKhLZoRtg6+d4A9w99ga3PLJnTzeMpNSUZatlm3LGhShIklif0LYmxln9pK12h+RpH2SE3Yp80jpn.Im2vjeYjCiEG+XmfCuukntBdi28sweyepOFwSdTN4S80wUsATuIZ8HndLZnFqtBsNfFhDCABYGe2pBDqFiVWgVOBqdDRLB0S5mgwP.MFmR76WLwrkD64ikIbkNN2I0d4QQvt.KvBjvYyUuuPMGyqFwdEAKlQv6y9u2oJGrUCTn3r4K3dCxBHeZ3rl+RFEt2TbZDIDvzZDUAKlRKXLjqjvvj6wEMTM8uGGFSPqyGmB0lvVpmiFDdlfvJup6gG4u1eM3k+xXcLNYLv5UiYok5wvx9IOtbpe00Rsp8Qsx5uy6U3BQr61abIMBVhAEhQAtb0..fBEESJsRbrxZKiGXsRXqp.uyWycxuv+Meb9u3m+mmuwi8Y4VtmWC9ff3SUfvnsFyZGdHUiGSABhWPPAhIVzgZbkk3K5Qb7Xb9xDiaugjM5Lwm7gIyRdIkJIOap02lTifoXVRj7yJnvFhRdY2IALOC2r6EDyijU2K51KAJJNWK+8YWhH4eftKq+tR3QStjexV3lBS8Lxz+itGGtlVuYy06zrLcIvtywzvItoFjY5JjQZ8Jltmmctzd8EA9yKvB7RZHr8aVIcu.2LhU0owAUEhfqrfw0AJK8sQQYmhTiyM4VTRqvZZdOaOhMaOZM6Nat4IB7cqXmNWmz618uqyoUeGyZQ2iOsaUF140.nHe+GqIfEPq1jQRc5hXlNhEMTIKQFwmrA8rfva9NUrTHmLh3LEeTS6SMlhfkES9ztE.IoE5PnlPUMVLltgkl5QvQTFSECWZIN5SebJFrJKuuivyerSw2zOjvMdC799Y+4PO3JrdzSnnO8G3nDGDTnr4tGYu3ZxYs7hTval.W50l5KgKtr8ujGBflzm17EngK8mjVhPqgjFFsE5lqSefG3a40vO6+o+zb3kFvW6K9En5zGmS87OGUadF1+pKwwe9uI8cEDFUgVUC0wN8BIEsth33QHw.RtTRcQCWzZMFstDklMRS6j0BL0msqR0PzYeD3RTrVfEXAdoMR2zdlIBIM2.+Bv1dFb0Vz+gysL.L49sSaQQSIOFSRo3qcchsFFpK6N6pEP0pjuSpU3BgT+CNDvUWSnZb59tNAw6w7NLmG0kHvMXog7bG6nr10csn8WhG64ed9JasEga3F3C+e4+0v9NH0KuenXED5gCWNRYiAcLXNDyMEOht7IZMB0sQ.aBgyKl3RtH22Kz8F0862GHUBnqt5p7dd3GlSHB+b+i9efG6+3miu0G3Myw2bK9FO9iy0eK2NG8oeZFr1Zn5.DQReX8BAGnZppBKnBJLjBONym+xxiyJA74R7jjX307+pSTg5Fkko0S01bgizy+Rrq6ewpZKWfEXAtBD6PJrtPM4y8LEZ6A1IuF7rc8uTiHVJRQsDllfj9ql94aROaaUza4bMjIWo4uaTi1VcCMczHyvrHh1DEKkZQQrHElgKFoHnHwjkLnnXw.pOUo9AmQzLTKjD3tZbhieBVZeGfitQEiGrLek3on2se67g9a92.dsuJvAkkk4.ujhZYAxEcsScgBW1SvJFSMBTUU7dOCGNjPHv3wiwFzmOv67ch577O7+9+w7nel+bd429cfINFchSPou.a7XLChjZ9yp2g3cTTX3Lgw5XjnhOVfD8HdGRYANKU9ntBeqH36FAKQRAeLNmziMOgo+BEmMhL7RGxgbksmue3roR.26C98bf3K+GCbAVfqdQWxTyTIahHKt78BDdgPXUkI9.oC+TDecLosmIsJZJ4B6VlfkRjnjSEnEvESDpHlD0da1dbNBhRPiTGhDpqQBo8R+96iMq7bJmvW74+lbM26qm2+O0GCd82KPETN.0D7l.n3x1pTJykom6x4rfbYOAqhhzgXHDnpphd85g26yDubTB79e6ODKUTvem+9+C3w9y+y39df2JiqqRzcqGQDZIoIEd7kE3nGRoQLFvkYl6o.viK6cFFQ7RANygYEj3zGSCZ3bP9GnlLouZkPyOT2tGaXctNPZzb1U3XBmpo0sP5B183G+KFgcAVfE3EHtPE0ocZ6b4tLOTRShcd2EwrTe2ElLLqtMtttViiR6LVsQLKUlj.3mzS+Tjb6kKcuvj8KP2fOnIsIGDK8mEIDgPzHFy9bkHf4wu79Y8Qi4qt0oYs69d3Q9X+jv218Q0nsvs59IpBhI3CIwzmNjEZrbfK2Cx3k8DrTUw4bTTTv3wioppBu2mCaH3HfTG388VdyT7w+37y+O7eDe5+veet063tQ6uD9UVEJCDz.RX.RYAD6Ss0DdTAEEuHnRMoF1CHRMHkDr5z92KSDstSnv4Q7VRnztsKNxyFX4rNdkbU0rWhveAVfE3pXzMp1crEfqFqR5KVHIt842yA200CxQXzSDZq5GWSt3HoyJwzjIghAZ.zHhlZhy8jbycN2NbhlhRxCrBXTaJgfQsk27RJCR.TQAe8u4Q4LCWgCb2uVde+0+nv8+FXT0Hb6+PbhpJVo2RTFc3iV9FyIaWHpFpyQwDyc5xRbYOAqXL1VAXkkkDBg1e.EegfYT...H.jDQAQEB0HhxAJ6yoGsEe6ukGj8sxp7e0O2+.dxu3ixK6UdmDTE5ODWHhaPDILH8iAUQqCTzuWxzyrj.6SlflOwfOFwbBl2iTXHlGP.uCmqDzj34LKKNPaRE0XxtOapKbA17ExV4B2.ac6iiMKWPvZAVfE.nkfU2pg6xIxUyymnlmODd4NrNkxo09+5Dwp4nQKHUklQx8Mv1O2.hlDzdLlLj6bKuQrHpF.0nTM70oTAFsPhLkEIfRH6p6.TqFwfkJbsr9tpBANopblU2G9a5l488y7y.26ciFqws1gXc.50CvOIZa4Bk.LhBnVDu3urVWyW9l7xLJKKottl55ZhwHPxtDJJJvggOFwSjk64od7Hdy26qk+a+Y+ayq4NdE7rOwWFc8SCatAx3Q3ppPpFgUMFcznTUIVMFqthX8HzpZzppzeiqITOFqJ.YOxh5HwF+zpNfEiX4vk1DEplPx1TEhmunaTwdg72EarWkQ74KtT+4aAVfE37GM9tzrjsNew4qH2uRG61mwytO+MMKYRBWOponTE0TjpHkBPAEG5DmA0RUbeQsRYklpLeUoBkw.0V5On.WzCQGZTnx7bZEd1PMe0pwrzq4t365u+OO7ZtGXbEtU1OiPXywiY.8aE1d1aIfBGT3aIqc4NtrOBVlYs5vZ1muWYIHdBiGi24Y4hD8l6+0d27w+Y9qyu3+z+Y7I9s+jbvq+kw0eK2FEzmyblSiT1mkWaeD0.Uw.lWnnWOVZkUAApFWCQkBoOlUQL68GT3w4JRL1UkXrH6YWBQwfXt463c3bNbdWJzpcmgjK8u8MyVJl7tj85hgYsJhlmyU1aWWu4M.T2mSz8d+N20qQLjc7Wp4UQgMo3s40ZHIOq2TsSGey67xTGSpLUNVS41e5GKcRKwTorXG1WWHKRgEXAtZGsBZOomBJJJvqoT6jt9Z6WO1L1PSAE08ZwlHK0nq1ca+tWW+NaZ0lMR7c2Wya+uWiOsS6uFLaF.lca389495Mn47jzxDI6wUM2moIrV9lTINYaq4y8M1RDhjIQ03sUZmnVkIWQd+DUznR8Vin2fA3K6SHLhpnQTbol37XkBSouziMWecrkWF+AN.ekm5qwoKLtm226lG3m9iCCWI4ClCWkfB8bEb39qQiId3ZEzNnNqsvxjTy06x5jDdYOAq8DpQQQI37XFbxScRJWYEdc2yqhelO1Gkq6ZND+K9M+s3K9Y+TbmulWGElm55JjA8IDpY08e.LmiXnlMN0IoXPe5szx3vndisnnWY9x+5TZDcBlyR8rPe.kzMzUWpxCMImdQqDAkBeIMIDLZQH5RMkZMUADdltRZ1Kisa1H2rW29eOGfYOV+KjnYfoEjVVfEXAZvN4iUKhP8dO9cae0aNmpb1zO+rSjWfTU+A4NmRRD6XZ9wonYk5efg102xDrL0vU3YyQawY1plwNgPYxXR8TRoujhZGm5DqSwJqwlk83S+3eYVeeKwa4i7WlukO7GF12PF2aPhHo3wTG9l9yrjIWQhXUpQ20TVXMU63k23JeBVhCstBWOOhCFzqDmnzSf69leY7Q+Q9A4fG7f7O4+yeY97ep+Xtm68akgk84jO8SxAutqii8rOM8VYY5uzvjytu0VrUcfhdkHk8xjhJQhJhOB9xjgo4bHZY52ilCwYnNPHB9TTUDSRylfTUJlxanhIVtKdOcUEN0GqYhFzrdISCh.yqcFzf8jLyKvDXOY61ce2MZP4mQx5VqshJa9SvLgEEp8Br.W8hq1SwGv1FWu6DQ2SBVN2NVjTcO01HikI0aPJZVpEyQ1mT0.Fi4nVAjibkogb0ANIBdAkTZD6WRsCTDJ7oNxQXb.sBbVIiFE4PW+MxW4DmfO8S8UX0W+cwi7i8Q3Fdj2Fb3CyYhkD8kThPQzQgBNMWYhNIc+cmQj785Z9rwk+jqfqvIXY3PDinBTWiqWICGlLizwZEmYys3lN7A4i8i9Cxq31uM9G+K7Kxm52+2kW48bObi2zswyeriRwfgr05qiVWyxqsF3cLd7XBUEze4UPiEIiSKpH9hT6zon.w6IZFQyinEXdCM6WVXBh2PLGQcLVC4n7q6EoMLtZb6kC7NMnybe98nU2bg.6sWbs2yzb2LzuEXAVfE3hAtberl4Mo4YSM4tAOIKVnch3y75lkDnd6zaaFGVmrTxjr5lRPKKpcS0rdrz1VxSro8woJqu0l3JRMb4PHP03.wHTXkTKF1fg73O6SySGp3NdWuCdnexeP3M85gANVGGa4cHXTPNiNVpqsPaZNSQtJRyzxgjqcoIWc+Eg6+c9fqnIXAPzfhACQDCSSdkk3EJKJYsk5yFasNKMbE9NdaOHqtzx7+7+6+evevezeJm54NJ2zq7No2JKyoqqnZzVb5XjgKsDCVZHJBi2XSJ5M.J8X0F9xHlOBwBnnWxuO7FZghoIcWYtTYnZZjnyAdOlzjq7b6+I8DXQW92Ra+Bpy1Y1sWW.dw2nS26We2z7zk2C+s.KvBbwDc0HIb0STqNawYy36s4GnQKUz47XN5TaqvBZ0daLSrJWkfwPhXkYYGaWyjonC4pTyZNZFisHElfoB0UADySuA8IHkbpnwW+TmjuoD399feGb++3+nvMcCXgHQ2P7ziBL7IaNkBxMcZQS4FzonMsnXljGDOf2bywkIu7CWQSvxDHDgBufCgp5HJFC7kHhPewS4POaVsIiBFOz25qiW4s+yw+S+u7Kx+xe8eCd7G8ufdWy0y9ttivRqrLipqXi0WmUDA+f9DBYAYGKw7FwXDuOhVFg.HkdhEJVzQoyilE2dSihV7onUY.pZfpXhf4DhMh4zWhxYW0wMqKomN3tndJdOONXNGqyiP0rBHcw.oKvBr.KvtmhvcCNCLU6zbsScVv1nYkIV0D4JYFhVNzjqrS1REz.VLjbqcyvxV2foJQ0vTgHIg4GUHZFkk8SErj4v2qfn44LwHO6nSx2XzX3kc87t9d+d4N9td+v9OH0Qkx8enj.0qGyxkCvg1JT8jTZRDszI0tHMpMKQtJ+VHc6mcRlMWNfqnIXgI37ouzq0T0.NrXHj6l2000zqWeHTyJCVFC3PqsL+89a8Sya8Ae.9O+uyeON4wONADB0GhdCGfgwoO0onW0PJFLjPUMhyR9fUvg5iIWju.H5gdQrBGhyinoHXQzgVjerk7RKQjrotMogZphCmIsNaa2Fs4tQzp6qsWzT16Pje1WkMuPe8c50r15ucAVfEXAt5C613y6EIKyHI17NQsZpJcz59dyYKnSjsTTzXrkPUJkfQHFxoNTAjTEy2RvRPUHDUTMaSQgTGOAofSt0F7LgQr4AWiUtiahG3G96ia5geGXqtFGaqwr1pWCQJn5jawxqLLIrpVwgYD8FJFlWxd3USpO0IQtRI6Akb4cIDxU5DrH88RRKbEHNACCyTrHzqWeB0QVZ3PDwwnXjdHTfv63Aue9U+m7+F+c+u6+Q9LO5Wju5S8TbC27MxAutqiMGWy35J5UTPTCn9BDMfyWRzLPSh7ya8RhsK5P7F3cfyg3AyRBfutNlzskq.oPv6KQEEGEfnIaKvklAPDEu3wbjZ5ztTAczzACZIVIM1Qfj8g9cNOz6IAqWDCjjY1NXMCM1t545RxSko4yXt2HZIucQrb1Xsz9PrTF8ur1Y5VfE3JdjhZeyPOZ2gfjldaWikBjFudx00BMWeaVLWjLMEie2We1GO+wIlr9ye7iKWhjtGoyXwSaID6FLKtsHTMQ.6YYmnMOVQLIMFXCho1ZiYVlvTRn6N0PMCGBhBRTIZfYdBVR2yASnW+kYKcLQwwHmimjHar1Z7pe2OButOx2M7ptChENNkYHC2G03otJR+F6EJDAIB3.mkLnamGW9ldMGoIA13ld53WAL27qnIX0JNNi7MMSLRDJxjb.m2mB0IFkBThPLpLDG20Md87K7y8yx+7eseM9k+m+ufu5W9yis9w4.W+Qnx63Y+JGiUulqkAKuFRoxnQiAmmkVdUh.UiFwxKuL.XNGthB78JwU5AKfFLhR.wKXdEhf5BfWRuWuOEHTmCQJ.yHZJljFXHoWdGRKwDIW0fIMc4w25sLSEUqtgAtrXpmSoSkonc5UUctPtajzZv77fKHEtV.lpmC1Qzks6i1QamHFyzEQZi.Bl6xXH.RtMMHY8F35PoRj7EkomTLvKNDDboNBJNuf2IfD.I6KWzDJ9yoexs.KvBjgXc54tsvktguj92HdbEd1JLFoXIbNXb8XDmCCgPnBGw73GVdLgTUWKXDCU.Fh3xksuGwIs6SqkUxNsTa2dcqh41iYYxDWm0SrLyPbMUAtl07Sd7IRoPqqHqktazFXSds450UM88VcFu0xR80uVerRmdb2lT+03X5oT9kHbk9dQl7sgKs9FgzWLMUNnZX5D+Fq8ysBVHcLDqporrOEZAiGWQDA+vdDEiM2ZLmRAa4CvSs4573m9jbMulWMO722GlW9a+sA6+.fIXQOq57DyeGZ87D5kN03cBNymzlrH4QrSm27j8zKx+NybSHteYdjqZvUzDrfl4JQG1+yFgjzESSBYpRAfYAbliC1yyO0O7Gh2x8+F3+0eoeI9s9c9jb5SdTNxsd6bjCbPN45mgiclyvfUVkUOvgPbEblSdBbtBVc0UQqqysWGIQBuviuWITjzfkqWIVTPJTPRDsRoNL2Okz.hq.7woIz3BSLQNWpTUEoYlgRVKWEo12SSN6k4DV45vzyfjI4rtgjAcVBSaFnSsdyY1Tolt810PPyxcOBZ4nIsKiQl3sMQNiFZJHUDyqpq0seSkVbt0EooyUwX.Sh3KxChj0bfL0muKOlE6Br.WIhccNJRm.FawjVpEKaZkR1kvmLNtvjHv.fXSddAIoi0zKj1jmkQfZauu75qcH1LOBVEEESutc0WZms6rFbb6y0Y8lhbjY.oI60XgBoA7xR9Op3xSxr69wMEQKCmj1NlIYmWGZZyMMCs1VkfM1tPlbE.TKjngESSt2TLLDG3UgZCV+TmghhdLbkUXypHO+QOIVYIqc8GgmdiJdrm64Id38ya869Gl6+C9WBdk2N3KXbsh20GAO9nCuKEMS0CAxGaNI2B5fFaQcpeOMmudubVyUyhq3IXc1h4WMaBkkBipTtqW0qf+1e7+Vbe2+2F+R+p+J74dzGkdqsF2xq7UQoqf5wi4DO8Si4Ko+fkn+vkINdS1TCYMV4QJ73iFtJChIwtWXoJhvEKRWLIRJxUdEuOE1UmK8dZrxgliunKIZdDIwXuitqLuKMPkEmrtvzCRL0mUlnErlyG4MV2euNajpl8b3r+6oFfYlsyYG182W2TJN2Hnka91QkjcXXN7paBIVSQolhfCsNzNHXSRJtB5Z0EXAthASHUQtO1kuotQpr+I0DgEM4OfsSbblsiqyDnkNoQq88uKN4NL839cGqpY8SYYLuemUOolkXDLCopo1Vyb29YGGbmxBPaTmJJHQPpcEZ22lllTdSOtcdSFtWHMJVJnW4OqMSf1RSpTyo.zH4kUVljkXzlJPyThnDrPxozsTzjnmih9qw3wUbhieRb8FvpG5ZXTcfuzS8T7zhia59uO9V+1eWbn2xC.W20.iGyVEQFLbUhYlTRCQ6rKL3xgo5xaSV37GW0PvBlSzcDEbdBwwD1R3fqtDe3uq2Guh67N3+q+U+q3O3O8SwewexeLG5Fd4ruCecIhQQECgspGy3XjxU2G99kT1uO8b8v45QTUD0mZ73ph26wBJ38IhS9XtpCSotTcVJUl4YQoRSZ5TDuGbo7f2P9BRWXXRZ1flNYfitWPqcFtRDAKx1Ih0r8HStSShLrYYiNl5tDmz9XTq8wMKaVucZ8mdosGu9zKQ679sTGU2R4aLorBSvrTeuJ8A0ik05ll6.7KvBr.uHfTPVRQkPsDwpZKWTOIGCW0XhnkzQFAMq91zr4zW6llf3db8bNhPSaILMQV2vK9Na1lnk0txX0cs2xNaVlcRj4nrmSw47drPlfoosKasHgLZ9z1LVkvjp.zYSuOMyPsLArbUp2zXlS1egRHDvjTE+YjawaRLoOKfxrqsG0HAKEEqj9pTvb3KFvYVeCTofhCd.VWUN5oONaEMh6aUd3O32M2xa6s.u56A54wpFQUYI9ACoxL7NWpcl0bVx.+N4LpuDDujmf0N4CSMOWzpY4g8Qww5i1hdCFx8cWuRtkW1Gk2wa8Q4e5u5uJe9G6w4q+4+Ox9O70vQtwaAeALtZDADFu0YPz9Di0ngd3yN8tykhXEwdXNGEEZ6yiKQpBmhqvAdCmpsQXJ0+kbXNAMFRDe5DkJERZNxontLArlAn5NiIl4BxYBWSyrLEQR1IQdo1lO+zLOmRBpy7XuHnwIKm88M66+bcY202xmaZ1uBfFpRo.UjTC.0HGG5bt6K5kJ+3.nw5j2kYSFzZgHrVfE37C6UjfaI.DUjXJUeJQvDblg3zIQnZpMbhzv11eyY77cbemEL91SmWdBo6wmscpWGJcHtkNnboIriOsL+XKuTxOuXoINCoLP3ZrDgF4Tzn4ntZhpIxULST7wgRpiX3rjYXWnfnZxJCLfnQzkZxLpXLVLBXTKFNSoGQbjpnPQsTJHUvoEI8VUVheXAaDUN1Vav2LVi+Z2G2ya5aiWyC9fL7gdXnnLkIESQWcMDWIFdBn3v2V6QoyaVhXY6I9WZGCqWxSvB1Nop1mSR8Xo.03ckr7fADzH00QNzxC4cc+udt264Uy+1e2+87a7I92vm8K7n7U9K9rLX0kYsCdHV4.GhyX0nAgnFXywagy4onnG86MDW+ATWsANeIVQ.qnDmDAuC0UfHF9xBvCAWINmCMqiJ06.uiPLlEAeGMBjITIdGTTlhnCdbdfl11yLTULYxiSB9LWkMVxM70rq3q4ptoY80TXiRWFlWOyhsutIdDwHz79Y96m3bWNo8GrSKapDFM+c1zKmL.bLq11j9EDj3DxnAsJcLFBTD0N+NXA4pEXAdgBSlTOecQN6O4apJ35PvRCoJ+SPwj.hjICHSR+W2sjF2NAmsQVZWPWhPyKBVsobrCwkoxDvNzLlaeb6XIMuuoeb2mOsNyLBWVCZM5nsIf.VVj6ciXuNSJBMQIfiHFRTwmqdde15FDijDILHHQpQo1kHWEjrisWEnn8TrCrhj2SINpwyyelsXyhRdtXMmpeIG4M954M8c9d4Zev2Hrx9gwVhbUYAi8NFaFgXMdOzmxTgIzMBgll9NOWKotIBsct3xkp77EJtpffUClmuhT3SmBTTpFuNh3Y09CaBnKGZXO9HuuGgG7M7sxu0u8+N9M9W+ulu3S7U3DasIUiVGV4.X8JwUzCwkpZFqHfFhDqqoVsj8NzqOQe.wWfj0VkHB9d9jWd4qmp58LWh.UpRVbHhGyMIsdNWh.F004TK5yWDmV1kPUCAqlWu44EwZIPsSk2bZ7ksWtysutEl6qa4sqsCa+y1Yt3btsOtaWnF3LhRmYjFEzXVCVgZBVEXBwpwHZc52BmU68EXAVf8ByijUKLRWil+ykMuxVgX6TPhSzDU2UMGA84gy1qe6t1ypAqz1wuMMml3ElIY0DAqYjVQmM5be94R7aFQzO6wU20qgfkGoUZDcIJZVZL9nDHliDXJXABhDSeeHohPRMHPRxGgflzXUtxH0pT2PAwS.gnJLxDFEU1RDd9pHq6JX+288vC7deXt424aEtwqmJIxYFGXPQe52eIvWfgPAdZ59rA0nuS57kkj9qgb6UA3pJBVyC0ZMpEnz0ik6O.AgXnlXvvUTxxENFGga65O.ezu+uGdn25Cx+2+a9+geme2eWdhm5on9zaP4xqhek0XvfAHNOZcfPUE0xlHkkPYObZjPYOvWjLkzrf16oE3JRlLpqCQJISfx6yCc0JBIGlnXT.DQq.ykHNkzCehnij0djYRKwJ0YSEoqTCoNUAIlJIxRyDR6Tw9jdrjWew0HthzLlZbd2oBMtJSsrMD5cW1hclrk1TFyyN6sbU33EAUzVxmoCMGwPSz97DswDjz2qEwbnv2085Br.KvKTLqMy4HecYljUZnrzXHVU.0EmR346VJ.m8w6TJ7lKZ1tc1FNmmFdbSEAKxQJqUeqaaUyGqyrsaNFmCwKalz90sJq6lhvI1kfh48ayA1a1ehEwqwD4Jrrf1MTjVe3LZyjpwb0IJVpffHHDbdpQXLBafxoLkSaBmgHG9tuKdj2yivM7vOLbjqCJfZWIinDcPMdFxnXxRhJJRzIhjHVLnQatsmZzVxVMeqsWoW9JcbUGAKY5qPnTbXTjmAV5JMuSv2KcQhVUSuhRpBFHvq5FuAdk+3+H7ddmODe5O2Wfek+k+l7jO6yxwdtuIqs+CvgN70hqWepppoVDjXevLBZj33sHHIOvpnW+jn4qghhlJLDJJ5QYudXQnZTfx98.WQJcfNALIEQUml7CJSxUoSLOnkC7BhSw6DpqS9tUxL2rzXbXI+jQx9JStDnSN2apTcI2dMsPdYqUIL8xlsSR3p.H40OsTUap2uLkHPAmL4mfyKbv5LO2rumHoPkGECUR9xUg4PhoyWADhTgS7DFMF+nJHjKM71wAmeiU8J8vSu.KvEaL0ULYgTKMdKPmn9nphVWmtguyiRjfFnfb+uau1Oyb84tYELy68sSHxtSPauV+tZveOM044sMkIw9aVxhBIe.LIl8oIlAoBcxUUQ+hRhEN1JFXrEotvSsXTGCTTzCMpHUozH1WEJTAIjpTPsbENiZbJKvwsZd5XEg8uJ2x89F30ceuAty266EFNDFrLzqOzn4VmikkjQaW5SlFZymp9j8vpzbfa9T2x7VyQSiVg++R2wYupif01wD2gUZScUNZQhlJbOA56gMFWgZNJKJ4Ntkalib3qkG9M+P7I++8+.ehOwmfO2i9X7Lm7DTt7xLb00nXokvYQV+LmjJUn2Jqvx6aMJJKITuIatQEEdeaTpLRDr5MbPx+UH0BfRtCeQaTZPlzVcHGgHm5PcJdyCEYhKNK854HbYh193j3CyDxliloZRsmyUvNmhujVpDwl6qaVbORwHLZ7FsCLMqez.jp.y4bAX6rMIMa3XVSClAp4QyQHybIBXlORrtBKVOIb6m8YpbAVfE3EBxo5R0Tk7Fkj0zDIhooHTK6BImFBT6D4kYGa3rkjyEJDic0c0NebrSigMQJWaehjFPntdaISSaz0jACbdPiTu0HpBAFKZJiIkk3kB13Lahy4om3wKEDrTquo.nxUxSd5yvyEBrUeOqbq2D2089p4ltu6ka7dec3t0akvViQ5ODe4.PJPUvmjQbpSi3XhuJlOPc4+8TmAjzqjjSe6S7RdbUMAKwZb9.2DgYBS8cuFi3JRQoYodkLNF.UYYmigqtDgH7C7AdX9K8HOL+Ie5OM+V+69s4O7O4Okm84dFhdGqbvChav.12RKiyEYzoNJacz.850igKuLnQ7TRYYI3RswfvnMoRMBwH8WZ4jlsJJaqjvjGYIIGKuIUb4T9o3gZCjhTpCUIUXKcHXkRwnOO4ooIVsMMTsqsZhcViVypAqchfkEivj3GN0.SMFpZWrsApLKK1SKm5wrXYMOfinWHJQDbDGmhdUqqHGTbktqnLttEXAthB4HtDiQBwHURjZyPkHwXxcw2MBV4Mx4PDrlN0b6YTn2iWdWIrIy+3tU.8ytqlaD5i636QExjPm90ZD8dzT1bzVT3EbtBJJJXkrE0LdTMwpZN3Jqv3p.iiFivv7dhBrYcEmHrEmZs8ypuhai649tWt823qmktq6.t1qAJ7fqGiwkMKzBbJ3BfKqHjFg2otYHT0MoEc+rIMjwxFnMS92uTEWUSvBHWZvoeMXhsMiiiROwX.QDbdOCc8nJTSccElYoe7oNVtDd6uo6k238cu7k+peM9c98983O5S8o3y9EdTV+X0HEEr1AODKuu8S+RGF0vVqigmwifsxyvSwQQuR5MXIJ5UR8oOMT5wmEGeZpCdbNGQQZStYqENHMdoUUxs26NCP2D8Mz7bMolbmFHZuz3ftCUYytUkOcetTd6mTpx5rgAOGC94MCOfjFxDkn.o9xkPzfdpGQ7TKBAmRLJTu0lDppwATLIh0KvBr.W.Qqe4AoTJADIIShfFIUlIQhpK6KcwcP.BSV5x5Spk5RNBOMKcMZ3py6u6quiaea28gOWVj46tu7o639e1i2YerYRaDoRyU1lPnRRhyfrFrlcLR.z9Eo6aLdKH.kTPoqGNojdkC3TmZDtkVh3R83jiGwyu0VTUJr1QtVV5FNB22i7t3v20cxv63UBqtLT5InQFUqToaQ+9KmziaS59lC4oF8psajUmlbkh7RbhUM3pbBVt11myDwiOI0RPh.VLFHDpoPSs1fBAJKSDdpFUSQuR.GipizWDtm63l41t0ajOz2yeY9S+zeJ9re9u.+A+w+w7XO9Svy7UebJ5UxAO3gYsCdPpoDeQOJ6URY+gXHTqQhatIi1vnnnGl2A9Rv2zmulLaNuqLK2gIoWqajfLy.uaayny4bS7OqcA60quSDol2yOusklaUDMqyr+ssVUQmsc5yQQxD8D.R9oSgIDMOp.U.0RDWPHrwVoVSjkR86K1oSXAVfWJhc8VkMS7CifoDhQRJtJRPmHRmNpYXaDNLmjp9P2zudDaxiyDS7HS83FBK6z12YmOut19XOS+5y93YONZdrYLWhWPhPRHjNaYVy6eB4pfCBEEDJcT1qOklmXvSccj5JiZS4Tpi0WeSNoaSz0VkUuyamW889svc8FuOFbm2Nb8WaxGqDgsFMhQaMlACWgAE8YHtTidNmbf1CzBxhrhrtea9tlsS.S5Rtp42KtVVpuTeRtWUSvJ8kayvCYhUxDh5PNDtNgh98RNoa1GkJ7EfA8FTlVOKhWpAWAdb3Kc3kd7teauYdKOvahumOvGfu3W9Kwe1e9mg+7OymgG+q9U4a7EdFJV9.PudrzRKwRMUhXp9ZSB7tuh3KfhHD7IAB14WqpqBL2TDr516Aahb0rsTctDB..nPPIQTPTQBRjTkJ1MBUmUkZ7Nct7EBAqrCq2HNdlQD7h.iGS6iaDO+jGa3ckDIk1SH4oNQSHhGLGiEnl.RMTswFrrjbHYm0JwxEXAVfWfXJeVOeo4TA3vkh.jpJAEpr.AyvjHQ0PHRikMrSQHBxUiFxTu975TD5LQbZpN+vbWt6Q3Zuh.VpYymzUV2HdE2kOOcerMaihEnoCbnRJMfsmeyCVotj2hoNgMFmJlJSUPCDBvnwAFaNB9R7Wy9Y4a3H7ZeM2Muh66Mvpu56FNx0mHHECToQT7HRA5fgzWJPH4EVwXjdRtcSKP1PyPcS5hHI5R4HR0Hp0lPZIfgq8dp.3rbEZlhTvzDzdIHtplf0D3H+KnVxUMKGGqoeQu1ejkLPcCB41J.wVi+z6coz3QpYB2Kc4NqU5Ysa3Z41tgqk29C7.77O+yym6y843u3K937G7Y9K3YO9I4nG8nbxu4QwU3YvRqvJqsJCVZE17TmBmu.euRb9xbuIzkiTkqc+2J58LrlPe2R5ZZkNz97pR2VvyrDhZZ7y6D5pAhYiR1NIP0tOV07.rM1+P1nTarIhXHO8orcRLqMOHRMFtjUMPjBSxURXxJLFCTQ.pgp0OC0kEXQchK12Lv2Br.KvEdXVJ8flRLZDHPsyvbAhlmjOurysTqlVwkCASsVBLMDU5RXI0ptl9w6MAK8bpUcM8RKoircY+OOhfce7T4ZKiXWsVIt1nVE6XECXFwZAuujMpGy500ro2iszRL3Fe4bva6VXkW1Mxc+lteV6ltE3k+xghRBaMh02bc5szxrzvUwOZL8rTHoDwnxhL1h3bEHdgl6k4rXxteRGYDyj.yiJmFCWrbkn2jdy13S1VyfPC4pzmUgWZSxZAAq1RfH8+Z+ASFCKF.jbCbMFYfuLI.PCbg.9xT09E0zZowJTwQoyiunGiqFS+d8AfwUQFHB29QtVt8i7N3c9Puc9PmYcdhm7o4wdruDO9W4I3q80eR9FO8Sw27YeJdlQawvkVghd8S85vACSst.eY1vREhlf3DP7IMJHRtYclHqnl0FAqtZsJo+pFM.n3wO0xFhNs9b077wprOYINCm4HJJNykCctCkHd7S87y9XmooPlajLKzYBkdaH+aFfZpW2kNdyC1IF4B.VR56Pb4hBM.0JxViATjnRL+RWkTLKKvBbQAMSZyiiF2Iuk2PNuRUliy3J4jhiXJlUoIEYIaiIc62NQ.oyRCAzTQwzppIK0AJLMN86ONw.kad7dazwYhB6PJB6N9y1WlBUkkG+ib+6qUOrs7FxzRZHElebTmTEg.YygNaiAMtatHDDHDiLVCTEfJMRLFYDNB8Ko7vGgCeSubtqWwswAt4al8eyuLFda2JbC2PRVIVxv0w6ws59YX1E.2JLhg8G.pR03JLmGeYAdYhKApYBVl.EsCVZ4n+muWRm7AN8jUmDeyqVGl8pZBVoVLyjYK.oaz6kbNzo4he.WI3JaCUM.RQubE3A9F+bRRWnHVZi0urLMPgYzy259aHhvxEB27AWka7f2Iu4W6cxFi1hSdxSwS9zOKewG+I3q+TOIO9S7U4Dm9z7bG8Xbxu4wnNnTTTfuWObdO9xdonD4Korre9Bjh7LdR9Uh07YURtCeiY9E0Hh3H4CV4AlZxMdNUctbvvMl+z3bQK+9bHtbHfEKM1pXTHdz7iEwv0r8yOVLR9wEIhhc0nfjGHiYVJcVJZ5GwhjlkaSjra0rPLRoCVckA70etmk58sOzPRnsUVjUb91AHV36UKvBbtgPajLb4bXofYnVDmFfs1hQ997zkKAHb8qcMrwoNYhvjDYfqDuMsD.LSyEnmzNNjzZmNBStAuCwD7xjmGRQkO4N4RJ0UIKieJoH.ona0PAX1LWzJSjjqLmDVNMMM4z1Icqfj2eoH4O+RazYLH2rkSU4WJUoon+XBXNgpv31VgFlfpPkl1Op33DatIQumf2i1e.EGXEV8PGh8e3qA+gN.27a9MS40bXN70e8Tb3CACFl4Plh7ugmnSnPDz74vjwkFozkk2h3vWzKWY2o6FXzXDptIe+NIQeaS2cyKHTBttIGI+8FIMD27LuDN5UvU4Drf4mdnluy2w1+.LgRtM6O0lr9S4hsFPmjzktzWIVUiuPnvUvvA84fW+0ysd8WOuwW2qlyrwVn.G6Dmhm7YdV9ZO4SwS9TOMO0y9Lbzm+3blMVm02bCFWWQ0nM3L0wjnIkTEzkLlTWdFQf3c4VrCs8GpIUiGSEZaX5Yr07QHEU9I80KQaFfUlRCBMgledUgy7zNwN891qp3wrz1xiepRENcpOR+g8HFq4TO0VL5zmha8UcGbv0VkHPuA652vKvBr.6Bls9hklt3.Z5FmhBG7PrzgOLGunOO+S9Lb7MGwZ8JneOgyrwlzyWfXQjbDua5MpsshqZsMhTc8aOYGhndiTBrr8yzrcLmep0O0fj28sSxGDKnoiTLqDEZjzP6iY5NTQazpbBpFQwn1H84T7ndiMqFkGGKOgdWA99KQugKA86w0bCuZJWaUV85tNNvK6F3vurajUtgi.W+0.6+f.BwgCwI4IRSpmqZ4A3cRikHH3mg7WZoKGIpI2GvkOdrNimlv4dk+Me9S1buu6KEgnpN2SAKvNiyEmBdas3fo1FZV74JAM4UL3Jv6KnwpQHur4u.PUDFsUMUUU7XOwWhSuw5b7ieBd9idbNwINEmY8MYqsFw35Z1Zqwoxtsph5XfXrFEKIV+18+1I5.SSzo43vQZ1UsWpoYcV0QyAcWOu31UMHzc8lUyBmMDrRoXHMiHeiHXy5fvLispFwfA8PqFyQtlCyOz26GhG4gdaLzC8yamERceAVfycXxDmPuDeaTSThnn3iwTjndhuJe1eseS9R+9+GX8m9onj.E8cbx0OMCVYYBRZVbln3viO6qStbVG2IBVI+uqI71Ye4SRDql3iec8cuIKiI8Fj6bd6rO+IhmtM49485MOVlw4hSh6NWs1dOtBONeI99dJ6M.oPXeWygvW5n+RKypqrO12AOHG7vWOqb3CCqsJruCBC5mhLUuT09gYDUkZygyM.bS5mhlY3bN5UlbW8P1k72oNUguiFulpRz67bKvKbrff0K.bgjfElQSSWNZIi6zDASbIwyiOoYAijgtIYxVJDTXPwDhWVdYVC9nJblMpXTUEat4lrwVaxVUinttNQzR0TJLmyms1dEUGAWNuOKN0l5h2YE59NY3ey95aSf96hOZ0fFATBYxU.dMQLqIEghGJbBVrla5HWG20M9xo.XqSuIKuTe568vBBVKvBbNiDAhzUgElOGxiTg+DwvqAhG8Xzak0fm8Dvm9OiS+keB5WXzeeKARjMQRs4pb0L6bNJKKozWj5vEY+uhbJ+lBR5Z7nqy3VyLFUiXrmcbllIM5b6dTYleOPc6ausGolTDkTUgROkk8v2uG9ACfkWBVZIXPOnzkM0SO3KRCX07u8EPUUxFEJKH5KPSMWHhTffiTM+4aO15ZCOBBgrGN18Xc5iRYAAqKh3+eYUhV4p0EgXH.....IUjSD4pPfIH" ],
					"embed" : 1,
					"id" : "obj-4",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 179.0, 4.0, 599.0, 534.0 ],
					"pic" : "HD2TB:/Gian/Macro/Projectes/SwitchOSC/JoyOSC/pics/joycons.jpg",
					"presentation" : 1,
					"presentation_rect" : [ 1.0, 3.0, 599.0, 534.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1428.5, 194.0, 46.0, 22.0 ],
					"style" : "",
					"text" : "poll $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "list", "" ],
					"patching_rect" : [ 1428.5, 259.0, 96.0, 22.0 ],
					"style" : "",
					"text" : "hi \"Joy-Con (R)\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "list", "" ],
					"patching_rect" : [ 1180.0, 249.0, 94.0, 22.0 ],
					"style" : "",
					"text" : "hi \"Joy-Con (L)\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1180.0, 194.0, 46.0, 22.0 ],
					"style" : "",
					"text" : "poll $1"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-116", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 373.5, 1023.0, 89.0, 1023.0 ],
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-128", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-15", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-187", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-196", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-198", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-188", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-201", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-223", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 92.5, 1032.0, 89.0, 1032.0 ],
					"source" : [ "obj-226", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 230.0, 1023.0, 89.0, 1023.0 ],
					"source" : [ "obj-227", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-210", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-275", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-267", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-267", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-274", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-268", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-284", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-268", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-267", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-283", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-275", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-283", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-283", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-285", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-274", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-285", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-284", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-285", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-283", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-287", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-285", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-288", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-278", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-293", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-287", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-294", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-295", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-298", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-299", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-305", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-306", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-305", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-307", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-305", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-308", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-320", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-308", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-317", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-310", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-318", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-311", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-307", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-317", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-306", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-318", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-316", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-320", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-321", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-322", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-207", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-323", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-326", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-35", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-35", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-364", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-365", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-365", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-63", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-283", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 92.5, 888.0, 69.0, 888.0, 69.0, 1023.0, 89.0, 1023.0 ],
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-285", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 230.0, 888.0, 180.0, 888.0, 180.0, 1023.0, 89.0, 1023.0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-227", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-293", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-97", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "joycons.jpg",
				"bootpath" : "/Volumes/HD2TB/Gian/Macro/Projectes/SwitchOSC/JoyOSC/pics",
				"patcherrelativepath" : "./pics/./pics/./pics",
				"type" : "JPEG",
				"implicit" : 1
			}
, 			{
				"name" : "1440px-GPLv3_Logo.png",
				"bootpath" : "~/Desktop",
				"patcherrelativepath" : "../../../../../../../Users/gian/Desktop",
				"type" : "PNG ",
				"implicit" : 1
			}
 ],
		"embedsnapshot" : 0
	}

}
